-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2016 at 08:11 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swatch_28_06_2016`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_adjustments`
--

INSERT INTO `sma_adjustments` (`id`, `date`, `product_id`, `option_id`, `quantity`, `warehouse_id`, `note`, `created_by`, `updated_by`, `type`) VALUES
(22, '2016-05-21 14:20:00', 19, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(23, '2016-05-24 04:55:00', 19, NULL, '20.0000', 1, '&lt;p&gt;ol&lt;&sol;p&gt;', 1, NULL, 'addition'),
(24, '2016-05-23 05:00:00', 19, NULL, '5.0000', 1, '&lt;p&gt;wrew&lt;&sol;p&gt;', 1, NULL, 'addition'),
(25, '2016-05-23 05:25:00', 20, NULL, '20.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(26, '2016-05-23 05:30:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(27, '0000-00-00 00:00:00', 20, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(28, '2016-05-23 06:05:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(29, '2016-05-23 06:25:00', 20, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(30, '2016-05-23 06:40:00', 20, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(31, '2016-05-23 06:45:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(32, '2016-05-23 06:55:00', 20, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(33, '2016-05-23 07:00:00', 20, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(34, '2016-05-23 07:25:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(35, '2016-05-23 07:30:00', 19, NULL, '5.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(36, '2016-05-23 09:15:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(37, '2016-05-23 09:15:00', 19, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(38, '2016-05-23 09:55:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(39, '2016-05-23 10:00:00', 19, NULL, '10.0000', 1, '&lt;p&gt;ojk&lt;&sol;p&gt;', 1, NULL, 'addition'),
(40, '2016-05-24 09:55:00', 20, NULL, '10.0000', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', 1, NULL, 'addition'),
(41, '2016-06-01 06:55:00', 20, NULL, '50.0000', 1, '&lt;p&gt;ol&lt;&sol;p&gt;', 1, NULL, 'addition'),
(42, '2016-06-01 10:20:00', 19, NULL, '50.0000', 1, '&lt;p&gt;8&lt;&sol;p&gt;', 1, NULL, 'addition');

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

CREATE TABLE `sma_calendar` (
  `date` date NOT NULL,
  `data` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `tax_rate_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_categories`
--

INSERT INTO `sma_categories` (`id`, `code`, `name`, `image`, `tax_rate_id`) VALUES
(1, 'C1', 'Category 1', NULL, 0),
(4, 'C2', 'Category 2', 'd9031f0fb319a0804479bc1831cc1e0a.jpg', 4),
(5, 'ACCESS', 'Accessories', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_combo_items`
--

INSERT INTO `sma_combo_items` (`id`, `product_id`, `item_code`, `quantity`, `unit_price`) VALUES
(3, 23, '1231', '1.0000', '3000.0000'),
(4, 23, '231321', '1.0000', '3000.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `lname` varchar(50) NOT NULL,
  `type` int(5) NOT NULL DEFAULT '0',
  `upd_flg` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `gender`, `salutation`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `lname`, `type`, `upd_flg`) VALUES
(1, 3, 'customer', 1, 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'Petaling Jaya', 'Selangor', '46000', 'Malaysia', '0123456789', 'customer@tecdiary.com', '', '', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, '', 0, 0),
(2, 4, 'supplier', NULL, NULL, 'Test Supplier', 'Supplier Company Name', NULL, 'Supplier Address', 'Petaling Jaya', 'Selangor', '46050', 'Malaysia', '0123456789', 'supplier@tecdiary.com', '-', '-', '', '', '-', '-', '-', '-', NULL, 0, 'logo.png', 0, '', 0, 0),
(3, NULL, 'biller', NULL, NULL, 'Mian Saleem', 'Test Biller', '5555', 'Biller adddress', 'City', '', '', 'Country', '012345678', 'saleem@tecdiary.com', '', '', '', '', '', '', '', '', ' Thank you for shopping with us. Please come again', 0, 'logo1.png', 0, '', 0, 0),
(4, NULL, 'biller', NULL, NULL, 'swatch', 'Swatch delhi', '', 'noida', 'noida', '', '', '', '8989898989', 'vivek@essindia.com', '', '', '', '', '', '', '', '', '', 0, 'logo.png', 0, '', 0, 0),
(5, NULL, 'biller', NULL, NULL, 'swatch', 'Swatch Mumbai', '', 'Mumbai', 'Mumbai', '', '', '', '76564765456', 'ayush.pant@essindia.co.in', '', '', '', '', '', '', '', '', '', 0, 'logo2.png', 0, '', 0, 0),
(9, 3, 'customer', 1, 'General', 'Akhilesh', 'Swatch Delhi', 'DDWPK3314K', 'B-65 Sector-63 Noida', 'Noida', 'Uttar Pradesh', '211010', 'INDIA', '8376951392', 'akilesh.kumar@essindia.com', 'M', '1', 'e3', 'e3', 'e3', 'e4', 'e5', 'e6', NULL, 0, 'logo.png', 0, '', 0, 0),
(10, 3, 'customer', 1, 'General', 'Adesh', 'Shotformats Digital Production Pvt Ltd', 'DDWPK3314K', 'B-65 Sector-63 Noida', 'Noida', 'Uttar Pradesh', '211010', 'India', '8376951392', 'ajay.kesharwanisf@gmail.com', 'M', '1', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, '', 0, 0),
(11, 3, 'customer', 1, 'General', 'Adesh', NULL, 'DDWPK3314K', 'B-65 Sector-63 Noida', 'Noida', 'Uttar Pradesh', '211010', 'India', '8527536265', 'ajayk.au@rediffmail.com', 'M', '1', '', '', NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0),
(31, 3, 'customer', 1, 'General', 'Wwww', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9845356267', 'qwe@gmail.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 0, 0),
(32, 3, 'customer', 1, 'General', 'Dsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'dfs@essindia.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Balwever', 0, 1),
(34, 3, 'customer', 1, 'General', 'Varsha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9804567835', 'varsha@essindia.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(35, 3, 'customer', 1, 'General', 'menaka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9480807890', 'menaka@essindia.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(36, 3, 'customer', 1, 'General', 'Dsfewdew', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8838739989', 'dsw@essindia.com', 'F', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(37, 3, 'customer', 1, 'General', 'Dfvdgds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8838739984', 'edfrer@essindia.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(38, 3, 'customer', 1, 'General', 'Varsha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(39, 3, 'customer', 1, 'General', 'neha', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(40, 3, 'customer', 1, 'General', 'bravo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(41, 3, 'customer', 1, 'General', 'Sadsad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'Sadsad@essindia.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Sadsad', 0, 0),
(42, 3, 'customer', 1, 'General', 'wqewq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(43, 3, 'customer', 1, 'General', 'aaa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(44, 3, 'customer', 1, 'General', 'ewrw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8376951392', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(45, 3, 'customer', 1, 'General', 'David', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527536265', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(46, 3, 'customer', 1, 'General', 'Flinch', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8528536265', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(47, 3, 'customer', 1, 'General', 'Von Nuemann', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8878906780', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(48, 3, 'customer', 1, 'General', 'ravindra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8870653565', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(49, 3, 'customer', 1, 'General', 'andrew', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6789056743', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(50, 3, 'customer', 1, 'General', 'raul', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3243243243', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(51, 3, 'customer', 1, 'General', 'wright', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3242221212', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(52, 3, 'customer', 1, 'General', 'ewrw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4242423424', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(53, 3, 'customer', 1, 'General', 'henriques', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9005665544', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(54, 3, 'customer', 1, 'General', 'ayush', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9599933745', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(55, 3, 'customer', 1, 'General', 'debopriyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8823456780', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(56, 3, 'customer', 1, 'General', 'priyank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9452686877', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(57, 3, 'customer', 1, 'General', 'priyank', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(58, 3, 'customer', 1, 'General', 'w3schools', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9008654564', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(59, 3, 'customer', 1, 'General', 'rahul tripathi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9999999999', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(60, 3, 'customer', 1, 'General', 'deepak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9008765434', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(61, 3, 'customer', 1, 'General', 'Deepika ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8888888888', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(62, 3, 'customer', 1, 'General', 'aaaaa1111', '$w', NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(63, 3, 'customer', 1, 'General', 'ankit', '', NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', 'ankit.kumar@essindia.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Khosa', 0, 1),
(64, 3, 'customer', 1, 'General', 'Naman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8877665544', 'naman@essindia.co.in', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Ojha', 0, 0),
(65, 3, 'customer', 1, 'General', 'Prince', 'Swatch delhi', NULL, NULL, NULL, NULL, NULL, NULL, '7788991122', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(66, 3, 'customer', 1, 'General', '213sdfdsfsfsdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1234567890', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(67, 3, 'customer', 1, 'General', 'qwwr', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8989898989', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(68, 3, 'customer', 1, 'General', 'werwer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7235345345', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(69, 3, 'customer', 1, 'General', 'a342', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9234652364', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(70, 3, 'customer', 1, 'General', 'ttt111', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7324234234', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(71, 3, 'customer', 1, 'General', 'qweqwe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7125313241', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(72, 3, 'customer', 1, 'General', 'Amp', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9999000000', 'amp@gmail.com', 'M', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Ddfr12', 0, 1),
(73, 3, 'customer', 1, 'General', 'ppp', 'Swatch delhi', NULL, NULL, NULL, NULL, NULL, NULL, '9999000000', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0),
(74, 3, 'customer', 1, 'General', 'Ccc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7125313241', 'ccc@gmail.com', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, 'Ccc1', 0, 1),
(75, 3, 'customer', 1, 'General', 'ajay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8527654380', 'N/A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'logo.png', 0, '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_costing`
--

INSERT INTO `sma_costing` (`id`, `date`, `product_id`, `sale_item_id`, `sale_id`, `purchase_item_id`, `quantity`, `purchase_net_unit_cost`, `purchase_unit_cost`, `sale_net_unit_price`, `sale_unit_price`, `quantity_balance`, `inventory`, `overselling`, `option_id`) VALUES
(1, '2016-01-06', 1, 1, 1, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(2, '2016-01-28', 1, 2, 2, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(3, '2016-03-14', 1, 3, 3, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(4, '2016-03-14', 1, 4, 4, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(5, '2016-03-14', 1, 5, 4, NULL, '0.0000', '0.0000', '0.0000', '909.0900', '1000.0000', '0.0000', NULL, 0, NULL),
(6, '2016-03-14', 1, 6, 5, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(7, '2016-03-14', 1, 7, 6, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(8, '2016-03-14', 1, 8, 6, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(9, '2016-03-14', 1, 9, 7, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(10, '2016-03-14', 1, 10, 7, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(11, '2016-03-14', 1, 11, 7, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(12, '2016-03-14', 1, 12, 7, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(13, '2016-03-15', 1, 13, 8, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(14, '2016-03-30', 1, 14, 9, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(15, '2016-03-30', 1, 15, 9, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(16, '2016-03-30', 1, 16, 10, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(17, '2016-03-30', 1, 17, 10, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(18, '2016-03-30', 1, 18, 10, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(19, '2016-03-30', 1, 19, 10, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(20, '2016-04-02', 1, 20, 11, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(21, '2016-04-02', 1, 21, 11, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(22, '2016-04-02', 1, 22, 11, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(23, '2016-04-02', 1, 23, 12, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(24, '2016-04-02', 1, 24, 13, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(25, '2016-04-02', 1, 25, 13, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(26, '2016-04-04', 1, 26, 14, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(27, '2016-04-04', 1, 27, 14, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(28, '2016-04-04', 1, 28, 14, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(29, '2016-04-04', 1, 29, 15, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(30, '2016-04-04', 1, 30, 15, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(31, '2016-04-04', 1, 31, 15, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(32, '2016-04-04', 1, 32, 15, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(33, '2016-04-04', 1, 33, 15, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(34, '2016-04-04', 1, 34, 16, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(35, '2016-04-04', 1, 35, 16, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(36, '2016-04-04', 1, 36, 16, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(37, '2016-04-04', 1, 37, 17, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(38, '2016-04-04', 1, 38, 17, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(39, '2016-04-04', 1, 39, 17, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(40, '2016-04-04', 1, 40, 17, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(41, '2016-04-04', 1, 41, 17, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(42, '2016-04-05', 1, 42, 18, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(43, '2016-04-05', 1, 43, 18, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(44, '2016-04-05', 1, 44, 18, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(45, '2016-04-05', 1, 45, 19, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(46, '2016-04-05', 1, 46, 19, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(47, '2016-04-05', 1, 47, 19, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(48, '2016-04-05', 1, 48, 20, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(49, '2016-04-05', 1, 49, 20, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(50, '2016-04-05', 1, 50, 20, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(51, '2016-04-05', 1, 51, 20, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(52, '2016-04-05', 1, 52, 21, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(53, '2016-04-05', 1, 53, 21, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(54, '2016-04-05', 1, 54, 21, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(55, '2016-04-05', 1, 55, 21, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(56, '2016-04-05', 1, 56, 21, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(57, '2016-04-05', 1, 57, 22, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(58, '2016-04-05', 1, 58, 22, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(59, '2016-04-05', 1, 59, 22, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(60, '2016-04-05', 1, 60, 22, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(61, '2016-04-05', 1, 61, 22, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(62, '2016-04-05', 1, 62, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(63, '2016-04-05', 1, 63, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(64, '2016-04-05', 1, 64, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(65, '2016-04-05', 1, 65, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(66, '2016-04-05', 1, 66, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(67, '2016-04-05', 1, 67, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(68, '2016-04-05', 1, 68, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(69, '2016-04-05', 1, 69, 23, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(70, '2016-04-05', 1, 70, 24, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(71, '2016-04-05', 1, 71, 24, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(72, '2016-04-05', 1, 72, 24, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(73, '2016-04-05', 1, 73, 24, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(74, '2016-04-05', 1, 74, 25, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(75, '2016-04-05', 1, 75, 25, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(76, '2016-04-05', 1, 76, 25, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(77, '2016-04-05', 1, 77, 25, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(78, '2016-04-05', 1, 78, 25, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(79, '2016-04-05', 1, 79, 26, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(80, '2016-04-05', 1, 80, 26, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(81, '2016-04-05', 1, 81, 26, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(82, '2016-04-05', 1, 82, 26, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(83, '2016-04-05', 1, 83, 27, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(84, '2016-04-05', 1, 84, 27, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(85, '2016-04-05', 1, 85, 27, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(86, '2016-04-05', 1, 86, 27, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(87, '2016-04-05', 1, 87, 27, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(88, '2016-04-05', 1, 88, 28, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(89, '2016-04-05', 1, 89, 28, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(90, '2016-04-05', 1, 90, 28, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(91, '2016-04-05', 1, 91, 28, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(92, '2016-04-05', 1, 92, 29, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(93, '2016-04-05', 1, 93, 29, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(94, '2016-04-05', 1, 94, 30, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(95, '2016-04-05', 1, 95, 30, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(96, '2016-04-06', 2, 96, 31, 2, '1.0000', '45454.5455', '50000.0000', '500000.0000', '550000.0000', '4.0000', 1, 0, 0),
(97, '2016-04-08', 2, 97, 32, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, 0),
(98, '2016-04-08', 2, 98, 32, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, 0),
(99, '2016-04-08', 2, 99, 32, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, 0),
(100, '2016-04-08', 2, 100, 33, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(101, '2016-04-08', 1, 101, 33, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(102, '2016-04-08', 1, 102, 33, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(103, '2016-04-08', 1, 103, 33, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(104, '2016-04-08', 1, 104, 33, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(105, '2016-04-08', 1, 105, 33, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(106, '2016-04-08', 1, 106, 34, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(107, '2016-04-08', 1, 107, 34, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(108, '2016-04-08', 1, 108, 34, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(109, '2016-04-08', 1, 109, 34, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(110, '2016-04-08', 1, 110, 35, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(111, '2016-04-08', 1, 111, 35, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(112, '2016-04-08', 1, 112, 35, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(113, '2016-04-08', 1, 113, 35, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(114, '2016-04-08', 2, 114, 36, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '1.0000', 1, 0, 0),
(115, '2016-04-08', 2, 115, 37, 3, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(116, '2016-04-08', 2, 116, 37, 3, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(117, '2016-04-08', 2, 117, 37, 3, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(118, '2016-04-08', 2, 118, 37, 3, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(119, '2016-04-08', 1, 119, 38, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(120, '2016-04-08', 1, 120, 38, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(121, '2016-04-08', 1, 121, 38, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(122, '2016-04-08', 1, 122, 38, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(123, '2016-04-08', 1, 123, 38, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(124, '2016-04-08', 1, 124, 39, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(125, '2016-04-08', 2, 125, 39, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(126, '2016-04-08', 1, 126, 40, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(127, '2016-04-08', 1, 127, 40, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(128, '2016-04-08', 1, 128, 40, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(129, '2016-04-08', 1, 129, 40, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(130, '2016-04-08', 1, 130, 40, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(131, '2016-04-08', 2, 131, 41, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(132, '2016-04-08', 2, 132, 41, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(133, '2016-04-08', 2, 133, 41, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(134, '2016-04-08', 2, 134, 41, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(135, '2016-04-08', 2, 135, 41, 2, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '4.0000', 1, 0, 0),
(141, '2016-04-12', 1, 141, 43, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(142, '2016-04-12', 1, 142, 43, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(143, '2016-04-12', 1, 143, 43, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(144, '2016-04-12', 1, 144, 43, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(145, '2016-04-12', 1, 145, 43, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(146, '2016-04-12', 1, 146, 44, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(147, '2016-04-12', 1, 147, 44, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(148, '2016-04-12', 1, 148, 44, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(149, '2016-04-12', 1, 149, 44, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(150, '2016-04-12', 1, 150, 44, NULL, '1.0000', '0.0000', '0.0000', '909.0900', '1000.0000', NULL, NULL, 0, NULL),
(151, '2016-04-12', 2, 151, 45, 3, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(152, '2016-04-14', 4, 152, 46, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(153, '2016-04-14', 4, 153, 46, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(154, '2016-04-14', 4, 154, 47, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(155, '2016-04-14', 4, 155, 47, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(156, '2016-04-15', 4, 156, 48, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(157, '2016-04-15', 4, 157, 49, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(158, '2016-04-15', 4, 158, 49, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(159, '2016-04-15', 4, 159, 49, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(160, '2016-04-15', 4, 160, 50, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(161, '2016-04-15', 4, 161, 50, 7, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(162, '2016-04-15', 3, 162, 51, 5, '1.0000', '10000.0000', '11000.0000', '13636.3600', '15000.0000', '4.0000', 1, 0, 0),
(163, '2016-04-15', 3, 163, 52, 5, '1.0000', '10000.0000', '11000.0000', '13636.3600', '15000.0000', '3.0000', 1, 0, 0),
(164, '2016-04-15', 3, 164, 53, 5, '1.0000', '10000.0000', '11000.0000', '13636.3600', '15000.0000', '2.0000', 1, 0, 0),
(165, '2016-04-15', 3, 165, 54, 5, '1.0000', '10000.0000', '11000.0000', '13636.3600', '15000.0000', '1.0000', 1, 0, 0),
(166, '2016-04-15', 3, 166, 55, 5, '1.0000', '10000.0000', '11000.0000', '13636.3600', '15000.0000', '0.0000', 1, 0, 0),
(167, '2016-04-15', 5, 167, 56, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(168, '2016-04-15', 5, 168, 57, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(169, '2016-04-15', 5, 169, 58, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(170, '2016-04-15', 5, 170, 58, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(171, '2016-04-15', 5, 171, 58, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(172, '2016-04-15', 5, 172, 59, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(173, '2016-04-15', 5, 173, 59, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(174, '2016-04-15', 5, 174, 60, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(175, '2016-04-15', 5, 175, 60, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(176, '2016-04-15', 5, 176, 61, 9, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(177, '2016-04-15', 6, 177, 62, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '19.0000', 1, 0, 0),
(178, '2016-04-15', 6, 178, 63, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '18.0000', 1, 0, 0),
(179, '2016-04-15', 6, 179, 64, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '17.0000', 1, 0, 0),
(180, '2016-04-15', 6, 180, 65, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '16.0000', 1, 0, 0),
(181, '2016-04-15', 6, 181, 66, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '15.0000', 1, 0, 0),
(182, '2016-04-15', 6, 182, 66, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '15.0000', 1, 0, 0),
(183, '2016-04-15', 6, 183, 67, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '13.0000', 1, 0, 0),
(184, '2016-04-15', 6, 184, 67, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '13.0000', 1, 0, 0),
(185, '2016-04-15', 6, 185, 68, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '11.0000', 1, 0, 0),
(186, '2016-04-15', 6, 186, 69, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '10.0000', 1, 0, 0),
(187, '2016-04-15', 6, 187, 70, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(188, '2016-04-15', 6, 188, 71, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(189, '2016-04-15', 6, 189, 72, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(190, '2016-04-15', 6, 190, 73, 11, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(192, '2016-04-15', 6, 192, 75, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(193, '2016-04-15', 6, 193, 76, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(194, '2016-04-15', 6, 194, 77, 11, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(195, '2016-04-15', 6, 195, 78, 11, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(196, '2016-04-16', 7, 196, 79, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(197, '2016-04-16', 7, 197, 80, 13, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(198, '2016-04-16', 7, 198, 80, 13, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(199, '2016-04-16', 7, 199, 81, 12, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(200, '2016-04-18', 7, 200, 82, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(201, '2016-04-18', 7, 201, 82, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(202, '2016-04-18', 7, 202, 82, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(203, '2016-04-18', 7, 203, 83, 13, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(204, '2016-04-18', 7, 204, 83, 13, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(205, '2016-04-19', 7, 205, 84, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(206, '2016-04-19', 7, 206, 84, 13, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(207, '2016-04-20', 7, 207, 85, 12, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(208, '2016-04-20', 7, 208, 85, 12, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(209, '2016-04-20', 7, 209, 86, 12, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(210, '2016-04-20', 7, 210, 87, 12, '1.0000', '1818.1818', '2000.0000', '2045.4500', '2250.0000', '5.0000', 1, 0, 0),
(211, '2016-04-20', 7, 211, 88, 12, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(212, '2016-04-20', 7, 212, 88, 12, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(215, '2016-04-21', 8, 215, 90, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '19.0000', 1, 0, 0),
(216, '2016-04-21', 8, 216, 90, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '19.0000', 1, 0, 0),
(219, '2016-04-25', 8, 219, 92, 15, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '16.0000', 1, 0, 0),
(220, '2016-04-25', 8, 220, 92, 15, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '16.0000', 1, 0, 0),
(221, '2016-04-25', 8, 221, 93, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '15.0000', 1, 0, 0),
(222, '2016-04-25', 8, 222, 93, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '15.0000', 1, 0, 0),
(223, '2016-04-25', 8, 223, 93, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '15.0000', 1, 0, 0),
(224, '2016-04-25', 8, 224, 94, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(225, '2016-04-25', 8, 225, 94, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(226, '2016-04-25', 8, 226, 94, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(227, '2016-04-25', 8, 227, 94, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(228, '2016-04-25', 8, 228, 94, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(229, '2016-04-25', 8, 229, 95, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(230, '2016-04-25', 8, 230, 95, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(231, '2016-04-25', 8, 231, 95, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(232, '2016-04-25', 8, 232, 95, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(233, '2016-04-25', 8, 233, 95, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(234, '2016-04-25', 11, 234, 96, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '9.0000', 1, 0, 0),
(235, '2016-04-25', 11, 235, 96, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '9.0000', 1, 0, 0),
(238, '2016-04-25', 8, 238, 98, 15, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(239, '2016-04-25', 11, 239, 99, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '5.0000', 1, 0, 0),
(240, '2016-04-25', 11, 240, 99, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '5.0000', 1, 0, 0),
(241, '2016-04-25', 11, 241, 100, 20, '0.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '8.0000', 1, 0, 0),
(242, '2016-04-25', 11, 242, 100, 20, '0.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '8.0000', 1, 0, 0),
(243, '2016-04-25', 8, 243, 101, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '16.0000', 1, 0, 0),
(244, '2016-04-25', 11, 244, 102, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '7.0000', 1, 0, 0),
(245, '2016-04-25', 11, 245, 102, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '7.0000', 1, 0, 0),
(247, '2016-04-25', 8, 247, 104, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '15.0000', 1, 0, 0),
(248, '2016-04-25', 11, 248, 104, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '5.0000', 1, 0, 0),
(249, '2016-04-25', 11, 249, 104, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '5.0000', 1, 0, 0),
(250, '2016-04-25', 8, 250, 105, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '13.0000', 1, 0, 0),
(251, '2016-04-25', 8, 251, 106, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '13.0000', 1, 0, 0),
(252, '2016-04-25', 11, 252, 107, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, 0),
(253, '2016-04-25', 11, 253, 107, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, 0),
(254, '2016-04-25', 8, 254, 108, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '11.0000', 1, 0, 0),
(255, '2016-04-25', 8, 255, 108, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '11.0000', 1, 0, 0),
(257, '2016-04-30', 8, 257, 110, 15, '1.0000', '10000.0000', '11000.0000', '10377.3600', '11000.0000', '1.0000', 1, 0, 0),
(258, '2016-04-30', 8, 258, 110, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(259, '2016-05-02', 8, 259, 111, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '8.0000', 1, 0, 0),
(260, '2016-05-02', 8, 260, 112, 14, '0.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '8.0000', 1, 0, 0),
(261, '2016-05-02', 11, 261, 113, 20, '0.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(262, '2016-05-02', 8, 262, 114, 14, '0.0000', '10000.0000', '11000.0000', '11500.0000', '11500.0000', '7.0000', 1, 0, 0),
(263, '2016-05-05', 11, 0, 115, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '2.0000', 1, 0, 0),
(264, '2016-05-05', 8, 0, 116, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '6.0000', 1, 0, 0),
(265, '2016-05-05', 8, 0, 117, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '5.0000', 1, 0, 0),
(266, '2016-05-05', 8, 0, 118, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '4.0000', 1, 0, 0),
(267, '2016-05-05', 8, 0, 119, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '3.0000', 1, 0, 0),
(268, '2016-05-06', 8, 0, 120, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '2.0000', 1, 0, 0),
(269, '2016-05-06', 8, 0, 121, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(270, '2016-05-06', 8, 0, 122, 14, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(271, '2016-05-09', 11, 0, 124, NULL, '1.0000', NULL, NULL, '50000.0000', '55000.0000', NULL, 1, 1, NULL),
(272, '2016-05-09', 8, 0, 125, 14, '-1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(273, '2016-05-09', 8, 0, 125, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(274, '2016-05-10', 11, 0, 126, 20, '-1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(275, '2016-05-10', 11, 0, 126, NULL, '1.0000', NULL, NULL, '50000.0000', '55000.0000', NULL, 1, 1, NULL),
(276, '2016-05-11', 11, 0, 127, 20, '-3.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(277, '2016-05-11', 11, 0, 127, NULL, '1.0000', NULL, NULL, '50000.0000', '55000.0000', NULL, 1, 1, NULL),
(278, '2016-05-11', 8, 0, 128, 14, '-3.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(279, '2016-05-11', 8, 0, 128, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(280, '2016-05-11', 8, 0, 129, 14, '-4.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(281, '2016-05-11', 8, 0, 129, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(282, '2016-05-11', 12, 0, 130, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(283, '2016-05-13', 12, 0, 131, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(284, '2016-05-13', 12, 0, 132, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(285, '2016-05-13', 12, 0, 133, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(286, '2016-05-13', 12, 0, 134, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(287, '2016-05-13', 11, 0, 135, 21, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '1.0000', 1, 0, 0),
(288, '2016-05-13', 12, 0, 136, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(289, '2016-05-13', 12, 0, 137, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(290, '2016-05-17', 8, 0, 138, 29, '-5.0000', '9716.9800', '10500.0000', '10377.3600', '11000.0000', '0.0000', 1, 0, 0),
(291, '2016-05-17', 8, 0, 138, 14, '-5.0000', '9716.9800', '10500.0000', '10377.3600', '11000.0000', '0.0000', 1, 0, 0),
(292, '2016-05-17', 8, 0, 138, NULL, '1.0000', NULL, NULL, '10377.3600', '11000.0000', NULL, 1, 1, NULL),
(293, '2016-05-17', 8, 0, 139, 29, '-6.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(294, '2016-05-17', 8, 0, 139, 14, '-6.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(295, '2016-05-17', 8, 0, 139, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(296, '2016-05-17', 8, 0, 140, 29, '-7.0000', '9716.9800', '10500.0000', '9433.9600', '10000.0000', '0.0000', 1, 0, 0),
(297, '2016-05-17', 8, 0, 140, 14, '-7.0000', '9716.9800', '10500.0000', '9433.9600', '10000.0000', '0.0000', 1, 0, 0),
(298, '2016-05-17', 8, 0, 140, NULL, '1.0000', NULL, NULL, '9433.9600', '10000.0000', NULL, 1, 1, NULL),
(299, '2016-05-17', 8, 0, 140, 29, '-8.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(300, '2016-05-17', 8, 0, 140, 14, '-8.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(301, '2016-05-17', 8, 0, 140, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(302, '2016-05-17', 8, 0, 141, 29, '-15.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(303, '2016-05-17', 8, 0, 141, 14, '-15.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(304, '2016-05-17', 8, 0, 141, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(305, '2016-05-17', 8, 0, 142, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '3.0000', 1, 0, 0),
(306, '2016-05-18', 8, 0, 143, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '37.0000', 1, 0, 0),
(307, '2016-05-18', 12, 0, 144, NULL, '1.0000', NULL, NULL, '10000.0000', '10000.0000', NULL, 1, 1, NULL),
(308, '2016-05-19', 8, 0, 145, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '36.0000', 1, 0, 0),
(309, '2016-05-19', 8, 0, 146, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '35.0000', 1, 0, 0),
(310, '2016-05-19', 8, 0, 147, 15, '-1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(311, '2016-05-19', 8, 0, 147, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(312, '2016-05-19', 8, 0, 148, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '34.0000', 1, 0, 0),
(313, '2016-05-19', 8, 0, 149, 15, '-3.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(314, '2016-05-19', 8, 0, 149, NULL, '1.0000', NULL, NULL, '11320.7500', '12000.0000', NULL, 1, 1, NULL),
(315, '2016-05-19', 11, 0, 150, 20, '-3.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, 0),
(316, '2016-05-19', 11, 0, 150, NULL, '1.0000', NULL, NULL, '50000.0000', '55000.0000', NULL, 1, 1, NULL),
(317, '2016-05-20', 8, 0, 151, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '33.0000', 1, 0, 0),
(318, '2016-05-21', 11, 0, 152, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '2.0000', 1, 0, NULL),
(319, '2016-05-21', 11, 0, 152, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '1.0000', 1, 0, NULL),
(320, '2016-05-21', 11, 0, 152, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, NULL),
(321, '2016-05-21', 11, 0, 153, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '3.0000', 1, 0, NULL),
(322, '2016-05-21', 11, 0, 153, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '2.0000', 1, 0, NULL),
(323, '2016-05-21', 11, 0, 153, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '1.0000', 1, 0, NULL),
(324, '2016-05-21', 11, 0, 153, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, NULL),
(325, '2016-05-21', 8, 0, 154, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '32.0000', 1, 0, 0),
(326, '2016-05-21', 11, 0, 155, 20, '1.0000', '45454.5455', '50000.0000', '50000.0000', '55000.0000', '0.0000', 1, 0, NULL),
(327, '2016-05-21', 8, 0, 157, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '31.0000', 1, 0, 0),
(328, '2016-05-21', 8, 0, 157, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '30.0000', 1, 0, 0),
(329, '2016-05-21', 8, 0, 158, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '29.0000', 1, 0, 0),
(330, '2016-05-21', 8, 0, 158, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '28.0000', 1, 0, 0),
(331, '2016-05-21', 8, 0, 158, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '27.0000', 1, 0, 0),
(332, '2016-05-21', 8, 0, 158, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '26.0000', 1, 0, 0),
(333, '2016-05-21', 8, 0, 158, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '25.0000', 1, 0, 0),
(334, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '24.0000', 1, 0, 0),
(335, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '23.0000', 1, 0, 0),
(336, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '22.0000', 1, 0, 0),
(337, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '21.0000', 1, 0, 0),
(338, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '20.0000', 1, 0, 0),
(339, '2016-05-21', 8, 0, 159, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '19.0000', 1, 0, 0),
(340, '2016-05-21', 8, 0, 160, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '18.0000', 1, 0, 0),
(341, '2016-05-21', 8, 0, 160, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '17.0000', 1, 0, 0),
(342, '2016-05-21', 8, 0, 161, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '16.0000', 1, 0, 0),
(343, '2016-05-21', 8, 0, 161, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '15.0000', 1, 0, 0),
(344, '2016-05-21', 8, 0, 161, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '14.0000', 1, 0, 0),
(345, '2016-05-21', 8, 0, 161, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '13.0000', 1, 0, 0),
(346, '2016-05-21', 8, 0, 161, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '12.0000', 1, 0, 0),
(347, '2016-05-21', 8, 0, 162, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '11.0000', 1, 0, 0),
(348, '2016-05-21', 8, 0, 163, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '10.0000', 1, 0, 0),
(349, '2016-05-21', 8, 0, 163, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '9.0000', 1, 0, 0),
(350, '2016-05-21', 8, 0, 164, 15, '1.0000', '10000.0000', '11000.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(351, '2016-05-21', 8, 0, 166, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '8.0000', 1, 0, 0),
(352, '2016-05-21', 8, 0, 166, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(353, '2016-05-21', 8, 0, 167, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '6.0000', 1, 0, 0),
(354, '2016-05-21', 8, 0, 168, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '5.0000', 1, 0, 0),
(355, '2016-05-21', 8, 0, 168, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '4.0000', 1, 0, 0),
(356, '2016-05-21', 8, 0, 168, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '3.0000', 1, 0, 0),
(357, '2016-05-21', 8, 0, 169, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '2.0000', 1, 0, 0),
(358, '2016-05-21', 8, 0, 169, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(359, '2016-05-21', 8, 0, 170, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(360, '2016-05-21', 8, 0, 172, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '8.0000', 1, 0, 0),
(361, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '7.0000', 1, 0, 0),
(362, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '6.0000', 1, 0, 0),
(363, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '5.0000', 1, 0, 0),
(364, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '4.0000', 1, 0, 0),
(365, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '3.0000', 1, 0, 0),
(366, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '2.0000', 1, 0, 0),
(367, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(368, '2016-05-21', 8, 0, 173, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(369, '2016-05-21', 8, 0, 174, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '3.0000', 1, 0, 0),
(370, '2016-05-21', 8, 0, 174, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '2.0000', 1, 0, 0),
(371, '2016-05-21', 8, 0, 175, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '1.0000', 1, 0, 0),
(372, '2016-05-21', 8, 0, 175, 29, '1.0000', '9716.9800', '10500.0000', '11320.7500', '12000.0000', '0.0000', 1, 0, 0),
(373, '2016-05-21', 14, 0, 176, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(374, '2016-05-21', 14, 0, 177, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(375, '2016-05-21', 14, 0, 178, 31, '1.0000', '4545.4545', '5000.0000', '4090.9100', '4500.0000', '6.0000', 1, 0, 0),
(376, '2016-05-21', 14, 0, 179, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(377, '2016-05-21', 14, 0, 180, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(378, '2016-05-21', 14, 0, 181, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(379, '2016-05-21', 14, 0, 182, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(380, '2016-05-21', 14, 0, 182, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(381, '2016-05-21', 14, 0, 183, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(382, '2016-05-21', 14, 0, 185, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(383, '2016-05-21', 14, 0, 185, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(384, '2016-05-21', 14, 0, 186, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(385, '2016-05-21', 14, 0, 187, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(386, '2016-05-21', 14, 0, 188, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(387, '2016-05-21', 14, 0, 189, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(388, '2016-05-21', 14, 0, 190, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(389, '2016-05-21', 14, 0, 191, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(390, '2016-05-21', 14, 0, 192, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(391, '2016-05-21', 14, 262, 193, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(392, '2016-05-21', 14, 0, 194, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(393, '2016-05-21', 14, 0, 195, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(394, '2016-05-21', 14, 0, 196, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(395, '2016-05-21', 14, 0, 197, 31, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(396, '2016-05-21', 14, 0, 199, 31, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(397, '2016-05-21', 14, 0, 199, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(398, '2016-05-21', 14, 0, 200, 31, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(399, '2016-05-21', 14, 0, 200, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(400, '2016-05-21', 16, 0, 204, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(401, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(402, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(403, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(404, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(405, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(406, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(407, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(408, '2016-05-21', 16, 0, 205, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(409, '2016-05-21', 16, 0, 206, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(410, '2016-05-21', 16, 0, 206, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(411, '2016-05-21', 16, 0, 206, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(412, '2016-05-21', 16, 0, 206, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(413, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '18.0000', 1, 0, 0),
(414, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '17.0000', 1, 0, 0),
(415, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '16.0000', 1, 0, 0),
(416, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '15.0000', 1, 0, 0),
(417, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '14.0000', 1, 0, 0),
(418, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '13.0000', 1, 0, 0),
(419, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '12.0000', 1, 0, 0),
(420, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '11.0000', 1, 0, 0),
(421, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(422, '2016-05-21', 16, 0, 208, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '9.0000', 1, 0, 0),
(423, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(424, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(425, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(426, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(427, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(428, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(429, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(430, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(431, '2016-05-21', 16, 0, 209, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(432, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(433, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(434, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(435, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(436, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(437, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(438, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(439, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(440, '2016-05-21', 16, 0, 211, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(441, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(442, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(443, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(444, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(445, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0);
INSERT INTO `sma_costing` (`id`, `date`, `product_id`, `sale_item_id`, `sale_id`, `purchase_item_id`, `quantity`, `purchase_net_unit_cost`, `purchase_unit_cost`, `sale_net_unit_price`, `sale_unit_price`, `quantity_balance`, `inventory`, `overselling`, `option_id`) VALUES
(446, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(447, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(448, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(449, '2016-05-21', 16, 0, 213, 35, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(450, '2016-05-21', 19, 0, 221, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(451, '2016-05-21', 19, 0, 221, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(452, '2016-05-21', 19, 0, 221, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(453, '2016-05-21', 19, 0, 221, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(454, '2016-05-21', 19, 0, 222, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(455, '2016-05-21', 19, 0, 222, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(456, '2016-05-21', 19, 0, 222, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(457, '2016-05-21', 19, 0, 222, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(458, '2016-05-21', 19, 0, 223, 40, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(459, '2016-05-21', 19, 0, 223, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(460, '2016-05-21', 19, 0, 223, 40, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(461, '2016-05-21', 19, 0, 223, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(462, '2016-05-23', 19, 0, 224, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(463, '2016-05-23', 19, 0, 224, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(464, '2016-05-23', 19, 0, 224, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(465, '2016-05-23', 19, 0, 224, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(466, '2016-05-23', 20, 0, 229, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(467, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(468, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(469, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(470, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(471, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(472, '2016-05-23', 20, 0, 230, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(473, '2016-05-23', 20, 0, 231, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(474, '2016-05-23', 20, 0, 233, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(475, '2016-05-23', 20, 0, 233, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(476, '2016-05-23', 20, 0, 233, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(477, '2016-05-23', 20, 0, 233, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(478, '2016-05-23', 20, 0, 236, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(479, '2016-05-23', 20, 0, 236, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(480, '2016-05-23', 20, 0, 236, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(481, '2016-05-23', 20, 0, 236, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(482, '2016-05-23', 20, 0, 236, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(483, '2016-05-23', 20, 0, 237, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(484, '2016-05-23', 20, 0, 237, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(485, '2016-05-23', 20, 0, 237, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(486, '2016-05-23', 20, 0, 237, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(487, '2016-05-23', 20, 0, 240, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(488, '2016-05-23', 20, 0, 240, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(489, '2016-05-23', 20, 0, 240, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(490, '2016-05-23', 20, 371, 242, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(491, '2016-05-23', 20, 372, 242, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(492, '2016-05-23', 20, 373, 242, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(493, '2016-05-23', 20, 374, 242, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(494, '2016-05-23', 20, 375, 243, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(495, '2016-05-23', 20, 376, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(496, '2016-05-23', 20, 377, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(497, '2016-05-23', 20, 378, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(498, '2016-05-23', 20, 379, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(499, '2016-05-23', 20, 380, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(500, '2016-05-23', 20, 381, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(501, '2016-05-23', 20, 382, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(502, '2016-05-23', 20, 383, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(503, '2016-05-23', 20, 384, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(504, '2016-05-23', 20, 385, 244, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(505, '2016-05-23', 20, 386, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(506, '2016-05-23', 20, 387, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(507, '2016-05-23', 20, 388, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(508, '2016-05-23', 20, 389, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(509, '2016-05-23', 20, 390, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(510, '2016-05-23', 20, 391, 245, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(511, '2016-05-23', 20, 392, 246, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(512, '2016-05-23', 20, 393, 246, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(513, '2016-05-23', 20, 394, 246, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(514, '2016-05-23', 20, 395, 246, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(515, '2016-05-23', 20, 396, 246, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(516, '2016-05-23', 20, 397, 247, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(517, '2016-05-23', 20, 398, 247, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(518, '2016-05-23', 20, 399, 247, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(519, '2016-05-23', 20, 400, 247, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(520, '2016-05-23', 20, 401, 247, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(521, '2016-05-23', 20, 402, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(522, '2016-05-23', 20, 403, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(523, '2016-05-23', 20, 404, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(524, '2016-05-23', 20, 405, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(525, '2016-05-23', 20, 406, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(526, '2016-05-23', 20, 407, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(527, '2016-05-23', 20, 408, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(528, '2016-05-23', 20, 409, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(529, '2016-05-23', 20, 410, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(530, '2016-05-23', 20, 411, 248, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(531, '2016-05-23', 20, 412, 249, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(532, '2016-05-23', 20, 412, 249, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(533, '2016-05-23', 20, 413, 249, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(534, '2016-05-23', 20, 413, 249, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(535, '2016-05-23', 19, 414, 249, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(536, '2016-05-23', 19, 415, 249, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(537, '2016-05-23', 20, 416, 250, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(538, '2016-05-23', 20, 416, 250, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(539, '2016-05-23', 20, 417, 250, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(540, '2016-05-23', 20, 417, 250, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(541, '2016-05-23', 19, 418, 250, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(542, '2016-05-23', 19, 419, 250, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(547, '2016-05-23', 19, 422, 251, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(548, '2016-05-23', 19, 423, 251, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(549, '2016-05-23', 20, 424, 252, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(550, '2016-05-23', 20, 424, 252, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(551, '2016-05-23', 20, 425, 252, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(552, '2016-05-23', 20, 425, 252, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(553, '2016-05-23', 20, 426, 252, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(554, '2016-05-23', 20, 426, 252, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(555, '2016-05-23', 20, 427, 252, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(556, '2016-05-23', 20, 427, 252, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(557, '2016-05-23', 19, 428, 252, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(558, '2016-05-23', 20, 429, 253, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(559, '2016-05-23', 19, 430, 253, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '12.0000', 1, 0, 0),
(560, '2016-05-23', 19, 431, 253, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '12.0000', 1, 0, 0),
(561, '2016-05-23', 20, 432, 254, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(562, '2016-05-23', 19, 433, 254, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(563, '2016-05-23', 19, 434, 254, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(564, '2016-05-23', 19, 435, 254, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(565, '2016-05-23', 19, 436, 254, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(566, '2016-05-23', 20, 437, 255, 42, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(567, '2016-05-23', 19, 438, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(568, '2016-05-23', 19, 439, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(569, '2016-05-23', 19, 440, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(570, '2016-05-23', 19, 441, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(571, '2016-05-23', 19, 442, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(572, '2016-05-23', 19, 443, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(573, '2016-05-23', 19, 444, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(574, '2016-05-23', 19, 445, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(575, '2016-05-23', 19, 446, 255, 40, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(576, '2016-05-23', 19, 447, 256, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(577, '2016-05-23', 20, 448, 256, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(578, '2016-05-23', 19, 449, 256, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(579, '2016-05-23', 20, 450, 256, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(580, '2016-05-23', 20, 451, 256, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(581, '2016-05-23', 20, 452, 256, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(582, '2016-05-23', 19, 453, 257, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(583, '2016-05-23', 19, 454, 257, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, 0),
(584, '2016-05-23', 20, 455, 257, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(585, '2016-05-23', 20, 455, 257, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(586, '2016-05-23', 20, 456, 257, 41, '-1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(587, '2016-05-23', 20, 456, 257, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(588, '2016-05-23', 19, 457, 258, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(589, '2016-05-23', 19, 458, 258, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(590, '2016-05-23', 19, 459, 258, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(591, '2016-05-23', 19, 460, 258, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(592, '2016-05-23', 20, 461, 259, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(593, '2016-05-23', 20, 461, 259, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(594, '2016-05-23', 20, 462, 259, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(595, '2016-05-23', 20, 462, 259, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(596, '2016-05-23', 20, 463, 260, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(597, '2016-05-23', 20, 463, 260, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(598, '2016-05-23', 20, 464, 260, 41, '-3.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(599, '2016-05-23', 20, 464, 260, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(600, '2016-05-23', 20, 465, 261, 41, '-5.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(601, '2016-05-23', 20, 465, 261, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(602, '2016-05-23', 20, 466, 261, 41, '-5.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(603, '2016-05-23', 20, 466, 261, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(604, '2016-05-23', 19, 467, 262, 39, '-1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(605, '2016-05-23', 19, 467, 262, NULL, '1.0000', NULL, NULL, '5000.0000', '5500.0000', NULL, 1, 1, NULL),
(606, '2016-05-23', 20, 468, 262, 41, '-7.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(607, '2016-05-23', 20, 468, 262, NULL, '1.0000', NULL, NULL, '2272.7300', '2500.0000', NULL, 1, 1, NULL),
(608, '2016-05-23', 19, 469, 263, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(609, '2016-05-23', 20, 470, 263, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(610, '2016-05-23', 19, 471, 264, 39, '1.0000', '4545.4545', '5000.0000', '4090.9100', '4500.0000', '6.0000', 1, 0, 0),
(611, '2016-05-23', 20, 472, 265, 42, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(612, '2016-05-23', 20, 473, 266, 42, '1.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '7.0000', 1, 0, 0),
(613, '2016-05-23', 20, 474, 267, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(614, '2016-05-23', 20, 475, 268, 42, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(615, '2016-05-23', 19, 476, 269, 39, '1.0000', '4545.4545', '5000.0000', '4545.4500', '5000.0000', '5.0000', 1, 0, 0),
(616, '2016-05-23', 19, 477, 269, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '5.0000', 1, 0, 0),
(617, '2016-05-23', 19, 478, 270, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '3.0000', 1, 0, 0),
(618, '2016-05-24', 19, 479, 271, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '2.0000', 1, 0, 0),
(619, '2016-05-24', 19, 480, 272, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(620, '2016-05-24', 19, 481, 272, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(621, '2016-05-24', 20, 482, 273, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(622, '2016-05-24', 20, 483, 274, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(623, '2016-05-24', 20, 484, 275, 42, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(624, '2016-05-24', 22, 485, 276, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '9.0000', 1, 0, 0),
(625, '2016-05-24', 22, 486, 277, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '8.0000', 1, 0, 0),
(626, '2016-05-24', 22, 487, 278, 45, '0.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '6.0000', 1, 0, 0),
(627, '2016-05-25', 20, 488, 279, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(628, '2016-05-25', 22, 489, 279, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '7.0000', 1, 0, 0),
(629, '2016-05-25', 20, 490, 279, 41, '1.0000', '1818.1818', '2000.0000', '1363.6400', '1500.0000', '7.0000', 1, 0, 0),
(630, '2016-05-25', 22, 491, 280, 45, '0.0000', '9090.9091', '10000.0000', '10000.0000', '11000.0000', '5.0000', 1, 0, 0),
(631, '2016-05-25', 21, 492, 280, 43, '0.0000', '3636.3636', '4000.0000', '3636.3600', '4000.0000', '8.0000', 1, 0, 0),
(632, '2016-05-25', 21, 493, 280, 43, '0.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '8.0000', 1, 0, 0),
(633, '2016-05-25', 20, 494, 281, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(634, '2016-05-25', 22, 495, 281, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '6.0000', 1, 0, 0),
(635, '2016-05-25', 22, 496, 282, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '5.0000', 1, 0, 0),
(636, '2016-05-25', 21, 497, 283, 43, '0.0000', '3636.3636', '4000.0000', '3636.3600', '4000.0000', '8.0000', 1, 0, 0),
(637, '2016-05-25', 20, 498, 283, 41, '0.0000', '1818.1818', '2000.0000', '2090.9100', '2300.0000', '3.0000', 1, 0, 0),
(638, '2016-05-25', 22, 499, 283, 45, '0.0000', '9090.9091', '10000.0000', '10000.0000', '11000.0000', '3.0000', 1, 0, 0),
(639, '2016-05-25', 22, 500, 284, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '4.0000', 1, 0, 0),
(640, '2016-05-25', 22, 501, 285, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '3.0000', 1, 0, 0),
(641, '2016-05-25', 22, 502, 286, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '2.0000', 1, 0, 0),
(642, '2016-05-25', 22, 503, 287, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '1.0000', 1, 0, 0),
(643, '2016-05-25', 22, 504, 288, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '0.0000', 1, 0, 0),
(644, '2016-05-25', 20, 505, 288, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(645, '2016-05-25', 20, 506, 288, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(646, '2016-05-25', 20, 507, 289, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(647, '2016-05-25', 20, 508, 290, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(648, '2016-05-25', 20, 509, 290, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(649, '2016-05-25', 21, 510, 291, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '9.0000', 1, 0, 0),
(650, '2016-05-25', 21, 511, 291, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '9.0000', 1, 0, 0),
(651, '2016-05-25', 21, 512, 292, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '7.0000', 1, 0, 0),
(652, '2016-05-25', 21, 513, 293, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '6.0000', 1, 0, 0),
(653, '2016-05-25', 21, 514, 293, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '6.0000', 1, 0, 0),
(654, '2016-05-25', 21, 515, 294, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '4.0000', 1, 0, 0),
(655, '2016-05-30', 21, 516, 295, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '3.0000', 1, 0, 0),
(656, '2016-05-31', 19, 517, 296, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '1.0000', 1, 0, 0),
(657, '2016-05-31', 21, 518, 297, 44, '0.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '8.0000', 1, 0, 0),
(658, '2016-05-31', 21, 519, 298, 44, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '8.0000', 1, 0, 0),
(659, '2016-06-01', 21, 520, 299, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '2.0000', 1, 0, 0),
(660, '2016-06-01', 21, 521, 300, 44, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '7.0000', 1, 0, 0),
(661, '2016-06-01', 21, 522, 301, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '1.0000', 1, 0, 0),
(662, '2016-06-01', 21, 523, 301, 43, '1.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '1.0000', 1, 0, 0),
(663, '2016-06-01', 19, 524, 302, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '0.0000', 1, 0, 0),
(664, '2016-06-01', 20, 525, 303, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '49.0000', 1, 0, 0),
(665, '2016-06-01', 20, 526, 304, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '48.0000', 1, 0, 0),
(666, '2016-06-01', 20, 527, 305, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '47.0000', 1, 0, 0),
(667, '2016-06-01', 20, 528, 306, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '46.0000', 1, 0, 0),
(668, '2016-06-01', 20, 529, 307, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '45.0000', 1, 0, 0),
(669, '2016-06-01', 20, 530, 308, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '44.0000', 1, 0, 0),
(670, '2016-06-01', 20, 531, 309, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '43.0000', 1, 0, 0),
(671, '2016-06-01', 21, 532, 310, 44, '0.0000', '3636.3636', '4000.0000', '4090.9100', '4500.0000', '5.0000', 1, 0, 0),
(672, '2016-06-01', 20, 533, 311, 42, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(673, '2016-06-01', 20, 534, 312, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '42.0000', 1, 0, 0),
(674, '2016-06-01', 19, 535, 313, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '49.0000', 1, 0, NULL),
(675, '2016-06-01', 19, 536, 314, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '47.0000', 1, 0, NULL),
(676, '2016-06-01', 20, 537, 314, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '40.0000', 1, 0, 0),
(677, '2016-06-01', 20, 538, 315, 42, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(678, '2016-06-01', 20, 539, 315, 42, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, NULL),
(679, '2016-06-01', 20, 540, 316, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '40.0000', 1, 0, NULL),
(680, '2016-06-01', 20, 541, 316, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '40.0000', 1, 0, 0),
(681, '2016-06-01', 19, 542, 316, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '47.0000', 1, 0, 0),
(682, '2016-06-01', 20, 543, 317, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '38.0000', 1, 0, NULL),
(683, '2016-06-01', 20, 544, 317, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '38.0000', 1, 0, 0),
(684, '2016-06-02', 20, 545, 318, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '36.0000', 1, 0, 0),
(685, '2016-06-02', 20, 546, 319, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '35.0000', 1, 0, 0),
(686, '2016-06-02', 20, 547, 320, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '35.0000', 1, 0, 0),
(687, '2016-06-02', 20, 548, 321, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '33.0000', 1, 0, 0),
(688, '2016-06-02', 20, 549, 322, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '32.0000', 1, 0, 0),
(689, '2016-06-02', 20, 550, 323, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '31.0000', 1, 0, 0),
(690, '2016-06-03', 20, 548, 321, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '36.0000', 1, 0, 0),
(691, '2016-06-03', 20, 549, 322, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '35.0000', 1, 0, 0),
(692, '2016-06-03', 20, 550, 323, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '34.0000', 1, 0, 0),
(693, '2016-06-03', 20, 551, 324, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '33.0000', 1, 0, 0),
(694, '2016-06-03', 20, 552, 325, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '32.0000', 1, 0, 0),
(695, '2016-06-03', 20, 553, 326, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '31.0000', 1, 0, 0),
(696, '2016-06-03', 20, 554, 327, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '30.0000', 1, 0, 0),
(697, '2016-06-03', 20, 555, 328, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '29.0000', 1, 0, 0),
(698, '2016-06-03', 20, 556, 329, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '27.0000', 1, 0, 0),
(699, '2016-06-03', 20, 557, 330, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '26.0000', 1, 0, 0),
(700, '2016-06-03', 20, 558, 331, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '25.0000', 1, 0, 0),
(701, '2016-06-04', 20, 559, 332, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '25.0000', 1, 0, 0),
(702, '2016-06-06', 20, 560, 333, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '23.0000', 1, 0, 0),
(703, '2016-06-06', 20, 561, 334, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '24.0000', 1, 0, 0),
(704, '2016-06-06', 19, 562, 335, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '47.0000', 1, 0, 0),
(705, '2016-06-06', 19, 563, 336, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '45.0000', 1, 0, 0),
(706, '2016-06-06', 19, 564, 336, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '45.0000', 1, 0, 0),
(707, '2016-06-06', 20, 565, 337, 41, '0.0000', '1818.1818', '2000.0000', '1818.1800', '2000.0000', '22.0000', 1, 0, 0),
(708, '2016-06-06', 20, 566, 338, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '21.0000', 1, 0, 0),
(709, '2016-06-06', 20, 567, 339, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '20.0000', 1, 0, 0),
(710, '2016-06-06', 19, 568, 339, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '43.0000', 1, 0, 0),
(711, '2016-06-06', 20, 569, 340, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '19.0000', 1, 0, 0),
(712, '2016-06-06', 19, 570, 340, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '42.0000', 1, 0, 0),
(713, '2016-06-06', 20, 571, 341, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '18.0000', 1, 0, 0),
(714, '2016-06-06', 20, 572, 341, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '18.0000', 1, 0, 0),
(715, '2016-06-06', 20, 573, 342, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '16.0000', 1, 0, 0),
(716, '2016-06-06', 20, 574, 342, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '16.0000', 1, 0, 0),
(717, '2016-06-06', 20, 575, 343, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '14.0000', 1, 0, 0),
(718, '2016-06-06', 20, 576, 343, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '14.0000', 1, 0, 0),
(719, '2016-06-06', 20, 577, 344, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '12.0000', 1, 0, 0),
(720, '2016-06-07', 20, 578, 345, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '11.0000', 1, 0, 0),
(721, '2016-06-15', 20, 579, 346, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '11.0000', 1, 0, 0),
(722, '2016-06-17', 19, 580, 347, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '45.0000', 1, 0, 0),
(723, '2016-06-17', 19, 581, 348, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '45.0000', 1, 0, 0),
(724, '2016-06-17', 20, 582, 349, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '25.0000', 1, 0, 0),
(725, '2016-06-17', 20, 583, 350, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '23.0000', 1, 0, 0),
(726, '2016-06-17', 20, 584, 350, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '23.0000', 1, 0, 0),
(727, '2016-06-17', 20, 585, 351, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '21.0000', 1, 0, 0),
(728, '2016-06-17', 20, 586, 352, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '20.0000', 1, 0, 0),
(729, '2016-06-17', 20, 587, 353, 41, '0.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '21.0000', 1, 0, 0),
(730, '2016-06-18', 20, 588, 354, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '21.0000', 1, 0, 0),
(731, '2016-06-18', 19, 589, 355, 39, '1.0000', '4545.4545', '5000.0000', '4545.4500', '5000.0000', '45.0000', 1, 0, 0),
(732, '2016-06-18', 20, 590, 356, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '23.0000', 1, 0, 0),
(733, '2016-06-18', 20, 591, 357, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '22.0000', 1, 0, 0),
(734, '2016-06-18', 20, 592, 357, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '22.0000', 1, 0, 0),
(735, '2016-06-18', 20, 593, 357, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '22.0000', 1, 0, 0),
(736, '2016-06-20', 20, 594, 358, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '19.0000', 1, 0, 0),
(737, '2016-06-20', 20, 595, 359, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '18.0000', 1, 0, 0),
(738, '2016-06-23', 19, 596, 360, 39, '1.0000', '4545.4545', '5000.0000', '4954.2500', '5449.6800', '44.0000', 1, 0, 0),
(739, '2016-06-28', 19, 597, 361, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '43.0000', 1, 0, 0),
(740, '2016-06-28', 19, 600, 363, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '42.0000', 1, 0, 0),
(741, '2016-06-28', 19, 601, 364, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '41.0000', 1, 0, 0),
(742, '2016-06-28', 19, 602, 365, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '40.0000', 1, 0, 0),
(743, '2016-06-28', 20, 603, 366, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '17.0000', 1, 0, 0),
(744, '2016-06-28', 20, 604, 366, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '17.0000', 1, 0, 0),
(745, '2016-06-28', 20, 605, 366, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '17.0000', 1, 0, 0),
(746, '2016-06-28', 20, 606, 366, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '17.0000', 1, 0, 0),
(747, '2016-06-28', 20, 607, 367, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '13.0000', 1, 0, 0),
(748, '2016-06-28', 20, 608, 367, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '13.0000', 1, 0, 0),
(749, '2016-06-28', 20, 609, 367, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '13.0000', 1, 0, 0),
(750, '2016-06-28', 19, 610, 368, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '39.0000', 1, 0, 0),
(751, '2016-06-29', 19, 611, 369, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '38.0000', 1, 0, 0),
(752, '2016-06-29', 19, 612, 370, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '37.0000', 1, 0, 0),
(753, '2016-06-29', 20, 613, 371, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '10.0000', 1, 0, 0),
(754, '2016-06-29', 20, 614, 372, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '9.0000', 1, 0, 0),
(755, '2016-06-29', 20, 615, 373, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '8.0000', 1, 0, 0),
(756, '2016-06-29', 20, 616, 374, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '7.0000', 1, 0, 0),
(757, '2016-06-29', 19, 617, 375, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '36.0000', 1, 0, 0),
(758, '2016-06-29', 20, 618, 376, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '6.0000', 1, 0, 0),
(759, '2016-06-29', 20, 619, 377, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '5.0000', 1, 0, 0),
(760, '2016-06-29', 19, 620, 377, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '35.0000', 1, 0, 0),
(761, '2016-06-29', 20, 621, 378, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '4.0000', 1, 0, 0),
(762, '2016-06-29', 19, 622, 378, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '34.0000', 1, 0, 0),
(763, '2016-06-29', 20, 623, 379, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '3.0000', 1, 0, 0),
(764, '2016-06-29', 19, 624, 379, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '33.0000', 1, 0, 0),
(765, '2016-06-29', 20, 625, 380, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '2.0000', 1, 0, 0),
(766, '2016-06-29', 19, 626, 380, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '32.0000', 1, 0, 0),
(767, '2016-06-29', 20, 627, 381, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '1.0000', 1, 0, 0),
(768, '2016-06-29', 19, 628, 381, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '31.0000', 1, 0, 0),
(769, '2016-06-29', 20, 629, 382, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '0.0000', 1, 0, 0),
(770, '2016-06-29', 19, 630, 382, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '30.0000', 1, 0, 0),
(771, '2016-06-29', 19, 631, 383, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '29.0000', 1, 0, 0),
(772, '2016-06-29', 19, 632, 384, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '28.0000', 1, 0, 0),
(773, '2016-06-29', 19, 633, 385, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '27.0000', 1, 0, 0),
(774, '2016-06-29', 19, 634, 386, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '26.0000', 1, 0, 0),
(775, '2016-06-29', 19, 635, 387, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '25.0000', 1, 0, 0),
(776, '2016-06-29', 19, 636, 388, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '24.0000', 1, 0, 0),
(777, '2016-06-29', 19, 637, 389, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '23.0000', 1, 0, 0),
(778, '2016-06-29', 19, 638, 390, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '22.0000', 1, 0, 0),
(779, '2016-06-29', 19, 639, 391, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '21.0000', 1, 0, 0),
(780, '2016-06-29', 19, 640, 392, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '20.0000', 1, 0, 0),
(781, '2016-06-29', 19, 641, 393, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '19.0000', 1, 0, 0),
(782, '2016-06-29', 19, 642, 394, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '18.0000', 1, 0, 0),
(783, '2016-06-29', 19, 643, 395, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '17.0000', 1, 0, 0),
(784, '2016-06-29', 19, 644, 396, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '16.0000', 1, 0, 0),
(785, '2016-06-29', 19, 645, 397, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '15.0000', 1, 0, 0),
(786, '2016-06-29', 19, 646, 398, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '14.0000', 1, 0, 0),
(787, '2016-06-29', 19, 647, 399, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '13.0000', 1, 0, 0),
(788, '2016-06-29', 19, 648, 400, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '12.0000', 1, 0, 0),
(789, '2016-06-29', 19, 649, 401, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '11.0000', 1, 0, 0),
(790, '2016-06-29', 19, 650, 402, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '10.0000', 1, 0, 0),
(791, '2016-06-29', 19, 651, 403, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '9.0000', 1, 0, 0),
(792, '2016-06-29', 19, 652, 404, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '8.0000', 1, 0, 0),
(793, '2016-06-29', 19, 653, 405, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '7.0000', 1, 0, 0),
(794, '2016-06-29', 19, 654, 406, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '6.0000', 1, 0, 0),
(795, '2016-06-30', 19, 655, 407, 39, '0.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, NULL),
(796, '2016-06-30', 19, 656, 408, 39, '1.0000', '4545.4545', '5000.0000', '5000.0000', '5500.0000', '4.0000', 1, 0, NULL),
(797, '2016-07-05', 19, 657, 409, 39, '0.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '3.0000', 1, 0, NULL),
(798, '2016-07-05', 24, 658, 410, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '9.0000', 1, 0, NULL),
(799, '2016-07-05', 19, 659, 411, 39, '1.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '4.0000', 1, 0, NULL),
(800, '2016-07-05', 24, 660, 412, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '8.0000', 1, 0, NULL),
(801, '2016-07-05', 24, 661, 413, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '7.0000', 1, 0, NULL),
(802, '2016-07-05', 24, 662, 414, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '6.0000', 1, 0, NULL),
(803, '2016-07-05', 24, 663, 415, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '5.0000', 1, 0, NULL),
(804, '2016-07-05', 24, 664, 416, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '4.0000', 1, 0, NULL),
(805, '2016-07-05', 24, 665, 417, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '3.0000', 1, 0, NULL),
(806, '2016-07-05', 24, 666, 418, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '2.0000', 1, 0, NULL),
(807, '2016-07-05', 24, 667, 419, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '1.0000', 1, 0, NULL),
(808, '2016-07-05', 24, 668, 420, 47, '1.0000', '18181.8182', '20000.0000', '22727.2700', '25000.0000', '0.0000', 1, 0, NULL),
(809, '2016-07-05', 19, 669, 421, 39, '1.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '3.0000', 1, 0, NULL),
(810, '2016-07-05', 19, 670, 422, 39, '1.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '2.0000', 1, 0, NULL),
(811, '2016-07-05', 19, 671, 423, 39, '1.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '1.0000', 1, 0, NULL),
(812, '2016-07-06', 19, 672, 424, 39, '1.0000', '4545.4545', '5000.0000', '3181.8200', '3500.0000', '0.0000', 1, 0, NULL),
(813, '2016-07-06', 22, 673, 425, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '99.0000', 1, 0, NULL),
(814, '2016-07-06', 22, 674, 426, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '98.0000', 1, 0, NULL),
(815, '2016-07-06', 22, 675, 427, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '97.0000', 1, 0, NULL),
(816, '2016-07-07', 20, 676, 428, 41, '1.0000', '1818.1818', '2000.0000', '2272.7300', '2500.0000', '99.0000', 1, 0, NULL),
(817, '2016-07-07', 22, 677, 429, 45, '1.0000', '9090.9091', '10000.0000', '10909.0900', '12000.0000', '96.0000', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_currencies`
--

INSERT INTO `sma_currencies` (`id`, `code`, `name`, `rate`, `auto_update`) VALUES
(1, 'USD', 'US Dollar', '1.0000', 0),
(2, 'ERU', 'EURO', '0.7340', 0),
(3, 'INR', 'Indian Rupee', '67.0700', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_groups`
--

CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_customer_groups`
--

INSERT INTO `sma_customer_groups` (`id`, `name`, `percent`) VALUES
(1, 'General', 0),
(2, 'Reseller', -5),
(3, 'Distributor', -15),
(4, 'New Customer (+10)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_service`
--

CREATE TABLE `sma_customer_service` (
  `id` int(5) NOT NULL,
  `product_id` int(5) NOT NULL,
  `warehouse_id` int(5) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `reference` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_customer_service`
--

INSERT INTO `sma_customer_service` (`id`, `product_id`, `warehouse_id`, `customer_name`, `mobile`, `reference`) VALUES
(1, 25, 1, 'ankit', '8527536265', 'ayush1212'),
(2, 25, 1, 'dsadsa', '8527536265', 'ayush1212'),
(3, 25, 1, 'ayush', '8527536265', 'access123');

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_date_format`
--

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `register_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_expenses`
--

INSERT INTO `sma_expenses` (`id`, `date`, `reference`, `amount`, `note`, `created_by`, `attachment`, `register_id`) VALUES
(1, '2016-07-04 11:36:00', 'text', '1200.0000', '<p>gtgtyu</p>', '1', NULL, 74),
(2, '2016-07-05 05:31:51', '2', '5000.0000', '<p>swfdsf</p>', '2', NULL, 74),
(3, '2016-07-05 05:33:13', '4', '10000.0000', '<p>given to ayush</p>', '2', NULL, 74),
(4, '2016-07-05 13:47:53', '', '2000.0000', '<p>sd</p>', '2', NULL, 79),
(5, '2016-07-05 13:57:21', '', '500.0000', '<p>ytyt</p>', '2', NULL, 79),
(6, '2016-07-06 06:16:16', '', '100.0000', '<p>weq</p>', '2', NULL, 80),
(7, '2016-07-06 06:22:32', '', '200.0000', '<p>dsfsd</p>', '2', NULL, 81),
(8, '2016-07-06 06:29:11', '', '100.0000', '<p>sss</p>', '2', NULL, 82),
(9, '2016-07-06 06:32:03', '', '200.0000', '', '2', NULL, 83),
(10, '2016-07-06 06:33:17', '', '500.0000', '<p>fff</p>', '2', NULL, 84),
(11, '2016-07-06 06:42:55', '', '100.0000', '', '2', NULL, 85),
(12, '2016-07-06 07:04:20', '', '1000.0000', '<p>ewq</p>', '2', NULL, 86),
(13, '2016-07-07 05:48:43', '', '2000.0000', '<p>amount</p>', '2', NULL, 88);

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(40) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `biller_id` int(5) NOT NULL,
  `year` int(5) NOT NULL,
  `invoice_no` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_gift_cards`
--

INSERT INTO `sma_gift_cards` (`id`, `date`, `card_no`, `value`, `customer_id`, `customer`, `balance`, `expiry`, `created_by`, `biller_id`, `year`, `invoice_no`) VALUES
(37, '2016-06-17 08:35:54', '9805365974888341', '11000.0000', 11, 'Adesh', '11000.0000', '2016-08-16', '1', 3, 2016, ''),
(38, '2016-06-17 08:55:35', '0824725552935139', '7000.0000', 9, 'Swatch Delhi', '7000.0000', '2016-08-16', '1', 3, 2016, ''),
(39, '2016-06-17 10:19:44', '0782293320016943', '5000.0000', 63, 'ankit', '5000.0000', '2016-08-16', '1', 3, 2016, ''),
(40, '2016-06-18 05:35:00', '7358911943882094', '20000.0000', 63, 'ankit', '20000.0000', '2016-08-17', '1', 3, 2016, ''),
(42, '2016-06-18 08:52:39', '6757501747437143', '5000.0000', 9, 'Swatch Delhi', '5000.0000', '2016-08-17', '1', 3, 2016, ''),
(43, '2016-06-18 10:25:53', '8337220220607165', '5000.0000', 9, 'Swatch Delhi', '5000.0000', '2016-08-17', '1', 3, 2016, 'SALE/POS/2016/06/0350'),
(44, '2016-07-05 09:21:02', '0369071016837857', '5500.0000', 63, 'ankit', '5500.0000', '2016-09-03', '2', 3, 2016, 'SALE/POS/2016/06/0407'),
(45, '2016-07-05 09:21:27', '7628147517396609', '5500.0000', 63, 'ankit', '2000.0000', '2016-09-03', '2', 3, 2016, 'SALE/POS/2016/06/0407'),
(46, '2016-07-05 11:11:23', '6604383630016308', '3500.0000', 63, 'ankit', '3500.0000', '2016-09-03', '2', 3, 2016, 'SALE/POS/2016/07/0409'),
(47, '2016-07-05 11:15:52', '0223500534345000', '5500.0000', 9, 'Swatch Delhi', '5500.0000', '2016-09-03', '2', 3, 2016, 'SALE/POS/2016/05/0258');

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

CREATE TABLE `sma_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_groups`
--

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(5, 'sales', 'Sales Staff'),
(6, 'manager', 'Manager'),
(14, 'test group', 'ankit');

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_migrations`
--

INSERT INTO `sma_migrations` (`version`) VALUES
(308);

-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_order_ref`
--

INSERT INTO `sma_order_ref` (`ref_id`, `date`, `so`, `qu`, `po`, `to`, `pos`, `do`, `pay`, `re`, `ex`) VALUES
(1, '2015-03-01', 1, 1, 1, 1, 430, 1, 1005, 59, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `card_type` varchar(20) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000',
  `register_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_payments`
--

INSERT INTO `sma_payments` (`id`, `date`, `sale_id`, `return_id`, `purchase_id`, `reference_no`, `transaction_id`, `paid_by`, `cheque_no`, `cc_no`, `cc_holder`, `cc_month`, `cc_year`, `cc_type`, `card_type`, `amount`, `currency`, `created_by`, `attachment`, `type`, `note`, `pos_paid`, `pos_balance`, `register_id`) VALUES
(1, '2016-01-06 05:41:41', 1, NULL, NULL, 'IPAY/2016/01/0001', NULL, 'cash', '', '', '', '', '', '', '', '1083.5000', NULL, 1, NULL, 'received', '', '1083.5000', '0.0000', 0),
(2, '2016-01-28 08:08:11', 2, NULL, NULL, 'IPAY/2016/01/0003', NULL, 'cash', '', '', '', '', '', '', '', '100.0000', NULL, 1, NULL, 'received', 'cash', '100.0000', '-900.0000', 0),
(3, '2016-03-14 15:10:26', 3, NULL, NULL, 'IPAY/2016/03/0005', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '0.0000', 0),
(4, '2016-03-14 15:30:53', 4, NULL, NULL, 'IPAY/2016/03/0007', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 4, NULL, 'received', '', '2000.0000', '0.0000', 0),
(5, '2016-03-14 15:35:52', 6, NULL, NULL, 'IPAY/2016/03/0009', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 5, NULL, 'received', '', '2000.0000', '0.0000', 0),
(6, '2016-03-14 15:36:04', 7, NULL, NULL, 'IPAY/2016/03/0011', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '4000.0000', '0.0000', 0),
(7, '2016-03-15 06:52:25', 8, NULL, NULL, 'IPAY/2016/03/0013', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '0.0000', 0),
(8, '2016-03-30 13:10:54', 9, NULL, NULL, 'IPAY/2016/03/0015', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 4, NULL, 'received', '', '2000.0000', '0.0000', 0),
(9, '2016-03-30 13:11:14', 10, NULL, NULL, 'IPAY/2016/03/0017', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 4, NULL, 'received', '', '4000.0000', '0.0000', 0),
(10, '2016-03-30 14:25:40', 4, 1, NULL, 'IPAY/2016/03/0019', NULL, 'cash', '', '', '', '', '', 'Visa', '', '1000.0000', NULL, 2, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(11, '2016-04-02 07:55:44', 12, NULL, NULL, 'IPAY/2016/04/0019', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '0.0000', 0),
(12, '2016-04-04 12:45:34', 14, NULL, NULL, 'IPAY/2016/04/0021', NULL, 'cash', '', '', '', '', '', '', '', '3000.0000', NULL, 1, NULL, 'received', '', '34500.0000', '31500.0000', 0),
(13, '2016-04-04 12:49:16', 15, NULL, NULL, 'IPAY/2016/04/0023', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', 'paid', '23000.0000', '18000.0000', 0),
(14, '2016-04-04 14:40:17', 16, NULL, NULL, 'IPAY/2016/04/0025', NULL, 'cash', '', '', '', '', '', '', '', '3000.0000', NULL, 2, NULL, 'received', 'good sale', '5000.0000', '2000.0000', 0),
(15, '2016-04-04 14:42:28', 17, NULL, NULL, 'IPAY/2016/04/0027', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(16, '2016-04-05 09:08:30', 18, NULL, NULL, 'IPAY/2016/04/0029', NULL, 'cash', '', '', '', '', '', '', '', '3000.0000', NULL, 5, NULL, 'received', 'how, are you', '4500.0000', '1500.0000', 0),
(17, '2016-04-05 09:30:57', 19, NULL, NULL, 'IPAY/2016/04/0031', NULL, 'cash', '', '', '', '', '', '', '', '3000.0000', NULL, 5, NULL, 'received', '', '30000.0000', '27000.0000', 0),
(18, '2016-04-05 09:41:54', 20, NULL, NULL, 'IPAY/2016/04/0033', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '4500.0000', '500.0000', 0),
(19, '2016-04-05 09:43:02', 21, NULL, NULL, 'IPAY/2016/04/0035', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(20, '2016-04-05 10:00:34', 22, NULL, NULL, 'IPAY/2016/04/0037', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(21, '2016-04-05 10:22:05', 23, NULL, NULL, 'IPAY/2016/04/0039', NULL, 'cash', '', '', '', '', '', '', '', '8000.0000', NULL, 5, NULL, 'received', '', '8000.0000', '0.0000', 0),
(22, '2016-04-05 11:13:26', 24, NULL, NULL, 'IPAY/2016/04/0041', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '6000.0000', '2000.0000', 0),
(23, '2016-04-05 11:16:17', 25, NULL, NULL, 'IPAY/2016/04/0043', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(24, '2016-04-05 11:51:44', 26, NULL, NULL, 'IPAY/2016/04/0045', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '4000.0000', '0.0000', 0),
(25, '2016-04-05 11:55:30', 27, NULL, NULL, 'IPAY/2016/04/0047', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(26, '2016-04-05 12:39:48', 28, NULL, NULL, 'IPAY/2016/04/0049', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '4500.0000', '500.0000', 0),
(27, '2016-04-05 12:53:46', 29, NULL, NULL, 'IPAY/2016/04/0051', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 5, NULL, 'received', '', '2000.0000', '0.0000', 0),
(28, '2016-04-05 14:32:47', 30, NULL, NULL, 'IPAY/2016/04/0053', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 5, NULL, 'received', '', '2000.0000', '0.0000', 0),
(29, '2016-04-06 12:40:49', 31, NULL, NULL, 'IPAY/2016/04/0055', NULL, 'cash', '', '', '', '', '', '', '', '550000.0000', NULL, 1, NULL, 'received', '', '550000.0000', '0.0000', 0),
(30, '2016-04-08 08:04:46', 32, NULL, NULL, 'IPAY/2016/04/0057', NULL, 'cash', '', '', '', '', '', '', '', '165000.0000', NULL, 1, NULL, 'received', '', '165000.0000', '0.0000', 0),
(31, '2016-04-08 09:42:40', 33, NULL, NULL, 'IPAY/2016/04/0059', NULL, 'cash', '', '', '', '', '', '', '', '60000.0000', NULL, 1, NULL, 'received', '', '60000.0000', '0.0000', 0),
(32, '2016-04-08 09:44:24', 34, NULL, NULL, 'IPAY/2016/04/0061', NULL, 'cash', '', '', '', '', '', '', '', '169000.0000', NULL, 1, NULL, 'received', '', '169000.0000', '0.0000', 0),
(33, '2016-04-08 09:53:15', 35, NULL, NULL, 'IPAY/2016/04/0063', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 1, NULL, 'received', '', '4000.0000', '0.0000', 0),
(34, '2016-04-08 09:54:57', 36, NULL, NULL, 'IPAY/2016/04/0065', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(35, '2016-04-08 10:03:13', 37, NULL, NULL, 'IPAY/2016/04/0067', NULL, 'cash', '', '', '', '', '', '', '', '220000.0000', NULL, 5, NULL, 'received', '', '220000.0000', '0.0000', 0),
(36, '2016-04-08 10:06:38', 38, NULL, NULL, 'IPAY/2016/04/0069', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(37, '2016-04-08 10:10:28', 39, NULL, NULL, 'IPAY/2016/04/0071', NULL, 'cash', '', '', '', '', '', '', '', '56000.0000', NULL, 2, NULL, 'received', '', '56000.0000', '0.0000', 0),
(38, '2016-04-08 10:12:46', 40, NULL, NULL, 'IPAY/2016/04/0073', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(39, '2016-04-08 10:16:19', 41, NULL, NULL, 'IPAY/2016/04/0075', NULL, 'cash', '', '', '', '', '', '', '', '275000.0000', NULL, 2, NULL, 'received', '', '275000.0000', '0.0000', 0),
(40, '2016-04-12 08:00:44', 42, NULL, NULL, 'IPAY/2016/04/0077', NULL, 'cash', '', '', '', '', '', '', '', '275000.0000', NULL, 2, NULL, 'received', '', '275000.0000', '0.0000', 0),
(41, '2016-04-12 08:02:33', 43, NULL, NULL, 'IPAY/2016/04/0079', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(42, '2016-04-12 08:03:17', 44, NULL, NULL, 'IPAY/2016/04/0081', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(43, '2016-04-12 08:10:46', 45, NULL, NULL, 'IPAY/2016/04/0083', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 5, NULL, 'received', '', '55000.0000', '0.0000', 0),
(44, '2016-04-14 12:47:42', 46, NULL, NULL, 'IPAY/2016/04/0085', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(45, '2016-04-14 13:34:43', 47, NULL, NULL, 'IPAY/2016/04/0087', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '50000.0000', '45000.0000', 0),
(46, '2016-04-15 06:55:33', 48, NULL, NULL, 'IPAY/2016/04/0089', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(47, '2016-04-15 07:29:21', 49, NULL, NULL, 'IPAY/2016/04/0091', NULL, 'credit_voucher', '', '566576', '', '', '', '', '', '7500.0000', NULL, 5, NULL, 'received', '', '7500.0000', '0.0000', 0),
(48, '2016-04-15 07:30:28', 50, NULL, NULL, 'IPAY/2016/04/0093', NULL, 'credit_voucher', '', '67889', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(49, '2016-04-15 08:03:35', 51, NULL, NULL, 'IPAY/2016/04/0095', NULL, 'credit_voucher', '', '232131', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '0.0000', 0),
(50, '2016-04-15 08:04:28', 52, NULL, NULL, 'IPAY/2016/04/0097', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '0.0000', 0),
(51, '2016-04-15 09:24:22', 53, NULL, NULL, 'IPAY/2016/04/0099', NULL, 'credit_voucher', '', '1256', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '0.0000', 0),
(52, '2016-04-15 09:27:07', 54, NULL, NULL, 'IPAY/2016/04/0101', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '0.0000', 0),
(53, '2016-04-15 09:29:39', 55, NULL, NULL, 'IPAY/2016/04/0103', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '0.0000', 0),
(54, '2016-04-15 09:33:49', 56, NULL, NULL, 'IPAY/2016/04/0105', NULL, 'credit_voucher', '', '1256', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2523.0000', '23.0000', 0),
(55, '2016-04-15 09:44:29', 57, NULL, NULL, 'IPAY/2016/04/0107', NULL, 'credit_voucher', '', '23422', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(56, '2016-04-15 09:49:07', 58, NULL, NULL, 'IPAY/2016/04/0109', NULL, 'cash', '', '', '', '', '', '', '', '7500.0000', NULL, 5, NULL, 'received', '', '7500.0000', '0.0000', 0),
(57, '2016-04-15 09:50:37', 59, NULL, NULL, 'IPAY/2016/04/0111', NULL, 'credit_voucher', '', '12333', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(58, '2016-04-15 09:52:25', 60, NULL, NULL, 'IPAY/2016/04/0113', NULL, 'credit_voucher', '', '3456', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(59, '2016-04-15 10:05:21', 61, NULL, NULL, 'IPAY/2016/04/0115', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '40000.0000', '37500.0000', 0),
(60, '2016-04-15 11:58:46', 62, NULL, NULL, 'IPAY/2016/04/0117', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '37500.0000', '35000.0000', 0),
(61, '2016-04-15 12:14:08', 64, NULL, NULL, 'IPAY/2016/04/0119', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(62, '2016-04-15 13:07:45', 65, NULL, NULL, 'IPAY/2016/04/0121', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(63, '2016-04-15 13:09:02', 66, NULL, NULL, 'IPAY/2016/04/0123', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(64, '2016-04-15 13:10:54', 67, NULL, NULL, 'IPAY/2016/04/0125', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '0.0000', 0),
(65, '2016-04-15 13:13:06', 68, NULL, NULL, 'IPAY/2016/04/0127', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(66, '2016-04-15 13:28:11', 69, NULL, NULL, 'IPAY/2016/04/0129', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(67, '2016-04-15 13:30:06', 70, NULL, NULL, 'IPAY/2016/04/0131', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(68, '2016-04-15 13:46:29', 71, NULL, NULL, 'IPAY/2016/04/0133', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(69, '2016-04-15 13:52:16', 72, NULL, NULL, 'IPAY/2016/04/0135', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(70, '2016-04-15 14:11:18', 73, NULL, NULL, 'IPAY/2016/04/0137', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(71, '2016-04-15 14:35:03', 74, NULL, NULL, 'IPAY/2016/04/0139', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '50000.0000', '47500.0000', 0),
(72, '2016-04-15 15:06:28', 75, NULL, NULL, 'IPAY/2016/04/0141', NULL, 'credit_voucher', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(73, '2016-04-15 15:12:43', 76, NULL, NULL, 'IPAY/2016/04/0143', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '47500.0000', '45000.0000', 0),
(74, '2016-04-15 15:16:12', 77, NULL, NULL, 'IPAY/2016/04/0145', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(75, '2016-04-15 15:17:20', 78, NULL, NULL, 'IPAY/2016/04/0147', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(76, '2016-04-16 09:17:56', 79, NULL, NULL, 'IPAY/2016/04/0149', NULL, 'credit_voucher', '', '1234', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '45000.0000', '42500.0000', 0),
(77, '2016-04-16 09:18:53', 80, NULL, NULL, 'IPAY/2016/04/0151', NULL, 'cash', '', '', '', '', '', '', '', '4000.0000', NULL, 5, NULL, 'received', '', '4000.0000', '0.0000', 0),
(78, '2016-04-16 12:31:09', 81, NULL, NULL, 'IPAY/2016/04/0153', NULL, 'cash', '', '', '', '', '', '', '', '1500.0000', NULL, 1, NULL, 'received', '', '1500.0000', '0.0000', 0),
(79, '2016-04-18 11:28:13', 82, NULL, NULL, 'IPAY/2016/04/0155', NULL, 'credit_voucher', '', '9712360760817780', '', '', '', '', '', '7500.0000', NULL, 5, NULL, 'received', '', '50000.0000', '42500.0000', 0),
(80, '2016-04-18 11:30:25', 83, NULL, NULL, 'IPAY/2016/04/0157', NULL, 'credit_voucher', '', '9712360760817780', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '42500.0000', '37500.0000', 0),
(81, '2016-04-19 12:42:29', 84, NULL, NULL, 'IPAY/2016/04/0159', NULL, 'credit_voucher', '', '6555590703672015', '', '', '', '', '', '4978.0000', NULL, 5, NULL, 'received', '', '50000.0000', '45022.0000', 0),
(82, '2016-04-20 09:27:53', 85, NULL, NULL, 'IPAY/2016/04/0161', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(83, '2016-04-20 11:47:58', 86, NULL, NULL, 'IPAY/2016/04/0163', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(84, '2016-04-20 11:54:48', 87, NULL, NULL, 'IPAY/2016/04/0165', NULL, 'CC', '', '21321321312321323121', 'dssadasddasdasd', '12', '2016', '', '', '2385.0000', NULL, 1, NULL, 'received', '', '2385.0000', '0.0000', 0),
(85, '2016-04-20 12:57:03', 88, NULL, NULL, 'IPAY/2016/04/0167', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(86, '2016-04-20 12:58:06', 89, NULL, NULL, 'IPAY/2016/04/0169', NULL, 'cash', '', '', '', '', '', '', '', '3999.0000', NULL, 2, NULL, 'received', '', '3999.0000', '0.0000', 0),
(87, '2016-04-25 09:35:46', 91, NULL, NULL, 'IPAY/2016/04/0171', NULL, 'cash', '', '', '', '', '', '', '', '21000.0000', NULL, 3, NULL, 'received', '', '21000.0000', '0.0000', 0),
(88, '2016-04-25 11:09:22', 92, NULL, NULL, 'IPAY/2016/04/0173', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 3, NULL, 'received', '', '24000.0000', '0.0000', 0),
(89, '2016-04-25 11:44:45', 93, NULL, NULL, 'IPAY/2016/04/0175', NULL, 'cash', '', '', '', '', '', '', '', '36000.0000', NULL, 5, NULL, 'received', '', '36000.0000', '0.0000', 0),
(90, '2016-04-25 12:09:43', 94, NULL, NULL, 'IPAY/2016/04/0177', NULL, 'cash', '', '', '', '', '', '', '', '60000.0000', NULL, 5, NULL, 'received', '', '60000.0000', '0.0000', 0),
(91, '2016-04-25 12:20:30', 95, NULL, NULL, 'IPAY/2016/04/0179', NULL, 'cash', '', '', '', '', '', '', '', '60000.0000', NULL, 5, NULL, 'received', '', '120000.0000', '60000.0000', 0),
(92, '2016-04-25 12:57:30', 96, NULL, NULL, 'IPAY/2016/04/0181', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 5, NULL, 'received', '', '110000.0000', '0.0000', 0),
(93, '2016-04-25 13:11:09', 97, NULL, NULL, 'IPAY/2016/04/0183', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 5, NULL, 'received', '', '110000.0000', '0.0000', 0),
(94, '2016-04-25 13:27:24', 98, NULL, NULL, 'IPAY/2016/04/0185', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 5, NULL, 'received', '', '12000.0000', '0.0000', 0),
(95, '2016-04-25 13:28:07', 99, NULL, NULL, 'IPAY/2016/04/0187', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 5, NULL, 'received', '', '110000.0000', '0.0000', 0),
(96, '2016-04-25 14:24:10', 100, NULL, NULL, 'IPAY/2016/04/0189', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 1, NULL, 'received', '', '110000.0000', '0.0000', 0),
(97, '2016-04-25 14:28:13', 101, NULL, NULL, 'IPAY/2016/04/0191', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(98, '2016-04-25 14:29:13', 102, NULL, NULL, 'IPAY/2016/04/0193', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 1, NULL, 'received', '', '110000.0000', '0.0000', 0),
(99, '2016-04-25 15:08:05', 103, NULL, NULL, 'IPAY/2016/04/0195', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(100, '2016-04-25 15:09:47', 104, NULL, NULL, 'IPAY/2016/04/0197', NULL, 'cash', '', '', '', '', '', '', '', '122000.0000', NULL, 1, NULL, 'received', '', '122000.0000', '0.0000', 0),
(101, '2016-04-25 15:24:23', 105, NULL, NULL, 'IPAY/2016/04/0199', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(102, '2016-04-25 15:33:09', 107, NULL, NULL, 'IPAY/2016/04/0201', NULL, 'cash', '', '', '', '', '', '', '', '110000.0000', NULL, 1, NULL, 'received', '', '110000.0000', '0.0000', 0),
(103, '2016-04-25 15:33:36', 108, NULL, NULL, 'IPAY/2016/04/0203', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(104, '2016-04-26 08:35:41', 109, NULL, NULL, 'IPAY/2016/04/0205', NULL, 'cash', '', '', '', '', '', '', '', '11700.0000', NULL, 1, NULL, 'received', '', '11700.0000', '0.0000', 0),
(105, '2016-04-30 10:09:14', 110, NULL, NULL, 'IPAY/2016/04/0207', NULL, 'cash', '', '', '', '', '', '', '', '27240.0000', NULL, 1, NULL, 'received', '', '27240.0000', '0.0000', 0),
(106, '2016-05-02 08:48:31', 111, NULL, NULL, 'IPAY/2016/05/0209', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(107, '2016-05-02 06:21:00', 111, 2, NULL, 'IPAY/2016/05/0211', NULL, 'cash', '', '', '', '', '', 'Visa', '', '12000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(108, '2016-05-02 11:16:46', 112, NULL, NULL, 'IPAY/2016/05/0211', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(109, '2016-05-02 11:30:28', 113, NULL, NULL, 'IPAY/2016/05/0213', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(110, '2016-05-02 12:19:32', 114, NULL, NULL, 'IPAY/2016/05/0215', NULL, 'cash', '', '', '', '', '', '', '', '11500.0000', NULL, 1, NULL, 'received', '', '11500.0000', '0.0000', 0),
(111, '2016-05-05 11:16:12', 115, NULL, NULL, 'IPAY/2016/05/0221', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(112, '2016-05-05 12:12:48', 116, NULL, NULL, 'IPAY/2016/05/0223', NULL, 'credit_voucher', '', '4776837744996877', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '50000.0000', '38000.0000', 0),
(113, '2016-05-05 12:29:07', 117, NULL, NULL, 'IPAY/2016/05/0225', NULL, 'credit_voucher', '', '4776837744996877', '', '', '', '', '', '12000.0000', NULL, 2, NULL, 'received', '', '38000.0000', '26000.0000', 0),
(114, '2016-05-05 14:37:37', 118, NULL, NULL, 'IPAY/2016/05/0227', NULL, 'credit_voucher', '', '4776837744996877', '', '', '', '', '', '12000.0000', NULL, 2, NULL, 'received', '', '12000.0000', '0.0000', 0),
(115, '2016-05-05 15:42:11', 119, NULL, NULL, 'IPAY/2016/05/0229', NULL, 'CC', '', '411111111111111', 'rahul', '3', '2017', '', '', '12000.0000', NULL, 2, NULL, 'received', '', '12000.0000', '0.0000', 0),
(116, '2016-05-06 12:13:50', 120, NULL, NULL, 'IPAY/2016/05/0231', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(117, '2016-05-06 12:33:47', 121, NULL, NULL, 'IPAY/2016/05/0233', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(118, '2016-05-06 12:38:20', 122, NULL, NULL, 'IPAY/2016/05/0235', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 2, NULL, 'received', '', '12000.0000', '0.0000', 0),
(119, '2016-05-09 12:04:02', 124, NULL, NULL, 'IPAY/2016/05/0241', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(120, '2016-05-09 12:05:13', 125, NULL, NULL, 'IPAY/2016/05/0243', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(121, '2016-05-10 15:31:02', 126, NULL, NULL, 'IPAY/2016/05/0245', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(122, '2016-05-11 07:25:16', 127, NULL, NULL, 'IPAY/2016/05/0247', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(123, '2016-05-11 12:58:32', 128, NULL, NULL, 'IPAY/2016/05/0249', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(124, '2016-05-11 13:16:46', 129, NULL, NULL, 'IPAY/2016/05/0251', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(125, '2016-05-11 13:21:12', 130, NULL, NULL, 'IPAY/2016/05/0253', NULL, 'cash', '', '', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(126, '2016-05-13 12:46:17', 131, NULL, NULL, 'IPAY/2016/05/0255', NULL, 'cash', '', '', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(127, '2016-05-13 12:47:11', 132, NULL, NULL, 'IPAY/2016/05/0257', NULL, 'cash', '', '', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(128, '2016-05-13 14:01:01', 135, NULL, NULL, 'IPAY/2016/05/0260', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 5, NULL, 'received', '', '15000.0000', '-40000.0000', 0),
(129, '2016-05-13 14:55:41', 136, NULL, NULL, 'IPAY/2016/05/0265', NULL, 'credit_voucher', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-5000.0000', 0),
(130, '2016-05-13 14:59:32', 137, NULL, NULL, 'IPAY/2016/05/0267', NULL, 'credit_voucher', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-5000.0000', 0),
(131, '2016-05-17 09:08:01', 138, NULL, NULL, 'IPAY/2016/05/0269', NULL, 'cash', '', '', '', '', '', '', '', '9800.0000', NULL, 1, NULL, 'received', '', '9800.0000', '0.0000', 0),
(132, '2016-05-17 09:13:00', 139, NULL, NULL, 'IPAY/2016/05/0271', NULL, 'cash', '', '', '', '', '', '', '', '10704.0000', NULL, 1, NULL, 'received', '', '10704.0000', '0.0000', 0),
(133, '2016-05-17 09:31:06', 140, NULL, NULL, 'IPAY/2016/05/0273', NULL, 'cash', '', '', '', '', '', '', '', '22000.0000', NULL, 1, NULL, 'received', '', '22000.0000', '0.0000', 0),
(134, '2016-05-17 13:48:53', 141, NULL, NULL, 'IPAY/2016/05/0275', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(135, '2016-05-17 14:55:36', 142, NULL, NULL, 'IPAY/2016/05/0277', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(136, '2016-05-18 08:08:11', 143, NULL, NULL, 'IPAY/2016/05/0280', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(137, '2016-05-18 08:50:22', 144, NULL, NULL, 'IPAY/2016/05/0282', NULL, 'credit_voucher', '', '4776837744996877', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(138, '2016-05-19 07:21:00', 109, 3, NULL, 'IPAY/2016/05/0284', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '11700.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(139, '2016-05-19 07:25:00', 105, 4, NULL, 'IPAY/2016/05/0284', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '12000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(140, '2016-05-19 13:16:12', 145, NULL, NULL, 'IPAY/2016/05/0284', NULL, 'credit_voucher', '', '3960106333476215', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(141, '2016-05-19 13:18:59', 146, NULL, NULL, 'IPAY/2016/05/0286', NULL, 'credit_voucher', '', '3960106333476215', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(142, '2016-05-19 13:28:33', 148, NULL, NULL, 'IPAY/2016/05/0288', NULL, 'credit_voucher', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-7000.0000', 0),
(143, '2016-05-19 13:56:00', 149, NULL, NULL, 'IPAY/2016/05/0290', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', '', '5000.0000', '-7000.0000', 0),
(144, '2016-05-19 13:56:00', 149, NULL, NULL, 'IPAY/2016/05/0291', NULL, 'credit_voucher', '', '', '', '', '', '', '', '7000.0000', NULL, 5, NULL, 'received', '', '7000.0000', '0.0000', 0),
(145, '2016-05-19 14:34:08', 150, NULL, NULL, 'IPAY/2016/05/0294', NULL, 'CC', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '0.0000', 0),
(146, '2016-05-19 14:34:08', 150, NULL, NULL, 'IPAY/2016/05/0295', NULL, 'cash', '', '', '', '', '', '', '', '50000.0000', NULL, 1, NULL, 'received', '', '50000.0000', '0.0000', 0),
(147, '2016-05-20 09:13:00', 112, 5, NULL, 'IPAY/2016/05/0298', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '12000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(148, '2016-05-20 09:16:00', 114, 6, NULL, 'IPAY/2016/05/0298', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '11000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(149, '2016-05-20 09:28:00', 108, 7, NULL, 'IPAY/2016/05/0298', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '24000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(150, '2016-05-20 12:58:39', 151, NULL, NULL, 'IPAY/2016/05/0298', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(151, '2016-05-20 10:36:00', 80, 8, NULL, 'IPAY/2016/05/0300', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '4000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(152, '2016-05-20 10:49:00', 98, 9, NULL, 'IPAY/2016/05/0300', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '12000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(153, '2016-05-20 10:56:00', 100, 10, NULL, 'IPAY/2016/05/0300', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '110000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(154, '2016-05-20 11:44:00', 74, 11, NULL, 'IPAY/2016/05/0300', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '2500.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(155, '2016-05-20 11:49:00', 101, 12, NULL, 'IPAY/2016/05/0300', NULL, 'gift_card', '', '', '', '', '', 'Visa', '', '12000.0000', NULL, 1, NULL, 'returned', NULL, '0.0000', '0.0000', 0),
(156, '2016-05-21 09:11:02', 154, NULL, NULL, 'IPAY/2016/05/0302', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(157, '2016-05-21 09:13:59', 155, NULL, NULL, 'IPAY/2016/05/0304', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(158, '2016-05-21 09:19:04', 157, NULL, NULL, 'IPAY/2016/05/0307', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(159, '2016-05-21 09:23:23', 158, NULL, NULL, 'IPAY/2016/05/0309', NULL, 'cash', '', '', '', '', '', '', '', '60000.0000', NULL, 1, NULL, 'received', '', '60000.0000', '0.0000', 0),
(160, '2016-05-21 09:24:34', 159, NULL, NULL, 'IPAY/2016/05/0311', NULL, 'cash', '', '', '', '', '', '', '', '72000.0000', NULL, 1, NULL, 'received', '', '72000.0000', '0.0000', 0),
(161, '2016-05-21 09:26:02', 160, NULL, NULL, 'IPAY/2016/05/0313', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(162, '2016-05-21 09:42:38', 161, NULL, NULL, 'IPAY/2016/05/0318', NULL, 'cash', '', '', '', '', '', '', '', '60000.0000', NULL, 1, NULL, 'received', '', '60000.0000', '0.0000', 0),
(163, '2016-05-21 09:43:55', 162, NULL, NULL, 'IPAY/2016/05/0320', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(164, '2016-05-21 09:47:41', 163, NULL, NULL, 'IPAY/2016/05/0322', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(165, '2016-05-21 09:53:55', 164, NULL, NULL, 'IPAY/2016/05/0324', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 5, NULL, 'received', '', '12000.0000', '0.0000', 0),
(166, '2016-05-21 10:01:25', 167, NULL, NULL, 'IPAY/2016/05/0327', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 4, NULL, 'received', '', '12000.0000', '0.0000', 0),
(167, '2016-05-21 10:04:44', 168, NULL, NULL, 'IPAY/2016/05/0329', NULL, 'cash', '', '', '', '', '', '', '', '36000.0000', NULL, 1, NULL, 'received', '', '36000.0000', '0.0000', 0),
(168, '2016-05-21 10:05:49', 169, NULL, NULL, 'IPAY/2016/05/0331', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(169, '2016-05-21 10:07:06', 170, NULL, NULL, 'IPAY/2016/05/0333', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(170, '2016-05-21 10:15:26', 172, NULL, NULL, 'IPAY/2016/05/0336', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(171, '2016-05-21 10:20:08', 174, NULL, NULL, 'IPAY/2016/05/0339', NULL, 'cash', '', '', '', '', '', '', '', '24000.0000', NULL, 1, NULL, 'received', '', '24000.0000', '0.0000', 0),
(172, '2016-05-21 11:07:29', 176, NULL, NULL, 'IPAY/2016/05/0343', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(173, '2016-05-21 12:43:55', 177, NULL, NULL, 'IPAY/2016/05/0347', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(174, '2016-05-21 12:44:41', 178, NULL, NULL, 'IPAY/2016/05/0349', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 1, NULL, 'received', '', '4500.0000', '0.0000', 0),
(175, '2016-05-21 13:37:21', 179, NULL, NULL, 'IPAY/2016/05/0351', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(176, '2016-05-21 13:39:36', 180, NULL, NULL, 'IPAY/2016/05/0353', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(177, '2016-05-21 13:43:22', 181, NULL, NULL, 'IPAY/2016/05/0355', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(178, '2016-05-21 13:49:00', 182, NULL, NULL, 'IPAY/2016/05/0358', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(179, '2016-05-21 13:52:44', 183, NULL, NULL, 'IPAY/2016/05/0360', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(180, '2016-05-21 13:58:36', 185, NULL, NULL, 'IPAY/2016/05/0364', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(181, '2016-05-21 14:00:56', 186, NULL, NULL, 'IPAY/2016/05/0366', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(182, '2016-05-21 14:03:21', 187, NULL, NULL, 'IPAY/2016/05/0368', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(183, '2016-05-21 14:05:46', 188, NULL, NULL, 'IPAY/2016/05/0370', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(184, '2016-05-21 14:06:59', 189, NULL, NULL, 'IPAY/2016/05/0372', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(185, '2016-05-21 14:08:41', 190, NULL, NULL, 'IPAY/2016/05/0374', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(186, '2016-05-21 14:11:40', 191, NULL, NULL, 'IPAY/2016/05/0376', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(187, '2016-05-21 14:11:41', 192, NULL, NULL, 'IPAY/2016/05/0378', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(188, '2016-05-21 14:29:40', 193, NULL, NULL, 'IPAY/2016/05/0380', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(189, '2016-05-21 14:36:49', 194, NULL, NULL, 'IPAY/2016/05/0384', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(190, '2016-05-21 14:38:24', 195, NULL, NULL, 'IPAY/2016/05/0386', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(191, '2016-05-21 14:41:23', 196, NULL, NULL, 'IPAY/2016/05/0388', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(192, '2016-05-21 14:42:16', 197, NULL, NULL, 'IPAY/2016/05/0390', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(193, '2016-05-21 14:57:17', 200, NULL, NULL, 'IPAY/2016/05/0394', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(194, '2016-05-21 15:19:29', 204, NULL, NULL, 'IPAY/2016/05/0404', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(195, '2016-05-21 15:24:14', 206, NULL, NULL, 'IPAY/2016/05/0407', NULL, 'cash', '', '', '', '', '', '', '', '22000.0000', NULL, 1, NULL, 'received', '', '22000.0000', '0.0000', 0),
(196, '2016-05-21 15:34:58', 208, NULL, NULL, 'IPAY/2016/05/0410', NULL, 'cash', '', '', '', '', '', '', '', '55000.0000', NULL, 1, NULL, 'received', '', '55000.0000', '0.0000', 0),
(197, '2016-05-21 15:36:05', 209, NULL, NULL, 'IPAY/2016/05/0412', NULL, 'cash', '', '', '', '', '', '', '', '49500.0000', NULL, 1, NULL, 'received', '', '49500.0000', '0.0000', 0),
(198, '2016-05-21 16:04:05', 213, NULL, NULL, 'IPAY/2016/05/0416', NULL, 'cash', '', '', '', '', '', '', '', '49500.0000', NULL, 1, NULL, 'received', '', '49500.0000', '0.0000', 0),
(199, '2016-05-21 16:53:31', 221, NULL, NULL, 'IPAY/2016/05/0425', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(200, '2016-05-21 16:55:41', 222, NULL, NULL, 'IPAY/2016/05/0427', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(201, '2016-05-21 16:58:05', 223, NULL, NULL, 'IPAY/2016/05/0429', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 5, NULL, 'received', '', '11000.0000', '0.0000', 0),
(202, '2016-05-23 07:21:38', 224, NULL, NULL, 'IPAY/2016/05/0431', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(203, '2016-05-23 08:03:34', 229, NULL, NULL, 'IPAY/2016/05/0437', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(204, '2016-05-23 08:04:09', 230, NULL, NULL, 'IPAY/2016/05/0439', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 1, NULL, 'received', '', '15000.0000', '0.0000', 0),
(205, '2016-05-23 08:08:36', 231, NULL, NULL, 'IPAY/2016/05/0441', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(206, '2016-05-23 08:12:40', 233, NULL, NULL, 'IPAY/2016/05/0444', NULL, 'cash', '', '', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(207, '2016-05-23 09:01:04', 240, NULL, NULL, 'IPAY/2016/05/0465', NULL, 'cash', '', '', '', '', '', '', '', '7500.0000', NULL, 1, NULL, 'received', '', '7500.0000', '0.0000', 0),
(208, '2016-05-23 09:15:10', 242, NULL, NULL, 'IPAY/2016/05/0469', NULL, 'cash', '', '', '', '', '', '', '', '10000.0000', NULL, 1, NULL, 'received', '', '10000.0000', '0.0000', 0),
(209, '2016-05-23 09:16:36', 243, NULL, NULL, 'IPAY/2016/05/0471', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(210, '2016-05-23 09:19:24', 244, NULL, NULL, 'IPAY/2016/05/0473', NULL, 'cash', '', '', '', '', '', '', '', '25000.0000', NULL, 1, NULL, 'received', '', '25000.0000', '0.0000', 0),
(211, '2016-05-23 09:29:30', 245, NULL, NULL, 'IPAY/2016/05/0475', NULL, 'cash', '', '', '', '', '', '', '', '15000.0000', NULL, 1, NULL, 'received', '', '15000.0000', '0.0000', 0),
(212, '2016-05-23 09:55:47', 247, NULL, NULL, 'IPAY/2016/05/0478', NULL, 'cash', '', '', '', '', '', '', '', '12500.0000', NULL, 1, NULL, 'received', '', '12500.0000', '0.0000', 0),
(213, '2016-05-23 09:57:29', 248, NULL, NULL, 'IPAY/2016/05/0480', NULL, 'cash', '', '', '', '', '', '', '', '25000.0000', NULL, 1, NULL, 'received', '', '25000.0000', '0.0000', 0),
(214, '2016-05-23 10:06:23', 251, NULL, NULL, 'IPAY/2016/05/0484', NULL, 'cash', '', '', '', '', '', '', '', '16000.0000', NULL, 1, NULL, 'received', '', '16000.0000', '0.0000', 0),
(215, '2016-05-23 10:08:31', 252, NULL, NULL, 'IPAY/2016/05/0486', NULL, 'cash', '', '', '', '', '', '', '', '15500.0000', NULL, 1, NULL, 'received', '', '15500.0000', '0.0000', 0),
(216, '2016-05-23 11:47:30', 253, NULL, NULL, 'IPAY/2016/05/0488', NULL, 'cash', '', '', '', '', '', '', '', '13500.0000', NULL, 2, NULL, 'received', '', '13500.0000', '0.0000', 0),
(217, '2016-05-23 11:48:21', 254, NULL, NULL, 'IPAY/2016/05/0490', NULL, 'cash', '', '', '', '', '', '', '', '24500.0000', NULL, 4, NULL, 'received', '', '24500.0000', '0.0000', 0),
(218, '2016-05-23 11:50:12', 255, NULL, NULL, 'IPAY/2016/05/0492', NULL, 'cash', '', '', '', '', '', '', '', '52000.0000', NULL, 5, NULL, 'received', '', '52000.0000', '0.0000', 0),
(219, '2016-05-23 11:51:24', 256, NULL, NULL, 'IPAY/2016/05/0494', NULL, 'cash', '', '', '', '', '', '', '', '21000.0000', NULL, 4, NULL, 'received', '', '21000.0000', '0.0000', 0),
(220, '2016-05-23 11:53:49', 257, NULL, NULL, 'IPAY/2016/05/0496', NULL, 'cash', '', '', '', '', '', '', '', '16000.0000', NULL, 1, NULL, 'received', '', '16000.0000', '0.0000', 0),
(221, '2016-05-23 12:00:08', 258, NULL, NULL, 'IPAY/2016/05/0498', NULL, 'cash', '', '', '', '', '', '', '', '22000.0000', NULL, 2, NULL, 'received', '', '22000.0000', '0.0000', 0),
(222, '2016-05-23 12:03:51', 260, NULL, NULL, 'IPAY/2016/05/0501', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(223, '2016-05-23 12:05:06', 261, NULL, NULL, 'IPAY/2016/05/0503', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 0),
(224, '2016-05-23 12:24:19', 262, NULL, NULL, 'IPAY/2016/05/0505', NULL, 'cash', '', '', '', '', '', '', '', '8000.0000', NULL, 4, NULL, 'received', '', '8000.0000', '0.0000', 0),
(225, '2016-05-23 12:30:59', 263, NULL, NULL, 'IPAY/2016/05/0507', NULL, 'cash', '', '', '', '', '', '', '', '8000.0000', NULL, 1, NULL, 'received', '', '8000.0000', '0.0000', 0),
(226, '2016-05-23 12:32:20', 264, NULL, NULL, 'IPAY/2016/05/0509', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 1, NULL, 'received', '', '4500.0000', '0.0000', 0),
(227, '2016-05-23 12:32:55', 265, NULL, NULL, 'IPAY/2016/05/0511', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(228, '2016-05-23 12:34:32', 266, NULL, NULL, 'IPAY/2016/05/0513', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 5, NULL, 'received', '', '2000.0000', '0.0000', 0),
(229, '2016-05-23 12:37:30', 267, NULL, NULL, 'IPAY/2016/05/0515', NULL, 'cash', '', '', '', '', '', '', '', '2250.0000', NULL, 1, NULL, 'received', '', '2250.0000', '0.0000', 0),
(230, '2016-05-23 12:38:25', 268, NULL, NULL, 'IPAY/2016/05/0517', NULL, 'cash', '', '', '', '', '', '', '', '2300.0000', NULL, 5, NULL, 'received', '', '2300.0000', '0.0000', 0),
(231, '2016-05-23 12:41:04', 269, NULL, NULL, 'IPAY/2016/05/0519', NULL, 'cash', '', '', '', '', '', '', '', '9500.0000', NULL, 2, NULL, 'received', '', '9500.0000', '0.0000', 0),
(232, '2016-05-23 14:12:38', 270, NULL, NULL, 'IPAY/2016/05/0521', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-4000.0000', 0),
(233, '2016-05-23 14:12:38', 270, NULL, NULL, 'IPAY/2016/05/0522', NULL, 'credit_voucher', '', '', '', '', '', '', '', '500.0000', NULL, 1, NULL, 'received', '', '500.0000', '0.0000', 0),
(234, '2016-05-23 14:12:38', 270, NULL, NULL, 'IPAY/2016/05/0523', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-3000.0000', 0),
(235, '2016-05-23 14:12:38', 270, NULL, NULL, 'IPAY/2016/05/0524', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-2000.0000', 0),
(236, '2016-05-23 14:12:38', 270, NULL, NULL, 'IPAY/2016/05/0525', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 1, NULL, 'received', '', '2000.0000', '0.0000', 0),
(237, '2016-05-24 07:09:27', 271, NULL, NULL, 'IPAY/2016/05/0531', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 0),
(238, '2016-05-24 07:37:41', 272, NULL, NULL, 'IPAY/2016/05/0533', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 0),
(239, '2016-05-24 12:27:31', 273, NULL, NULL, 'IPAY/2016/05/0535', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(240, '2016-05-24 12:28:27', 274, NULL, NULL, 'IPAY/2016/05/0537', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 0),
(241, '2016-05-24 12:29:25', 275, NULL, NULL, 'IPAY/2016/05/0539', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 0),
(242, '2016-05-24 15:19:11', 276, NULL, NULL, 'IPAY/2016/05/0541', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-7000.0000', 0),
(243, '2016-05-24 15:19:11', 276, NULL, NULL, 'IPAY/2016/05/0542', NULL, 'CC', '', '', '', '', '', '', '', '7000.0000', NULL, 1, NULL, 'received', '', '7000.0000', '0.0000', 0),
(244, '2016-05-24 15:20:52', 277, NULL, NULL, 'IPAY/2016/05/0545', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(245, '2016-05-24 15:23:01', 278, NULL, NULL, 'IPAY/2016/05/0547', NULL, 'CC', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(246, '2016-05-25 07:33:23', 279, NULL, NULL, 'IPAY/2016/05/0550', NULL, 'cash', '', '', '', '', '', '', '', '16000.0000', NULL, 1, NULL, 'received', '', '16000.0000', '0.0000', 0),
(247, '2016-05-25 07:38:28', 280, NULL, NULL, 'IPAY/2016/05/0552', NULL, 'cash', '', '', '', '', '', '', '', '19500.0000', NULL, 1, NULL, 'received', 'sale', '19500.0000', '0.0000', 0),
(248, '2016-05-25 08:19:13', 281, NULL, NULL, 'IPAY/2016/05/0554', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', 'ewewew', '5000.0000', '-9500.0000', 0),
(249, '2016-05-25 08:19:13', 281, NULL, NULL, 'IPAY/2016/05/0555', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-4500.0000', 0),
(250, '2016-05-25 08:19:13', 281, NULL, NULL, 'IPAY/2016/05/0556', NULL, 'CC', '', '', '', '', '', '', '', '4500.0000', NULL, 1, NULL, 'received', '', '4500.0000', '0.0000', 0),
(251, '2016-05-25 08:22:38', 282, NULL, NULL, 'IPAY/2016/05/0560', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', 'wqeqe', '5000.0000', '-7000.0000', 0),
(252, '2016-05-25 08:22:38', 282, NULL, NULL, 'IPAY/2016/05/0561', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-6000.0000', 0),
(253, '2016-05-25 08:22:38', 282, NULL, NULL, 'IPAY/2016/05/0562', NULL, 'CC', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-1000.0000', 0),
(254, '2016-05-25 08:22:38', 282, NULL, NULL, 'IPAY/2016/05/0563', NULL, 'CC', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '0.0000', 0),
(255, '2016-05-25 08:31:56', 283, NULL, NULL, 'IPAY/2016/05/0568', NULL, 'cash', '', '', '', '', '', '', '', '17300.0000', NULL, 1, NULL, 'received', '', '17300.0000', '0.0000', 0),
(256, '2016-05-25 09:03:32', 284, NULL, NULL, 'IPAY/2016/05/0570', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(257, '2016-05-25 09:54:15', 285, NULL, NULL, 'IPAY/2016/05/0572', NULL, 'cash', '', '', '', '', '', '', '', '12000.0000', NULL, 1, NULL, 'received', '', '12000.0000', '0.0000', 0),
(258, '2016-05-25 09:55:26', 286, NULL, NULL, 'IPAY/2016/05/0574', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-7000.0000', 0),
(259, '2016-05-25 09:55:26', 286, NULL, NULL, 'IPAY/2016/05/0575', NULL, 'CC', '', '', '', '', '', '', '', '3000.0000', NULL, 1, NULL, 'received', '', '3000.0000', '-4000.0000', 0),
(260, '2016-05-25 09:55:26', 286, NULL, NULL, 'IPAY/2016/05/0576', NULL, 'CC', '', '', '', '', '', '', '', '4000.0000', NULL, 1, NULL, 'received', '', '4000.0000', '0.0000', 0),
(261, '2016-05-25 09:57:57', 287, NULL, NULL, 'IPAY/2016/05/0580', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-11000.0000', 0),
(262, '2016-05-25 09:57:57', 287, NULL, NULL, 'IPAY/2016/05/0581', NULL, 'CC', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '-6000.0000', 0),
(263, '2016-05-25 09:57:57', 287, NULL, NULL, 'IPAY/2016/05/0582', NULL, 'CC', '', '', '', '', '', '', '', '6000.0000', NULL, 1, NULL, 'received', '', '6000.0000', '0.0000', 0),
(264, '2016-05-25 10:03:52', 288, NULL, NULL, 'IPAY/2016/05/0586', NULL, 'cash', '', '', '', '', '', '', '', '17000.0000', NULL, 2, NULL, 'received', '', '17000.0000', '0.0000', 0),
(265, '2016-05-25 12:59:25', 289, NULL, NULL, 'IPAY/2016/05/0588', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 0),
(266, '2016-05-25 11:17:47', 290, NULL, NULL, 'IPAY/2016/05/0590', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4000.0000', 0),
(267, '2016-05-25 11:17:47', 290, NULL, NULL, 'IPAY/2016/05/0591', NULL, 'CC', '', '', '', '', '', '', '', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '-2000.0000', 0),
(268, '2016-05-25 11:17:47', 290, NULL, NULL, 'IPAY/2016/05/0592', NULL, 'CC', '', '', '', '', '', '', '', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 0),
(269, '2016-05-25 11:22:42', 291, NULL, NULL, 'IPAY/2016/05/0596', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '-4500.0000', 0),
(270, '2016-05-25 11:22:42', 291, NULL, NULL, 'IPAY/2016/05/0597', NULL, 'CC', '', '', '', '', '', '', '', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '-3000.0000', 0),
(271, '2016-05-25 11:22:42', 291, NULL, NULL, 'IPAY/2016/05/0598', NULL, 'CC', '', '', '', '', '', '', '', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '-1500.0000', 0),
(272, '2016-05-25 11:22:42', 291, NULL, NULL, 'IPAY/2016/05/0599', NULL, 'CC', '', '', '', '', '', '', '', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '0.0000', 0),
(273, '2016-05-25 11:34:35', 292, NULL, NULL, 'IPAY/2016/05/0604', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 0);
INSERT INTO `sma_payments` (`id`, `date`, `sale_id`, `return_id`, `purchase_id`, `reference_no`, `transaction_id`, `paid_by`, `cheque_no`, `cc_no`, `cc_holder`, `cc_month`, `cc_year`, `cc_type`, `card_type`, `amount`, `currency`, `created_by`, `attachment`, `type`, `note`, `pos_paid`, `pos_balance`, `register_id`) VALUES
(274, '2016-05-25 14:28:40', 293, NULL, NULL, 'IPAY/2016/05/0606', NULL, 'cash', '', '', '', '', '', '', '', '9000.0000', NULL, 1, NULL, 'received', '', '9000.0000', '0.0000', 42),
(275, '2016-05-25 14:30:12', 294, NULL, NULL, 'IPAY/2016/05/0608', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-3500.0000', 42),
(276, '2016-05-25 14:30:12', 294, NULL, NULL, 'IPAY/2016/05/0609', NULL, 'CC', '', '', '', '', '', '', '', '2000.0000', NULL, 1, NULL, 'received', '', '2000.0000', '-1500.0000', 42),
(277, '2016-05-25 14:30:12', 294, NULL, NULL, 'IPAY/2016/05/0610', NULL, 'CC', '', '', '', '', '', '', '', '1500.0000', NULL, 1, NULL, 'received', '', '1500.0000', '0.0000', 42),
(278, '2016-05-30 16:02:47', 295, NULL, NULL, 'IPAY/2016/05/0614', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 41),
(279, '2016-05-31 14:38:06', 296, NULL, NULL, 'IPAY/2016/05/0616', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', 'df', '5500.0000', '0.0000', 42),
(280, '2016-05-31 14:41:47', 297, NULL, NULL, 'IPAY/2016/05/0618', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 5, NULL, 'received', '', '4500.0000', '0.0000', 31),
(281, '2016-05-31 15:12:07', 298, NULL, NULL, 'IPAY/2016/05/0631', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 5, NULL, 'received', 'jjuuii', '4500.0000', '0.0000', 31),
(282, '2016-06-01 07:13:46', 299, NULL, NULL, 'IPAY/2016/06/0633', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 1, NULL, 'received', '', '4500.0000', '0.0000', 42),
(283, '2016-06-01 07:15:02', 300, NULL, NULL, 'IPAY/2016/06/0635', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 5, NULL, 'received', '', '4500.0000', '0.0000', 31),
(284, '2016-06-01 08:31:00', 301, NULL, NULL, 'IPAY/2016/06/0637', NULL, 'cash', '', '', '', '', '', '', '', '9000.0000', NULL, 1, NULL, 'received', '', '9000.0000', '0.0000', 42),
(285, '2016-06-01 08:38:07', 302, NULL, NULL, 'IPAY/2016/06/0640', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 42),
(286, '2016-06-01 09:53:47', 303, NULL, NULL, 'IPAY/2016/06/0644', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(287, '2016-06-01 10:57:34', 304, NULL, NULL, 'IPAY/2016/06/0647', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(288, '2016-06-01 10:59:11', 305, NULL, NULL, 'IPAY/2016/06/0649', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(289, '2016-06-01 11:01:11', 306, NULL, NULL, 'IPAY/2016/06/0651', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(290, '2016-06-01 11:07:31', 307, NULL, NULL, 'IPAY/2016/06/0653', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(291, '2016-06-01 11:08:43', 308, NULL, NULL, 'IPAY/2016/06/0655', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', 'dsfdsfds', '2500.0000', '0.0000', 42),
(292, '2016-06-01 11:36:07', 309, NULL, NULL, 'IPAY/2016/06/0657', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(293, '2016-06-01 11:42:16', 310, NULL, NULL, 'IPAY/2016/06/0659', NULL, 'cash', '', '', '', '', '', '', '', '4500.0000', NULL, 5, NULL, 'received', '', '4500.0000', '0.0000', 31),
(294, '2016-06-01 11:56:45', 311, NULL, NULL, 'IPAY/2016/06/0661', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 5, NULL, 'received', '', '2500.0000', '0.0000', 31),
(295, '2016-06-01 11:59:11', 312, NULL, NULL, 'IPAY/2016/06/0663', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 4, NULL, 'received', '', '2500.0000', '0.0000', 33),
(296, '2016-06-01 14:15:40', 313, NULL, NULL, 'IPAY/2016/06/0665', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 42),
(297, '2016-06-01 14:20:55', 314, NULL, NULL, 'IPAY/2016/06/0667', NULL, 'cash', '', '', '', '', '', '', '', '8000.0000', NULL, 4, NULL, 'received', 'dsadad', '8000.0000', '0.0000', 33),
(298, '2016-06-01 14:27:05', 315, NULL, NULL, 'IPAY/2016/06/0669', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 5, NULL, 'received', 'wqewqeq', '5000.0000', '0.0000', 31),
(299, '2016-06-01 14:44:58', 316, NULL, NULL, 'IPAY/2016/06/0671', NULL, 'cash', '', '', '', '', '', '', '', '10500.0000', NULL, 4, NULL, 'received', 'aghh', '10500.0000', '0.0000', 33),
(300, '2016-06-01 15:10:22', 317, NULL, NULL, 'IPAY/2016/06/0673', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', 'hjkl', '5000.0000', '0.0000', 42),
(301, '2016-06-02 11:20:39', 318, NULL, NULL, 'IPAY/2016/06/0675', NULL, 'credit_voucher', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(302, '2016-06-02 11:56:59', 319, NULL, NULL, 'IPAY/2016/06/0677', NULL, 'credit_voucher', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(303, '2016-06-02 12:26:59', 320, NULL, NULL, 'IPAY/2016/06/0679', NULL, 'credit_voucher', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(304, '2016-06-02 12:55:39', 321, NULL, NULL, 'IPAY/2016/06/0681', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(305, '2016-06-02 13:06:30', 322, NULL, NULL, 'IPAY/2016/06/0683', NULL, 'credit_voucher', '', '2013946568361679', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', 'ankit khosa', '2500.0000', '0.0000', 42),
(306, '2016-06-02 13:22:26', 323, NULL, NULL, 'IPAY/2016/06/0685', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(307, '2016-06-03 08:20:18', 321, NULL, NULL, 'IPAY/2016/06/0687', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(308, '2016-06-03 08:31:33', 322, NULL, NULL, 'IPAY/2016/06/0689', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(309, '2016-06-03 08:33:13', 323, NULL, NULL, 'IPAY/2016/06/0691', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', 'sadsadsa', '2500.0000', '0.0000', 41),
(310, '2016-06-03 08:39:08', 324, NULL, NULL, 'IPAY/2016/06/0693', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 41),
(311, '2016-06-03 08:44:10', 325, NULL, NULL, 'IPAY/2016/06/0695', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 41),
(312, '2016-06-03 08:45:33', 326, NULL, NULL, 'IPAY/2016/06/0697', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(313, '2016-06-03 08:46:51', 327, NULL, NULL, 'IPAY/2016/06/0699', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(314, '2016-06-03 12:33:38', 328, NULL, NULL, 'IPAY/2016/06/0701', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', 'ytyug', '2500.0000', '0.0000', 42),
(315, '2016-06-03 12:36:36', 329, NULL, NULL, 'IPAY/2016/06/0703', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(316, '2016-06-03 14:09:49', 330, NULL, NULL, 'IPAY/2016/06/0705', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(317, '2016-06-03 14:36:06', 331, NULL, NULL, 'IPAY/2016/06/0707', NULL, 'CC', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(318, '2016-06-04 07:35:00', 332, NULL, NULL, 'IPAY/2016/06/0709', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-1500.0000', 42),
(319, '2016-06-04 07:35:00', 332, NULL, NULL, 'IPAY/2016/06/0710', NULL, 'CC', '', '', '', '', '', '', '', '1500.0000', NULL, 1, NULL, 'received', '', '1500.0000', '0.0000', 42),
(320, '2016-06-06 08:53:23', 333, NULL, NULL, 'IPAY/2016/06/0713', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(321, '2016-06-06 11:59:59', 334, NULL, NULL, 'IPAY/2016/06/0715', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 41),
(322, '2016-06-06 12:16:12', 335, NULL, NULL, 'IPAY/2016/06/0717', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 42),
(323, '2016-06-06 12:18:10', 336, NULL, NULL, 'IPAY/2016/06/0719', NULL, 'cash', '', '', '', '', '', '', '', '11000.0000', NULL, 1, NULL, 'received', '', '11000.0000', '0.0000', 42),
(324, '2016-06-06 12:43:52', 337, NULL, NULL, 'IPAY/2016/06/0721', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 41),
(325, '2016-06-06 12:54:02', 338, NULL, NULL, 'IPAY/2016/06/0723', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 42),
(326, '2016-06-06 13:03:16', 339, NULL, NULL, 'IPAY/2016/06/0725', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '8000.0000', NULL, 1, NULL, 'received', '', '8000.0000', '0.0000', 42),
(327, '2016-06-06 13:07:26', 340, NULL, NULL, 'IPAY/2016/06/0727', NULL, 'cash', '', '', '', '', '', '', '', '6500.0000', NULL, 1, NULL, 'received', '', '6500.0000', '0.0000', 42),
(328, '2016-06-06 14:56:40', 341, NULL, NULL, 'IPAY/2016/06/0729', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 1, NULL, 'received', '', '2000.0000', '-3000.0000', 42),
(329, '2016-06-06 14:56:40', 341, NULL, NULL, 'IPAY/2016/06/0730', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '3000.0000', NULL, 1, NULL, 'received', '', '3000.0000', '0.0000', 42),
(330, '2016-06-06 14:58:55', 342, NULL, NULL, 'IPAY/2016/06/0733', NULL, 'cash', '', '', '', '', '', '', '', '2000.0000', NULL, 1, NULL, 'received', '', '2000.0000', '-3000.0000', 42),
(331, '2016-06-06 14:58:55', 342, NULL, NULL, 'IPAY/2016/06/0734', NULL, 'credit_voucher', '', '9119064397367324', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-2000.0000', 42),
(332, '2016-06-06 14:58:55', 342, NULL, NULL, 'IPAY/2016/06/0735', NULL, 'CC', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-1000.0000', 42),
(333, '2016-06-06 14:58:55', 342, NULL, NULL, 'IPAY/2016/06/0736', NULL, 'CC', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '0.0000', 42),
(334, '2016-06-06 15:01:10', 343, NULL, NULL, 'IPAY/2016/06/0741', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '0.0000', 43),
(335, '2016-06-06 15:04:15', 344, NULL, NULL, 'IPAY/2016/06/0743', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 44),
(336, '2016-06-07 07:56:45', 345, NULL, NULL, 'IPAY/2016/06/0745', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', 'ewrewr', '2500.0000', '0.0000', 45),
(337, '2016-06-15 12:06:26', 346, NULL, NULL, 'IPAY/2016/06/0747', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 43),
(338, '2016-06-17 09:18:19', 347, NULL, NULL, 'IPAY/2016/06/0749', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 43),
(339, '2016-06-17 09:20:06', 348, NULL, NULL, 'IPAY/2016/06/0751', NULL, 'cash', '', '', '', '', '', '', '', '5500.0000', NULL, 1, NULL, 'received', '', '5500.0000', '0.0000', 43),
(340, '2016-06-17 09:55:15', 349, NULL, NULL, 'IPAY/2016/06/0753', NULL, 'credit_voucher', '', '3/2016/0824725552935', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', 'g', '2500.0000', '0.0000', 43),
(341, '2016-06-17 10:04:37', 350, NULL, NULL, 'IPAY/2016/06/0755', NULL, 'credit_voucher', '', '3/2016/0824725552935', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '0.0000', 43),
(342, '2016-06-17 10:16:29', 351, NULL, NULL, 'IPAY/2016/06/0757', NULL, 'credit_voucher', '', '3/2016/0824725552935', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 43),
(343, '2016-06-17 10:22:18', 352, NULL, NULL, 'IPAY/2016/06/0759', NULL, 'credit_voucher', '', '3/2016/0782293320016', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 43),
(344, '2016-06-17 11:39:57', 353, NULL, NULL, 'IPAY/2016/06/0761', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 43),
(345, '2016-06-18 05:07:34', 354, NULL, NULL, 'IPAY/2016/06/0763', NULL, 'credit_voucher', '', '3/2016/0782293320016', '', '', '', '', '', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 46),
(346, '2016-06-18 09:19:07', 355, NULL, NULL, 'IPAY/2016/06/0765', NULL, 'cash', '', '', '', '', '', '', '', '5000.0000', NULL, 1, NULL, 'received', '', '5000.0000', '0.0000', 43),
(347, '2016-06-18 12:30:39', 356, NULL, NULL, 'IPAY/2016/06/0767', NULL, 'cash', '', '', '', '', '', '', '', '2500.0000', NULL, 1, NULL, 'received', '', '2500.0000', '0.0000', 43),
(348, '2016-06-20 07:03:37', 358, NULL, NULL, 'IPAY/2016/06/0769', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-1500.0000', 46),
(349, '2016-06-20 07:03:37', 358, NULL, NULL, 'IPAY/2016/06/0770', NULL, 'cash', '', '', '', '', '', '', '', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '0.0000', 46),
(350, '2016-06-20 07:10:47', 359, NULL, NULL, 'IPAY/2016/06/0773', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 1, NULL, 'received', '', '1000.0000', '-1500.0000', 43),
(351, '2016-06-20 07:10:47', 359, NULL, NULL, 'IPAY/2016/06/0774', NULL, 'cash', '', '', '', '', '', '', '', '1500.0000', NULL, 1, NULL, 'received', '', '1500.0000', '0.0000', 43),
(352, '2016-06-23 07:56:21', 360, NULL, NULL, 'IPAY/2016/06/0777', NULL, 'cash', '', '', '', '', '', '', '', '5449.6800', NULL, 2, NULL, 'received', '', '5449.6800', '0.0000', 46),
(353, '2016-06-28 09:12:20', 361, NULL, NULL, 'IPAY/2016/06/0779', NULL, 'cash', '', '', '', '', '', '', '', '500.0000', NULL, 2, NULL, 'received', '', '500.0000', '-5000.0000', 72),
(354, '2016-06-28 12:25:05', 362, NULL, NULL, 'IPAY/2016/06/0781', NULL, 'cash', '', '', '', '', '', '', '', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-3000.0000', 72),
(355, '2016-06-28 12:25:05', 362, NULL, NULL, 'IPAY/2016/06/0782', NULL, 'CC', '', '1234567887654321', 'ajay', '07', '2016', 'MasterCard', '', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-2000.0000', 72),
(356, '2016-06-28 12:25:05', 362, NULL, NULL, 'IPAY/2016/06/0783', NULL, 'CC', '', '7890567845673452', 'ankit', '12', '2018', 'Maestro', 'DC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 72),
(357, '2016-06-28 12:37:47', 363, NULL, NULL, 'IPAY/2016/06/0787', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 72),
(358, '2016-06-28 12:37:47', 363, NULL, NULL, 'IPAY/2016/06/0788', NULL, 'CC', '', '1234567887654321', 'ahj', '12', '2025', 'Visa', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-2000.0000', 72),
(359, '2016-06-28 12:37:47', 363, NULL, NULL, 'IPAY/2016/06/0789', NULL, 'CC', '', '1234561234567891', 'ankit', '12', '2020', 'Maestro', 'DC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 72),
(360, '2016-06-28 12:42:20', 364, NULL, NULL, 'IPAY/2016/06/0793', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 72),
(361, '2016-06-28 12:42:20', 364, NULL, NULL, 'IPAY/2016/06/0794', NULL, 'CC', '', '1234567898765432', 'ajay', '12', '2020', 'MasterCard', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '-1000.0000', 72),
(362, '2016-06-28 12:42:20', 364, NULL, NULL, 'IPAY/2016/06/0795', NULL, 'CC', '', '9876456789084567', 'ankit', '12', '2025', 'Maestro', 'DC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 72),
(363, '2016-06-28 12:51:03', 365, NULL, NULL, 'IPAY/2016/06/0799', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 73),
(364, '2016-06-28 12:51:03', 365, NULL, NULL, 'IPAY/2016/06/0800', NULL, 'CC', '', '9876678912344321', 'ravi', '12', '2016', 'Visa', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '-1000.0000', 73),
(365, '2016-06-28 12:51:03', 365, NULL, NULL, 'IPAY/2016/06/0801', NULL, 'CC', '', '9876543212345678', 'ankit', '01', '2030', 'Maestro', 'DC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 73),
(366, '2016-06-28 12:57:40', 366, NULL, NULL, 'IPAY/2016/06/0805', NULL, 'cash', '', '', '', '', '', '', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '-8000.0000', 74),
(367, '2016-06-28 12:57:40', 366, NULL, NULL, 'IPAY/2016/06/0806', NULL, 'CC', '', '1630218512345678', 'a3', '11', '2020', 'Maestro', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '-6000.0000', 74),
(368, '2016-06-28 12:57:40', 366, NULL, NULL, 'IPAY/2016/06/0807', NULL, 'CC', '', '1630218512345674', 'a1', '12', '2023', 'Visa', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '-4000.0000', 74),
(369, '2016-06-28 12:57:40', 366, NULL, NULL, 'IPAY/2016/06/0808', NULL, 'CC', '', '1630518512345678', 'a2', '03', '2020', 'Visa', 'DC', '4000.0000', NULL, 2, NULL, 'received', '', '4000.0000', '0.0000', 74),
(370, '2016-06-28 13:04:04', 367, NULL, NULL, 'IPAY/2016/06/0813', NULL, 'CC', '', '1630218512345678', 'asd', '12', '2110', 'MasterCard', 'DC', '7500.0000', NULL, 2, NULL, 'received', '', '7500.0000', '0.0000', 74),
(371, '2016-06-28 13:10:24', 368, NULL, NULL, 'IPAY/2016/06/0815', NULL, 'CC', '', '1630218512345678', 'ajay', '12', '2020', '', 'DC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(372, '2016-06-29 04:52:43', 369, NULL, NULL, 'IPAY/2016/06/0817', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(373, '2016-06-29 04:55:43', 370, NULL, NULL, 'IPAY/2016/06/0819', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(374, '2016-06-29 04:55:43', 370, NULL, NULL, 'IPAY/2016/06/0820', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '-1000.0000', 74),
(375, '2016-06-29 04:55:43', 370, NULL, NULL, 'IPAY/2016/06/0821', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 74),
(376, '2016-06-29 05:12:03', 371, NULL, NULL, 'IPAY/2016/06/0825', NULL, 'cash', '', '', '', '', '', '', 'CC', '500.0000', NULL, 2, NULL, 'received', '', '500.0000', '-2000.0000', 74),
(377, '2016-06-29 05:13:54', 372, NULL, NULL, 'IPAY/2016/06/0827', NULL, 'CC', '', '1234567887654321', 'ajay', '12', '2016', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 74),
(378, '2016-06-29 05:16:28', 373, NULL, NULL, 'IPAY/2016/06/0829', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-1500.0000', 74),
(379, '2016-06-29 05:16:28', 373, NULL, NULL, 'IPAY/2016/06/0830', NULL, 'CC', '', '1234567887654321', 'ayush', '12', '2020', 'MasterCard', 'DC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-500.0000', 74),
(380, '2016-06-29 05:16:28', 373, NULL, NULL, 'IPAY/2016/06/0831', NULL, 'CC', '', '6789098765431234', 'ayushi', '10', '2020', 'Maestro', 'CC', '500.0000', NULL, 2, NULL, 'received', '', '500.0000', '0.0000', 74),
(381, '2016-06-29 05:29:46', 374, NULL, NULL, 'IPAY/2016/06/0835', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-1500.0000', 74),
(382, '2016-06-29 05:29:46', 374, NULL, NULL, 'IPAY/2016/06/0836', NULL, 'CC', '', '1234567888891234', 'ankit', '12', '2020', 'Maestro', 'DC', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '0.0000', 74),
(383, '2016-06-29 05:39:21', 375, NULL, NULL, 'IPAY/2016/06/0839', NULL, 'cash', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(384, '2016-06-29 05:46:32', 376, NULL, NULL, 'IPAY/2016/06/0841', NULL, 'CC', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 74),
(385, '2016-06-29 06:09:35', 377, NULL, NULL, 'IPAY/2016/06/0843', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-7000.0000', 74),
(386, '2016-06-29 06:09:35', 377, NULL, NULL, 'IPAY/2016/06/0844', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '-5000.0000', 74),
(387, '2016-06-29 06:09:35', 377, NULL, NULL, 'IPAY/2016/06/0845', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '0.0000', 74),
(388, '2016-06-29 07:24:46', 378, NULL, NULL, 'IPAY/2016/06/0849', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-7000.0000', 74),
(389, '2016-06-29 07:24:46', 378, NULL, NULL, 'IPAY/2016/06/0850', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 74),
(390, '2016-06-29 07:24:46', 378, NULL, NULL, 'IPAY/2016/06/0851', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '6000.0000', NULL, 2, NULL, 'received', '', '6000.0000', '0.0000', 74),
(391, '2016-06-29 07:26:53', 379, NULL, NULL, 'IPAY/2016/06/0855', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-7000.0000', 74),
(392, '2016-06-29 07:26:53', 379, NULL, NULL, 'IPAY/2016/06/0856', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 74),
(393, '2016-06-29 07:26:53', 379, NULL, NULL, 'IPAY/2016/06/0857', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '4000.0000', NULL, 2, NULL, 'received', '', '4000.0000', '0.0000', 74),
(394, '2016-06-29 07:26:53', 379, NULL, NULL, 'IPAY/2016/06/0858', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 74),
(395, '2016-06-29 07:28:36', 380, NULL, NULL, 'IPAY/2016/06/0863', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-7000.0000', 74),
(396, '2016-06-29 07:28:36', 380, NULL, NULL, 'IPAY/2016/06/0864', NULL, 'CC', '', '', '', '', '', 'Visa', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 74),
(397, '2016-06-29 07:40:02', 381, NULL, NULL, 'IPAY/2016/06/0867', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '8000.0000', NULL, 2, NULL, 'received', '', '8000.0000', '0.0000', 74),
(398, '2016-06-29 07:47:44', 382, NULL, NULL, 'IPAY/2016/06/0869', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '8000.0000', NULL, 2, NULL, 'received', '', '8000.0000', '0.0000', 74),
(399, '2016-06-29 07:51:05', 383, NULL, NULL, 'IPAY/2016/06/0871', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(400, '2016-06-29 07:52:31', 384, NULL, NULL, 'IPAY/2016/06/0873', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(401, '2016-06-29 08:38:52', 385, NULL, NULL, 'IPAY/2016/06/0875', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(402, '2016-06-29 08:38:52', 385, NULL, NULL, 'IPAY/2016/06/0876', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 74),
(403, '2016-06-29 08:41:01', 386, NULL, NULL, 'IPAY/2016/06/0879', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(404, '2016-06-29 08:42:11', 387, NULL, NULL, 'IPAY/2016/06/0881', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(405, '2016-06-29 08:45:15', 388, NULL, NULL, 'IPAY/2016/06/0883', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(406, '2016-06-29 08:46:26', 389, NULL, NULL, 'IPAY/2016/06/0885', NULL, 'CC', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(407, '2016-06-29 08:48:11', 390, NULL, NULL, 'IPAY/2016/06/0887', NULL, 'credit_voucher', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(408, '2016-06-29 09:06:28', 391, NULL, NULL, 'IPAY/2016/06/0889', NULL, 'CC', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(409, '2016-06-29 09:20:00', 392, NULL, NULL, 'IPAY/2016/06/0891', NULL, 'CC', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(410, '2016-06-29 09:25:27', 393, NULL, NULL, 'IPAY/2016/06/0893', NULL, 'CC', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(411, '2016-06-29 09:32:19', 394, NULL, NULL, 'IPAY/2016/06/0895', NULL, 'CC', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(412, '2016-06-29 09:56:08', 395, NULL, NULL, 'IPAY/2016/06/0897', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(413, '2016-06-29 10:01:56', 396, NULL, NULL, 'IPAY/2016/06/0899', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(414, '2016-06-29 10:01:56', 396, NULL, NULL, 'IPAY/2016/06/0900', NULL, 'CC', '', '1234567887654321', 'ajay', '12', '2020', 'Visa', 'DC', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 74),
(415, '2016-06-29 10:41:09', 397, NULL, NULL, 'IPAY/2016/06/0903', NULL, 'cash', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(416, '2016-06-29 11:50:52', 398, NULL, NULL, 'IPAY/2016/06/0905', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(417, '2016-06-29 11:54:51', 399, NULL, NULL, 'IPAY/2016/06/0907', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(418, '2016-06-29 11:58:36', 400, NULL, NULL, 'IPAY/2016/06/0909', NULL, 'cash', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(419, '2016-06-29 12:08:37', 401, NULL, NULL, 'IPAY/2016/06/0911', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(420, '2016-06-29 12:08:37', 401, NULL, NULL, 'IPAY/2016/06/0912', NULL, 'CC', '', '1234567887654321', 'ajay', '12', '2016', 'MasterCard', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '-1000.0000', 74),
(421, '2016-06-29 12:08:37', 401, NULL, NULL, 'IPAY/2016/06/0913', NULL, 'CC', '', '5678543212345678', 'ankit', '10', '2016', 'Maestro', 'DC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 74),
(422, '2016-06-29 12:14:21', 402, NULL, NULL, 'IPAY/2016/06/0917', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(423, '2016-06-29 12:18:12', 403, NULL, NULL, 'IPAY/2016/06/0919', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(424, '2016-06-29 12:18:12', 403, NULL, NULL, 'IPAY/2016/06/0920', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-2000.0000', 74),
(425, '2016-06-29 12:18:12', 403, NULL, NULL, 'IPAY/2016/06/0921', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '2000.0000', NULL, 2, NULL, 'received', '', '2000.0000', '0.0000', 74),
(426, '2016-06-29 12:22:23', 404, NULL, NULL, 'IPAY/2016/06/0925', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(427, '2016-06-29 12:22:23', 404, NULL, NULL, 'IPAY/2016/06/0926', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 74),
(428, '2016-06-29 12:44:55', 405, NULL, NULL, 'IPAY/2016/06/0929', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(429, '2016-06-29 12:44:55', 405, NULL, NULL, 'IPAY/2016/06/0930', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 74),
(430, '2016-06-29 12:46:16', 406, NULL, NULL, 'IPAY/2016/06/0933', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-4500.0000', 74),
(431, '2016-06-29 12:46:16', 406, NULL, NULL, 'IPAY/2016/06/0934', NULL, 'cash', '', '', '', '', '', 'Visa', 'CC', '4500.0000', NULL, 2, NULL, 'received', '', '4500.0000', '0.0000', 74),
(432, '2016-06-30 08:54:57', 407, NULL, NULL, 'IPAY/2016/06/0937', NULL, 'cash', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(433, '2016-06-30 09:48:10', 408, NULL, NULL, 'IPAY/2016/06/0939', NULL, 'cash', '', '', '', '', '', '', 'CC', '5500.0000', NULL, 2, NULL, 'received', '', '5500.0000', '0.0000', 74),
(434, '2016-07-05 09:48:55', 409, NULL, NULL, 'IPAY/2016/07/0941', NULL, 'credit_voucher', '', '3/2016/7628147517396', '', '', '', '', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '0.0000', 74),
(435, '2016-07-05 12:19:59', 410, NULL, NULL, 'IPAY/2016/07/0943', NULL, 'cash', '', '', '', '', '', '', 'CC', '25000.0000', NULL, 4, NULL, 'received', '', '25000.0000', '0.0000', 33),
(436, '2016-07-05 12:43:34', 411, NULL, NULL, 'IPAY/2016/07/0945', NULL, 'cash', '', '', '', '', '', '', 'CC', '350.0000', NULL, 2, NULL, 'received', '', '350.0000', '-3150.0000', 77),
(437, '2016-07-05 12:43:34', 411, NULL, NULL, 'IPAY/2016/07/0946', NULL, 'CC', '', '12345678', 'fdfdf', '10', '2016', '', 'DC', '315.0000', NULL, 2, NULL, 'received', 'dsdsdsd', '315.0000', '-2835.0000', 77),
(438, '2016-07-05 12:45:11', 412, NULL, NULL, 'IPAY/2016/07/0949', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-22500.0000', 77),
(439, '2016-07-05 12:45:11', 412, NULL, NULL, 'IPAY/2016/07/0950', NULL, 'CC', '', '122112121212121212', 'sasa', '', '2016', 'MasterCard', 'DC', '2250.0000', NULL, 2, NULL, 'received', '', '2250.0000', '-20250.0000', 77),
(440, '2016-07-05 12:48:37', 413, NULL, NULL, 'IPAY/2016/07/0953', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-22500.0000', 77),
(441, '2016-07-05 12:48:37', 413, NULL, NULL, 'IPAY/2016/07/0954', NULL, 'cash', '', '', '', '', '', '', 'CC', '2250.0000', NULL, 2, NULL, 'received', '', '2250.0000', '-20250.0000', 77),
(442, '2016-07-05 12:50:31', 414, NULL, NULL, 'IPAY/2016/07/0957', NULL, 'cash', '', '', '', '', '', '', 'CC', '78.0000', NULL, 2, NULL, 'received', '', '78.0000', '-24922.0000', 77),
(443, '2016-07-05 12:53:36', 415, NULL, NULL, 'IPAY/2016/07/0959', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-22500.0000', 77),
(444, '2016-07-05 12:53:36', 415, NULL, NULL, 'IPAY/2016/07/0960', NULL, 'CC', '', '1234567887654321', 'hhh', '10', '2017', '', 'CC', '2250.0000', NULL, 2, NULL, 'received', '', '2250.0000', '-20250.0000', 77),
(445, '2016-07-05 12:54:30', 416, NULL, NULL, 'IPAY/2016/07/0963', NULL, 'cash', '', '', '', '', '', '', 'CC', '500.0000', NULL, 2, NULL, 'received', '', '500.0000', '-24500.0000', 77),
(446, '2016-07-05 12:57:02', 417, NULL, NULL, 'IPAY/2016/07/0965', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-24000.0000', 77),
(447, '2016-07-05 12:57:02', 417, NULL, NULL, 'IPAY/2016/07/0966', NULL, 'cash', '', '', '', '', '', '', 'CC', '2400.0000', NULL, 2, NULL, 'received', '', '2400.0000', '-21600.0000', 77),
(448, '2016-07-05 13:16:36', 418, NULL, NULL, 'IPAY/2016/07/0969', NULL, 'cash', '', '', '', '', '', '', 'CC', '25000.0000', NULL, 2, NULL, 'received', '', '25000.0000', '0.0000', 78),
(449, '2016-07-05 13:18:21', 419, NULL, NULL, 'IPAY/2016/07/0971', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-22500.0000', 78),
(450, '2016-07-05 13:19:46', 420, NULL, NULL, 'IPAY/2016/07/0973', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '-22500.0000', 78),
(451, '2016-07-05 13:29:35', 421, NULL, NULL, 'IPAY/2016/07/0975', NULL, 'cash', '', '', '', '', '', '', 'CC', '350.0000', NULL, 2, NULL, 'received', '', '350.0000', '-3150.0000', 78),
(452, '2016-07-05 13:43:36', 422, NULL, NULL, 'IPAY/2016/07/0977', NULL, 'cash', '', '', '', '', '', '', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '0.0000', 79),
(453, '2016-07-05 13:44:32', 423, NULL, NULL, 'IPAY/2016/07/0979', NULL, 'CC', '', '12345678', 'vivek', '10', '2019', '', 'CC', '3500.0000', NULL, 2, NULL, 'received', '', '3500.0000', '0.0000', 79),
(454, '2016-07-06 06:35:10', 424, NULL, NULL, 'IPAY/2016/07/0981', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-2500.0000', 84),
(455, '2016-07-06 06:35:10', 424, NULL, NULL, 'IPAY/2016/07/0982', NULL, 'CC', '', '1234567890987654', 'raj', '03', '2022', 'MasterCard', 'CC', '1500.0000', NULL, 2, NULL, 'received', '', '1500.0000', '-1000.0000', 84),
(456, '2016-07-06 06:35:10', 424, NULL, NULL, 'IPAY/2016/07/0983', NULL, 'CC', '', '6543213456789098', 'ankit', '07', '2020', '', 'DC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '0.0000', 84),
(457, '2016-07-06 06:55:33', 425, NULL, NULL, 'IPAY/2016/07/0987', NULL, 'cash', '', '', '', '', '', '', 'CC', '12000.0000', NULL, 2, NULL, 'received', '', '12000.0000', '0.0000', 85),
(458, '2016-07-06 07:02:42', 426, NULL, NULL, 'IPAY/2016/07/0989', NULL, 'cash', '', '', '', '', '', '', 'CC', '1000.0000', NULL, 2, NULL, 'received', '', '1000.0000', '-11000.0000', 86),
(459, '2016-07-06 07:02:42', 426, NULL, NULL, 'IPAY/2016/07/0990', NULL, 'CC', '', '5401234567890123', 'ajay', '03', '2020', '', 'CC', '1100.0000', NULL, 2, NULL, 'received', '', '1100.0000', '-9900.0000', 86),
(460, '2016-07-06 07:02:42', 426, NULL, NULL, 'IPAY/2016/07/0991', NULL, 'CC', '', '8901234567891234', 'ankit', '02', '2025', 'Maestro', 'DC', '9900.0000', NULL, 2, NULL, 'received', '', '9900.0000', '0.0000', 86),
(461, '2016-07-06 13:59:58', 427, NULL, NULL, 'IPAY/2016/07/0995', NULL, 'cash', '', '', '', '', '', '', 'CC', '12000.0000', NULL, 2, NULL, 'received', '', '12000.0000', '0.0000', 87),
(462, '2016-07-07 04:56:26', 428, NULL, NULL, 'IPAY/2016/07/0997', NULL, 'cash', '', '', '', '', '', '', 'CC', '2500.0000', NULL, 2, NULL, 'received', '', '2500.0000', '0.0000', 87),
(463, '2016-07-07 05:41:47', 429, NULL, NULL, 'IPAY/2016/07/0999', NULL, 'cash', '', '', '', '', '', '', 'CC', '4000.0000', NULL, 2, NULL, 'received', '', '4000.0000', '-8000.0000', 88),
(464, '2016-07-07 05:41:47', 429, NULL, NULL, 'IPAY/2016/07/1000', NULL, 'CC', '', '1234567887654321', 'anurag', '12', '2020', 'Maestro', 'CC', '5000.0000', NULL, 2, NULL, 'received', '', '5000.0000', '-3000.0000', 88),
(465, '2016-07-07 05:41:47', 429, NULL, NULL, 'IPAY/2016/07/1001', NULL, 'CC', '', '5401234567899876', 'aniket', '12', '2021', 'MasterCard', 'DC', '3000.0000', NULL, 2, NULL, 'received', '', '3000.0000', '0.0000', 88);

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_paypal`
--

INSERT INTO `sma_paypal` (`id`, `active`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_permissions`
--

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`) VALUES
(1, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(2, 6, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 0, 0, 1, NULL, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, 1),
(3, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 7, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, NULL),
(5, 9, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, NULL),
(7, 11, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, NULL),
(8, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 13, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, NULL),
(10, 14, 1, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_cash_drawer`
--

CREATE TABLE `sma_pos_cash_drawer` (
  `id` int(11) NOT NULL,
  `pos_register_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thousand` int(5) NOT NULL,
  `five_hundred` int(5) NOT NULL,
  `hundred` int(5) NOT NULL,
  `fifty` int(5) NOT NULL,
  `twenty` int(5) NOT NULL,
  `ten` int(5) NOT NULL,
  `five` int(5) NOT NULL,
  `two` int(5) NOT NULL,
  `one` int(5) NOT NULL,
  `status` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cash_in_hand` decimal(25,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_pos_cash_drawer`
--

INSERT INTO `sma_pos_cash_drawer` (`id`, `pos_register_id`, `user_id`, `thousand`, `five_hundred`, `hundred`, `fifty`, `twenty`, `ten`, `five`, `two`, `one`, `status`, `date`, `cash_in_hand`) VALUES
(1, 1, 5, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'open', '2016-04-05 12:39:31', '5040'),
(2, 1, 5, 29, 2, 2, 2, 2, 2, 0, 0, 0, 'closed', '2016-04-05 12:41:55', '30360'),
(3, 2, 5, 4, 4, 3, 32, 23, 23, 0, 0, 0, 'open', '2016-04-05 12:44:51', '8590'),
(4, 2, 5, 25, 2, 2, 2, 2, 2, 0, 0, 0, 'closed', '2016-04-05 12:45:39', '26360'),
(5, 3, 5, 6, 6, 6, 6, 6, 6, 0, 0, 0, 'open', '2016-04-05 12:53:31', '10080'),
(6, 3, 5, 4, 4, 4, 43, 34, 3432, 0, 0, 0, 'closed', '2016-04-05 12:54:22', '43550'),
(7, 4, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 'open', '2016-04-05 13:21:56', '1680'),
(8, 4, 5, 43, 4, 4, 4, 4, 3, 0, 0, 0, 'closed', '2016-04-05 14:05:39', '45710'),
(9, 5, 5, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'open', '2016-04-05 14:13:37', '5040'),
(10, 5, 5, 4, 4, 4, 4, 4, 4, 0, 0, 0, 'closed', '2016-04-05 14:31:34', '6720'),
(11, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 'open', '2016-04-05 14:32:33', '1680'),
(12, 1, 5, 2, 2, 2, 2, 2, 2, 0, 0, 0, 'closed', '2016-04-05 14:33:17', '3360'),
(13, 2, 5, 4, 4, 4, 4, 4, 4, 0, 0, 0, 'open', '2016-04-05 14:39:29', '6720'),
(14, 3, 5, 3, 3, 3, 34, 3, 3, 0, 0, 0, 'open', '2016-04-05 14:45:37', '6590'),
(15, 4, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'open', '2016-04-06 12:36:10', '5040'),
(16, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-06 12:55:50', '0'),
(17, 5, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'open', '2016-04-06 12:56:10', '5040'),
(18, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-06 12:57:20', '0'),
(19, 6, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-06 13:00:38', '3000'),
(20, 6, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-06 13:37:41', '6000'),
(21, 7, 1, 3, 3, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-06 13:39:35', '4500'),
(22, 7, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-07 13:52:25', '4000'),
(23, 8, 1, 4, 4, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-07 13:54:36', '6000'),
(24, 8, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-08 09:56:15', '4000'),
(25, 9, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-08 09:58:56', '2000'),
(26, 10, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-08 10:10:08', '2000'),
(27, 9, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-08 14:46:51', '3000'),
(28, 11, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-08 15:07:38', '3000'),
(29, 2, 5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-12 12:12:54', '4000'),
(30, 11, 1, 20, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-13 08:54:31', '20000'),
(31, 12, 1, 3, 3, 5, 0, 0, 0, 0, 0, 0, 'open', '2016-04-13 08:55:20', '5000'),
(32, 12, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'closed', '2016-04-19 07:42:49', '5040'),
(33, 12, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'closed', '2016-04-19 07:43:16', '5040'),
(34, 12, 1, 4, 4, 4, 4, 4, 4, 0, 0, 0, 'closed', '2016-04-19 07:46:03', '6720'),
(35, 12, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'closed', '2016-04-19 07:54:58', '5040'),
(36, 12, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'closed', '2016-04-19 07:55:41', '5040'),
(37, 12, 1, 4, 4, 4, 4, 4, 4, 0, 0, 0, 'closed', '2016-04-19 08:10:17', '6720'),
(38, 12, 1, 4, 4, 4, 4, 4, 4, 0, 0, 0, 'closed', '2016-04-19 08:15:28', '6720'),
(39, 12, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'closed', '2016-04-19 08:31:06', '5040'),
(40, 12, 1, 5, 5, 5, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-19 08:31:29', '8000'),
(41, 12, 1, 4, 4, 4, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-19 08:59:24', '6400'),
(42, 10, 2, 3, 3, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-19 11:25:07', '4500'),
(43, 3, 5, 3, 3, 3, 3, 0, 0, 0, 0, 0, 'closed', '2016-04-19 11:47:41', '4950'),
(44, 13, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-19 11:48:44', '1000'),
(45, 14, 5, 3, 3, 3, 0, 0, 0, 0, 0, 0, 'open', '2016-04-19 12:41:04', '4800'),
(46, 15, 2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-20 09:27:36', '4000'),
(47, 13, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-20 12:13:36', '1'),
(48, 16, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 'open', '2016-04-20 12:14:05', '2280'),
(49, 16, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-21 12:28:57', '4000'),
(50, 17, 1, 3, 3, 3, 3, 3, 3, 0, 0, 0, 'open', '2016-04-22 08:05:36', '5040'),
(51, 17, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-22 08:06:38', '3000'),
(52, 18, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-22 08:06:51', '3000'),
(53, 18, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-22 08:07:48', '5000'),
(54, 19, 1, 0, 4, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-22 08:08:52', '2000'),
(55, 14, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-25 08:51:00', '3000'),
(56, 20, 5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-25 08:51:19', '4000'),
(57, 20, 5, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-04-25 08:51:43', '4000'),
(58, 21, 5, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-25 08:52:01', '3000'),
(59, 22, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-04-25 09:18:18', '2000'),
(60, 19, 1, 50, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-05 09:09:31', '50000'),
(61, 23, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-05 09:46:36', '0'),
(62, 23, 1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-05 09:54:20', '80000'),
(63, 24, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-05 10:11:01', '0'),
(64, 24, 1, 40, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-05 11:09:52', '40000'),
(65, 25, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-05 11:10:07', '0'),
(66, 15, 2, 80, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-05 12:32:44', '80000'),
(67, 26, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-05 12:51:22', '0'),
(68, 26, 2, 0, 4, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-06 07:55:11', '38000'),
(69, 27, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-06 08:08:54', '0'),
(70, 25, 1, 0, 0, 0, 0, 67, 6, 0, 0, 0, 'closed', '2016-05-10 15:44:34', '1400'),
(71, 28, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-10 15:44:50', '0'),
(72, 28, 1, 34, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-11 12:43:34', '34000'),
(73, 29, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-11 12:43:56', '0'),
(74, 27, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-13 13:57:16', '0'),
(75, 30, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-13 13:57:24', '0'),
(76, 30, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-13 13:57:37', '0'),
(77, 21, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-13 14:04:24', '0'),
(78, 31, 5, 19, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-13 14:04:34', '19000'),
(79, 32, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-19 09:47:00', '2000'),
(80, 33, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-21 10:01:04', '3000'),
(81, 32, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-23 12:28:40', '2000'),
(82, 34, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-23 12:28:48', '0'),
(83, 34, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-23 12:40:09', '0'),
(84, 35, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-23 12:40:15', '0'),
(85, 29, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-24 15:20:27', '0'),
(86, 36, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-24 15:20:41', '5000'),
(87, 36, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 12:19:28', '2000'),
(88, 37, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 09:53:18', '2000'),
(89, 37, 1, 11, 6, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 09:57:13', '14000'),
(90, 38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 09:57:17', '0'),
(91, 35, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 11:16:40', '0'),
(92, 39, 2, 5000, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 11:16:49', '5000000'),
(93, 39, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 11:17:00', '0'),
(94, 40, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 11:17:07', '2000'),
(95, 40, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 11:19:53', '3000'),
(96, 41, 2, 10, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 11:20:01', '10000'),
(97, 38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-05-25 14:28:14', '0'),
(98, 42, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-05-25 14:28:22', '1000'),
(99, 42, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-06 15:00:39', '0'),
(100, 43, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-06 15:00:46', '1000'),
(101, 41, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-06 15:03:46', '0'),
(102, 44, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-06 15:03:53', '1000'),
(103, 44, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-06 15:04:49', '3000'),
(104, 45, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-07 07:55:49', '1000'),
(105, 45, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-07 07:58:20', '1000'),
(106, 46, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-18 07:22:44', '1000'),
(107, 47, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-24 09:41:01', '3000'),
(108, 48, 2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-24 09:47:47', '4000'),
(109, 48, 2, 0, 0, 0, 0, 0, 0, 6, 7, 8, 'closed', '2016-06-24 09:50:02', '52'),
(110, 49, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-24 10:01:14', '0'),
(111, 49, 2, 0, 0, 0, 0, 0, 0, 0, 0, 131, 'closed', '2016-06-24 10:12:34', '131'),
(112, 50, 2, 0, 0, 1, 0, 0, 0, 4, 6, 5, 'open', '2016-06-24 10:17:38', '137'),
(113, 51, 2, 0, 0, 0, 0, 0, 0, 0, 0, 140, 'open', '2016-06-24 10:34:17', '140'),
(114, 52, 2, 0, 0, 0, 0, 0, 0, 0, 0, 200, 'open', '2016-06-24 10:36:43', '200'),
(115, 53, 2, 0, 0, 0, 0, 0, 0, 0, 0, 300, 'open', '2016-06-24 10:38:06', '300'),
(116, 53, 2, 0, 0, 0, 0, 0, 0, 0, 0, 20, 'closed', '2016-06-24 10:38:37', '20'),
(117, 54, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-24 11:40:31', '1000'),
(118, 55, 2, 0, 0, 0, 0, 0, 0, 0, 213, 0, 'closed', '2016-06-24 11:41:53', '426'),
(119, 56, 2, 0, 0, 0, 0, 0, 0, 0, 32423, 0, 'closed', '2016-06-24 11:42:11', '64846'),
(120, 57, 2, 0, 0, 0, 0, 0, 0, 0, 0, 34, 'closed', '2016-06-24 11:51:21', '34'),
(121, 58, 2, 0, 0, 0, 0, 0, 0, 0, 0, 324, 'closed', '2016-06-24 11:53:58', '324'),
(122, 59, 2, 0, 0, 0, 0, 4, 4, 0, 0, 0, 'closed', '2016-06-24 11:57:37', '120'),
(123, 60, 2, 0, 0, 0, 45, 0, 0, 0, 0, 0, 'closed', '2016-06-24 12:02:35', '2295'),
(124, 43, 1, 2, 0, 7, 0, 0, 2, 0, 0, 0, 'closed', '2016-06-24 12:08:29', '2723'),
(125, 61, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-24 12:12:10', '1345'),
(126, 62, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-24 12:42:04', '500'),
(127, 62, 2, 0, 0, 3, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-24 12:40:19', '300'),
(128, 67, 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 'open', '2016-06-24 12:42:23', '200'),
(129, 63, 2, 0, 4, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-24 12:47:36', '2000'),
(130, 64, 2, 0, 34, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 05:29:00', '17000'),
(131, 65, 2, 0, 0, 4, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 05:30:42', '400'),
(132, 66, 2, 0, 10, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 05:33:24', '5000'),
(133, 69, 10, 1, 1, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-27 05:34:29', '1500'),
(134, 69, 10, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 05:35:06', '1000'),
(135, 67, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 06:04:07', '2000'),
(136, 70, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-27 06:06:22', '2000'),
(137, 70, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-27 06:07:15', '3000'),
(138, 71, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-27 06:10:37', '3000'),
(139, 71, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-28 09:10:37', '1000'),
(140, 72, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-28 09:10:44', '1000'),
(141, 72, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-28 12:49:16', '1000'),
(142, 73, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-28 12:49:21', '1000'),
(143, 73, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-06-28 12:54:41', '1000'),
(144, 74, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-06-28 12:54:46', '1000'),
(145, 75, 1, 2, 0, 7, 0, 0, 0, 0, 0, 23, 'open', '2016-06-29 04:54:03', '2723'),
(146, 74, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-05 12:30:22', '2000'),
(147, 76, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-05 12:30:29', '2000'),
(148, 76, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-05 12:31:10', '1000'),
(149, 77, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-05 12:31:17', '1000'),
(150, 77, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-05 13:09:34', '0'),
(151, 78, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-05 13:11:49', '0'),
(152, 78, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-05 13:42:50', '1000'),
(153, 79, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-05 13:42:54', '1000'),
(154, 79, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 05:55:00', '0'),
(155, 80, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:07:35', '0'),
(156, 80, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:17:23', '1000'),
(157, 81, 2, 0, 1, 2, 1, 5, 5, 10, 5, 40, 'open', '2016-07-06 06:21:57', '1000'),
(158, 81, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:27:09', '1000'),
(159, 82, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:29:01', '1000'),
(160, 82, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:29:30', '1000'),
(161, 83, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:31:52', '1000'),
(162, 83, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:32:21', '800'),
(163, 84, 2, 0, 1, 3, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:33:06', '800'),
(164, 84, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:40:34', '500'),
(165, 85, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:42:43', '500'),
(166, 85, 2, 12, 1, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 06:58:16', '12400'),
(167, 86, 2, 12, 0, 4, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 06:59:21', '12400'),
(168, 86, 2, 13, 0, 4, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-06 07:05:04', '12400'),
(169, 87, 2, 12, 0, 4, 0, 0, 0, 0, 0, 0, 'open', '2016-07-06 07:06:32', '12400'),
(170, 87, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-07 05:13:58', '1000'),
(171, 88, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-07 05:39:10', '1000'),
(172, 88, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 'closed', '2016-07-07 05:49:22', '3000'),
(173, 89, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 'open', '2016-07-07 05:50:38', '3000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_register`
--

INSERT INTO `sma_pos_register` (`id`, `date`, `user_id`, `cash_in_hand`, `status`, `total_cash`, `total_cheques`, `total_cc_slips`, `total_cash_submitted`, `total_cheques_submitted`, `total_cc_slips_submitted`, `note`, `closed_at`, `transfer_opened_bills`, `closed_by`) VALUES
(1, '2016-04-05 14:32:33', 5, '1680.0000', 'close', '3680.0000', 0, 0, '3360.0000', 0, 0, '', '2016-04-05 14:33:17', NULL, 5),
(2, '2016-04-05 14:39:29', 5, '6720.0000', 'close', '286720.0000', 0, 0, '4000.0000', 0, 0, '', '2016-04-12 12:12:54', NULL, 5),
(3, '2016-04-05 14:45:37', 5, '6590.0000', 'close', '375590.0000', 0, 0, '4950.0000', 0, 0, '', '2016-04-19 11:47:40', NULL, 1),
(4, '2016-04-06 12:36:10', 1, '5040.0000', 'close', '555040.0000', 0, 0, '0.0000', 0, 0, '', '2016-04-06 12:55:50', NULL, 1),
(5, '2016-04-06 12:56:10', 1, '5040.0000', 'close', '5040.0000', 0, 0, '0.0000', 0, 0, '', '2016-04-06 12:57:20', NULL, 1),
(6, '2016-04-06 13:00:38', 1, '3000.0000', 'close', '3000.0000', 0, 0, '6000.0000', 0, 0, '', '2016-04-06 13:37:41', NULL, 1),
(7, '2016-04-06 13:39:35', 1, '4500.0000', 'close', '4500.0000', 0, 0, '4000.0000', 0, 0, '', '2016-04-07 13:52:25', NULL, 1),
(8, '2016-04-07 13:54:36', 1, '6000.0000', 'close', '459000.0000', 0, 0, '4000.0000', 0, 0, '', '2016-04-08 09:56:15', NULL, 1),
(9, '2016-04-08 09:58:56', 1, '2000.0000', 'close', '2000.0000', 0, 0, '3000.0000', 0, 0, '', '2016-04-08 14:46:51', NULL, 1),
(10, '2016-04-08 10:10:08', 2, '2000.0000', 'close', '623000.0000', 0, 0, '4500.0000', 0, 0, '', '2016-04-19 11:25:07', NULL, 1),
(11, '2016-04-08 15:07:38', 1, '3000.0000', 'close', '3000.0000', 0, 0, '20000.0000', 0, 0, '', '2016-04-13 08:54:31', '0', 1),
(12, '2016-04-13 08:55:19', 1, '5000.0000', 'close', '8090.0000', 0, 0, '6400.0000', 0, 0, '', '2016-04-19 08:59:24', NULL, 1),
(13, '2016-04-19 11:48:44', 1, '1000.0000', 'close', '3500.0000', 0, 1, '1.0000', 0, 1, '', '2016-04-20 12:13:36', '0', 1),
(14, '2016-04-19 12:41:04', 5, '4800.0000', 'close', '4800.0000', NULL, 0, '3000.0000', NULL, 0, '', '2016-04-25 08:51:00', NULL, 5),
(15, '2016-04-20 09:27:36', 2, '4000.0000', 'close', '17999.0000', NULL, 0, '80000.0000', NULL, 0, '', '2016-05-05 12:32:44', NULL, 2),
(16, '2016-04-20 12:14:05', 1, '2280.0000', 'close', '2280.0000', NULL, 0, '4000.0000', NULL, 0, '', '2016-04-21 12:28:57', '0', 1),
(17, '2016-04-22 08:05:36', 1, '5040.0000', 'close', '5040.0000', NULL, 0, '3000.0000', NULL, 0, '', '2016-04-22 08:06:38', '0', 1),
(18, '2016-04-22 08:06:51', 1, '3000.0000', 'close', '3000.0000', NULL, 0, '5000.0000', NULL, 0, '', '2016-04-22 08:07:48', '0', 1),
(19, '2016-04-22 08:08:52', 1, '2000.0000', 'close', '631440.0000', NULL, 0, '50000.0000', NULL, 0, '', '2016-05-05 09:09:31', '0', 1),
(20, '2016-04-25 08:51:19', 5, '4000.0000', 'close', '4000.0000', NULL, 0, '4000.0000', NULL, 0, '', '2016-04-25 08:51:43', NULL, 5),
(21, '2016-04-25 08:52:01', 5, '3000.0000', 'close', '516000.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-13 14:04:23', NULL, 5),
(22, '2016-04-25 09:18:18', 3, '2000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '2016-05-05 09:46:36', 1, '0.0000', 'close', '0.0000', NULL, 0, '80000.0000', NULL, 0, '', '2016-05-05 09:54:20', '0', 1),
(24, '2016-05-05 10:11:01', 1, '0.0000', 'close', '0.0000', NULL, 0, '40000.0000', NULL, 0, '', '2016-05-05 11:09:52', '0', 1),
(25, '2016-05-05 11:10:07', 1, '0.0000', 'close', '201000.0000', NULL, 0, '13400.0000', NULL, 0, '', '2016-05-10 15:44:34', NULL, 1),
(26, '2016-05-05 12:51:22', 2, '0.0000', 'close', '0.0000', NULL, 1, '2000.0000', NULL, 1, '', '2016-05-06 07:55:11', NULL, 2),
(27, '2016-05-06 08:08:54', 2, '0.0000', 'close', '12000.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-13 13:57:16', NULL, 2),
(28, '2016-05-10 15:44:50', 1, '0.0000', 'close', '55000.0000', NULL, 0, '34000.0000', NULL, 0, '', '2016-05-11 12:43:34', NULL, 1),
(29, '2016-05-11 12:43:56', 1, '0.0000', 'close', '1209754.0000', NULL, 2, '61500.0000', NULL, 2, '', '2016-05-24 15:20:27', NULL, 1),
(30, '2016-05-13 13:57:24', 2, '0.0000', 'close', '0.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-13 13:57:37', NULL, 2),
(31, '2016-05-13 14:04:34', 5, '19000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '2016-05-19 09:47:00', 2, '2000.0000', 'close', '47500.0000', NULL, 0, '2000.0000', NULL, 0, '', '2016-05-23 12:28:40', NULL, 2),
(33, '2016-05-21 10:01:04', 4, '3000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '2016-05-23 12:28:48', 2, '0.0000', 'close', '0.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-23 12:40:09', NULL, 2),
(35, '2016-05-23 12:40:15', 2, '0.0000', 'close', '29000.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-25 11:16:40', NULL, 2),
(36, '2016-05-24 15:20:41', 1, '5000.0000', 'close', '91800.0000', NULL, 4, '37500.0000', NULL, 4, '', '2016-05-25 12:19:28', NULL, 1),
(37, '2016-05-25 09:53:18', 1, '2000.0000', 'close', '19000.0000', NULL, 2, '37500.0000', NULL, 2, '', '2016-05-25 09:57:13', NULL, 1),
(38, '2016-05-25 09:57:17', 1, '0.0000', 'close', '3500.0000', NULL, 2, '34500.0000', NULL, 2, '', '2016-05-25 14:28:14', NULL, 1),
(39, '2016-05-25 11:16:49', 2, '5000000.0000', 'close', '5000000.0000', NULL, 0, '0.0000', NULL, 0, '', '2016-05-25 11:17:00', NULL, 2),
(40, '2016-05-25 11:17:06', 2, '2000.0000', 'close', '3000.0000', NULL, 2, '7000.0000', NULL, 2, '', '2016-05-25 11:19:52', NULL, 2),
(41, '2016-05-25 11:20:01', 2, '10000.0000', 'close', '33000.0000', NULL, 3, '2500.0000', NULL, 3, '', '2016-06-06 15:03:46', NULL, 2),
(42, '2016-05-25 14:28:22', 1, '1000.0000', 'close', '104000.0000', NULL, 6, '40000.0000', NULL, 6, '', '2016-06-06 15:00:39', NULL, 1),
(43, '2016-06-06 15:00:46', 1, '1000.0000', 'close', '32000.0000', NULL, 0, '27223.0000', NULL, 0, '', '2016-06-24 12:08:29', NULL, 1),
(44, '2016-06-06 15:03:53', 2, '1000.0000', 'close', '3500.0000', NULL, 0, '3000.0000', NULL, 0, '', '2016-06-06 15:04:49', NULL, 2),
(45, '2016-06-07 07:55:49', 2, '1000.0000', 'close', '3500.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-06-07 07:58:20', NULL, 2),
(46, '2016-06-18 07:22:44', 2, '1000.0000', 'close', '8949.6800', NULL, 0, '5559.0000', NULL, 0, '', '2016-06-24 09:39:26', NULL, 2),
(47, '2016-06-24 09:41:01', 2, '3000.0000', 'close', '3000.0000', NULL, 0, '40.0000', NULL, 0, '', '2016-06-24 09:42:49', NULL, 2),
(48, '2016-06-24 09:47:47', 2, '4000.0000', 'close', '4000.0000', NULL, 0, '52.0000', NULL, 0, '', '2016-06-24 09:50:02', NULL, 2),
(49, '2016-06-24 10:01:14', 2, '0.0000', 'close', '0.0000', NULL, 0, '131.0000', NULL, 0, '', '2016-06-24 10:12:34', NULL, 2),
(50, '2016-06-24 10:17:38', 2, '137.0000', 'close', '137.0000', NULL, 0, '1.0000', NULL, 0, '', '2016-06-24 10:32:53', NULL, 2),
(51, '2016-06-24 10:34:17', 2, '140.0000', 'close', '140.0000', NULL, 0, '10.0000', NULL, 0, '', '2016-06-24 10:34:38', NULL, 2),
(52, '2016-06-24 10:36:43', 2, '200.0000', 'close', '200.0000', NULL, 0, '50.0000', NULL, 0, '', '2016-06-24 10:37:21', NULL, 2),
(53, '2016-06-24 10:38:06', 2, '300.0000', 'close', '300.0000', NULL, 0, '20.0000', NULL, 0, '', '2016-06-24 10:38:36', NULL, 2),
(54, '2016-06-24 11:10:02', 2, '230.0000', 'close', '230.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-06-24 11:40:31', NULL, 2),
(55, '2016-06-24 11:13:33', 2, '5678.0000', 'close', '5678.0000', NULL, 0, '426.0000', NULL, 0, '', '2016-06-24 11:41:53', NULL, 2),
(56, '2016-06-24 11:14:52', 2, '12312.0000', 'close', '12312.0000', NULL, 0, '64846.0000', NULL, 0, '', '2016-06-24 11:42:11', NULL, 2),
(57, '2016-06-24 11:15:21', 2, '1243.0000', 'close', '1243.0000', NULL, 0, '34.0000', NULL, 0, '', '2016-06-24 11:51:21', NULL, 2),
(58, '2016-06-24 11:19:05', 2, '3.0000', 'close', '3.0000', NULL, 0, '324.0000', NULL, 0, '', '2016-06-24 11:53:58', NULL, 2),
(59, '2016-06-24 11:22:14', 2, '3.0000', 'close', '3.0000', NULL, 0, '120.0000', NULL, 0, '', '2016-06-24 11:57:36', NULL, 2),
(60, '2016-06-24 11:22:40', 2, '5.0000', 'close', '5.0000', NULL, 0, '2295.0000', NULL, 0, '', '2016-06-24 12:02:35', NULL, 2),
(61, '2016-06-24 11:23:46', 2, '23.0000', 'close', '23.0000', NULL, 0, '1345.0000', NULL, 0, '', '2016-06-24 12:12:10', NULL, 2),
(62, '2016-06-24 12:15:19', 2, '0.0000', 'close', '0.0000', NULL, 0, '300.0000', NULL, 0, '', '2016-06-24 12:40:19', NULL, 2),
(63, '2016-06-24 12:40:30', 2, '500.0000', 'close', '500.0000', NULL, 0, '2000.0000', NULL, 0, '', '2016-06-24 12:47:36', NULL, 2),
(64, '2016-06-24 12:40:48', 2, '4.0000', 'close', '4.0000', NULL, 0, '17000.0000', NULL, 0, '', '2016-06-27 05:29:00', NULL, 2),
(65, '2016-06-24 12:40:57', 2, '300.0000', 'close', '300.0000', NULL, 0, '400.0000', NULL, 0, '', '2016-06-27 05:30:42', NULL, 2),
(66, '2016-06-24 12:42:14', 2, '400.0000', 'close', '400.0000', NULL, 0, '5000.0000', NULL, 0, '', '2016-06-27 05:33:24', NULL, 2),
(67, '2016-06-24 12:42:23', 2, '200.0000', 'close', '200.0000', NULL, 0, '2000.0000', NULL, 0, '', '2016-06-27 06:04:07', NULL, 2),
(68, '2016-06-24 12:44:48', 10, '400.0000', 'close', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, '2016-06-27 05:34:29', 10, '1500.0000', 'close', '1500.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-06-27 05:35:06', NULL, 10),
(70, '2016-06-27 06:06:22', 2, '2000.0000', 'close', '2000.0000', NULL, 0, '3000.0000', NULL, 0, '', '2016-06-27 06:07:15', NULL, 2),
(71, '2016-06-27 06:10:37', 2, '3000.0000', 'close', '3000.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-06-28 09:10:37', NULL, 2),
(72, '2016-06-28 09:10:43', 2, '1000.0000', 'close', '4500.0000', NULL, 2, '12000.0000', NULL, 2, '', '2016-06-28 12:49:16', NULL, 2),
(73, '2016-06-28 12:49:21', 2, '1000.0000', 'close', '2000.0000', NULL, 1, '5500.0000', NULL, 1, '', '2016-06-28 12:54:41', NULL, 2),
(74, '2016-06-28 12:54:46', 2, '1000.0000', 'close', '71000.0000', NULL, 21, '145500.0000', NULL, 21, '', '2016-07-05 12:15:12', NULL, 2),
(75, '2016-06-29 04:54:03', 1, '2723.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, '2016-07-05 12:30:29', 2, '2000.0000', 'close', '2000.0000', NULL, 0, '4500.0000', NULL, 0, '', '2016-07-05 12:31:10', NULL, 2),
(77, '2016-07-05 12:31:17', 2, '1000.0000', 'close', '15078.0000', NULL, 1, '4815.0000', NULL, 1, '', '2016-07-05 13:09:34', NULL, 2),
(78, '2016-07-05 13:11:49', 2, '0.0000', 'close', '30350.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-07-05 13:42:50', NULL, 2),
(79, '2016-07-05 13:42:54', 2, '1000.0000', 'close', '2000.0000', NULL, 1, '1000.0000', NULL, 1, '', '2016-07-06 05:55:00', NULL, 2),
(80, '2016-07-06 06:07:35', 2, '0.0000', 'close', '-100.0000', NULL, 0, '900.0000', NULL, 0, '', '2016-07-06 06:17:23', NULL, 2),
(81, '2016-07-06 06:21:57', 2, '1000.0000', 'close', '800.0000', NULL, 0, '800.0000', NULL, 0, '', '2016-07-06 06:27:09', NULL, 2),
(82, '2016-07-06 06:29:00', 2, '1000.0000', 'close', '900.0000', NULL, 0, '900.0000', NULL, 0, '', '2016-07-06 06:29:30', NULL, 2),
(83, '2016-07-06 06:31:52', 2, '1000.0000', 'close', '800.0000', NULL, 0, '800.0000', NULL, 0, '', '2016-07-06 06:32:21', NULL, 2),
(84, '2016-07-06 06:33:06', 2, '800.0000', 'close', '1300.0000', NULL, 1, '3000.0000', NULL, 1, '', '2016-07-06 06:40:34', NULL, 2),
(85, '2016-07-06 06:42:43', 2, '500.0000', 'close', '12400.0000', NULL, 0, '12400.0000', NULL, 0, '', '2016-07-06 06:58:14', NULL, 2),
(86, '2016-07-06 06:59:21', 2, '12400.0000', 'close', '12400.0000', NULL, 1, '23400.0000', NULL, 1, '', '2016-07-06 07:05:04', NULL, 2),
(87, '2016-07-06 07:06:32', 2, '12400.0000', 'close', '26900.0000', NULL, 0, '1000.0000', NULL, 0, '', '2016-07-07 05:13:58', NULL, 2),
(88, '2016-07-07 05:39:10', 2, '1000.0000', 'close', '3000.0000', NULL, 1, '11000.0000', NULL, 1, '', '2016-07-07 05:49:21', NULL, 2),
(89, '2016-07-07 05:50:37', 2, '3000.0000', 'open', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.0.1.21',
  `order_discount` int(11) NOT NULL DEFAULT '0',
  `max_pan_limit` int(11) NOT NULL,
  `logout_after_payment` tinyint(1) NOT NULL,
  `discount_type` tinyint(1) DEFAULT NULL,
  `order_discount_type` varchar(20) NOT NULL,
  `cv_expiry` int(5) NOT NULL,
  `cv_grace_period` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_settings`
--

INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`, `order_discount`, `max_pan_limit`, `logout_after_payment`, `discount_type`, `order_discount_type`, `cv_expiry`, `cv_grace_period`) VALUES
(1, 22, 20, 1, 1, 3, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', 1, 'BIXOLON SRP-350II, BIXOLON SRP-350II', 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.0.1.21', 10, 100000, 0, 3, 'percent', 60, 10);

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '20.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `max_discount` int(11) NOT NULL,
  `serialized` tinyint(1) NOT NULL DEFAULT '0',
  `serial_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_products`
--

INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `max_discount`, `serialized`, `serial_number`) VALUES
(19, '231321', 'iphone', '10', '3000.0000', '3500.0000', '5.0000', 'no_image.png', 1, 0, '', '', '', '', '', '', '100.0000', 2, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 0),
(20, '123456', 'Lumia 520', '10', '2000.0000', '2500.0000', '0.0000', 'no_image.png', 1, 0, '', '', '', '', '', '', '103.0000', 2, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 0),
(21, '1231', 'LG E', '10', '4000.0000', '4500.0000', '5.0000', 'no_image.png', 1, 0, '', '', '', '', '', '', '8.0000', 2, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 0),
(22, '34232432', 'Vivo S2', '2', '10000.0000', '12000.0000', '5.0000', 'no_image.png', 1, 0, '', '', '', '', '', '', '106.0000', 2, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 0),
(24, 'S234556', 'Galaxy S4', '1', '20000.0000', '25000.0000', '5.0000', 'no_image.png', 1, 0, '', '', '', '', '', '', '10.0000', 2, 1, '', NULL, 'code39', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 0),
(25, 'STR123', 'straps', '1', '0.0000', '0.0000', '5.0000', 'no_image.png', 5, 0, '', '', '', '', '', '', '70.0000', 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0),
(26, 'BTR123', 'batteries', '1', '0.0000', '0.0000', '5.0000', 'no_image.png', 5, 0, '', '', '', '', '', '', '100.0000', 1, 1, '', NULL, 'code128', NULL, '', 0, 'standard', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_product_photos`
--

INSERT INTO `sma_product_photos` (`id`, `product_id`, `photo`) VALUES
(1, 2, '771a9e7f52af4bccad1154245833ed0a.jpg'),
(2, 3, 'f238b0263804753b84fe36a500156563.jpg'),
(3, 5, '0f3252d29e5226e2c0f81f97f1bc23f1.jpg'),
(4, 6, '8a158ac936efb721556fd0d40980bc24.jpg'),
(5, 7, 'c401456d54cb8c8a8d65545b46d88511.jpg'),
(6, 10, '6ac46d6e7b8a1721c655b66b38170d27.jpg'),
(7, 11, '896bde4e01782524a21496552585a3a7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_purchases`
--

INSERT INTO `sma_purchases` (`id`, `reference_no`, `date`, `supplier_id`, `supplier`, `warehouse_id`, `note`, `total`, `product_discount`, `order_discount_id`, `order_discount`, `total_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `paid`, `status`, `payment_status`, `created_by`, `updated_by`, `updated_at`, `attachment`) VALUES
(5, '456547547', '2016-05-12 16:30:36', 2, 'Supplier Company Name', 1, '', '9433.9600', '0.0000', NULL, '0.0000', '0.0000', '566.0400', 1, '0.0000', '566.0400', '0.0000', '10000.0000', '0.0000', 'received', 'pending', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_purchase_items`
--

INSERT INTO `sma_purchase_items` (`id`, `purchase_id`, `transfer_id`, `product_id`, `product_code`, `product_name`, `option_id`, `net_unit_cost`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `expiry`, `subtotal`, `quantity_balance`, `date`, `status`, `unit_cost`, `real_unit_cost`) VALUES
(1, NULL, NULL, 1, '', '', NULL, '0.0000', '0.0000', 1, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', '5.0000', '0000-00-00', '', NULL, NULL),
(2, NULL, NULL, 2, '22345678', 'iphone 6', NULL, '45454.5455', '5.0000', 1, '22727.2727', 2, '10.0000%', NULL, NULL, NULL, '250000.0000', '35.0000', '2016-04-06', 'received', '50000.0000', NULL),
(3, NULL, NULL, 2, '22345678', 'iphone 6', NULL, '45454.5455', '5.0000', 2, '22727.2727', 2, '10.0000%', NULL, NULL, NULL, '250000.0000', '0.0000', '2016-04-06', 'received', '50000.0000', NULL),
(4, NULL, NULL, 3, '673267326', 'Sony Bravia', NULL, '10000.0000', '5.0000', 1, '5000.0000', 2, '10.0000%', NULL, NULL, NULL, '55000.0000', '5.0000', '2016-04-08', 'received', '11000.0000', NULL),
(5, NULL, NULL, 3, '673267326', 'Sony Bravia', NULL, '10000.0000', '5.0000', 2, '5000.0000', 2, '10.0000%', NULL, NULL, NULL, '55000.0000', '0.0000', '2016-04-08', 'received', '11000.0000', NULL),
(6, NULL, NULL, 4, '3221321', 'LG e', NULL, '1818.1818', '20.0000', 1, '3636.3636', 2, '10.0000%', NULL, NULL, NULL, '40000.0000', '20.0000', '2016-04-13', 'received', '2000.0000', NULL),
(7, NULL, NULL, 4, '3221321', 'LG e', NULL, '1818.1818', '10.0000', 2, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '0.0000', '2016-04-13', 'received', '2000.0000', NULL),
(8, NULL, NULL, 5, '21131', 'iphone', NULL, '1818.1818', '10.0000', 1, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '10.0000', '2016-04-15', 'received', '2000.0000', NULL),
(9, NULL, NULL, 5, '21131', 'iphone', NULL, '1818.1818', '10.0000', 2, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '0.0000', '2016-04-15', 'received', '2000.0000', NULL),
(10, NULL, NULL, 6, '2322312', 'iphone', NULL, '1818.1818', '20.0000', 1, '3636.3636', 2, '10.0000%', NULL, NULL, NULL, '40000.0000', '20.0000', '2016-04-15', 'received', '2000.0000', NULL),
(11, NULL, NULL, 6, '2322312', 'iphone', NULL, '1818.1818', '20.0000', 2, '3636.3636', 2, '10.0000%', NULL, NULL, NULL, '40000.0000', '4.0000', '2016-04-15', 'received', '2000.0000', NULL),
(12, NULL, NULL, 7, '21212121', 'iphone', NULL, '1818.1818', '10.0000', 1, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '5.0000', '2016-04-16', 'received', '2000.0000', NULL),
(13, NULL, NULL, 7, '21212121', 'iphone', NULL, '1818.1818', '10.0000', 2, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '4.0000', '2016-04-16', 'received', '2000.0000', NULL),
(14, NULL, NULL, 8, '1212114', 'LG E', NULL, '10000.0000', '20.0000', 1, '20000.0000', 2, '10.0000%', NULL, NULL, NULL, '220000.0000', '-85.0000', '2016-04-20', 'received', '11000.0000', NULL),
(15, NULL, NULL, 8, '1212114', 'LG E', NULL, '10000.0000', '20.0000', 2, '20000.0000', 2, '10.0000%', NULL, NULL, NULL, '220000.0000', '0.0000', '2016-04-20', 'received', '11000.0000', NULL),
(16, NULL, NULL, 9, 'E3134', 'Micromax convas express2', NULL, '4545.4545', '10.0000', 1, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '10.0000', '2016-04-22', 'received', '5000.0000', NULL),
(17, NULL, NULL, 9, 'E3134', 'Micromax convas express2', NULL, '4545.4545', '10.0000', 2, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '10.0000', '2016-04-22', 'received', '5000.0000', NULL),
(18, NULL, NULL, 10, '231321321', 'iphone', NULL, '45454.5455', '10.0000', 1, '45454.5455', 2, '10.0000%', NULL, NULL, NULL, '500000.0000', '10.0000', '2016-04-25', 'received', '50000.0000', NULL),
(19, NULL, NULL, 10, '231321321', 'iphone', NULL, '45454.5455', '10.0000', 2, '45454.5455', 2, '10.0000%', NULL, NULL, NULL, '500000.0000', '10.0000', '2016-04-25', 'received', '50000.0000', NULL),
(20, NULL, NULL, 11, '231321321', 'iphone', NULL, '45454.5455', '10.0000', 1, '45454.5455', 2, '10.0000%', NULL, NULL, NULL, '500000.0000', '-12.0000', '2016-04-25', 'received', '50000.0000', NULL),
(21, NULL, NULL, 11, '231321321', 'iphone', NULL, '45454.5455', '10.0000', 2, '45454.5455', 2, '10.0000%', NULL, NULL, NULL, '500000.0000', '4.0000', '2016-04-25', 'received', '50000.0000', NULL),
(22, NULL, NULL, 12, '', '', NULL, '0.0000', '0.0000', 1, '0.0000', NULL, NULL, NULL, NULL, NULL, '0.0000', '-15.0000', '0000-00-00', '', NULL, NULL),
(29, 5, NULL, 8, '1212114', 'LG E', NULL, '9433.9600', '1.0000', 1, '566.0400', 3, '6.0000%', '0', '0.0000', NULL, '10000.0000', '0.0000', '2016-05-12', 'received', '10000.0000', '10000.0000'),
(30, NULL, NULL, 11, '', '', 0, '0.0000', '0.0000', 2, NULL, NULL, NULL, NULL, NULL, NULL, '0.0000', '1.0000', '0000-00-00', '', NULL, NULL),
(31, NULL, NULL, 14, '321321321', 'canvas pro', NULL, '4545.4545', '10.0000', 1, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '-36.0000', '2016-05-21', 'received', '5000.0000', NULL),
(32, NULL, NULL, 14, '321321321', 'canvas pro', NULL, '4545.4545', '10.0000', 2, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '10.0000', '2016-05-21', 'received', '5000.0000', NULL),
(33, NULL, NULL, 15, '2332323', 'canvas pro', NULL, '1818.1818', '10.0000', 1, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '-1.0000', '2016-05-21', 'received', '2000.0000', NULL),
(34, NULL, NULL, 15, '2332323', 'canvas pro', NULL, '1818.1818', '10.0000', 2, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '10.0000', '2016-05-21', 'received', '2000.0000', NULL),
(35, NULL, NULL, 16, '23243', 'Lumia 520', NULL, '4545.4545', '10.0000', 1, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '-66.0000', '2016-05-21', 'received', '5000.0000', NULL),
(36, NULL, NULL, 16, '23243', 'Lumia 520', NULL, '4545.4545', '10.0000', 2, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '10.0000', '2016-05-21', 'received', '5000.0000', NULL),
(37, NULL, NULL, 18, '21313213', 'iphone', NULL, '1818.1818', '5.0000', 1, '909.0909', 2, '10.0000%', NULL, NULL, NULL, '10000.0000', '-11.0000', '2016-05-21', 'received', '2000.0000', NULL),
(38, NULL, NULL, 18, '21313213', 'iphone', NULL, '1818.1818', '5.0000', 2, '909.0909', 2, '10.0000%', NULL, NULL, NULL, '10000.0000', '5.0000', '2016-05-21', 'received', '2000.0000', NULL),
(39, NULL, NULL, 19, '23123212', 'iphone', NULL, '4545.4545', '10.0000', 1, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '0.0000', '2016-05-21', 'received', '5000.0000', NULL),
(40, NULL, NULL, 19, '23123212', 'iphone', NULL, '4545.4545', '10.0000', 2, '4545.4545', 2, '10.0000%', NULL, NULL, NULL, '50000.0000', '100.0000', '2016-05-21', 'received', '5000.0000', NULL),
(41, NULL, NULL, 20, '1213213', 'Lumia 520', NULL, '1818.1818', '10.0000', 1, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '99.0000', '2016-05-23', 'received', '2000.0000', NULL),
(42, NULL, NULL, 20, '1213213', 'Lumia 520', NULL, '1818.1818', '10.0000', 2, '1818.1818', 2, '10.0000%', NULL, NULL, NULL, '20000.0000', '4.0000', '2016-05-23', 'received', '2000.0000', NULL),
(43, NULL, NULL, 21, '1231321313', 'LG E', NULL, '3636.3636', '10.0000', 1, '3636.3636', 2, '10.0000%', NULL, NULL, NULL, '40000.0000', '100.0000', '2016-05-24', 'received', '4000.0000', NULL),
(44, NULL, NULL, 21, '1231321313', 'LG E', NULL, '3636.3636', '10.0000', 2, '3636.3636', 2, '10.0000%', NULL, NULL, NULL, '40000.0000', '8.0000', '2016-05-24', 'received', '4000.0000', NULL),
(45, NULL, NULL, 22, '34232432', 'Vivo S2', NULL, '9090.9091', '10.0000', 1, '9090.9091', 2, '10.0000%', NULL, NULL, NULL, '100000.0000', '96.0000', '2016-05-24', 'received', '10000.0000', NULL),
(46, NULL, NULL, 22, '34232432', 'Vivo S2', NULL, '9090.9091', '10.0000', 2, '9090.9091', 2, '10.0000%', NULL, NULL, NULL, '100000.0000', '10.0000', '2016-05-24', 'received', '10000.0000', NULL),
(47, NULL, NULL, 24, 'S234556', 'Galaxy S4', NULL, '18181.8182', '10.0000', 1, '18181.8182', 2, '10.0000%', NULL, NULL, NULL, '200000.0000', '30.0000', '2016-06-06', 'received', '20000.0000', NULL),
(48, NULL, NULL, 24, 'S234556', 'Galaxy S4', NULL, '18181.8182', '10.0000', 2, '18181.8182', 2, '10.0000%', NULL, NULL, NULL, '200000.0000', '10.0000', '2016-06-06', 'received', '20000.0000', NULL),
(49, NULL, NULL, 25, 'STR123', 'straps', NULL, '0.0000', '50.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '48.0000', '2016-07-04', 'received', '0.0000', NULL),
(50, NULL, NULL, 25, 'STR123', 'straps', NULL, '0.0000', '50.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '50.0000', '2016-07-04', 'received', '0.0000', NULL),
(51, NULL, NULL, 26, 'BTR123', 'batteries', NULL, '0.0000', '50.0000', 1, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '50.0000', '2016-07-04', 'received', '0.0000', NULL),
(52, NULL, NULL, 26, 'BTR123', 'batteries', NULL, '0.0000', '50.0000', 2, '0.0000', 1, '0.0000', NULL, NULL, NULL, '0.0000', '50.0000', '2016-07-04', 'received', '0.0000', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_return_items`
--

INSERT INTO `sma_return_items` (`id`, `sale_id`, `return_id`, `sale_item_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`) VALUES
(1, 0, 1, 5, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000'),
(2, 0, 2, 259, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(3, 0, 3, 256, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(4, 0, 4, 250, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(5, 0, 5, 260, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(6, 0, 6, 262, 8, '1212114', 'LG E', 'standard', 0, '11000.0000', '1.0000', 1, '0.0000', 1, '0.0000', '500', '500.0000', '11000.0000', '', '12000.0000'),
(7, 0, 7, 254, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(8, 0, 7, 255, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(9, 0, 8, 197, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(10, 0, 8, 198, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(11, 0, 9, 238, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(12, 0, 10, 241, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000'),
(13, 0, 10, 242, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000'),
(14, 0, 11, 191, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(15, 0, 12, 243, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(16, 0, 13, 246, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(17, 0, 14, 219, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(18, 0, 14, 220, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(19, 0, 15, 217, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(20, 0, 15, 218, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(21, 0, 16, 237, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000'),
(22, 0, 17, 237, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000'),
(23, 0, 18, 236, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000'),
(24, 0, 19, 213, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(25, 0, 19, 214, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(26, 0, 20, 211, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(27, 0, 20, 212, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(28, 0, 21, 203, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(29, 0, 21, 204, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(30, 0, 22, 195, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(31, 0, 23, 190, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(32, 0, 24, 265, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(33, 0, 25, 420, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(34, 0, 25, 421, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(35, 0, 25, 422, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(36, 0, 25, 423, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(37, 0, 26, 487, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000'),
(38, 0, 27, 491, 22, '34232432', 'Vivo S2', 'standard', 0, '9909.0900', '1.0000', 1, '1090.9100', 2, '10.0000%', '1000', '1000.0000', '11000.0000', '', '12000.0000'),
(39, 0, 27, 492, 21, '1231321313', 'LG E', 'standard', 0, '3590.9100', '1.0000', 1, '409.0900', 2, '10.0000%', '500', '500.0000', '4000.0000', '', '4500.0000'),
(40, 0, 27, 493, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000'),
(41, 0, 28, 480, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(42, 0, 28, 481, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(43, 0, 29, 497, 21, '1231321313', 'LG E', 'standard', 0, '3590.9100', '1.0000', 1, '409.0900', 2, '10.0000%', '500', '500.0000', '4000.0000', '', '4500.0000'),
(44, 0, 29, 498, 20, '1213213', 'Lumia 520', 'standard', 0, '2072.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '200', '200.0000', '2300.0000', '', '2500.0000'),
(45, 0, 29, 499, 22, '34232432', 'Vivo S2', 'standard', 0, '9909.0900', '1.0000', 1, '1090.9100', 2, '10.0000%', '1000', '1000.0000', '11000.0000', '', '12000.0000'),
(46, 0, 30, 538, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(47, 0, 30, 539, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(48, 0, 31, 536, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(49, 0, 31, 537, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(50, 0, 32, 532, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '1.0000', 2, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000'),
(51, 0, 33, 546, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(52, 0, 34, 549, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(53, 0, 35, 550, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(54, 0, 36, 548, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(55, 0, 37, 545, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(56, 0, 38, 557, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(57, 0, 39, 578, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(58, 0, 40, 577, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(59, 0, 41, 571, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(60, 0, 41, 572, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(61, 0, 42, 573, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(62, 0, 42, 574, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(63, 0, 43, 575, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(64, 0, 43, 576, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(65, 0, 44, 565, 20, '123456', 'Lumia 520', 'standard', 0, '1772.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '500', '500.0000', '2000.0000', '', '2500.0000'),
(66, 0, 45, 560, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(67, 0, 46, 569, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(68, 0, 46, 570, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(69, 0, 47, 566, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(70, 0, 48, 563, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(71, 0, 48, 564, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(72, 0, 49, 556, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(73, 0, 50, 518, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '1.0000', 2, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000'),
(74, 0, 51, 567, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(75, 0, 51, 568, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(76, 0, 52, 558, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(77, 0, 53, 586, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(78, 0, 54, 585, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(79, 0, 55, 587, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(80, 0, 56, 580, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(81, 0, 57, 583, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(82, 0, 57, 584, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000'),
(83, 0, 58, 655, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000'),
(84, 0, 59, 657, 19, '231321', 'iphone', 'standard', 0, '3181.8200', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_sales`
--

CREATE TABLE `sma_return_sales` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `sales_reference_no` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_return_sales`
--

INSERT INTO `sma_return_sales` (`id`, `sale_id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `surcharge`, `grand_total`, `created_by`, `updated_by`, `updated_at`, `attachment`, `sales_reference_no`) VALUES
(1, 4, '2016-03-30 14:25:40', 'RETURNSL/2016/03/0001', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 2, NULL, NULL, NULL, ''),
(2, 111, '2016-05-02 06:21:00', 'RETURNSL/2016/05/0002', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(3, 109, '2016-05-19 07:21:00', 'RETURNSL/2016/05/0003', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '&lt;p&gt;hello world&lt;&sol;p&gt;', '11320.7500', '0.0000', NULL, '0.0000', NULL, '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(4, 105, '2016-05-19 07:25:00', 'RETURNSL/2016/05/0004', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(5, 112, '2016-05-20 09:13:00', 'RETURNSL/2016/05/0005', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '&lt;p&gt;wew&lt;&sol;p&gt;', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(6, 114, '2016-05-20 09:16:00', 'RETURNSL/2016/05/0006', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '&lt;p&gt;ewewrw&lt;&sol;p&gt;', '11000.0000', '500.0000', NULL, '500.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '11000.0000', 1, NULL, NULL, NULL, ''),
(7, 108, '2016-05-20 09:28:00', 'RETURNSL/2016/05/0007', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 1, NULL, NULL, NULL, ''),
(8, 80, '2016-05-20 10:36:00', 'RETURNSL/2016/05/0008', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '4545.4600', '0.0000', NULL, '0.0000', NULL, '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(9, 98, '2016-05-20 10:49:00', 'RETURNSL/2016/05/0009', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(10, 100, '2016-05-20 10:56:00', 'RETURNSL/2016/05/0010', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '100000.0000', '0.0000', NULL, '0.0000', '0.0000', '10000.0000', 1, '0.0000', '10000.0000', '0.0000', '110000.0000', 1, NULL, NULL, NULL, ''),
(11, 74, '2016-05-20 11:44:00', 'RETURNSL/2016/05/0011', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(12, 101, '2016-05-20 11:49:00', 'RETURNSL/2016/05/0012', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '&lt;p&gt;wee&lt;&sol;p&gt;', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(13, 103, '2016-05-20 11:58:00', 'RETURNSL/2016/05/0013', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '&lt;p&gt;wqewq&lt;&sol;p&gt;', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(14, 92, '2016-05-20 12:02:00', 'RETURNSL/2016/05/0014', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;ww&lt;&sol;p&gt;', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 1, NULL, NULL, NULL, ''),
(15, 91, '2016-05-20 12:04:00', 'RETURNSL/2016/05/0015', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;wewqe&lt;&sol;p&gt;', '22641.5000', '0.0000', NULL, '0.0000', NULL, '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 1, NULL, NULL, NULL, ''),
(16, 97, '2016-05-20 12:27:00', 'RETURNSL/2016/05/0016', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;wqewqeq&lt;&sol;p&gt;', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 1, NULL, NULL, NULL, ''),
(17, 97, '2016-05-20 12:27:00', 'RETURNSL/2016/05/0017', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;wqewqeq&lt;&sol;p&gt;', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 1, NULL, NULL, NULL, ''),
(18, 97, '2016-05-20 12:29:00', 'RETURNSL/2016/05/0018', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;qweq&lt;&sol;p&gt;', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 1, NULL, NULL, NULL, ''),
(19, 89, '2016-05-20 12:32:00', 'RETURNSL/2016/05/0019', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '4545.4600', '0.0000', NULL, '0.0000', NULL, '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(20, 88, '2016-05-20 12:36:00', 'RETURNSL/2016/05/0020', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '&lt;p&gt;ewdew&lt;&sol;p&gt;', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(21, 83, '2016-05-21 09:45:00', 'RETURNSL/2016/05/0021', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;ok&lt;&sol;p&gt;', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(22, 78, '2016-05-21 09:58:00', 'RETURNSL/2016/05/0022', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '&lt;p&gt;ok&lt;&sol;p&gt;', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(23, 73, '2016-05-21 10:16:00', 'RETURNSL/2016/05/0023', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '500.0000', '2000.0000', 1, NULL, NULL, NULL, ''),
(24, 200, '2016-05-21 12:29:00', 'RETURNSL/2016/05/0024', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 1, NULL, NULL, NULL, ''),
(25, 251, '2016-05-23 07:51:00', 'RETURNSL/2016/05/0025', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '&lt;p&gt;ok&lt;&sol;p&gt;', '14545.4600', '0.0000', NULL, '0.0000', '0.0000', '1454.5400', 1, '0.0000', '1454.5400', '0.0000', '16000.0000', 1, NULL, NULL, NULL, ''),
(26, 278, '2016-05-25 04:53:00', 'RETURNSL/2016/05/0026', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '&lt;p&gt;gift card added&lt;&sol;p&gt;', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 1, NULL, NULL, NULL, ''),
(27, 280, '2016-05-25 05:30:00', 'RETURNSL/2016/05/0027', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '17590.9100', '1500.0000', NULL, '1500.0000', '0.0000', '1909.0900', 1, '0.0000', '1909.0900', '0.0000', '19500.0000', 1, NULL, NULL, NULL, ''),
(28, 272, '2016-05-25 05:33:00', 'RETURNSL/2016/05/0028', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, '&lt;p&gt;shot&lt;&sol;p&gt;', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 1, NULL, NULL, NULL, ''),
(29, 283, '2016-05-25 06:27:00', 'RETURNSL/2016/05/0029', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '&lt;p&gt;dew&lt;&sol;p&gt;', '15572.7300', '1700.0000', NULL, '1700.0000', '0.0000', '1727.2700', 1, '0.0000', '1727.2700', '0.0000', '17300.0000', 1, NULL, NULL, NULL, ''),
(30, 315, '2016-06-02 05:41:00', 'RETURNSL/2016/06/0030', 58, 'w3schools', 5, 'Swatch Mumbai', 2, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(31, 314, '2016-06-02 06:05:00', 'RETURNSL/2016/06/0031', 55, 'debopriyo', 4, 'Swatch delhi', 1, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 1, NULL, NULL, NULL, ''),
(32, 310, '2016-06-02 07:01:00', 'RETURNSL/2016/06/0032', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 1, NULL, NULL, NULL, ''),
(33, 319, '2016-06-02 12:14:00', 'RETURNSL/2016/06/0033', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(34, 322, '2016-06-02 12:17:00', 'RETURNSL/2016/06/0034', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(35, 323, '2016-06-03 04:43:00', 'RETURNSL/2016/06/0035', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(36, 321, '2016-06-03 04:50:00', 'RETURNSL/2016/06/0036', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(37, 318, '2016-06-03 04:52:00', 'RETURNSL/2016/06/0037', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(38, 330, '2016-06-06 07:41:00', 'RETURNSL/2016/06/0038', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(39, 345, '2016-06-16 05:26:00', 'RETURNSL/2016/06/0039', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(40, 344, '2016-06-16 05:28:00', 'RETURNSL/2016/06/0040', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(41, 341, '2016-06-16 06:05:00', 'SALE/POS/2016/06/0344', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(42, 342, '2016-06-16 06:10:00', 'RETURNSL/2016/06/0041', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '2', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(43, 343, '2016-06-16 06:21:00', 'RETURNSL/2016/06/0042', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '1', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, ''),
(44, 337, '2016-06-16 06:36:00', 'RETURNSL/2016/06/0043', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '1772.7300', '500.0000', NULL, '500.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2000.0000', 1, NULL, NULL, NULL, ''),
(45, 333, '2016-06-16 11:46:00', 'RETURNSL/2016/06/0044', 32, 'Dsf', 4, 'Swatch delhi', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(46, 340, '2016-06-17 07:29:00', 'RETURNSL/2016/06/0045', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '3', '7272.7300', '0.0000', NULL, '0.0000', NULL, '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 1, NULL, NULL, NULL, ''),
(47, 338, '2016-06-17 07:41:00', 'RETURNSL/2016/06/0046', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(48, 336, '2016-06-17 08:35:00', 'RETURNSL/2016/06/0047', 11, 'Adesh', 3, 'Test Biller', 1, '1', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 1, NULL, NULL, NULL, ''),
(49, 329, '2016-06-17 08:50:00', 'RETURNSL/2016/06/0048', 11, 'Adesh', 5, 'Swatch Mumbai', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(50, 297, '2016-06-17 08:55:00', 'RETURNSL/2016/06/0049', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, '1', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 1, NULL, NULL, NULL, ''),
(51, 339, '2016-06-17 08:57:00', 'RETURNSL/2016/06/0050', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, '1', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 1, NULL, NULL, NULL, ''),
(52, 331, '2016-06-17 09:15:00', 'RETURNSL/2016/06/0051', 9, 'Swatch Delhi', 3, 'Test Biller', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, ''),
(53, 352, '2016-06-17 10:50:00', 'RETURNSL/2016/06/0052', 63, 'ankit', 5, 'Swatch Mumbai', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, 'SALE/POS/2016/06/0352'),
(54, 351, '2016-06-17 11:19:00', 'RETURNSL/2016/06/0053', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, 'SALE/POS/2016/06/0351'),
(55, 353, '2016-06-18 05:37:00', 'RETURNSL/2016/06/0054', 63, 'ankit', 5, 'Swatch Mumbai', 1, '1', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 1, NULL, NULL, NULL, 'SALE/POS/2016/06/0353'),
(56, 347, '2016-06-18 05:42:00', 'RETURNSL/2016/06/0055', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, '1', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 1, NULL, NULL, NULL, 'SALE/POS/2016/06/0346'),
(57, 350, '2016-06-18 10:25:00', 'RETURNSL/2016/06/0056', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, '1', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 1, NULL, NULL, NULL, 'SALE/POS/2016/06/0350'),
(58, 407, '2016-07-05 09:21:31', 'RETURNSL/2016/07/0057', 63, 'ankit', 4, 'Swatch delhi', 1, '1', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 2, NULL, NULL, NULL, 'SALE/POS/2016/06/0407'),
(59, 409, '2016-07-05 11:11:29', 'RETURNSL/2016/07/0058', 63, 'ankit', 4, 'Swatch delhi', 1, '1', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 2, NULL, NULL, NULL, 'SALE/POS/2016/07/0409');

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` tinyint(4) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `sales_executive_id` int(11) NOT NULL,
  `invoice_discount_reason` varchar(50) NOT NULL,
  `pan_number` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sales`
--

INSERT INTO `sma_sales` (`id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `staff_note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `sale_status`, `payment_status`, `payment_term`, `due_date`, `created_by`, `updated_by`, `updated_at`, `total_items`, `pos`, `paid`, `return_id`, `surcharge`, `attachment`, `sales_executive_id`, `invoice_discount_reason`, `pan_number`) VALUES
(1, '2016-01-06 05:41:41', 'SALE/POS/2016/01/0001', 1, 'Walk-in Customer', 3, 'Test Biller', 1, 'sdcd', 'test', '909.0900', '0.0000', '15', '15.0000', '15.0000', '90.9100', 2, '98.5000', '189.4100', '0.0000', '1083.5000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1083.5000', NULL, '0.0000', NULL, 0, '', ''),
(2, '2016-01-28 08:08:11', 'SALE/POS/2016/01/0002', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'partial', 0, NULL, 1, NULL, NULL, 1, 1, '100.0000', NULL, '0.0000', NULL, 0, '', ''),
(3, '2016-03-14 15:10:26', 'SALE/POS/2016/03/0003', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 0, '', ''),
(4, '2016-03-14 15:30:53', 'SALE/POS/2016/03/0004', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '1000.0000', 1, '0.0000', NULL, 0, '', ''),
(5, '2016-03-14 15:31:05', 'SALE/POS/2016/03/0005', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'due', 0, NULL, 4, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 0, '', ''),
(6, '2016-03-14 15:35:52', 'SALE/POS/2016/03/0006', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '', '1818.1800', '0.0000', NULL, '0.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '2000.0000', NULL, '0.0000', NULL, 0, '', ''),
(7, '2016-03-14 15:36:04', 'SALE/POS/2016/03/0007', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, '', '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 0, '', ''),
(8, '2016-03-15 06:52:25', 'SALE/POS/2016/03/0008', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 0, '', ''),
(9, '2016-03-30 13:10:54', 'SALE/POS/2016/03/0009', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '', '1818.1800', '0.0000', NULL, '0.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 2, 1, '2000.0000', NULL, '0.0000', NULL, 0, '', ''),
(10, '2016-03-30 13:11:14', 'SALE/POS/2016/03/0010', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, '', '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 0, '', ''),
(11, '2016-04-02 07:54:56', 'SALE/POS/2016/04/0011', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '2727.2700', '0.0000', NULL, '0.0000', '0.0000', '272.7300', 1, '0.0000', '272.7300', '0.0000', '3000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 3, 1, '0.0000', NULL, '0.0000', NULL, 0, '', ''),
(12, '2016-04-02 07:55:44', 'SALE/POS/2016/04/0012', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, 'test', 'test1', '909.0900', '0.0000', NULL, '0.0000', '0.0000', '90.9100', 1, '0.0000', '90.9100', '0.0000', '1000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 0, '', ''),
(13, '2016-04-02 07:56:13', 'SALE/POS/2016/04/0013', 1, 'Walk-in Customer', 3, 'Test Biller', 1, '', '', '1818.1800', '0.0000', NULL, '0.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 0, '', ''),
(16, '2016-04-04 14:40:17', 'SALE/POS/2016/04/0016', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '2727.2700', '0.0000', NULL, '0.0000', '0.0000', '272.7300', 1, '0.0000', '272.7300', '0.0000', '3000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 3, 1, '3000.0000', NULL, '0.0000', NULL, 4, '', ''),
(17, '2016-04-04 14:42:28', 'SALE/POS/2016/04/0017', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(18, '2016-04-05 09:08:30', 'SALE/POS/2016/04/0018', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2727.2700', '0.0000', NULL, '0.0000', '0.0000', '272.7300', 1, '0.0000', '272.7300', '0.0000', '3000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '3000.0000', NULL, '0.0000', NULL, 5, '', ''),
(19, '2016-04-05 09:30:57', 'SALE/POS/2016/04/0019', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2727.2700', '0.0000', NULL, '0.0000', '0.0000', '272.7300', 1, '0.0000', '272.7300', '0.0000', '3000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '3000.0000', NULL, '0.0000', NULL, 5, '', ''),
(20, '2016-04-05 09:41:54', 'SALE/POS/2016/04/0020', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 5, '', ''),
(21, '2016-04-05 09:43:02', 'SALE/POS/2016/04/0021', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(22, '2016-04-05 10:00:34', 'SALE/POS/2016/04/0022', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(23, '2016-04-05 10:22:05', 'SALE/POS/2016/04/0023', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '7272.7200', '0.0000', NULL, '0.0000', '0.0000', '727.2800', 1, '0.0000', '727.2800', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 8, 1, '8000.0000', NULL, '0.0000', NULL, 5, '', ''),
(24, '2016-04-05 11:13:26', 'SALE/POS/2016/04/0024', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 5, '', ''),
(25, '2016-04-05 11:16:17', 'SALE/POS/2016/04/0025', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(26, '2016-04-05 11:51:44', 'SALE/POS/2016/04/0026', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 5, '', ''),
(27, '2016-04-05 11:55:30', 'SALE/POS/2016/04/0027', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(28, '2016-04-05 12:39:48', 'SALE/POS/2016/04/0028', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 5, '', ''),
(29, '2016-04-05 12:53:46', 'SALE/POS/2016/04/0029', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '1818.1800', '0.0000', NULL, '0.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '2000.0000', NULL, '0.0000', NULL, 5, '', ''),
(30, '2016-04-05 14:32:47', 'SALE/POS/2016/04/0030', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '1818.1800', '0.0000', NULL, '0.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '2000.0000', NULL, '0.0000', NULL, 5, '', ''),
(31, '2016-04-06 12:40:49', 'SALE/POS/2016/04/0031', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '500000.0000', '0.0000', NULL, '0.0000', '0.0000', '50000.0000', 1, '0.0000', '50000.0000', '0.0000', '550000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '550000.0000', NULL, '0.0000', NULL, 3, '', ''),
(32, '2016-04-08 08:04:46', 'SALE/POS/2016/04/0032', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '150000.0000', '0.0000', NULL, '0.0000', '0.0000', '15000.0000', 1, '0.0000', '15000.0000', '0.0000', '165000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 3, 1, '165000.0000', NULL, '0.0000', NULL, 3, '', ''),
(33, '2016-04-08 09:42:40', 'SALE/POS/2016/04/0033', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '54545.4500', '0.0000', NULL, '0.0000', '0.0000', '5454.5500', 1, '0.0000', '5454.5500', '0.0000', '60000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 6, 1, '60000.0000', NULL, '0.0000', NULL, 3, '', ''),
(34, '2016-04-08 09:44:24', 'SALE/POS/2016/04/0034', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 7, 1, '169000.0000', NULL, '0.0000', NULL, 3, '', ''),
(35, '2016-04-08 09:53:15', 'SALE/POS/2016/04/0035', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 4, 1, '4000.0000', NULL, '0.0000', NULL, 3, '', ''),
(36, '2016-04-08 09:54:57', 'SALE/POS/2016/04/0036', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(37, '2016-04-08 10:03:13', 'SALE/POS/2016/04/0037', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '200000.0000', '0.0000', NULL, '0.0000', '0.0000', '20000.0000', 1, '0.0000', '20000.0000', '0.0000', '220000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 4, 1, '220000.0000', NULL, '0.0000', NULL, 5, '', ''),
(38, '2016-04-08 10:06:38', 'SALE/POS/2016/04/0038', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(39, '2016-04-08 10:10:28', 'SALE/POS/2016/04/0039', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '50909.0900', '0.0000', NULL, '0.0000', '0.0000', '5090.9100', 1, '0.0000', '5090.9100', '0.0000', '56000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '56000.0000', NULL, '0.0000', NULL, 4, '', ''),
(40, '2016-04-08 10:12:46', 'SALE/POS/2016/04/0040', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(41, '2016-04-08 10:16:19', 'SALE/POS/2016/04/0041', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '250000.0000', '0.0000', NULL, '0.0000', '0.0000', '25000.0000', 1, '0.0000', '25000.0000', '0.0000', '275000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '275000.0000', NULL, '0.0000', NULL, 4, '', ''),
(42, '2016-04-12 08:00:44', 'SALE/POS/2016/04/0042', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '250000.0000', '0.0000', NULL, '0.0000', '0.0000', '25000.0000', 1, '0.0000', '25000.0000', '0.0000', '275000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '275000.0000', NULL, '0.0000', NULL, 4, '', ''),
(43, '2016-04-12 08:02:33', 'SALE/POS/2016/04/0043', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(44, '2016-04-12 08:03:17', 'SALE/POS/2016/04/0044', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '4545.4500', '0.0000', NULL, '0.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 5, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(45, '2016-04-12 08:10:46', 'SALE/POS/2016/04/0045', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 5, '', ''),
(46, '2016-04-14 12:47:42', 'SALE/POS/2016/04/0046', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(47, '2016-04-14 13:34:43', 'SALE/POS/2016/04/0047', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(48, '2016-04-15 06:55:33', 'SALE/POS/2016/04/0048', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(49, '2016-04-15 07:29:21', 'SALE/POS/2016/04/0049', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '7500.0000', NULL, '0.0000', NULL, 5, '', ''),
(50, '2016-04-15 07:30:28', 'SALE/POS/2016/04/0050', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(51, '2016-04-15 08:03:35', 'SALE/POS/2016/04/0051', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '13636.3600', '0.0000', NULL, '0.0000', '0.0000', '1363.6400', 1, '0.0000', '1363.6400', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(52, '2016-04-15 08:04:28', 'SALE/POS/2016/04/0052', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '13636.3600', '0.0000', NULL, '0.0000', '0.0000', '1363.6400', 1, '0.0000', '1363.6400', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(53, '2016-04-15 09:24:22', 'SALE/POS/2016/04/0053', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '13636.3600', '0.0000', NULL, '0.0000', '0.0000', '1363.6400', 1, '0.0000', '1363.6400', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(54, '2016-04-15 09:27:07', 'SALE/POS/2016/04/0054', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '13636.3600', '0.0000', NULL, '0.0000', '0.0000', '1363.6400', 1, '0.0000', '1363.6400', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(55, '2016-04-15 09:29:39', 'SALE/POS/2016/04/0055', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '13636.3600', '0.0000', NULL, '0.0000', '0.0000', '1363.6400', 1, '0.0000', '1363.6400', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(56, '2016-04-15 09:33:49', 'SALE/POS/2016/04/0056', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(57, '2016-04-15 09:44:29', 'SALE/POS/2016/04/0057', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(58, '2016-04-15 09:49:07', 'SALE/POS/2016/04/0058', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '7500.0000', NULL, '0.0000', NULL, 5, '', ''),
(59, '2016-04-15 09:50:37', 'SALE/POS/2016/04/0059', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(60, '2016-04-15 09:52:25', 'SALE/POS/2016/04/0060', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(61, '2016-04-15 10:05:21', 'SALE/POS/2016/04/0061', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(62, '2016-04-15 11:58:46', 'SALE/POS/2016/04/0062', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(63, '2016-04-15 12:06:18', 'SALE/POS/2016/04/0063', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 5, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 5, '', ''),
(64, '2016-04-15 12:14:08', 'SALE/POS/2016/04/0064', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(65, '2016-04-15 13:07:45', 'SALE/POS/2016/04/0065', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(66, '2016-04-15 13:09:02', 'SALE/POS/2016/04/0066', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(67, '2016-04-15 13:10:54', 'SALE/POS/2016/04/0067', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 5, '', ''),
(68, '2016-04-15 13:13:06', 'SALE/POS/2016/04/0068', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(69, '2016-04-15 13:28:11', 'SALE/POS/2016/04/0069', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(70, '2016-04-15 13:30:06', 'SALE/POS/2016/04/0070', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(71, '2016-04-15 13:46:29', 'SALE/POS/2016/04/0071', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(72, '2016-04-15 13:52:16', 'SALE/POS/2016/04/0072', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(75, '2016-04-15 15:06:28', 'SALE/POS/2016/04/0075', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(76, '2016-04-15 15:12:43', 'SALE/POS/2016/04/0076', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(77, '2016-04-15 15:16:12', 'SALE/POS/2016/04/0077', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(79, '2016-04-16 09:17:56', 'SALE/POS/2016/04/0079', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(81, '2016-04-16 12:31:09', 'SALE/POS/2016/04/0081', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', '1000', '1000.0000', '1000.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '1500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '1500.0000', NULL, '0.0000', NULL, 3, '', ''),
(82, '2016-04-18 11:28:13', 'SALE/POS/2016/04/0082', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '7500.0000', NULL, '0.0000', NULL, 5, '', ''),
(84, '2016-04-19 12:42:29', 'SALE/POS/2016/04/0084', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '4545.4600', '0.0000', '22', '22.0000', '22.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '4978.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '4978.0000', NULL, '0.0000', NULL, 5, '', ''),
(85, '2016-04-20 09:27:53', 'SALE/POS/2016/04/0085', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(86, '2016-04-20 11:47:58', 'SALE/POS/2016/04/0086', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(87, '2016-04-20 11:54:48', 'SALE/POS/2016/04/0087', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '2045.4500', '250.0000', NULL, '250.0000', '0.0000', '204.5500', 3, '135.0000', '339.5500', '0.0000', '2385.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2385.0000', NULL, '0.0000', NULL, 3, '', ''),
(90, '2016-04-21 10:43:15', 'SALE/POS/2016/04/0090', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '22641.5000', '0.0000', '1001', '1001.0000', '1001.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '22999.0000', 'completed', 'due', 0, NULL, 2, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 4, 'saasa', ''),
(93, '2016-04-25 11:44:45', 'SALE/POS/2016/04/0093', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '33962.2500', '0.0000', NULL, '0.0000', '0.0000', '2037.7500', 1, '0.0000', '2037.7500', '0.0000', '36000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 3, 1, '36000.0000', NULL, '0.0000', NULL, 5, '', ''),
(94, '2016-04-25 12:09:43', 'SALE/POS/2016/04/0094', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '56603.7500', '0.0000', NULL, '0.0000', '0.0000', '3396.2500', 1, '0.0000', '3396.2500', '0.0000', '60000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '60000.0000', NULL, '0.0000', NULL, 5, '', ''),
(95, '2016-04-25 12:20:30', 'SALE/POS/2016/04/0095', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '56603.7500', '0.0000', NULL, '0.0000', '0.0000', '3396.2500', 1, '0.0000', '3396.2500', '0.0000', '60000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 5, 1, '60000.0000', NULL, '0.0000', NULL, 5, '', ''),
(96, '2016-04-25 12:57:30', 'SALE/POS/2016/04/0096', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '100000.0000', '0.0000', NULL, '0.0000', '0.0000', '10000.0000', 1, '0.0000', '10000.0000', '0.0000', '110000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '110000.0000', NULL, '0.0000', NULL, 5, '', ''),
(99, '2016-04-25 13:28:07', 'SALE/POS/2016/04/0099', 1, 'Walk-in Customer', 5, 'Swatch Mumbai', 2, NULL, '', '100000.0000', '0.0000', NULL, '0.0000', '0.0000', '10000.0000', 1, '0.0000', '10000.0000', '0.0000', '110000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '110000.0000', NULL, '0.0000', NULL, 5, '', 'MMWPK4413K'),
(102, '2016-04-25 14:29:13', 'SALE/POS/2016/04/0102', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '100000.0000', '0.0000', NULL, '0.0000', '0.0000', '10000.0000', 1, '0.0000', '10000.0000', '0.0000', '110000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '110000.0000', NULL, '0.0000', NULL, 3, '', 'MMWER5623O'),
(104, '2016-04-25 15:09:47', 'SALE/POS/2016/04/0104', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '111320.7500', '0.0000', NULL, '0.0000', '0.0000', '10679.2500', 1, '0.0000', '10679.2500', '0.0000', '122000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 3, 1, '122000.0000', NULL, '0.0000', NULL, 3, '', 'DDWPK3314K'),
(106, '2016-04-25 15:28:50', 'SALE/POS/2016/04/0106', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(107, '2016-04-25 15:33:09', 'SALE/POS/2016/04/0107', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '100000.0000', '0.0000', NULL, '0.0000', '0.0000', '10000.0000', 1, '0.0000', '10000.0000', '0.0000', '110000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '110000.0000', NULL, '0.0000', NULL, 3, '', 'DDWPK3314K'),
(110, '2016-04-30 10:09:14', 'SALE/POS/2016/04/0110', 1, 'Walk-in Customer', 3, 'Test Biller', 2, NULL, '', '21698.1100', '1000.0000', '300', '1300.0000', '300.0000', '1301.8900', 4, '4540.0000', '5841.8900', '0.0000', '27240.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '27240.0000', NULL, '0.0000', NULL, 3, '', ''),
(113, '2016-05-02 11:30:28', 'SALE/POS/2016/05/0113', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(115, '2016-05-05 11:16:12', 'SALE/POS/2016/05/0115', 1, 'Walk-in Customer', 3, 'Test Biller', 2, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(116, '2016-05-05 12:12:48', 'SALE/POS/2016/05/0116', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(117, '2016-05-05 12:29:07', 'SALE/POS/2016/05/0117', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(118, '2016-05-05 14:37:37', 'SALE/POS/2016/05/0118', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(119, '2016-05-05 15:42:11', 'SALE/POS/2016/05/0119', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(120, '2016-05-06 12:13:50', 'SALE/POS/2016/05/0120', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(121, '2016-05-06 12:33:47', 'SALE/POS/2016/05/0121', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(122, '2016-05-06 12:38:20', 'SALE/POS/2016/05/0122', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(123, '2016-05-06 12:40:28', 'SALE/POS/2016/05/0123', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'due', 0, NULL, 2, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 4, '', ''),
(124, '2016-05-09 12:04:02', 'SALE/POS/2016/05/0124', 4, 'Swatch delhi', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(125, '2016-05-09 12:05:13', 'SALE/POS/2016/05/0125', 4, 'Swatch delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(126, '2016-05-10 15:31:02', 'SALE/POS/2016/05/0126', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(127, '2016-05-11 07:25:16', 'SALE/POS/2016/05/0127', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(128, '2016-05-11 12:58:32', 'SALE/POS/2016/05/0128', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(129, '2016-05-11 13:16:46', 'SALE/POS/2016/05/0129', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(130, '2016-05-11 13:21:12', 'SALE/POS/2016/05/0130', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(131, '2016-05-13 12:46:17', 'SALE/POS/2016/05/0131', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(132, '2016-05-13 12:47:11', 'SALE/POS/2016/05/0132', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(133, '2016-05-13 13:00:42', 'SALE/POS/2016/05/0133', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(134, '2016-05-13 13:01:22', 'SALE/POS/2016/05/0134', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(135, '2016-05-13 14:01:01', 'SALE/POS/2016/05/0135', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'partial', 0, NULL, 5, NULL, NULL, 1, 1, '15000.0000', NULL, '0.0000', NULL, 5, '', ''),
(136, '2016-05-13 14:55:41', 'SALE/POS/2016/05/0136', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'partial', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(137, '2016-05-13 14:59:32', 'SALE/POS/2016/05/0137', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'partial', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(138, '2016-05-17 09:08:01', 'SALE/POS/2016/05/0138', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10377.3600', '1000.0000', '1200', '2200.0000', '1200.0000', '622.6400', 1, '0.0000', '622.6400', '0.0000', '9800.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '9800.0000', NULL, '0.0000', NULL, 3, '10 percent', ''),
(139, '2016-05-17 09:13:00', 'SALE/POS/2016/05/0139', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', '1296', '1296.0000', '1296.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '10704.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '10704.0000', NULL, '0.0000', NULL, 3, 'twelve hundred', ''),
(140, '2016-05-17 09:31:06', 'SALE/POS/2016/05/0140', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '20754.7100', '2000.0000', NULL, '2000.0000', '0.0000', '1245.2900', 1, '0.0000', '1245.2900', '0.0000', '22000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '22000.0000', NULL, '0.0000', NULL, 3, '', ''),
(141, '2016-05-17 13:48:53', 'SALE/POS/2016/05/0141', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(142, '2016-05-17 14:55:36', 'SALE/POS/2016/05/0142', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(143, '2016-05-18 08:08:11', 'SALE/POS/2016/05/0143', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(144, '2016-05-18 08:50:22', 'SALE/POS/2016/05/0144', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '0.0000', 1, '0.0000', '0.0000', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(145, '2016-05-19 13:16:12', 'SALE/POS/2016/05/0145', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(146, '2016-05-19 13:18:59', 'SALE/POS/2016/05/0146', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(147, '2016-05-19 13:27:06', 'SALE/POS/2016/05/0147', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'due', 0, NULL, 5, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 5, '', ''),
(148, '2016-05-19 13:28:33', 'SALE/POS/2016/05/0148', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'partial', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(149, '2016-05-19 13:56:00', 'SALE/POS/2016/05/0149', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 5, '', ''),
(150, '2016-05-19 14:34:08', 'SALE/POS/2016/05/0150', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(151, '2016-05-20 12:58:39', 'SALE/POS/2016/05/0151', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(152, '2016-05-21 09:08:19', 'SALE/POS/2016/05/0152', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '200000.0000', '0.0000', NULL, '0.0000', '0.0000', '20000.0000', 1, '0.0000', '20000.0000', '0.0000', '220000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 4, 1, '0.0000', NULL, '0.0000', NULL, 3, '', 'DDWPK3314K'),
(153, '2016-05-21 09:10:23', 'SALE/POS/2016/05/0153', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '250000.0000', '0.0000', NULL, '0.0000', '0.0000', '25000.0000', 1, '0.0000', '25000.0000', '0.0000', '275000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 5, 1, '0.0000', NULL, '0.0000', NULL, 3, '', 'DDWPK3314K'),
(154, '2016-05-21 09:11:02', 'SALE/POS/2016/05/0154', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(155, '2016-05-21 09:13:59', 'SALE/POS/2016/05/0155', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(156, '2016-05-21 09:16:30', 'SALE/POS/2016/05/0156', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(157, '2016-05-21 09:19:04', 'SALE/POS/2016/05/0157', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '24000.0000', NULL, '0.0000', NULL, 3, '', ''),
(158, '2016-05-21 09:23:23', 'SALE/POS/2016/05/0158', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '56603.7500', '0.0000', NULL, '0.0000', '0.0000', '3396.2500', 1, '0.0000', '3396.2500', '0.0000', '60000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 5, 1, '60000.0000', NULL, '0.0000', NULL, 3, '', ''),
(159, '2016-05-21 09:24:34', 'SALE/POS/2016/05/0159', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '67924.5000', '0.0000', NULL, '0.0000', '0.0000', '4075.5000', 1, '0.0000', '4075.5000', '0.0000', '72000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 6, 1, '72000.0000', NULL, '0.0000', NULL, 3, '', ''),
(160, '2016-05-21 09:26:02', 'SALE/POS/2016/05/0160', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '24000.0000', NULL, '0.0000', NULL, 3, '', ''),
(161, '2016-05-21 09:42:38', 'SALE/POS/2016/05/0161', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '56603.7500', '0.0000', NULL, '0.0000', '0.0000', '3396.2500', 1, '0.0000', '3396.2500', '0.0000', '60000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 5, 1, '60000.0000', NULL, '0.0000', NULL, 3, '', ''),
(162, '2016-05-21 09:43:55', 'SALE/POS/2016/05/0162', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(163, '2016-05-21 09:47:41', 'SALE/POS/2016/05/0163', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '24000.0000', NULL, '0.0000', NULL, 3, '', ''),
(164, '2016-05-21 09:53:55', 'SALE/POS/2016/05/0164', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 5, '', ''),
(165, '2016-05-21 09:54:46', 'SALE/POS/2016/05/0165', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'due', 0, NULL, 5, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 5, '', ''),
(166, '2016-05-21 09:57:58', 'SALE/POS/2016/05/0166', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'due', 0, NULL, 2, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 4, '', ''),
(167, '2016-05-21 10:01:25', 'SALE/POS/2016/05/0167', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(168, '2016-05-21 10:04:44', 'SALE/POS/2016/05/0168', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '33962.2500', '0.0000', NULL, '0.0000', '0.0000', '2037.7500', 1, '0.0000', '2037.7500', '0.0000', '36000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 3, 1, '36000.0000', NULL, '0.0000', NULL, 3, '', ''),
(169, '2016-05-21 10:05:49', 'SALE/POS/2016/05/0169', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '24000.0000', NULL, '0.0000', NULL, 3, '', ''),
(170, '2016-05-21 10:07:06', 'SALE/POS/2016/05/0170', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(171, '2016-05-21 10:11:20', 'SALE/POS/2016/05/0171', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(172, '2016-05-21 10:15:26', 'SALE/POS/2016/05/0172', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11320.7500', '0.0000', NULL, '0.0000', '0.0000', '679.2500', 1, '0.0000', '679.2500', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(173, '2016-05-21 10:17:17', 'SALE/POS/2016/05/0173', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '203773.5000', '0.0000', NULL, '0.0000', '0.0000', '12226.5000', 1, '0.0000', '12226.5000', '0.0000', '216000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 18, 1, '0.0000', NULL, '0.0000', NULL, 3, '', 'DDWPK3314K'),
(174, '2016-05-21 10:20:08', 'SALE/POS/2016/05/0174', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22641.5000', '0.0000', NULL, '0.0000', '0.0000', '1358.5000', 1, '0.0000', '1358.5000', '0.0000', '24000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '24000.0000', NULL, '0.0000', NULL, 3, '', ''),
(175, '2016-05-21 10:20:46', 'SALE/POS/2016/05/0175', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '67924.5000', '0.0000', NULL, '0.0000', '0.0000', '4075.5000', 1, '0.0000', '4075.5000', '0.0000', '72000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 6, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(176, '2016-05-21 11:07:29', 'SALE/POS/2016/05/0176', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(177, '2016-05-21 12:43:55', 'SALE/POS/2016/05/0177', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(178, '2016-05-21 12:44:41', 'SALE/POS/2016/05/0178', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '4090.9100', '1000.0000', NULL, '1000.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 3, '', '');
INSERT INTO `sma_sales` (`id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `staff_note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `sale_status`, `payment_status`, `payment_term`, `due_date`, `created_by`, `updated_by`, `updated_at`, `total_items`, `pos`, `paid`, `return_id`, `surcharge`, `attachment`, `sales_executive_id`, `invoice_discount_reason`, `pan_number`) VALUES
(179, '2016-05-21 13:37:21', 'SALE/POS/2016/05/0179', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(180, '2016-05-21 13:39:36', 'SALE/POS/2016/05/0180', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(181, '2016-05-21 13:43:22', 'SALE/POS/2016/05/0181', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(182, '2016-05-21 13:49:00', 'SALE/POS/2016/05/0182', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 3, '', ''),
(183, '2016-05-21 13:52:44', 'SALE/POS/2016/05/0183', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(184, '2016-05-21 13:56:52', 'SALE/POS/2016/05/0184', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(185, '2016-05-21 13:58:36', 'SALE/POS/2016/05/0185', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 3, '', ''),
(186, '2016-05-21 14:00:56', 'SALE/POS/2016/05/0186', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(187, '2016-05-21 14:03:21', 'SALE/POS/2016/05/0187', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(188, '2016-05-21 14:05:46', 'SALE/POS/2016/05/0188', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(189, '2016-05-21 14:06:59', 'SALE/POS/2016/05/0189', 1, 'Walk-in Customer', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(190, '2016-05-21 14:08:41', 'SALE/POS/2016/05/0190', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(191, '2016-05-21 14:11:40', 'SALE/POS/2016/05/0191', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(192, '2016-05-21 14:11:41', 'SALE/POS/2016/05/0192', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(193, '2016-05-21 14:29:40', 'SALE/POS/2016/05/0193', 1, 'Walk-in Customer', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(194, '2016-05-21 14:36:49', 'SALE/POS/2016/05/0194', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(195, '2016-05-21 14:38:24', 'SALE/POS/2016/05/0195', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(196, '2016-05-21 14:41:23', 'SALE/POS/2016/05/0196', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(197, '2016-05-21 14:42:16', 'SALE/POS/2016/05/0197', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(198, '2016-05-21 14:52:43', 'SALE/POS/2016/05/0198', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(199, '2016-05-21 14:53:57', 'SALE/POS/2016/05/0199', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(201, '2016-05-21 15:03:55', 'SALE/POS/2016/05/0201', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '15000.0000', '0.0000', NULL, '0.0000', '0.0000', '1500.0000', 1, '0.0000', '1500.0000', '0.0000', '16500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 3, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(202, '2016-05-21 15:08:19', 'SALE/POS/2016/05/0202', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(203, '2016-05-21 15:10:18', 'SALE/POS/2016/05/0203', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(204, '2016-05-21 15:19:29', 'SALE/POS/2016/05/0204', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(205, '2016-05-21 15:20:28', 'SALE/POS/2016/05/0205', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '45000.0000', '0.0000', NULL, '0.0000', '0.0000', '4500.0000', 1, '0.0000', '4500.0000', '0.0000', '49500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 9, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(206, '2016-05-21 15:24:14', 'SALE/POS/2016/05/0206', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '20000.0000', '0.0000', NULL, '0.0000', '0.0000', '2000.0000', 1, '0.0000', '2000.0000', '0.0000', '22000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 4, 1, '22000.0000', NULL, '0.0000', NULL, 3, '', ''),
(207, '2016-05-21 15:25:32', 'SALE/POS/2016/05/0207', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(208, '2016-05-21 15:34:58', 'SALE/POS/2016/05/0208', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '50000.0000', '0.0000', NULL, '0.0000', '0.0000', '5000.0000', 1, '0.0000', '5000.0000', '0.0000', '55000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 10, 1, '55000.0000', NULL, '0.0000', NULL, 3, '', ''),
(209, '2016-05-21 15:36:05', 'SALE/POS/2016/05/0209', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '45000.0000', '0.0000', NULL, '0.0000', '0.0000', '4500.0000', 1, '0.0000', '4500.0000', '0.0000', '49500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 9, 1, '49500.0000', NULL, '0.0000', NULL, 3, '', ''),
(210, '2016-05-21 15:37:04', 'SALE/POS/2016/05/0210', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(211, '2016-05-21 15:43:43', 'SALE/POS/2016/05/0211', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '45000.0000', '0.0000', NULL, '0.0000', '0.0000', '4500.0000', 1, '0.0000', '4500.0000', '0.0000', '49500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 9, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(212, '2016-05-21 15:59:44', 'SALE/POS/2016/05/0212', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(213, '2016-05-21 16:04:05', 'SALE/POS/2016/05/0213', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '45000.0000', '0.0000', NULL, '0.0000', '0.0000', '4500.0000', 1, '0.0000', '4500.0000', '0.0000', '49500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 9, 1, '49500.0000', NULL, '0.0000', NULL, 3, '', ''),
(214, '2016-05-21 16:04:33', 'SALE/POS/2016/05/0214', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(215, '2016-05-21 16:39:25', 'SALE/POS/2016/05/0215', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(216, '2016-05-21 16:40:33', 'SALE/POS/2016/05/0216', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(217, '2016-05-21 16:44:58', 'SALE/POS/2016/05/0217', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '9090.9200', '0.0000', NULL, '0.0000', '0.0000', '909.0800', 1, '0.0000', '909.0800', '0.0000', '10000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 4, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(218, '2016-05-21 16:45:57', 'SALE/POS/2016/05/0218', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(219, '2016-05-21 16:48:34', 'SALE/POS/2016/05/0219', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(220, '2016-05-21 16:52:15', 'SALE/POS/2016/05/0220', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(221, '2016-05-21 16:53:31', 'SALE/POS/2016/05/0221', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 3, '', ''),
(222, '2016-05-21 16:55:41', 'SALE/POS/2016/05/0222', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 3, '', ''),
(223, '2016-05-21 16:58:05', 'SALE/POS/2016/05/0223', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 5, '', ''),
(224, '2016-05-23 07:21:38', 'SALE/POS/2016/05/0224', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10000.0000', '0.0000', NULL, '0.0000', '0.0000', '1000.0000', 1, '0.0000', '1000.0000', '0.0000', '11000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '11000.0000', NULL, '0.0000', NULL, 3, '', ''),
(225, '2016-05-23 07:30:22', 'SALE/POS/2016/05/0225', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '60000.0000', '0.0000', NULL, '0.0000', '0.0000', '6000.0000', 1, '0.0000', '6000.0000', '0.0000', '66000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 12, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(226, '2016-05-23 07:36:25', 'SALE/POS/2016/05/0226', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(227, '2016-05-23 07:54:28', 'SALE/POS/2016/05/0227', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(228, '2016-05-23 07:57:06', 'SALE/POS/2016/05/0228', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(229, '2016-05-23 08:03:34', 'SALE/POS/2016/05/0229', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(230, '2016-05-23 08:04:09', 'SALE/POS/2016/05/0230', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '13636.3800', '0.0000', NULL, '0.0000', '0.0000', '1363.6200', 1, '0.0000', '1363.6200', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 6, 1, '15000.0000', NULL, '0.0000', NULL, 3, '', ''),
(231, '2016-05-23 08:08:36', 'SALE/POS/2016/05/0231', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(232, '2016-05-23 08:09:02', 'SALE/POS/2016/05/0232', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(233, '2016-05-23 08:12:40', 'SALE/POS/2016/05/0233', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '9090.9200', '0.0000', NULL, '0.0000', '0.0000', '909.0800', 1, '0.0000', '909.0800', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 4, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(234, '2016-05-23 08:35:03', 'SALE/POS/2016/05/0234', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(235, '2016-05-23 08:37:02', 'SALE/POS/2016/05/0235', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11363.6500', '0.0000', NULL, '0.0000', '0.0000', '1136.3500', 1, '0.0000', '1136.3500', '0.0000', '12500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 5, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(236, '2016-05-23 08:45:06', 'SALE/POS/2016/05/0236', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11363.6500', '0.0000', NULL, '0.0000', '0.0000', '1136.3500', 1, '0.0000', '1136.3500', '0.0000', '12500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 5, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(237, '2016-05-23 08:45:35', 'SALE/POS/2016/05/0237', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '9090.9200', '0.0000', NULL, '0.0000', '0.0000', '909.0800', 1, '0.0000', '909.0800', '0.0000', '10000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 4, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(238, '2016-05-23 08:55:16', 'SALE/POS/2016/05/0238', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(239, '2016-05-23 08:57:12', 'SALE/POS/2016/05/0239', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(240, '2016-05-23 09:01:04', 'SALE/POS/2016/05/0240', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 3, 1, '7500.0000', NULL, '0.0000', NULL, 3, '', ''),
(241, '2016-05-23 09:10:15', 'SALE/POS/2016/05/0241', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 1, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(242, '2016-05-23 09:15:10', 'SALE/POS/2016/05/0242', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '9090.9200', '0.0000', NULL, '0.0000', '0.0000', '909.0800', 1, '0.0000', '909.0800', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 4, 1, '10000.0000', NULL, '0.0000', NULL, 3, '', ''),
(243, '2016-05-23 09:16:36', 'SALE/POS/2016/05/0243', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(244, '2016-05-23 09:19:24', 'SALE/POS/2016/05/0244', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22727.3000', '0.0000', NULL, '0.0000', '0.0000', '2272.7000', 1, '0.0000', '2272.7000', '0.0000', '25000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 10, 1, '25000.0000', NULL, '0.0000', NULL, 3, '', ''),
(245, '2016-05-23 09:29:30', 'SALE/POS/2016/05/0245', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '13636.3800', '0.0000', NULL, '0.0000', '0.0000', '1363.6200', 1, '0.0000', '1363.6200', '0.0000', '15000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 6, 1, '15000.0000', NULL, '0.0000', NULL, 3, '', ''),
(246, '2016-05-23 09:35:28', 'SALE/POS/2016/05/0246', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11363.6500', '0.0000', NULL, '0.0000', '0.0000', '1136.3500', 1, '0.0000', '1136.3500', '0.0000', '12500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 5, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(247, '2016-05-23 09:55:47', 'SALE/POS/2016/05/0247', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '11363.6500', '0.0000', NULL, '0.0000', '0.0000', '1136.3500', 1, '0.0000', '1136.3500', '0.0000', '12500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 5, 1, '12500.0000', NULL, '0.0000', NULL, 3, '', ''),
(248, '2016-05-23 09:57:29', 'SALE/POS/2016/05/0248', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '22727.3000', '0.0000', NULL, '0.0000', '0.0000', '2272.7000', 1, '0.0000', '2272.7000', '0.0000', '25000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 10, 1, '25000.0000', NULL, '0.0000', NULL, 3, '', ''),
(249, '2016-05-23 10:02:21', 'SALE/POS/2016/05/0249', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '14545.4600', '0.0000', NULL, '0.0000', '0.0000', '1454.5400', 1, '0.0000', '1454.5400', '0.0000', '16000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 4, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(250, '2016-05-23 10:03:40', 'SALE/POS/2016/05/0250', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '14545.4600', '0.0000', NULL, '0.0000', '0.0000', '1454.5400', 1, '0.0000', '1454.5400', '0.0000', '16000.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 4, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(252, '2016-05-23 10:08:31', 'SALE/POS/2016/05/0252', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '14090.9200', '0.0000', NULL, '0.0000', '0.0000', '1409.0800', 1, '0.0000', '1409.0800', '0.0000', '15500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 5, 1, '15500.0000', NULL, '0.0000', NULL, 3, '', ''),
(253, '2016-05-23 11:47:30', 'SALE/POS/2016/05/0253', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '12272.7300', '0.0000', NULL, '0.0000', '0.0000', '1227.2700', 1, '0.0000', '1227.2700', '0.0000', '13500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 3, 1, '13500.0000', NULL, '0.0000', NULL, 4, '', ''),
(254, '2016-05-23 11:48:21', 'SALE/POS/2016/05/0254', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '22272.7300', '0.0000', NULL, '0.0000', '0.0000', '2227.2700', 1, '0.0000', '2227.2700', '0.0000', '24500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 5, 1, '24500.0000', NULL, '0.0000', NULL, 4, '', ''),
(255, '2016-05-23 11:50:12', 'SALE/POS/2016/05/0255', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '47272.7300', '0.0000', NULL, '0.0000', '0.0000', '4727.2700', 1, '0.0000', '4727.2700', '0.0000', '52000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 10, 1, '52000.0000', NULL, '0.0000', NULL, 5, '', ''),
(256, '2016-05-23 11:51:24', 'SALE/POS/2016/05/0256', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '19090.9200', '0.0000', NULL, '0.0000', '0.0000', '1909.0800', 1, '0.0000', '1909.0800', '0.0000', '21000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 6, 1, '21000.0000', NULL, '0.0000', NULL, 4, '', ''),
(257, '2016-05-23 11:53:49', 'SALE/POS/2016/05/0257', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '14545.4600', '0.0000', NULL, '0.0000', '0.0000', '1454.5400', 1, '0.0000', '1454.5400', '0.0000', '16000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 4, 1, '16000.0000', NULL, '0.0000', NULL, 3, '', ''),
(258, '2016-05-23 12:00:08', 'SALE/POS/2016/05/0258', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '20000.0000', '0.0000', NULL, '0.0000', '0.0000', '2000.0000', 1, '0.0000', '2000.0000', '0.0000', '22000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 4, 1, '22000.0000', NULL, '0.0000', NULL, 4, '', ''),
(259, '2016-05-23 12:03:21', 'SALE/POS/2016/05/0259', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'due', 0, NULL, 2, NULL, NULL, 2, 1, '0.0000', NULL, '0.0000', NULL, 4, '', ''),
(260, '2016-05-23 12:03:51', 'SALE/POS/2016/05/0260', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(261, '2016-05-23 12:05:06', 'SALE/POS/2016/05/0261', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(262, '2016-05-23 12:24:19', 'SALE/POS/2016/05/0262', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(263, '2016-05-23 12:30:59', 'SALE/POS/2016/05/0263', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 3, '', ''),
(264, '2016-05-23 12:32:20', 'SALE/POS/2016/05/0264', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '4090.9100', '1000.0000', NULL, '1000.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 3, '', ''),
(265, '2016-05-23 12:32:55', 'SALE/POS/2016/05/0265', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(266, '2016-05-23 12:34:32', 'SALE/POS/2016/05/0266', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '1818.1800', '500.0000', NULL, '500.0000', '0.0000', '181.8200', 1, '0.0000', '181.8200', '0.0000', '2000.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2000.0000', NULL, '0.0000', NULL, 5, '', ''),
(267, '2016-05-23 12:37:30', 'SALE/POS/2016/05/0267', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', '250', '250.0000', '250.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2250.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2250.0000', NULL, '0.0000', NULL, 3, 'good sale', ''),
(268, '2016-05-23 12:38:25', 'SALE/POS/2016/05/0268', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', '200', '200.0000', '200.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2300.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2300.0000', NULL, '0.0000', NULL, 5, 'u', ''),
(269, '2016-05-23 12:41:04', 'SALE/POS/2016/05/0269', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '9545.4500', '500.0000', '1000', '1500.0000', '1000.0000', '954.5500', 1, '0.0000', '954.5500', '0.0000', '9500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '9500.0000', NULL, '0.0000', NULL, 4, 'bumper sale', ''),
(270, '2016-05-23 14:12:38', 'SALE/POS/2016/05/0270', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(271, '2016-05-24 07:09:27', 'SALE/POS/2016/05/0271', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(273, '2016-05-24 12:27:31', 'SALE/POS/2016/05/0273', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(274, '2016-05-24 12:28:27', 'SALE/POS/2016/05/0274', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(275, '2016-05-24 12:29:25', 'SALE/POS/2016/05/0275', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(276, '2016-05-24 15:19:11', 'SALE/POS/2016/05/0276', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(277, '2016-05-24 15:20:52', 'SALE/POS/2016/05/0277', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(279, '2016-05-25 07:33:23', 'SALE/POS/2016/05/0279', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '14545.4600', '1000.0000', NULL, '1000.0000', '0.0000', '1454.5400', 1, '0.0000', '1454.5400', '0.0000', '16000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 3, 1, '16000.0000', NULL, '0.0000', NULL, 3, '', ''),
(281, '2016-05-25 08:19:13', 'SALE/POS/2016/05/0281', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '13181.8200', '0.0000', NULL, '0.0000', '0.0000', '1318.1800', 1, '0.0000', '1318.1800', '0.0000', '14500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '14500.0000', NULL, '0.0000', NULL, 3, '', ''),
(282, '2016-05-25 08:22:38', 'SALE/POS/2016/05/0282', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(284, '2016-05-25 09:03:32', 'SALE/POS/2016/05/0284', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(285, '2016-05-25 09:54:15', 'SALE/POS/2016/05/0285', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(286, '2016-05-25 09:55:26', 'SALE/POS/2016/05/0286', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(287, '2016-05-25 09:57:57', 'SALE/POS/2016/05/0287', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 3, '', ''),
(288, '2016-05-25 10:03:52', 'SALE/POS/2016/05/0288', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '15454.5500', '0.0000', NULL, '0.0000', '0.0000', '1545.4500', 1, '0.0000', '1545.4500', '0.0000', '17000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 3, 1, '17000.0000', NULL, '0.0000', NULL, 4, '', ''),
(289, '2016-05-25 12:59:25', 'SALE/POS/2016/05/0289', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(290, '2016-05-25 11:17:47', 'SALE/POS/2016/05/0290', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(291, '2016-05-25 11:22:42', 'SALE/POS/2016/05/0291', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '8181.8200', '0.0000', NULL, '0.0000', '0.0000', '818.1800', 1, '0.0000', '818.1800', '0.0000', '9000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '9000.0000', NULL, '0.0000', NULL, 4, '', ''),
(292, '2016-05-25 11:34:35', 'SALE/POS/2016/05/0292', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 4, '', ''),
(293, '2016-05-25 14:28:40', 'SALE/POS/2016/05/0293', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '8181.8200', '0.0000', NULL, '0.0000', '0.0000', '818.1800', 1, '0.0000', '818.1800', '0.0000', '9000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '9000.0000', NULL, '0.0000', NULL, 3, '', ''),
(294, '2016-05-25 14:30:12', 'SALE/POS/2016/05/0294', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 3, '', ''),
(295, '2016-05-30 16:02:47', 'SALE/POS/2016/05/0295', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 4, '', ''),
(296, '2016-05-31 14:38:06', 'SALE/POS/2016/05/0296', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(298, '2016-05-31 15:12:07', 'SALE/POS/2016/05/0298', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 2, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 5, '', ''),
(299, '2016-06-01 07:13:46', 'SALE/POS/2016/06/0299', 34, '', 4, 'Swatch delhi', 1, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 3, '', ''),
(300, '2016-06-01 07:15:02', 'SALE/POS/2016/06/0300', 34, '', 5, 'Swatch Mumbai', 2, NULL, '', '4090.9100', '0.0000', NULL, '0.0000', '0.0000', '409.0900', 1, '0.0000', '409.0900', '0.0000', '4500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '4500.0000', NULL, '0.0000', NULL, 5, '', ''),
(301, '2016-06-01 08:31:00', 'SALE/POS/2016/06/0301', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '8181.8200', '0.0000', NULL, '0.0000', '0.0000', '818.1800', 1, '0.0000', '818.1800', '0.0000', '9000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '9000.0000', NULL, '0.0000', NULL, 3, '', ''),
(302, '2016-06-01 08:38:07', 'SALE/POS/2016/06/0302', 34, 'Varsha', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(303, '2016-06-01 09:53:47', 'SALE/POS/2016/06/0303', 46, 'Flinch', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(304, '2016-06-01 10:57:34', 'SALE/POS/2016/06/0304', 48, 'ravindra', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(305, '2016-06-01 10:59:11', 'SALE/POS/2016/06/0305', 34, 'Varsha', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(306, '2016-06-01 11:01:11', 'SALE/POS/2016/06/0306', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(307, '2016-06-01 11:07:31', 'SALE/POS/2016/06/0307', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(308, '2016-06-01 11:08:43', 'SALE/POS/2016/06/0308', 49, 'andrew', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(309, '2016-06-01 11:36:07', 'SALE/POS/2016/06/0309', 50, 'raul', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(311, '2016-06-01 11:56:45', 'SALE/POS/2016/06/0311', 51, 'wright', 5, 'Swatch Mumbai', 2, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 5, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 5, '', ''),
(312, '2016-06-01 11:59:11', 'SALE/POS/2016/06/0312', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(313, '2016-06-01 14:15:40', 'SALE/POS/2016/06/0313', 54, 'ayush', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(316, '2016-06-01 14:44:58', 'SALE/POS/2016/06/0316', 59, 'rahul tripathi', 4, 'Swatch delhi', 1, NULL, '', '9545.4600', '0.0000', NULL, '0.0000', '0.0000', '954.5400', 1, '0.0000', '954.5400', '0.0000', '10500.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 3, 1, '10500.0000', NULL, '0.0000', NULL, 4, '', ''),
(317, '2016-06-01 15:10:22', 'SALE/POS/2016/06/0317', 61, 'Deepika ', 3, 'Test Biller', 1, NULL, '', '4545.4600', '0.0000', NULL, '0.0000', '0.0000', '454.5400', 1, '0.0000', '454.5400', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 2, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(320, '2016-06-02 12:26:59', 'SALE/POS/2016/06/0320', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(321, '2016-06-03 08:20:18', 'SALE/POS/2016/06/0324', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(322, '2016-06-03 08:31:33', 'SALE/POS/2016/06/0325', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(323, '2016-06-03 08:33:13', 'SALE/POS/2016/06/0326', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 4, '', ''),
(324, '2016-06-03 08:39:08', 'SALE/POS/2016/06/0327', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(325, '2016-06-03 08:44:10', 'SALE/POS/2016/06/0328', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(326, '2016-06-03 08:45:33', 'SALE/POS/2016/06/0329', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(327, '2016-06-03 08:46:51', 'SALE/POS/2016/06/0330', 10, 'Shotformats Digital Production Pvt Ltd', 5, 'Swatch Mumbai', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(328, '2016-06-03 12:33:38', 'SALE/POS/2016/06/0331', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(332, '2016-06-04 07:35:00', 'SALE/POS/2016/06/0335', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(334, '2016-06-06 11:59:59', 'SALE/POS/2016/06/0337', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(335, '2016-06-06 12:16:12', 'SALE/POS/2016/06/0338', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(346, '2016-06-15 12:06:26', 'SALE/POS/2016/06/0349', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(348, '2016-06-17 09:20:06', 'SALE/POS/2016/06/0348', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 3, '', ''),
(349, '2016-06-17 09:55:15', 'SALE/POS/2016/06/0349', 9, 'Swatch Delhi', 5, 'Swatch Mumbai', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(354, '2016-06-18 05:07:34', 'SALE/POS/2016/06/0354', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(355, '2016-06-18 09:19:07', 'SALE/POS/2016/06/0355', 10, 'Shotformats Digital Production Pvt Ltd', 3, 'Test Biller', 1, NULL, '', '4545.4500', '500.0000', NULL, '500.0000', '0.0000', '454.5500', 1, '0.0000', '454.5500', '0.0000', '5000.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '5000.0000', NULL, '0.0000', NULL, 3, '', ''),
(356, '2016-06-18 12:30:39', 'SALE/POS/2016/06/0356', 63, 'ankit', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(357, '2016-06-18 12:34:18', 'SALE/POS/2016/06/0357', 9, 'Swatch Delhi', 3, 'Test Biller', 1, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'due', 0, NULL, 1, NULL, NULL, 3, 1, '0.0000', NULL, '0.0000', NULL, 3, '', ''),
(358, '2016-06-20 07:03:37', 'SALE/POS/2016/06/0358', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(359, '2016-06-20 07:10:47', 'SALE/POS/2016/06/0359', 63, 'ankit', 3, 'Test Biller', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 1, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 3, '', ''),
(360, '2016-06-23 07:56:21', 'SALE/POS/2016/06/0360', 73, 'ppp', 4, 'Swatch delhi', 1, NULL, '', '4954.2500', '50.3200', NULL, '50.3200', '0.0000', '495.4300', 1, '0.0000', '495.4300', '0.0000', '5449.6800', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5449.6800', NULL, '0.0000', NULL, 4, '', ''),
(361, '2016-06-28 09:12:20', 'SALE/POS/2016/06/0361', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '500.0000', NULL, '0.0000', NULL, 4, '', ''),
(362, '2016-06-28 12:25:05', 'SALE/POS/2016/06/0362', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '3636.3600', '0.0000', NULL, '0.0000', '0.0000', '363.6400', 1, '0.0000', '363.6400', '0.0000', '4000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '4000.0000', NULL, '0.0000', NULL, 4, '', ''),
(363, '2016-06-28 12:37:47', 'SALE/POS/2016/06/0363', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(364, '2016-06-28 12:42:20', 'SALE/POS/2016/06/0364', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(365, '2016-06-28 12:51:03', 'SALE/POS/2016/06/0365', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(366, '2016-06-28 12:57:40', 'SALE/POS/2016/06/0366', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '9090.9200', '0.0000', NULL, '0.0000', '0.0000', '909.0800', 1, '0.0000', '909.0800', '0.0000', '10000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 4, 1, '10000.0000', NULL, '0.0000', NULL, 4, '', ''),
(367, '2016-06-28 13:04:04', 'SALE/POS/2016/06/0367', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '6818.1900', '0.0000', NULL, '0.0000', '0.0000', '681.8100', 1, '0.0000', '681.8100', '0.0000', '7500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 3, 1, '7500.0000', NULL, '0.0000', NULL, 4, '', '');
INSERT INTO `sma_sales` (`id`, `date`, `reference_no`, `customer_id`, `customer`, `biller_id`, `biller`, `warehouse_id`, `note`, `staff_note`, `total`, `product_discount`, `order_discount_id`, `total_discount`, `order_discount`, `product_tax`, `order_tax_id`, `order_tax`, `total_tax`, `shipping`, `grand_total`, `sale_status`, `payment_status`, `payment_term`, `due_date`, `created_by`, `updated_by`, `updated_at`, `total_items`, `pos`, `paid`, `return_id`, `surcharge`, `attachment`, `sales_executive_id`, `invoice_discount_reason`, `pan_number`) VALUES
(368, '2016-06-28 13:10:24', 'SALE/POS/2016/06/0368', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(369, '2016-06-29 04:52:43', 'SALE/POS/2016/06/0369', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 4, '', ''),
(370, '2016-06-29 04:55:43', 'SALE/POS/2016/06/0370', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(371, '2016-06-29 05:12:03', 'SALE/POS/2016/06/0371', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '500.0000', NULL, '0.0000', NULL, 4, '', ''),
(372, '2016-06-29 05:13:54', 'SALE/POS/2016/06/0372', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(373, '2016-06-29 05:16:28', 'SALE/POS/2016/06/0373', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(374, '2016-06-29 05:29:46', 'SALE/POS/2016/06/0374', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(375, '2016-06-29 05:39:21', 'SALE/POS/2016/06/0375', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(376, '2016-06-29 05:46:32', 'SALE/POS/2016/06/0376', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(377, '2016-06-29 06:09:35', 'SALE/POS/2016/06/0377', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(378, '2016-06-29 07:24:46', 'SALE/POS/2016/06/0378', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(379, '2016-06-29 07:26:53', 'SALE/POS/2016/06/0379', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(380, '2016-06-29 07:28:36', 'SALE/POS/2016/06/0380', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 2, 1, '3000.0000', NULL, '0.0000', NULL, 4, '', ''),
(381, '2016-06-29 07:40:02', 'SALE/POS/2016/06/0381', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(382, '2016-06-29 07:47:44', 'SALE/POS/2016/06/0382', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '7272.7300', '0.0000', NULL, '0.0000', '0.0000', '727.2700', 1, '0.0000', '727.2700', '0.0000', '8000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 2, 1, '8000.0000', NULL, '0.0000', NULL, 4, '', ''),
(383, '2016-06-29 07:51:05', 'SALE/POS/2016/06/0383', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(384, '2016-06-29 07:52:31', 'SALE/POS/2016/06/0384', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(385, '2016-06-29 08:38:52', 'SALE/POS/2016/06/0385', 64, 'Naman', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(386, '2016-06-29 08:41:01', 'SALE/POS/2016/06/0386', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(387, '2016-06-29 08:42:11', 'SALE/POS/2016/06/0387', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(388, '2016-06-29 08:45:15', 'SALE/POS/2016/06/0388', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(389, '2016-06-29 08:46:26', 'SALE/POS/2016/06/0389', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(390, '2016-06-29 08:48:11', 'SALE/POS/2016/06/0390', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(391, '2016-06-29 09:06:28', 'SALE/POS/2016/06/0391', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(392, '2016-06-29 09:20:00', 'SALE/POS/2016/06/0392', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(393, '2016-06-29 09:25:27', 'SALE/POS/2016/06/0393', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(394, '2016-06-29 09:32:19', 'SALE/POS/2016/06/0394', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(395, '2016-06-29 09:56:08', 'SALE/POS/2016/06/0395', 75, 'ajay', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 4, '', ''),
(396, '2016-06-29 10:01:56', 'SALE/POS/2016/06/0396', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(397, '2016-06-29 10:41:09', 'SALE/POS/2016/06/0397', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(398, '2016-06-29 11:50:52', 'SALE/POS/2016/06/0398', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 4, '', ''),
(399, '2016-06-29 11:54:51', 'SALE/POS/2016/06/0399', 41, 'Sadsad', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 4, '', ''),
(400, '2016-06-29 11:58:36', 'SALE/POS/2016/06/0400', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(401, '2016-06-29 12:08:37', 'SALE/POS/2016/06/0401', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(402, '2016-06-29 12:14:21', 'SALE/POS/2016/06/0402', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '1000.0000', NULL, '0.0000', NULL, 4, '', ''),
(403, '2016-06-29 12:18:12', 'SALE/POS/2016/06/0403', 31, 'Wwww', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '4000.0000', NULL, '0.0000', NULL, 4, '', ''),
(404, '2016-06-29 12:22:23', 'SALE/POS/2016/06/0404', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(405, '2016-06-29 12:44:55', 'SALE/POS/2016/06/0405', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(406, '2016-06-29 12:46:16', 'SALE/POS/2016/06/0406', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(408, '2016-06-30 09:48:10', 'SALE/POS/2016/06/0408', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '5000.0000', '0.0000', NULL, '0.0000', '0.0000', '500.0000', 1, '0.0000', '500.0000', '0.0000', '5500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '5500.0000', NULL, '0.0000', NULL, 4, '', ''),
(410, '2016-07-05 12:19:59', 'SALE/POS/2016/07/0410', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'paid', 0, NULL, 4, NULL, NULL, 1, 1, '25000.0000', NULL, '0.0000', NULL, 4, '', ''),
(411, '2016-07-05 12:43:34', 'SALE/POS/2016/07/0411', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '665.0000', NULL, '0.0000', NULL, 4, '', ''),
(412, '2016-07-05 12:45:11', 'SALE/POS/2016/07/0412', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '4750.0000', NULL, '0.0000', NULL, 4, '', ''),
(413, '2016-07-05 12:48:37', 'SALE/POS/2016/07/0413', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '4750.0000', NULL, '0.0000', NULL, 4, '', ''),
(414, '2016-07-05 12:50:31', 'SALE/POS/2016/07/0414', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '78.0000', NULL, '0.0000', NULL, 4, '', ''),
(415, '2016-07-05 12:53:36', 'SALE/POS/2016/07/0415', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '4750.0000', NULL, '0.0000', NULL, 4, '', ''),
(416, '2016-07-05 12:54:30', 'SALE/POS/2016/07/0416', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '500.0000', NULL, '0.0000', NULL, 4, '', ''),
(417, '2016-07-05 12:57:02', 'SALE/POS/2016/07/0417', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '3400.0000', NULL, '0.0000', NULL, 4, '', ''),
(418, '2016-07-05 13:16:36', 'SALE/POS/2016/07/0418', 31, 'Wwww', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '25000.0000', NULL, '0.0000', NULL, 4, '', ''),
(419, '2016-07-05 13:18:21', 'SALE/POS/2016/07/0419', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(420, '2016-07-05 13:19:46', 'SALE/POS/2016/07/0420', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '22727.2700', '0.0000', NULL, '0.0000', '0.0000', '2272.7300', 1, '0.0000', '2272.7300', '0.0000', '25000.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(421, '2016-07-05 13:29:35', 'SALE/POS/2016/07/0421', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 'completed', 'partial', 0, NULL, 2, NULL, NULL, 1, 1, '350.0000', NULL, '0.0000', NULL, 4, '', ''),
(422, '2016-07-05 13:43:36', 'SALE/POS/2016/07/0422', 32, 'Dsf', 4, 'Swatch delhi', 1, NULL, '', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '3500.0000', NULL, '0.0000', NULL, 4, '', ''),
(423, '2016-07-05 13:44:32', 'SALE/POS/2016/07/0423', 9, 'Swatch Delhi', 4, 'Swatch delhi', 1, NULL, '', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '3500.0000', NULL, '0.0000', NULL, 4, '', ''),
(424, '2016-07-06 06:35:10', 'SALE/POS/2016/07/0424', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '3181.8200', '0.0000', NULL, '0.0000', '0.0000', '318.1800', 1, '0.0000', '318.1800', '0.0000', '3500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '3500.0000', NULL, '0.0000', NULL, 4, '', ''),
(425, '2016-07-06 06:55:33', 'SALE/POS/2016/07/0425', 10, 'Shotformats Digital Production Pvt Ltd', 4, 'Swatch delhi', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(426, '2016-07-06 07:02:42', 'SALE/POS/2016/07/0426', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(427, '2016-07-06 13:59:58', 'SALE/POS/2016/07/0427', 63, 'ankit', 4, 'Swatch delhi', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', ''),
(428, '2016-07-07 04:56:26', 'SALE/POS/2016/07/0428', 11, 'Adesh', 4, 'Swatch delhi', 1, NULL, '', '2272.7300', '0.0000', NULL, '0.0000', '0.0000', '227.2700', 1, '0.0000', '227.2700', '0.0000', '2500.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '2500.0000', NULL, '0.0000', NULL, 4, '', ''),
(429, '2016-07-07 05:41:47', 'SALE/POS/2016/07/0429', 31, 'Wwww', 4, 'Swatch delhi', 1, NULL, '', '10909.0900', '0.0000', NULL, '0.0000', '0.0000', '1090.9100', 1, '0.0000', '1090.9100', '0.0000', '12000.0000', 'completed', 'paid', 0, NULL, 2, NULL, NULL, 1, 1, '12000.0000', NULL, '0.0000', NULL, 4, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `advance_booking` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sale_items`
--

INSERT INTO `sma_sale_items` (`id`, `sale_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`, `advance_booking`) VALUES
(1, 1, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(2, 2, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(3, 3, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(4, 4, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(6, 5, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(7, 6, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(8, 6, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(9, 7, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(10, 7, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(11, 7, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(12, 7, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(13, 8, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(14, 9, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(15, 9, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(16, 10, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(17, 10, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(18, 10, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(19, 10, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(20, 11, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(21, 11, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(22, 11, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(23, 12, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(24, 13, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(25, 13, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(26, 14, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(27, 14, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(28, 14, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(29, 15, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(30, 15, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(31, 15, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(32, 15, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(33, 15, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(34, 16, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(35, 16, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(36, 16, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(37, 17, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(38, 17, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(39, 17, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(40, 17, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(41, 17, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(42, 18, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(43, 18, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(44, 18, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(45, 19, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(46, 19, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(47, 19, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(48, 20, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(49, 20, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(50, 20, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(51, 20, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(52, 21, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(53, 21, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(54, 21, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(55, 21, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(56, 21, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(57, 22, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(58, 22, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(59, 22, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(60, 22, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(61, 22, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(62, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(63, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(64, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(65, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(66, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(67, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(68, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(69, 23, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(70, 24, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(71, 24, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(72, 24, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(73, 24, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(74, 25, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(75, 25, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(76, 25, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(77, 25, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(78, 25, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(79, 26, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(80, 26, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(81, 26, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(82, 26, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(83, 27, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(84, 27, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(85, 27, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(86, 27, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(87, 27, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(88, 28, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(89, 28, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(90, 28, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(91, 28, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(92, 29, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(93, 29, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(94, 30, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(95, 30, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(96, 31, 2, '22345678', 'iphone 6', 'standard', 0, '500000.0000', '550000.0000', '1.0000', 1, '50000.0000', 2, '10.0000%', '0', '0.0000', '550000.0000', '', '550000.0000', 0),
(97, 32, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(98, 32, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(99, 32, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(100, 33, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(101, 33, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(102, 33, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(103, 33, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(104, 33, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(105, 33, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(106, 34, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(107, 34, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(108, 34, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(109, 34, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(110, 35, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(111, 35, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(112, 35, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(113, 35, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(114, 36, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(115, 37, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(116, 37, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(117, 37, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(118, 37, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(119, 38, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(120, 38, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(121, 38, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(122, 38, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(123, 38, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 2, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(124, 39, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(125, 39, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(126, 40, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(127, 40, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(128, 40, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(129, 40, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(130, 40, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(131, 41, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(132, 41, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(133, 41, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(134, 41, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(135, 41, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(136, 42, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(137, 42, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(138, 42, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(139, 42, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(140, 42, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(141, 43, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(142, 43, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(143, 43, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(144, 43, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(145, 43, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(146, 44, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(147, 44, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(148, 44, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(149, 44, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(150, 44, 1, '123456789', 'Nexus 6', 'digital', 0, '909.0900', '1000.0000', '1.0000', 1, '90.9100', 2, '10.0000%', '0', '0.0000', '1000.0000', '', '1000.0000', 0),
(151, 45, 2, '22345678', 'iphone 6', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(152, 46, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(153, 46, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(154, 47, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(155, 47, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(156, 48, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(157, 49, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(158, 49, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(159, 49, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(160, 50, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(161, 50, 4, '3221321', 'LG e', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(162, 51, 3, '673267326', 'Sony Bravia', 'standard', 0, '13636.3600', '15000.0000', '1.0000', 2, '1363.6400', 2, '10.0000%', '0', '0.0000', '15000.0000', '', '15000.0000', 0),
(163, 52, 3, '673267326', 'Sony Bravia', 'standard', 0, '13636.3600', '15000.0000', '1.0000', 2, '1363.6400', 2, '10.0000%', '0', '0.0000', '15000.0000', '', '15000.0000', 0),
(164, 53, 3, '673267326', 'Sony Bravia', 'standard', 0, '13636.3600', '15000.0000', '1.0000', 2, '1363.6400', 2, '10.0000%', '0', '0.0000', '15000.0000', '', '15000.0000', 0),
(165, 54, 3, '673267326', 'Sony Bravia', 'standard', 0, '13636.3600', '15000.0000', '1.0000', 2, '1363.6400', 2, '10.0000%', '0', '0.0000', '15000.0000', '', '15000.0000', 0),
(166, 55, 3, '673267326', 'Sony Bravia', 'standard', 0, '13636.3600', '15000.0000', '1.0000', 2, '1363.6400', 2, '10.0000%', '0', '0.0000', '15000.0000', '', '15000.0000', 0),
(167, 56, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(168, 57, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(169, 58, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(170, 58, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(171, 58, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(172, 59, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(173, 59, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(174, 60, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(175, 60, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(176, 61, 5, '21131', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(177, 62, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(178, 63, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(179, 64, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(180, 65, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(181, 66, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(182, 66, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(183, 67, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(184, 67, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(185, 68, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(186, 69, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(187, 70, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(188, 71, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(189, 72, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(192, 75, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(193, 76, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(194, 77, 6, '2322312', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(196, 79, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(199, 81, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(200, 82, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(201, 82, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(202, 82, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(205, 84, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(206, 84, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(207, 85, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(208, 85, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(209, 86, 7, '21212121', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(210, 87, 7, '21212121', 'iphone', 'standard', 0, '2045.4500', '2250.0000', '1.0000', 1, '204.5500', 2, '10.0000%', '250', '250.0000', '2250.0000', '', '2500.0000', 0),
(215, 90, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(216, 90, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(221, 93, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(222, 93, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(223, 93, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(224, 94, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(225, 94, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(226, 94, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(227, 94, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(228, 94, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(229, 95, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(230, 95, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(231, 95, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(232, 95, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(233, 95, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(234, 96, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(235, 96, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(239, 99, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(240, 99, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 2, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(244, 102, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(245, 102, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(247, 104, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(248, 104, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(249, 104, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(251, 106, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 1, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(252, 107, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(253, 107, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(257, 110, 8, '1212114', 'LG E', 'standard', 0, '10377.3600', '11000.0000', '1.0000', 2, '622.6400', 3, '6.0000%', '1000', '1000.0000', '11000.0000', '', '12000.0000', 0),
(258, 110, 8, '1212114', 'LG E', 'standard', 0, '11320.7500', '12000.0000', '1.0000', 2, '679.2500', 3, '6.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(261, 113, 11, '231321321', 'iphone', 'standard', 0, '50000.0000', '55000.0000', '1.0000', 1, '5000.0000', 2, '10.0000%', '0', '0.0000', '55000.0000', '', '55000.0000', 0),
(262, 193, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(263, 198, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(264, 199, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(266, 201, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(267, 202, 14, '321321321', 'canvas pro', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(268, 203, 15, '2332323', 'canvas pro', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(269, 204, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(270, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(271, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(272, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(273, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(274, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(275, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(276, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(277, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(278, 205, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(279, 206, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(280, 206, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(281, 206, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(282, 206, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(283, 207, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(284, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(285, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(286, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(287, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(288, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(289, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(290, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(291, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(292, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(293, 208, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(294, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(295, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(296, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(297, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(298, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(299, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(300, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(301, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(302, 209, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(303, 210, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(304, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(305, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(306, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(307, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(308, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(309, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(310, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(311, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(312, 211, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(313, 212, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(314, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(315, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(316, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(317, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(318, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(319, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(320, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(321, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(322, 213, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(323, 214, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(324, 215, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(325, 216, 16, '23243', 'Lumia 520', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(326, 217, 18, '21313213', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(327, 218, 18, '21313213', 'iphone', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(328, 219, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(329, 220, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(330, 221, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(331, 221, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(332, 222, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(333, 222, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0);
INSERT INTO `sma_sale_items` (`id`, `sale_id`, `product_id`, `product_code`, `product_name`, `product_type`, `option_id`, `net_unit_price`, `unit_price`, `quantity`, `warehouse_id`, `item_tax`, `tax_rate_id`, `tax`, `discount`, `item_discount`, `subtotal`, `serial_no`, `real_unit_price`, `advance_booking`) VALUES
(334, 223, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(335, 223, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(336, 224, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(337, 224, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(338, 225, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(339, 226, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(340, 227, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(341, 228, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(342, 229, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(343, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(344, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(345, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(346, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(347, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(348, 230, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(349, 231, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(350, 232, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(351, 233, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(352, 233, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(353, 233, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(354, 233, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(355, 234, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(356, 236, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(357, 236, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(358, 236, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(359, 236, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(360, 236, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(361, 237, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(362, 237, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(363, 237, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(364, 237, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(365, 238, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(366, 239, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(367, 240, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(368, 240, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(369, 240, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(370, 241, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(371, 242, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(372, 242, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(373, 242, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(374, 242, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(375, 243, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(376, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(377, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(378, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(379, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(380, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(381, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(382, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(383, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(384, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(385, 244, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(386, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(387, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(388, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(389, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(390, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(391, 245, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(392, 246, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(393, 246, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(394, 246, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(395, 246, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(396, 246, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(397, 247, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(398, 247, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(399, 247, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(400, 247, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(401, 247, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(402, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(403, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(404, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(405, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(406, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(407, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(408, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(409, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(410, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(411, 248, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(412, 249, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(413, 249, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(414, 249, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(415, 249, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(416, 250, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(417, 250, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(418, 250, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(419, 250, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(424, 252, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(425, 252, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(426, 252, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(427, 252, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(428, 252, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(429, 253, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(430, 253, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(431, 253, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(432, 254, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(433, 254, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(434, 254, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(435, 254, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(436, 254, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(437, 255, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(438, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(439, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(440, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(441, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(442, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(443, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(444, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(445, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(446, 255, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 2, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(447, 256, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(448, 256, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(449, 256, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(450, 256, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(451, 256, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(452, 256, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(453, 257, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(454, 257, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(455, 257, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(456, 257, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(457, 258, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(458, 258, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(459, 258, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(460, 258, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(461, 259, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(462, 259, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(463, 260, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(464, 260, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(465, 261, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(466, 261, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(467, 262, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 1),
(468, 262, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(469, 263, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(470, 263, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(471, 264, 19, '23123212', 'iphone', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '1000', '1000.0000', '4500.0000', '', '5500.0000', 0),
(472, 265, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(473, 266, 20, '1213213', 'Lumia 520', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 2, '181.8200', 2, '10.0000%', '500', '500.0000', '2000.0000', '', '2500.0000', 0),
(474, 267, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(475, 268, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(476, 269, 19, '23123212', 'iphone', 'standard', 0, '4545.4500', '5000.0000', '1.0000', 1, '454.5500', 2, '10.0000%', '500', '500.0000', '5000.0000', '', '5500.0000', 0),
(477, 269, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(478, 270, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(479, 271, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(482, 273, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(483, 274, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(484, 275, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(485, 276, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(486, 277, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(488, 279, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(489, 279, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(490, 279, 20, '1213213', 'Lumia 520', 'standard', 0, '1363.6400', '1500.0000', '1.0000', 1, '136.3600', 2, '10.0000%', '1000', '1000.0000', '1500.0000', '', '2500.0000', 0),
(494, 281, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(495, 281, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(496, 282, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(500, 284, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(501, 285, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(502, 286, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(503, 287, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(504, 288, 22, '34232432', 'Vivo S2', 'standard', 0, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 1),
(505, 288, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(506, 288, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(507, 289, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(508, 290, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(509, 290, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(510, 291, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(511, 291, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(512, 292, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(513, 293, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(514, 293, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(515, 294, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(516, 295, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(517, 296, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(519, 298, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 2, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(520, 299, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(521, 300, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 2, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(522, 301, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(523, 301, 21, '1231321313', 'LG E', 'standard', 0, '4090.9100', '4500.0000', '1.0000', 1, '409.0900', 2, '10.0000%', '0', '0.0000', '4500.0000', '', '4500.0000', 0),
(524, 302, 19, '23123212', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 1),
(525, 303, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(526, 304, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(527, 305, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(528, 306, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(529, 307, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(530, 308, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(531, 309, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(533, 311, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 2, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(534, 312, 20, '1213213', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(535, 313, 19, '231321', 'iphone', 'standard', NULL, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(540, 316, 20, '123456', 'Lumia 520', 'standard', NULL, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(541, 316, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(542, 316, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(543, 317, 20, '123456', 'Lumia 520', 'standard', NULL, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(544, 317, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(547, 320, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(548, 321, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(549, 322, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(550, 323, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(551, 324, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(552, 325, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(553, 326, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(554, 327, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(555, 328, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(559, 332, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(561, 334, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(562, 335, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(579, 346, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(581, 348, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(582, 349, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(588, 354, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(589, 355, 19, '231321', 'iphone', 'standard', 0, '4545.4500', '5000.0000', '1.0000', 1, '454.5500', 2, '10.0000%', '500', '500.0000', '5000.0000', '', '5500.0000', 0),
(590, 356, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(591, 357, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(592, 357, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(593, 357, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(594, 358, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(595, 359, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(596, 360, 19, '231321', 'iphone', 'standard', 0, '4954.2500', '5449.6800', '1.0000', 1, '495.4300', 2, '10.0000%', '50.32', '50.3200', '5449.6800', 'undefined', '5500.0000', 0),
(597, 361, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(598, 362, 26, 'LG678', 'LG E', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 1, '181.8200', 2, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', 1),
(599, 362, 26, 'LG678', 'LG E', 'standard', 0, '1818.1800', '2000.0000', '1.0000', 1, '181.8200', 2, '10.0000%', '0', '0.0000', '2000.0000', '', '2000.0000', 1),
(600, 363, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(601, 364, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(602, 365, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(603, 366, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(604, 366, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(605, 366, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(606, 366, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(607, 367, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(608, 367, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(609, 367, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(610, 368, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(611, 369, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(612, 370, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(613, 371, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(614, 372, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(615, 373, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(616, 374, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(617, 375, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(618, 376, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(619, 377, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(620, 377, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(621, 378, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(622, 378, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(623, 379, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(624, 379, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(625, 380, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(626, 380, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(627, 381, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(628, 381, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(629, 382, 20, '123456', 'Lumia 520', 'standard', 0, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 1),
(630, 382, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(631, 383, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(632, 384, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(633, 385, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(634, 386, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(635, 387, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(636, 388, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(637, 389, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(638, 390, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(639, 391, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(640, 392, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(641, 393, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(642, 394, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(643, 395, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(644, 396, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(645, 397, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(646, 398, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(647, 399, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(648, 400, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(649, 401, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(650, 402, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(651, 403, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(652, 404, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(653, 405, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(654, 406, 19, '231321', 'iphone', 'standard', 0, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(656, 408, 19, '231321', 'iphone', 'standard', NULL, '5000.0000', '5500.0000', '1.0000', 1, '500.0000', 2, '10.0000%', '0', '0.0000', '5500.0000', '', '5500.0000', 0),
(658, 410, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(659, 411, 19, '231321', 'iphone', 'standard', NULL, '3181.8200', '3500.0000', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000', 0),
(660, 412, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(661, 413, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(662, 414, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(663, 415, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(664, 416, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(665, 417, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(666, 418, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(667, 419, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 0),
(668, 420, 24, 'S234556', 'Galaxy S4', 'standard', NULL, '22727.2700', '25000.0000', '1.0000', 1, '2272.7300', 2, '10.0000%', '0', '0.0000', '25000.0000', '', '25000.0000', 1),
(669, 421, 19, '231321', 'iphone', 'standard', NULL, '3181.8200', '3500.0000', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000', 0),
(670, 422, 19, '231321', 'iphone', 'standard', NULL, '3181.8200', '3500.0000', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000', 0),
(671, 423, 19, '231321', 'iphone', 'standard', NULL, '3181.8200', '3500.0000', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000', 0),
(672, 424, 19, '231321', 'iphone', 'standard', NULL, '3181.8200', '3500.0000', '1.0000', 1, '318.1800', 2, '10.0000%', '0', '0.0000', '3500.0000', '', '3500.0000', 1),
(673, 425, 22, '34232432', 'Vivo S2', 'standard', NULL, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(674, 426, 22, '34232432', 'Vivo S2', 'standard', NULL, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(675, 427, 22, '34232432', 'Vivo S2', 'standard', NULL, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0),
(676, 428, 20, '123456', 'Lumia 520', 'standard', NULL, '2272.7300', '2500.0000', '1.0000', 1, '227.2700', 2, '10.0000%', '0', '0.0000', '2500.0000', '', '2500.0000', 0),
(677, 429, 22, '34232432', 'Vivo S2', 'standard', NULL, '10909.0900', '12000.0000', '1.0000', 1, '1090.9100', 2, '10.0000%', '0', '0.0000', '12000.0000', '', '12000.0000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_return_reason`
--

CREATE TABLE `sma_sale_return_reason` (
  `id` int(11) NOT NULL,
  `reason` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_sale_return_reason`
--

INSERT INTO `sma_sale_return_reason` (`id`, `reason`) VALUES
(1, 'Defective goods'),
(2, 'Product specifications are incorrect'),
(3, 'Color is not fine');

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sessions`
--

INSERT INTO `sma_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('340deb2bae8cc294538cb1f2840a29fc4d0cc30e', '::1', 1467867633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836373335303b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373836373633333b72656769737465725f69647c733a323a223837223b636173685f696e5f68616e647c733a31303a2231323430302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30362031323a33363a3332223b72656d6f76655f706f736c737c693a313b),
('3b6b8d7cbf5c7caf00a69dd7e143a8e31ad37669', '::1', 1467871794, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373837313739303b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373837313734313b72656769737465725f69647c733a323a223839223b636173685f696e5f68616e647c733a393a22333030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30372031313a32303a3337223b),
('4154c65683450332287e7b32f01d2ad07508c23c', '::1', 1467868352, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836383034393b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373836383335323b72656769737465725f69647c733a323a223837223b636173685f696e5f68616e647c733a31303a2231323430302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30362031323a33363a3332223b),
('6a6ddd1e8294cb7edc4bc8ea7e2d6ed375deafab', '::1', 1467866724, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836363730373b7265717565737465645f706167657c733a303a22223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373231343237223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2239663463383461623561326237656137383264656661646133306466666530642e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b64656661756c745f62696c6c65727c733a313a2233223b),
('994069593bb15ad487154e5df6964505e87634d2', '::1', 1467871741, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373837313430363b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373837313734313b72656769737465725f69647c733a323a223839223b636173685f696e5f68616e647c733a393a22333030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30372031313a32303a3337223b),
('9dbc489b98e283e9c731d09c1608b885466eac59', '::1', 1467870513, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836393835393b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373837303530393b72656769737465725f69647c733a323a223838223b636173685f696e5f68616e647c733a393a22313030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30372031313a30393a3130223b),
('a1732d715409a0d7244b0f252937c470bfd24704', '::1', 1467870872, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373837303532333b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373837303633383b72656769737465725f69647c733a323a223839223b636173685f696e5f68616e647c733a393a22333030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30372031313a32303a3337223b),
('b121980efdfe341e330c26f9a13603e9cf2dfaba', '::1', 1467867816, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836373638353b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373836373831363b72656769737465725f69647c733a323a223837223b636173685f696e5f68616e647c733a31303a2231323430302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30362031323a33363a3332223b72656d6f76655f706f736c737c693a313b),
('e6fdb29545eb0b1ddcc4cf1da84d5e3367272804', '::1', 1467868608, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373836383336393b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373836383630383b72656769737465725f69647c733a323a223837223b636173685f696e5f68616e647c733a31303a2231323430302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30362031323a33363a3332223b6572726f727c733a35323a224f70656e696e672062616c616e63652073686f756c6420626520657175616c206c61737420636c6f73696e672062616c616e6365223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('e91eefc415a46da3d4b4f957366ddb29a2e48444', '::1', 1467870953, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436373837303838373b6964656e746974797c733a31303a22766976656b5f31303130223b757365726e616d657c733a31303a22766976656b5f31303130223b656d61696c7c733a31383a22766976656b40657373696e6469612e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343637373831313338223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2236223b77617265686f7573655f69647c733a313a2231223b62696c6c65725f69647c733a313a2234223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2231223b73686f775f70726963657c733a313a2231223b64656661756c745f62696c6c65727c733a313a2233223b6c6173745f61637469766974797c693a313436373837303838383b72656769737465725f69647c733a323a223839223b636173685f696e5f68616e647c733a393a22333030302e30303030223b72656769737465725f6f70656e5f74696d657c733a31393a22323031362d30372d30372031313a32303a3337223b);

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0',
  `products_menu_tab` tinyint(1) NOT NULL,
  `quotations_menu_tab` tinyint(1) NOT NULL,
  `sales_menu_tab` tinyint(1) NOT NULL,
  `people_menu_tab` tinyint(1) NOT NULL,
  `purchases_menu_tab` tinyint(1) NOT NULL,
  `transfers_menu_tab` tinyint(1) NOT NULL,
  `notifications_menu_tab` tinyint(1) NOT NULL,
  `reports_menu_tab` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_settings`
--

INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`, `products_menu_tab`, `quotations_menu_tab`, `sales_menu_tab`, `people_menu_tab`, `purchases_menu_tab`, `transfers_menu_tab`, `notifications_menu_tab`, `reports_menu_tab`) VALUES
(1, 'logo2.png', 'images.png', 'Ess India Pvt Ltd', 'english', 1, 2, 'INR', 1, 10, '3.0.1.21', 1, 5, 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'RETURNSL', '', 0, 'default', 1, 1, 1, 1, 1, 1, 0, 1, 0, 'Asia/Kolkata', 800, 800, 60, 60, 0, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@tecdiary.com', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', NULL, NULL, 1, 'contact@tecdiary.com', 0, 4, 1, 0, 2, 1, 1, 1, 2, 2, '.', ',', 0, 3, 'luhartripathi', 'f8a1a55d-0d35-475d-a88c-a61dac988052', 0, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_skrill`
--

INSERT INTO `sma_skrill` (`id`, `active`, `account_email`, `secret_word`, `skrill_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_subcategories`
--

CREATE TABLE `sma_subcategories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_tax_rates`
--

INSERT INTO `sma_tax_rates` (`id`, `name`, `code`, `rate`, `type`) VALUES
(1, 'No Tax', 'NT', '0.0000', '2'),
(2, 'VAT @10%', 'VAT10', '10.0000', '1'),
(3, 'GST @6%', 'GST', '6.0000', '1'),
(4, 'VAT @20%', 'VT20', '20.0000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_till`
--

CREATE TABLE `sma_till` (
  `id` int(5) NOT NULL,
  `till_name` varchar(50) NOT NULL,
  `till_ip` varchar(50) NOT NULL,
  `store_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_till`
--

INSERT INTO `sma_till` (`id`, `till_name`, `till_ip`, `store_id`, `user_id`) VALUES
(7, 'till1', '132.132.20.120', 1, 4),
(8, 'till2', '132.132.5.59', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `show_discount` tinyint(1) NOT NULL DEFAULT '0',
  `upd_flg` tinyint(1) NOT NULL,
  `usr_id` varbinary(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_users`
--

INSERT INTO `sma_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `show_discount`, `upd_flg`, `usr_id`) VALUES
(1, 0x3a3a31, 0x0000, 'owner', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'owner@tecdiary.com', NULL, NULL, NULL, NULL, 1351661704, 1467866719, 1, 'Owner', 'Owner', 'Stock Manager', '012345678', '9f4c84ab5a2b7ea782defada30dffe0d.jpg', 'male', 1, NULL, NULL, NULL, 0, 0, 0, 1, 0, ''),
(2, 0x3a3a31, 0x3132372e302e302e31, 'vivek_1010', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'vivek@essindia.com', NULL, NULL, NULL, NULL, 1457960254, 1467867355, 1, 'vivek', 'verma', 'Swatch', '8989898989', NULL, 'male', 6, 1, 4, NULL, 1, 1, 0, 0, 0, ''),
(4, 0x3a3a31, 0x3132372e302e302e31, 'ayush_1010', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'ayush@essindia.com', NULL, NULL, NULL, NULL, 1457960353, 1467721145, 1, 'ayush', 'pant', 'Swatch', '7503104845', NULL, 'male', 5, 1, 4, NULL, NULL, NULL, 0, 0, 0, ''),
(5, 0x3a3a31, 0x3132372e302e302e31, 'manav_1010', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'manav@essindia.com', NULL, NULL, NULL, NULL, 1457960415, 1464870940, 1, 'manav', 'pandey', 'Swatch', '76564765456', NULL, 'male', 5, 2, 5, NULL, NULL, NULL, 0, 1, 0, ''),
(8, NULL, 0x3a3a31, 'ajay_1010', '48a0f06f3e488052b2fea0d6e11e4ed6288d45d0', NULL, 'ajay.kesharwani@essindia.com', NULL, NULL, NULL, NULL, 1461239465, 1461239465, 1, 'ajay', 'keshari', 'Eastern Software Systems', '8376951392', NULL, 'male', 5, 2, 4, NULL, NULL, NULL, 0, 0, 1, ''),
(10, 0x3a3a31, 0x3a3a31, 'smuankit123', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'ankit@gmail.com', NULL, NULL, NULL, NULL, 1462429638, 1467721387, 1, 'Ankit', 'Kumar', 'Info', '9812345678', NULL, 'male', 6, 2, 5, NULL, 1, 1, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_user_logins`
--

INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(1, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-06 09:16:19'),
(2, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-07 07:40:52'),
(3, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-01-14 12:13:52'),
(4, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-01-28 12:28:05'),
(5, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-02-02 05:49:35'),
(6, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-02-12 05:07:12'),
(7, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-02-12 06:18:44'),
(8, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-14 12:40:05'),
(9, 4, NULL, 0x3132372e302e302e31, 'ayush@essindia.com', '2016-03-14 13:00:37'),
(10, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-14 13:02:30'),
(11, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-14 13:02:53'),
(12, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-14 13:04:22'),
(13, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-14 13:05:37'),
(14, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-14 13:06:31'),
(15, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-14 13:21:28'),
(16, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-15 04:20:56'),
(17, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-15 04:36:36'),
(18, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-15 04:47:30'),
(19, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-15 12:20:52'),
(20, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-17 05:19:04'),
(21, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-17 09:04:08'),
(22, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-17 09:04:32'),
(23, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-17 09:09:03'),
(24, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-17 09:10:26'),
(25, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-17 09:12:05'),
(26, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-17 09:38:43'),
(27, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-18 04:29:20'),
(28, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-18 05:04:22'),
(29, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-18 07:21:11'),
(30, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-18 07:23:52'),
(31, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-18 07:24:10'),
(32, 4, NULL, 0x3132372e302e302e31, 'ayush@essindia.com', '2016-03-18 07:25:15'),
(33, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-18 07:25:40'),
(34, 3, NULL, 0x3132372e302e302e31, 'ravi@essindia.com', '2016-03-18 07:26:11'),
(35, 4, NULL, 0x3132372e302e302e31, 'ayush@essindia.com', '2016-03-18 07:27:24'),
(36, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-18 07:28:01'),
(37, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-18 10:37:24'),
(38, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-18 12:00:31'),
(39, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 05:44:54'),
(40, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 05:45:10'),
(41, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-19 05:45:16'),
(42, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 05:45:35'),
(43, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-19 05:49:43'),
(44, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 05:50:11'),
(45, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 08:40:18'),
(46, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-19 09:11:55'),
(47, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-19 11:38:14'),
(48, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-21 04:37:42'),
(49, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-21 10:34:43'),
(50, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-21 10:37:25'),
(51, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-28 12:16:34'),
(52, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-28 12:33:39'),
(53, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-29 04:51:29'),
(54, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-29 10:01:58'),
(55, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 05:21:18'),
(56, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 05:34:14'),
(57, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 06:59:39'),
(58, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:09:44'),
(59, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:18:28'),
(60, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:19:05'),
(61, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:19:59'),
(62, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-30 07:22:27'),
(63, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:25:52'),
(64, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:26:55'),
(65, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-30 07:27:13'),
(66, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 07:28:25'),
(67, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-30 07:30:06'),
(68, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 08:17:12'),
(69, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-03-30 08:19:40'),
(70, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 08:20:10'),
(71, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 08:20:17'),
(72, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-30 08:36:28'),
(73, 4, NULL, 0x3132372e302e302e31, 'ayush@essindia.com', '2016-03-30 10:40:45'),
(74, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-03-30 10:42:04'),
(75, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-31 04:56:17'),
(76, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-31 08:49:23'),
(77, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-03-31 16:47:46'),
(78, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-01 04:01:45'),
(79, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-04-01 04:02:07'),
(80, 5, NULL, 0x3132372e302e302e31, 'manav@essindia.com', '2016-04-01 04:02:31'),
(81, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-01 04:02:45'),
(82, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-02 05:13:50'),
(83, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-02 10:02:57'),
(84, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-04 04:40:16'),
(85, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-04 08:39:09'),
(86, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-04 08:43:07'),
(87, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-04 08:43:59'),
(88, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-04 08:54:05'),
(89, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-04-04 10:18:10'),
(90, 1, NULL, 0x3132372e302e302e31, 'owner@tecdiary.com', '2016-04-04 10:57:52'),
(91, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-04-04 11:03:00'),
(92, 2, NULL, 0x3132372e302e302e31, 'vivek@essindia.com', '2016-04-04 12:04:36'),
(93, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-05 05:10:23'),
(94, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-05 06:33:22'),
(95, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-05 09:52:39'),
(96, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-05 09:53:50'),
(97, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-05 10:45:32'),
(98, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-06 04:25:01'),
(99, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-06 08:25:47'),
(100, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-06 09:08:51'),
(101, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-06 11:13:07'),
(102, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-06 11:15:31'),
(103, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-07 04:15:49'),
(104, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-07 07:51:37'),
(105, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-07 12:49:57'),
(106, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 04:56:02'),
(107, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 06:28:36'),
(108, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 06:47:46'),
(109, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 07:30:50'),
(110, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-08 07:39:39'),
(111, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 08:24:27'),
(112, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 08:25:55'),
(113, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 08:27:10'),
(114, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 08:36:27'),
(115, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 08:39:17'),
(116, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 10:47:04'),
(117, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 11:21:25'),
(118, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 11:55:41'),
(119, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 11:58:09'),
(120, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:00:53'),
(121, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 12:02:19'),
(122, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:03:21'),
(123, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 12:14:13'),
(124, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:16:27'),
(125, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:21:12'),
(126, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-08 12:34:52'),
(127, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:37:29'),
(128, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 12:40:33'),
(129, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:43:06'),
(130, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 12:56:14'),
(131, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 12:58:18'),
(132, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 13:11:08'),
(133, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 13:15:14'),
(134, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 13:17:04'),
(135, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 13:17:42'),
(136, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 13:21:04'),
(137, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-08 13:21:43'),
(138, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-08 13:22:21'),
(139, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-11 04:44:59'),
(140, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-11 08:35:38'),
(141, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-11 08:39:42'),
(142, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-11 11:04:06'),
(143, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-11 12:08:35'),
(144, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-12 04:32:48'),
(145, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-12 05:20:01'),
(146, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-12 05:23:39'),
(147, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-12 05:29:37'),
(148, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-12 05:34:45'),
(149, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 05:39:51'),
(150, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 05:51:58'),
(151, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 07:23:03'),
(152, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 09:20:06'),
(153, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 09:20:25'),
(154, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 10:44:44'),
(155, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 11:49:00'),
(156, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-12 12:26:31'),
(157, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-13 04:26:26'),
(158, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-13 05:49:16'),
(159, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-13 05:52:33'),
(160, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-13 07:34:13'),
(161, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-13 08:47:43'),
(162, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-13 08:59:12'),
(163, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-13 10:55:47'),
(164, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-13 10:59:35'),
(165, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-14 04:29:53'),
(166, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-14 07:37:14'),
(167, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-14 08:45:19'),
(168, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-14 09:02:29'),
(169, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-14 09:04:23'),
(170, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-14 09:33:29'),
(171, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-14 10:16:28'),
(172, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-14 10:17:14'),
(173, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-14 11:04:10'),
(174, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 04:23:56'),
(175, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-15 07:00:06'),
(176, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 07:03:06'),
(177, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 07:25:28'),
(178, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 07:26:18'),
(179, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 07:27:07'),
(180, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 07:29:40'),
(181, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-15 08:40:08'),
(182, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 09:05:53'),
(183, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-15 09:25:23'),
(184, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 09:28:22'),
(185, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 10:38:44'),
(186, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 10:40:32'),
(187, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 10:48:32'),
(188, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-15 11:58:20'),
(189, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 12:01:02'),
(190, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 12:18:21'),
(191, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-15 12:45:56'),
(192, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 04:23:39'),
(193, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 04:24:29'),
(194, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 04:26:45'),
(195, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 05:27:39'),
(196, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 05:32:15'),
(197, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 05:43:30'),
(198, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 06:20:01'),
(199, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 06:20:32'),
(200, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 06:47:24'),
(201, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 07:19:07'),
(202, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 08:35:59'),
(203, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 08:58:39'),
(204, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 11:00:12'),
(205, 1, NULL, 0x3a3a31, 'owner@tecdiary.com', '2016-04-16 12:10:58'),
(206, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 12:11:37'),
(207, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 12:14:34'),
(208, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-16 12:17:25'),
(209, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-18 04:43:02'),
(210, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-18 05:27:08'),
(211, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-18 05:30:29'),
(212, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-18 05:33:33'),
(213, 2, NULL, 0x3a3a31, 'vivek@essindia.com', '2016-04-18 05:43:55'),
(214, 5, NULL, 0x3a3a31, 'manav@essindia.com', '2016-04-18 06:45:18'),
(215, 1, NULL, 0x3a3a31, 'owner', '2016-04-18 06:56:49'),
(216, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 07:10:38'),
(217, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-18 07:18:26'),
(218, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 08:41:46'),
(219, 1, NULL, 0x3a3a31, 'owner', '2016-04-18 08:42:57'),
(220, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 08:54:07'),
(221, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 09:04:35'),
(222, 1, NULL, 0x3a3a31, 'owner', '2016-04-18 09:05:38'),
(223, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 09:09:52'),
(224, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-18 09:16:52'),
(225, 1, NULL, 0x3a3a31, 'owner', '2016-04-18 09:19:28'),
(226, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-18 09:27:29'),
(227, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 09:29:43'),
(228, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-18 09:30:54'),
(229, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 10:09:43'),
(230, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 10:10:37'),
(231, 1, NULL, 0x3a3a31, 'owner', '2016-04-18 10:11:55'),
(232, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-18 10:36:59'),
(233, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-18 12:54:53'),
(234, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-19 04:23:00'),
(235, 1, NULL, 0x3a3a31, 'owner', '2016-04-19 04:51:38'),
(236, 1, NULL, 0x3a3a31, 'owner', '2016-04-19 07:29:42'),
(237, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-19 10:10:55'),
(238, 1, NULL, 0x3a3a31, 'owner', '2016-04-19 10:28:03'),
(239, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 04:29:46'),
(240, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-20 05:53:27'),
(241, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 06:13:37'),
(242, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 06:32:29'),
(243, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-20 06:51:03'),
(244, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 06:51:38'),
(245, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-20 07:05:47'),
(246, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 07:09:30'),
(247, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 08:39:19'),
(248, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 08:41:56'),
(249, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-20 09:59:45'),
(250, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 10:18:35'),
(251, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-20 10:26:17'),
(252, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 10:26:44'),
(253, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-20 10:29:01'),
(254, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 10:31:30'),
(255, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-20 10:33:34'),
(256, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 10:35:17'),
(257, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 11:37:54'),
(258, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 11:38:45'),
(259, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 11:53:05'),
(260, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-20 11:54:35'),
(261, 1, NULL, 0x3a3a31, 'owner', '2016-04-20 11:56:03'),
(262, 1, NULL, 0x3a3a31, 'owner', '2016-04-21 04:15:48'),
(263, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-21 06:40:24'),
(264, 1, NULL, 0x3a3a31, 'owner', '2016-04-21 06:41:21'),
(265, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-21 06:42:06'),
(266, 1, NULL, 0x3a3a31, 'owner', '2016-04-21 07:12:24'),
(267, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-21 07:29:09'),
(268, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-21 07:38:44'),
(269, 1, NULL, 0x3a3a31, 'owner', '2016-04-21 08:33:52'),
(270, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 04:35:29'),
(271, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 05:34:45'),
(272, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 05:37:03'),
(273, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-22 08:46:51'),
(274, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 08:47:32'),
(275, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-22 08:48:11'),
(276, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-22 09:22:32'),
(277, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 10:28:37'),
(278, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-22 10:35:52'),
(279, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 10:38:56'),
(280, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-22 10:39:25'),
(281, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-22 10:49:05'),
(282, 1, NULL, 0x3a3a31, 'owner', '2016-04-22 10:53:00'),
(283, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 04:58:35'),
(284, 2, NULL, 0x3133322e3133322e352e3937, 'vivek_1010', '2016-04-25 05:24:04'),
(285, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 05:58:24'),
(286, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 05:59:31'),
(287, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 06:13:29'),
(288, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 06:21:12'),
(289, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 06:21:32'),
(290, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 06:21:55'),
(291, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 06:22:11'),
(292, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-25 06:24:40'),
(293, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 06:26:08'),
(294, 3, NULL, 0x3a3a31, 'ravi_1010', '2016-04-25 06:48:03'),
(295, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 08:39:49'),
(296, 3, NULL, 0x3a3a31, 'ravi_1010', '2016-04-25 08:49:39'),
(297, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 08:49:50'),
(298, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 08:50:13'),
(299, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 08:51:05'),
(300, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 10:02:06'),
(301, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-25 10:25:23'),
(302, 1, NULL, 0x3a3a31, 'owner', '2016-04-25 11:05:56'),
(303, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-26 04:30:06'),
(304, 1, NULL, 0x3a3a31, 'owner', '2016-04-26 04:30:18'),
(305, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-26 04:44:56'),
(306, 2, NULL, 0x3133322e3133322e352e3937, 'vivek_1010', '2016-04-26 04:46:37'),
(307, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-26 04:49:56'),
(308, 2, NULL, 0x3133322e3133322e352e3937, 'vivek_1010', '2016-04-26 04:51:13'),
(309, 1, NULL, 0x3133322e3133322e352e3937, 'owner', '2016-04-26 04:58:31'),
(310, 1, NULL, 0x3a3a31, 'owner', '2016-04-26 06:02:17'),
(311, 1, NULL, 0x3a3a31, 'owner', '2016-04-26 12:21:22'),
(312, 1, NULL, 0x3a3a31, 'owner', '2016-04-26 12:40:46'),
(313, 1, NULL, 0x3a3a31, 'owner', '2016-04-26 12:45:33'),
(314, 1, NULL, 0x3a3a31, 'owner', '2016-04-27 04:46:03'),
(315, 3, NULL, 0x3a3a31, 'ravi_1010', '2016-04-27 04:52:20'),
(316, 1, NULL, 0x3a3a31, 'owner', '2016-04-27 05:09:25'),
(317, 1, NULL, 0x3a3a31, 'owner', '2016-04-27 05:51:59'),
(318, 3, NULL, 0x3a3a31, 'ravi_1010', '2016-04-27 10:39:16'),
(319, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-27 11:13:05'),
(320, 1, NULL, 0x3a3a31, 'owner', '2016-04-27 11:47:01'),
(321, 1, NULL, 0x3a3a31, 'owner', '2016-04-28 04:47:29'),
(322, 1, NULL, 0x3a3a31, 'owner', '2016-04-28 12:11:05'),
(323, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-29 04:43:27'),
(324, 1, NULL, 0x3a3a31, 'owner', '2016-04-29 04:46:50'),
(325, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-29 06:27:06'),
(326, 1, NULL, 0x3a3a31, 'owner', '2016-04-29 06:33:11'),
(327, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 05:06:17'),
(328, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 06:26:39'),
(329, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 07:16:23'),
(330, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 07:38:26'),
(331, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-30 08:50:14'),
(332, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 08:54:23'),
(333, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 08:55:11'),
(334, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-30 09:08:06'),
(335, 1, NULL, 0x3a3a31, 'owner', '2016-04-30 09:12:58'),
(336, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-30 09:24:53'),
(337, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-30 09:44:25'),
(338, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-30 09:53:04'),
(339, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-30 10:22:57'),
(340, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-30 10:23:16'),
(341, 5, NULL, 0x3a3a31, 'manav_1010', '2016-04-30 10:29:17'),
(342, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-04-30 10:32:40'),
(343, 1, NULL, 0x3a3a31, 'owner', '2016-05-02 04:34:37'),
(344, 1, NULL, 0x3a3a31, 'owner', '2016-05-02 05:55:42'),
(345, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-02 10:45:51'),
(346, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-02 10:46:33'),
(347, 1, NULL, 0x3a3a31, 'owner', '2016-05-02 10:58:54'),
(348, 1, NULL, 0x3a3a31, 'owner', '2016-05-03 04:56:33'),
(349, 1, NULL, 0x3a3a31, 'owner', '2016-05-04 06:19:28'),
(350, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-04 07:27:41'),
(351, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-04 07:28:12'),
(352, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-04 09:22:43'),
(353, 1, NULL, 0x3a3a31, 'owner', '2016-05-04 10:38:57'),
(354, 1, NULL, 0x3a3a31, 'owner', '2016-05-05 04:38:45'),
(355, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-05 08:41:01'),
(356, 1, NULL, 0x3a3a31, 'owner', '2016-05-05 08:42:38'),
(357, 1, NULL, 0x3a3a31, 'owner', '2016-05-05 09:41:41'),
(358, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-05 09:56:40'),
(359, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-05 10:15:36'),
(360, 1, NULL, 0x3a3a31, 'owner', '2016-05-05 12:47:17'),
(361, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-05 12:56:47'),
(362, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 04:41:49'),
(363, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-06 04:42:16'),
(364, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 06:17:45'),
(365, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-06 06:33:52'),
(366, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-06 07:02:59'),
(367, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 07:03:19'),
(368, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-06 10:07:58'),
(369, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 10:08:58'),
(370, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-06 10:09:53'),
(371, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 10:17:15'),
(372, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 10:38:42'),
(373, 1, NULL, 0x3a3a31, 'owner', '2016-05-06 11:10:42'),
(374, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-07 05:54:49'),
(375, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-07 06:01:35'),
(376, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-07 06:09:29'),
(377, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 06:10:27'),
(378, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 06:10:46'),
(379, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 06:11:20'),
(380, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-07 06:12:49'),
(381, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 06:22:12'),
(382, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 10:13:04'),
(383, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 10:33:09'),
(384, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 10:35:03'),
(385, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 10:36:55'),
(386, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 10:39:06'),
(387, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 11:46:56'),
(388, 1, NULL, 0x3a3a31, 'owner', '2016-05-07 12:06:36'),
(389, 1, NULL, 0x3a3a31, 'owner', '2016-05-09 04:17:33'),
(390, 1, NULL, 0x3a3a31, 'owner', '2016-05-09 04:41:09'),
(391, 1, NULL, 0x3a3a31, 'owner', '2016-05-09 04:46:58'),
(392, 1, NULL, 0x3a3a31, 'owner', '2016-05-09 09:34:31'),
(393, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 04:40:13'),
(394, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 06:11:32'),
(395, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 06:14:36'),
(396, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 07:45:38'),
(397, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 08:52:26'),
(398, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 11:32:46'),
(399, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 11:35:02'),
(400, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 11:37:37'),
(401, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 11:42:32'),
(402, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 11:47:38'),
(403, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 12:49:02'),
(404, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 12:59:38'),
(405, 1, NULL, 0x3a3a31, 'owner', '2016-05-10 13:02:50'),
(406, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 04:50:22'),
(407, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 04:55:34'),
(408, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 05:22:45'),
(409, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 05:43:48'),
(410, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 06:01:01'),
(411, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 06:28:02'),
(412, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 06:31:38'),
(413, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 08:35:03'),
(414, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-11 10:00:52'),
(415, 1, NULL, 0x3a3a31, 'owner', '2016-05-11 10:04:20'),
(416, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 04:51:56'),
(417, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 06:13:12'),
(418, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 07:24:26'),
(419, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 08:39:02'),
(420, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 08:44:31'),
(421, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 09:35:15'),
(422, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 09:40:20'),
(423, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 11:23:46'),
(424, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 11:56:12'),
(425, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 12:12:13'),
(426, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 12:24:24'),
(427, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 12:25:47'),
(428, 1, NULL, 0x3a3a31, 'owner', '2016-05-12 14:00:50'),
(429, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-12 14:02:10'),
(430, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 09:58:49'),
(431, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 09:59:06'),
(432, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 10:16:48'),
(433, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 10:17:36'),
(434, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 10:31:04'),
(435, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 10:31:39'),
(436, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 10:51:57'),
(437, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 11:17:08'),
(438, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-13 11:29:22'),
(439, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:32:10'),
(440, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:33:20'),
(441, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-13 11:34:11'),
(442, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:36:43'),
(443, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 11:37:33'),
(444, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:38:31'),
(445, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 11:39:45'),
(446, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:43:06'),
(447, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:58:03'),
(448, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 11:58:35'),
(449, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 11:59:23'),
(450, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 12:00:12'),
(451, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 12:07:31'),
(452, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 12:26:14'),
(453, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 12:29:47'),
(454, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 12:44:55'),
(455, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 12:59:13'),
(456, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 13:16:34'),
(457, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 13:20:22'),
(458, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 13:21:13'),
(459, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 13:22:19'),
(460, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 13:27:18'),
(461, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 13:40:09'),
(462, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 13:49:46'),
(463, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 13:52:21'),
(464, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 13:53:40'),
(465, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 14:01:47'),
(466, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 14:02:10'),
(467, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-13 14:03:08'),
(468, 1, NULL, 0x3a3a31, 'owner', '2016-05-13 14:04:52'),
(469, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 05:23:41'),
(470, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-16 06:55:30'),
(471, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 07:12:50'),
(472, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 07:32:00'),
(473, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 09:27:58'),
(474, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 10:21:26'),
(475, 1, NULL, 0x3a3a31, 'owner', '2016-05-16 10:48:12'),
(476, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 04:45:36'),
(477, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 06:41:04'),
(478, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 06:43:21'),
(479, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 07:01:27'),
(480, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 10:49:56'),
(481, 1, NULL, 0x3a3a31, 'owner', '2016-05-17 12:25:47'),
(482, 1, NULL, 0x3a3a31, 'owner', '2016-05-18 04:48:31'),
(483, 1, NULL, 0x3a3a31, 'owner', '2016-05-18 05:38:24'),
(484, 1, NULL, 0x3a3a31, 'owner', '2016-05-18 06:29:00'),
(485, 1, NULL, 0x3a3a31, 'owner', '2016-05-18 08:56:08'),
(486, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 04:56:46'),
(487, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 05:45:14'),
(488, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 06:01:05'),
(489, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 07:06:33'),
(490, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-19 07:16:49'),
(491, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 07:17:57'),
(492, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 10:46:39'),
(493, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-19 10:51:50'),
(494, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 10:58:04'),
(495, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-19 11:02:25'),
(496, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 11:26:28'),
(497, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 12:05:28'),
(498, 1, NULL, 0x3a3a31, 'owner', '2016-05-19 12:14:27'),
(499, 1, NULL, 0x3a3a31, 'owner', '2016-05-20 04:40:03'),
(500, 1, NULL, 0x3a3a31, 'owner', '2016-05-20 05:02:38'),
(501, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-20 10:00:58'),
(502, 1, NULL, 0x3a3a31, 'owner', '2016-05-20 10:02:17'),
(503, 1, NULL, 0x3a3a31, 'owner', '2016-05-20 10:27:06'),
(504, 1, NULL, 0x3a3a31, 'owner', '2016-05-20 10:30:05'),
(505, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 04:40:26'),
(506, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 04:47:18'),
(507, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 04:48:44'),
(508, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 04:50:42'),
(509, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 04:54:20'),
(510, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 04:59:16'),
(511, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 05:13:35'),
(512, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 05:15:16'),
(513, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 05:17:18'),
(514, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 05:25:50'),
(515, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:41:18'),
(516, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:44:19'),
(517, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:49:26'),
(518, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:53:38'),
(519, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:54:45'),
(520, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 06:56:13'),
(521, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:12:59'),
(522, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:14:05'),
(523, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:17:55'),
(524, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-21 07:18:48'),
(525, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:19:25'),
(526, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-21 07:23:25'),
(527, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-21 07:24:25'),
(528, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 07:27:30'),
(529, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:28:32'),
(530, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-05-21 07:30:59'),
(531, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-05-21 07:31:51'),
(532, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:32:31'),
(533, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:34:56'),
(534, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:36:02'),
(535, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:41:05'),
(536, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:45:38'),
(537, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 07:50:23'),
(538, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 08:37:43'),
(539, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-21 10:12:08'),
(540, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 10:13:27'),
(541, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 10:14:11'),
(542, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 10:14:58'),
(543, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:06:41'),
(544, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:09:13'),
(545, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:10:27'),
(546, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:18:22'),
(547, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:21:00'),
(548, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:23:03'),
(549, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:30:40'),
(550, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:31:11'),
(551, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:35:30'),
(552, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:36:40'),
(553, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:37:14'),
(554, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:40:15'),
(555, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 11:43:34'),
(556, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:08:07'),
(557, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:10:51'),
(558, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:11:55'),
(559, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:16:23'),
(560, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:25:41'),
(561, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:28:05'),
(562, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:50:02'),
(563, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 12:55:11'),
(564, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 13:05:23'),
(565, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 13:06:29'),
(566, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 13:13:57'),
(567, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 13:34:19'),
(568, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 14:08:51'),
(569, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 14:09:10'),
(570, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 14:20:15'),
(571, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 14:24:58'),
(572, 1, NULL, 0x3a3a31, 'owner', '2016-05-21 14:26:30'),
(573, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-21 14:27:52'),
(574, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 04:48:37'),
(575, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 04:56:10'),
(576, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:22:49'),
(577, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:32:10'),
(578, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:33:48'),
(579, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:38:18'),
(580, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:38:49'),
(581, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 05:43:09'),
(582, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 06:31:23'),
(583, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 06:45:57'),
(584, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 06:47:01'),
(585, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 06:51:32'),
(586, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 06:56:16'),
(587, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 07:04:16'),
(588, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 07:26:06'),
(589, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 07:30:45'),
(590, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 07:38:04'),
(591, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 07:50:45'),
(592, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 09:15:13'),
(593, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 09:17:02'),
(594, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-05-23 09:18:01'),
(595, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-23 09:19:48'),
(596, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-05-23 09:21:04'),
(597, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 09:23:01'),
(598, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 09:29:20'),
(599, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 09:29:52'),
(600, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 09:33:06'),
(601, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 09:34:27'),
(602, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-05-23 09:54:05'),
(603, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 09:56:29'),
(604, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 09:57:47'),
(605, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 09:59:05'),
(606, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 10:01:17'),
(607, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-23 10:02:42'),
(608, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-23 10:03:11'),
(609, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 10:04:46'),
(610, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-23 10:07:54'),
(611, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 10:08:52'),
(612, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-23 10:09:48'),
(613, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 10:51:22'),
(614, 1, NULL, 0x3a3a31, 'owner', '2016-05-23 11:48:34'),
(615, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 04:28:47'),
(616, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 04:55:57'),
(617, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 07:32:21'),
(618, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 08:31:20'),
(619, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 09:53:21'),
(620, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-24 09:58:10'),
(621, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-24 09:59:03'),
(622, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 09:59:50'),
(623, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 11:24:12'),
(624, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 12:41:54'),
(625, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 12:49:42'),
(626, 1, NULL, 0x3a3a31, 'owner', '2016-05-24 12:53:12'),
(627, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 04:52:45'),
(628, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 05:03:50'),
(629, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 05:10:50'),
(630, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 05:49:33'),
(631, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 05:56:36'),
(632, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 06:02:10'),
(633, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 06:34:10'),
(634, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 09:54:30'),
(635, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 09:55:49'),
(636, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-25 10:02:44'),
(637, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 10:05:13'),
(638, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 10:28:49'),
(639, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 10:30:17'),
(640, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-25 11:16:08'),
(641, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-25 11:18:50'),
(642, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-25 11:22:58'),
(643, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 11:34:47'),
(644, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 11:57:47'),
(645, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 11:58:58'),
(646, 1, NULL, 0x3a3a31, 'owner', '2016-05-25 12:00:25'),
(647, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 04:48:11'),
(648, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:04:19'),
(649, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:24:59'),
(650, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:30:28'),
(651, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:31:54'),
(652, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:34:30'),
(653, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:34:49'),
(654, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:39:58'),
(655, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:41:06'),
(656, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:41:43'),
(657, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:44:46'),
(658, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:45:07'),
(659, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:45:41'),
(660, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:49:18'),
(661, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:49:36'),
(662, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:50:23'),
(663, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 10:53:24'),
(664, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 10:59:00'),
(665, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-26 11:03:18'),
(666, 1, NULL, 0x3a3a31, 'owner', '2016-05-26 11:15:58'),
(667, 1, NULL, 0x3a3a31, 'owner', '2016-05-27 04:29:38'),
(668, 1, NULL, 0x3a3a31, 'owner', '2016-05-30 04:36:41'),
(669, 1, NULL, 0x3a3a31, 'owner', '2016-05-30 11:12:38'),
(670, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-30 12:11:57'),
(671, 1, NULL, 0x3a3a31, 'owner', '2016-05-30 12:30:22'),
(672, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-30 12:30:55'),
(673, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-30 13:28:22'),
(674, 1, NULL, 0x3a3a31, 'owner', '2016-05-31 04:35:52'),
(675, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-31 05:01:11'),
(676, 1, NULL, 0x3a3a31, 'owner', '2016-05-31 05:02:34'),
(677, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-05-31 05:02:56'),
(678, 1, NULL, 0x3a3a31, 'owner', '2016-05-31 12:07:33'),
(679, 1, NULL, 0x3a3a31, 'owner', '2016-05-31 12:09:26'),
(680, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-31 12:11:29'),
(681, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-31 12:12:05'),
(682, 5, NULL, 0x3a3a31, 'manav_1010', '2016-05-31 12:14:43'),
(683, 1, NULL, 0x3a3a31, 'owner', '2016-05-31 12:42:19'),
(684, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 04:35:07'),
(685, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 04:44:46'),
(686, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 04:51:19'),
(687, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 04:51:50'),
(688, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 05:01:06'),
(689, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-01 05:12:42'),
(690, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 05:22:20'),
(691, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 06:01:12'),
(692, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 06:03:03'),
(693, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 06:10:11'),
(694, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 06:54:22'),
(695, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 06:54:42'),
(696, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 06:55:34'),
(697, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 06:57:28'),
(698, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 06:58:45'),
(699, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 07:09:55'),
(700, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 07:24:44'),
(701, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 07:25:16'),
(702, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 07:32:07'),
(703, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 08:26:11'),
(704, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 08:28:44'),
(705, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 08:30:03'),
(706, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 08:32:30'),
(707, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 08:37:09'),
(708, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 08:37:48'),
(709, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 08:45:31'),
(710, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-01 08:52:42'),
(711, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 09:05:21'),
(712, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 09:11:00'),
(713, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 09:11:48'),
(714, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 09:24:14'),
(715, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 09:26:03'),
(716, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 09:28:32'),
(717, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 09:29:28'),
(718, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 10:03:16'),
(719, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-01 10:23:19'),
(720, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 10:23:57'),
(721, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-01 10:28:06'),
(722, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 10:51:17'),
(723, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 11:01:35'),
(724, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 11:49:31'),
(725, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 11:52:38'),
(726, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-01 12:13:43'),
(727, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-01 12:22:27'),
(728, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 12:39:07'),
(729, 1, NULL, 0x3a3a31, 'owner', '2016-06-01 12:49:44'),
(730, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 04:47:12'),
(731, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 09:05:26'),
(732, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 09:32:28'),
(733, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 09:58:37'),
(734, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-02 10:26:33'),
(735, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 10:30:58'),
(736, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 10:36:48'),
(737, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 10:52:49'),
(738, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-02 12:31:02'),
(739, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-02 12:34:43'),
(740, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-02 12:35:12'),
(741, 5, NULL, 0x3a3a31, 'manav_1010', '2016-06-02 12:35:40'),
(742, 1, NULL, 0x3a3a31, 'owner', '2016-06-02 12:36:49'),
(743, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 04:42:56'),
(744, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-03 05:02:24'),
(745, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 05:12:24'),
(746, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 05:29:08'),
(747, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 06:01:08'),
(748, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-03 06:02:46'),
(749, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-03 06:08:44'),
(750, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 06:09:47'),
(751, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-03 06:10:41'),
(752, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 06:14:29'),
(753, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 07:21:32'),
(754, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-03 08:40:09'),
(755, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 09:04:12'),
(756, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 10:05:05'),
(757, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 10:58:27'),
(758, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 11:08:31'),
(759, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 11:41:45'),
(760, 1, NULL, 0x3a3a31, 'owner', '2016-06-03 12:07:23'),
(761, 1, NULL, 0x3a3a31, 'owner', '2016-06-04 04:53:47'),
(762, 1, NULL, 0x3a3a31, 'owner', '2016-06-04 05:06:38'),
(763, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-04 08:53:52'),
(764, 1, NULL, 0x3a3a31, 'owner', '2016-06-04 09:26:41'),
(765, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 04:41:58'),
(766, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 06:22:45'),
(767, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 06:34:56'),
(768, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 09:18:42'),
(769, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 09:27:48'),
(770, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-06 09:28:16'),
(771, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-06 09:29:17'),
(772, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 09:30:49'),
(773, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 09:48:38'),
(774, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-06 10:04:20'),
(775, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 10:11:00'),
(776, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-06 10:11:50'),
(777, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 10:17:21'),
(778, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 11:11:51'),
(779, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 11:16:14'),
(780, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 12:23:35'),
(781, 1, NULL, 0x3a3a31, 'owner', '2016-06-06 12:32:35'),
(782, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-06 12:33:22'),
(783, 1, NULL, 0x3a3a31, 'owner', '2016-06-07 05:05:10'),
(784, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-07 05:25:40'),
(785, 1, NULL, 0x3a3a31, 'owner', '2016-06-15 09:23:59'),
(786, 1, NULL, 0x3a3a31, 'owner', '2016-06-15 12:30:30'),
(787, 1, NULL, 0x3a3a31, 'owner', '2016-06-16 04:49:31'),
(788, 1, NULL, 0x3a3a31, 'owner', '2016-06-16 09:37:06'),
(789, 1, NULL, 0x3a3a31, 'owner', '2016-06-16 10:38:28'),
(790, 1, NULL, 0x3132372e302e302e31, 'owner', '2016-06-17 04:54:37'),
(791, 2, NULL, 0x3132372e302e302e31, 'vivek_1010', '2016-06-17 06:59:36'),
(792, 1, NULL, 0x3132372e302e302e31, 'owner', '2016-06-17 06:59:53'),
(793, 1, NULL, 0x3a3a31, 'owner', '2016-06-18 04:51:45'),
(794, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-18 04:52:33'),
(795, 1, NULL, 0x3a3a31, 'owner', '2016-06-18 04:54:59'),
(796, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-18 04:57:08'),
(797, 1, NULL, 0x3a3a31, 'owner', '2016-06-18 05:03:01'),
(798, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-18 05:03:56'),
(799, 1, NULL, 0x3a3a31, 'owner', '2016-06-18 05:34:10'),
(800, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-18 10:45:47'),
(801, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-18 11:18:49'),
(802, 1, NULL, 0x3a3a31, 'owner', '2016-06-18 12:19:15');
INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(803, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 05:02:44'),
(804, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 05:47:35'),
(805, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-20 05:47:53'),
(806, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 06:46:57'),
(807, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-20 07:01:25'),
(808, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 07:07:43'),
(809, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 08:33:54'),
(810, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-20 08:37:43'),
(811, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 09:51:56'),
(812, 1, NULL, 0x3a3a31, 'owner', '2016-06-20 10:28:29'),
(813, 1, NULL, 0x3a3a31, 'owner', '2016-06-21 06:54:15'),
(814, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-21 07:07:36'),
(815, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-21 07:21:03'),
(816, 1, NULL, 0x3a3a31, 'owner', '2016-06-21 10:06:40'),
(817, 1, NULL, 0x3a3a31, 'owner', '2016-06-21 12:41:29'),
(818, 1, NULL, 0x3a3a31, 'owner', '2016-06-22 05:02:26'),
(819, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-22 13:05:11'),
(820, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-22 13:46:35'),
(821, 1, NULL, 0x3a3a31, 'owner', '2016-06-22 14:21:52'),
(822, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-22 14:28:11'),
(823, 1, NULL, 0x3a3a31, 'owner', '2016-06-22 14:42:42'),
(824, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-22 14:46:24'),
(825, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-23 04:48:32'),
(826, 1, NULL, 0x3a3a31, 'owner', '2016-06-23 05:44:59'),
(827, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-23 06:07:54'),
(828, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 04:48:22'),
(829, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 06:42:26'),
(830, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 06:42:56'),
(831, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 06:47:52'),
(832, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 07:03:33'),
(833, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 09:38:08'),
(834, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 11:33:42'),
(835, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 11:39:02'),
(836, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 11:54:17'),
(837, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 11:57:11'),
(838, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 12:02:10'),
(839, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 12:08:01'),
(840, 1, NULL, 0x3a3a31, 'owner', '2016-06-24 12:09:58'),
(841, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 12:11:08'),
(842, 10, NULL, 0x3a3a31, 'smuankit123', '2016-06-24 12:44:31'),
(843, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-24 12:47:11'),
(844, 10, NULL, 0x3a3a31, 'smuankit123', '2016-06-24 12:48:17'),
(845, 10, NULL, 0x3a3a31, 'smuankit123', '2016-06-24 12:49:23'),
(846, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-27 05:28:28'),
(847, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-27 05:30:20'),
(848, 10, NULL, 0x3a3a31, 'smuankit123', '2016-06-27 05:34:12'),
(849, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-27 05:37:57'),
(850, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-27 06:27:04'),
(851, 1, NULL, 0x3a3a31, 'owner', '2016-06-27 06:39:03'),
(852, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-27 06:39:59'),
(853, 1, NULL, 0x3a3a31, 'owner', '2016-06-27 07:11:38'),
(854, 1, NULL, 0x3a3a31, 'owner', '2016-06-28 09:09:04'),
(855, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-28 09:10:12'),
(856, 1, NULL, 0x3a3a31, 'owner', '2016-06-28 09:14:48'),
(857, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-28 09:15:03'),
(858, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-06-28 09:16:20'),
(859, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-28 12:23:25'),
(860, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 04:38:57'),
(861, 1, NULL, 0x3a3a31, 'owner', '2016-06-29 04:48:54'),
(862, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 04:51:52'),
(863, 1, NULL, 0x3a3a31, 'owner', '2016-06-29 04:53:37'),
(864, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 04:54:21'),
(865, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 05:01:43'),
(866, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 06:35:56'),
(867, 1, NULL, 0x3a3a31, 'owner', '2016-06-29 09:56:46'),
(868, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 09:57:29'),
(869, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-29 12:44:05'),
(870, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-30 04:48:20'),
(871, 1, NULL, 0x3a3a31, 'owner', '2016-06-30 06:08:18'),
(872, 1, NULL, 0x3a3a31, 'owner', '2016-06-30 06:12:04'),
(873, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-30 06:17:11'),
(874, 1, NULL, 0x3a3a31, 'owner', '2016-06-30 06:29:14'),
(875, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-06-30 08:53:59'),
(876, 1, NULL, 0x3a3a31, 'owner', '2016-07-04 07:46:10'),
(877, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-04 09:37:23'),
(878, 1, NULL, 0x3a3a31, 'owner', '2016-07-04 09:47:46'),
(879, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-04 10:41:53'),
(880, 1, NULL, 0x3a3a31, 'owner', '2016-07-04 11:21:11'),
(881, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-07-04 11:21:30'),
(882, 1, NULL, 0x3a3a31, 'owner', '2016-07-04 11:22:12'),
(883, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-04 12:34:07'),
(884, 1, NULL, 0x3a3a31, 'owner', '2016-07-04 12:34:37'),
(885, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-04 12:35:25'),
(886, 1, NULL, 0x3a3a31, 'owner', '2016-07-05 04:46:49'),
(887, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 04:53:51'),
(888, 1, NULL, 0x3a3a31, 'owner', '2016-07-05 09:19:04'),
(889, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 09:20:06'),
(890, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 10:41:29'),
(891, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 11:10:00'),
(892, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 12:18:36'),
(893, 4, NULL, 0x3a3a31, 'ayush_1010', '2016-07-05 12:19:05'),
(894, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 12:20:59'),
(895, 10, NULL, 0x3a3a31, 'smuankit123', '2016-07-05 12:23:08'),
(896, 1, NULL, 0x3a3a31, 'owner', '2016-07-05 12:23:47'),
(897, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-05 12:24:57'),
(898, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-06 04:58:58'),
(899, 1, NULL, 0x3a3a31, 'owner', '2016-07-07 04:45:19'),
(900, 2, NULL, 0x3a3a31, 'vivek_1010', '2016-07-07 04:55:56');

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses`
--

INSERT INTO `sma_warehouses` (`id`, `code`, `name`, `address`, `map`, `phone`, `email`) VALUES
(1, 'S1', 'Store 1', '<p>Address, City</p>', NULL, '012345678', 'S1@tecdiary.com'),
(2, 'S2', 'Store 2', '<p>Warehouse 2, Jalan Sultan Ismail, 54000, Kuala Lumpur</p>', NULL, '0105292122', 's2@tecdiary.com');

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses_products`
--

INSERT INTO `sma_warehouses_products` (`id`, `product_id`, `warehouse_id`, `quantity`, `rack`) VALUES
(11, 2, 1, '-5.0000', NULL),
(12, 2, 2, '0.0000', NULL),
(25, 6, 1, '20.0000', NULL),
(26, 7, 2, '4.0000', NULL),
(27, 6, 2, '4.0000', NULL),
(28, 7, 1, '5.0000', NULL),
(35, 11, 1, '-12.0000', NULL),
(36, 8, 1, '-85.0000', NULL),
(37, 12, 1, '-15.0000', NULL),
(38, 14, 1, '-36.0000', NULL),
(41, 19, 1, '30.0000', NULL),
(42, 19, 2, '-1.0000', NULL),
(43, 16, 1, '-66.0000', NULL),
(44, 18, 1, '-11.0000', NULL),
(45, 20, 1, '99.0000', NULL),
(46, 20, 2, '4.0000', NULL),
(47, 21, 1, '30.0000', NULL),
(48, 21, 2, '8.0000', NULL),
(49, 22, 1, '96.0000', '1'),
(50, 22, 2, '10.0000', '1'),
(53, 24, 1, '30.0000', '2'),
(54, 24, 2, '10.0000', '2'),
(55, 26, 1, '50.0000', NULL),
(56, 25, 1, '20.0000', '1'),
(57, 25, 2, '50.0000', '1'),
(58, 26, 1, '50.0000', '1'),
(59, 26, 2, '50.0000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_service`
--
ALTER TABLE `sma_customer_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_paypal`
--
ALTER TABLE `sma_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_cash_drawer`
--
ALTER TABLE `sma_pos_cash_drawer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_settings`
--
ALTER TABLE `sma_pos_settings`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_sale_return_reason`
--
ALTER TABLE `sma_sale_return_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sma_settings`
--
ALTER TABLE `sma_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sma_skrill`
--
ALTER TABLE `sma_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_subcategories`
--
ALTER TABLE `sma_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_till`
--
ALTER TABLE `sma_till`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  ADD KEY `group_id_2` (`group_id`,`company_id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_categories`
--
ALTER TABLE `sma_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `sma_costing`
--
ALTER TABLE `sma_costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=818;
--
-- AUTO_INCREMENT for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_customer_service`
--
ALTER TABLE `sma_customer_service`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `sma_groups`
--
ALTER TABLE `sma_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sma_payments`
--
ALTER TABLE `sma_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=466;
--
-- AUTO_INCREMENT for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sma_pos_cash_drawer`
--
ALTER TABLE `sma_pos_cash_drawer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `sma_products`
--
ALTER TABLE `sma_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `sma_sales`
--
ALTER TABLE `sma_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=430;
--
-- AUTO_INCREMENT for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=678;
--
-- AUTO_INCREMENT for table `sma_sale_return_reason`
--
ALTER TABLE `sma_sale_return_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sma_subcategories`
--
ALTER TABLE `sma_subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sma_till`
--
ALTER TABLE `sma_till`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_users`
--
ALTER TABLE `sma_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=901;
--
-- AUTO_INCREMENT for table `sma_variants`
--
ALTER TABLE `sma_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
