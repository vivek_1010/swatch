<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-briefcase"></i><?= lang("open_register"); ?></h2>
    </div>
    <div class="box-content">

        <div class="well well-sm">
            <?php
            $attrib = array('data-toggle' => 'validator','role' => 'form', 'id' => 'open-register-form');
            echo form_open_multipart("pos/open_register", $attrib);
            ?>
           
            <div class="row">
                <div class="form-group">                  
                    <div class="col-md-1">
                        <label for="thousand">1000</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <?php echo form_error('thousand'); ?>
                    <div class="col-md-2">                        
                        <input id="thousand" type="number" min="1" step="1"  name="thousand" maxlength="10" rel="1000" class="curr" value="<?php echo set_value('thousand'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_thousand" type="text" name="total_thousand" maxlength="10" readonly="readonly" value="<?php echo set_value('total_thousand'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">                    
                    <div class="col-md-1">
                        <label for="five_hundred">500</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <?php echo form_error('five_hundred'); ?>
                        <input id="five_hundred" type="number"  min="1" step="1" class="curr" data-fv-numeric="true" rel="500" name="five_hundred" maxlength="10" value="<?php echo set_value('five_hundred'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_five_hundred" type="text" name="total_five_hundred" maxlength="10" value="<?php echo set_value('total_five_hundred'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="hundred">100</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <?php echo form_error('hundred'); ?>
                        <input id="hundred" type="number" name="hundred" min="1" step="1" data-fv-numeric="true" rel="100" class="curr" maxlength="10" value="<?php echo set_value('hundred'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_hundred" type="text" name="total_hundred" maxlength="10" value="<?php echo set_value('total_hundred'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="fifty">50 </label>
                    </div>
                    <?php echo form_error('fifty'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="fifty" type="number" name="fifty" min="1" step="1" rel="50" data-fv-numeric="true"  class="curr" maxlength="10" value="<?php echo set_value('fifty'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_fifty" type="text" name="total_fifty" data-fv-numeric="true"  maxlength="10" value="<?php echo set_value('total_fifty'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="twenty">20</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <?php echo form_error('twenty'); ?>
                    <div class="col-md-2">
                        <input id="twenty" type="number" min="1" step="1" name="twenty" rel="20" data-fv-numeric="true"  class="curr" maxlength="10" value="<?php echo set_value('twenty'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_twenty" type="text" name="total_twenty" maxlength="10" value="<?php echo set_value('total_twenty'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="ten">10</label>
                    </div>
                    <?php echo form_error('ten'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="ten" type="number" min="1" step="1" name="ten" data-fv-numeric="true" rel="10" class="curr" maxlength="10" value="<?php echo set_value('ten'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_ten" type="text" name="total_ten" maxlength="10" value="<?php echo set_value('total_ten'); ?>"  />
                    </div>
                </div>
            </div>
            
            
            <!--Code add by Ankit-->
            
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="five">5</label>
                    </div>
                    <?php echo form_error('five'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="five" type="number" min="1" step="1" name="five" data-fv-numeric="true" rel="5" class="curr" maxlength="10" value="<?php echo set_value('five'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_five" type="text" name="total_five" maxlength="10" value="<?php echo set_value('total_five'); ?>"  />
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="two">2</label>
                    </div>
                    <?php echo form_error('two'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="two" type="number" min="1" step="1" name="two" data-fv-numeric="true" rel="2" class="curr" maxlength="10" value="<?php echo set_value('two'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_two" type="text" name="total_two" maxlength="10" value="<?php echo set_value('total_two'); ?>"  />
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="one">1</label>
                    </div>
                    <?php echo form_error('one'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="one" type="number" min="1" step="1" name="one" data-fv-numeric="true" rel="1" class="curr" maxlength="10" value="<?php echo set_value('one'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_one" type="text" name="total_one" maxlength="10" value="<?php echo set_value('total_one'); ?>"  />
                    </div>
                </div>
            </div>
            
           <!--End code add by Ankit-->
            
            
            
            
            
            
            <div class="row">
                <div class="form-group">

                    <div class="col-md-3 col-md-offset-2">
                        <label for="total">Total Cash in Hand</label>
                    </div>
                   
                    <div class="col-md-2">						
                        <input id="cash_in_hand" type="text" name="cash_in_hand" maxlength="10"  value="" />					
                    </div>
                </div>
            </div>
            <?php echo form_submit('open_register', lang('open_register'), 'class="btn btn-primary"'); ?>
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
	$('#cash_in_hand').val(0);
    $('.curr').on('change input propertychange paste', function () {
		    var currency = parseInt($(this).attr("rel"));
			var number = $(this).val();
			
			if((number != null) && (number != undefined)){
				//bootbox.alert("number : "+number);
				var amount = currency * parseInt(number);
				//bootbox.alert("amount : "+amount);
				if(!isNaN(amount)){
					$('#total_' + $(this).attr('name')).val(amount);
				}else{
					var amount = 0;
					$('#total_' + $(this).attr('name')).val(amount);
				}
				var t_thousand = isNaN(parseInt($('#total_thousand').val())) ? 0 : parseInt($('#total_thousand').val());
				var t_five_hundred = isNaN(parseInt($('#total_five_hundred').val())) ? 0 : parseInt($('#total_five_hundred').val());
				var t_hundred = isNaN(parseInt($('#total_hundred').val())) ? 0 : parseInt($('#total_hundred').val());
				var t_fifty = isNaN(parseInt($('#total_fifty').val())) ? 0 : parseInt($('#total_fifty').val());
				var t_twenty = isNaN(parseInt($('#total_twenty').val())) ? 0 : parseInt($('#total_twenty').val());
				var t_ten = isNaN(parseInt($('#total_ten').val())) ? 0 : parseInt($('#total_ten').val());
                                var t_five = isNaN(parseInt($('#total_five').val())) ? 0 : parseInt($('#total_five').val());
                                var t_two = isNaN(parseInt($('#total_two').val())) ? 0 : parseInt($('#total_two').val());
                                var t_one = isNaN(parseInt($('#total_one').val())) ? 0 : parseInt($('#total_one').val());

				var total = t_thousand + t_five_hundred + t_hundred + t_fifty + t_twenty + t_ten + t_five + t_two + t_one;
				
				$('#cash_in_hand').val(total);
			}
    });	
});
</script>


