<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('customer_service'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form','id'=>'add_accessories_sale','class'=>'form-horizontal','method'=>'POST');
        echo form_open_multipart("pos/add_accessories_sale", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
			<div class="form-group">
					<label class="control-label col-sm-4" for="product">Products</label>
					<div class="col-sm-8">
						<?php
							/*
							$prod = array();
							foreach($products as $k=>$v){
								$prod[$v->id] = $v->name;
							}
						echo form_dropdown('category_products',$prod,'class="form-control" id="category_products"');
						*/
						?>
						<select class="form-control" id="category_products" name="category_products">
							<option value="">Select Product</option>
							<?php foreach($products as $key=>$val):?>
							<option value="<?=$val->id?>"><?=$val->name?></option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="pwd">Available Quantity:</label>
					<div class="col-sm-8"> 
						<input type="text" name="balance_quantity" class="form-control" id="balance_quantity" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="pwd">Quantity:</label>
					<div class="col-sm-8"> 
						<input type="text" name="quantity" class="form-control" id="quantity">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="pwd">Customer Name:</label>
					<div class="col-sm-8"> 
						<input type="text" name="customer_name" class="form-control" id="customer_name">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="pwd">Customer Mobile No:</label>
					<div class="col-sm-8"> 
						<input type="text" name="mobile_no" class="form-control" id="mobile_no" maxlength="10" pattern="^[7-9][0-9]{9}$">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="pwd">Reference:</label>
					<div class="col-sm-8"> 
						<input type="text" name="reference" class="form-control" id="reference">
					</div>
				</div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_accessories_sale', lang('submit'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
	
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#category_products').select2({
			placeholder: '--Select a Product--'
		});
		
		$('#category_products').on('change',function(){
			var product_id = $(this).val();
			$.ajax({
				url:'<?=site_url('pos/getBalanaceQtyAccessory')?>',
				type:'POST',
				datatype:'json',
				data:{'product_id':product_id},
				success:function(response){
					var data = $.parseJSON(response); 
					$('#balance_quantity').val(parseInt(data.balance));
				}
				
			});
		});
		
			$('#add_accessories_sale').bootstrapValidator({
			feedbackIcons: {
				valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
			},
			fields: {
				quantity: {
					validators: {
						notEmpty: {
							message: 'The quantity is required and cannot be empty'
						},
						numeric:{
							message:'Enter only numbers'
						}
					}
				},
				customer_name: {
					validators: {
						notEmpty: {
							message: 'The customer_name is required and cannot be empty'
						},
						stringLength: {
							max: 100,
							message: 'The title must be less than 100 characters long'
						}
					}
				},
				mobile_no: {
					validators:{
						notEmpty:{message:'mobile number is required and cannot be left empty'},
						digits: {message: 'Only digits are allowed in mobile number.'},
						regexp: {
									regexp: /^[9 8 7]\d{9}$/,
									message: 'Mobile number should be of 10 digits only.'
						}
					}
				},
				reference: {
					validators: {
						notEmpty: {
							message: 'The reference is required and cannot be empty'
						}
					}
				}
			}
		});
	
	});
	$('#quantity').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') ); }
        );
		$('#mobile_no').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') ); }
        );
</script>
