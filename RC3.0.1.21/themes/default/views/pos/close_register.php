
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
<!--            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>-->
            <h4 class="modal-title"
                id="myModalLabel"><?= lang('close_register') . ' (' . $this->sma->hrld($register_open_time ? $register_open_time : $this->session->userdata('register_open_time')) . ' - ' . $this->sma->hrld(date('Y-m-d H:i:s')) . ')'; ?></h4>
        </div>
        <?php
//        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'formfield', 'name' => 'close_register');
        $attrib = array('role' => 'form', 'id' => 'formfield', 'name' => 'close_register');
        echo form_open_multipart("pos/close_register/" . $user_id, $attrib);
        ?>
        <div class="modal-body">
            <div id="alerts"></div>
            <!---
            <table width="100%" class="stable">
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('cash_in_hand'); ?>:</h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($cash_in_hand ? $cash_in_hand : $this->session->userdata('cash_in_hand')); ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('cash_sale'); ?>:</h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($cashsales->paid ? $cashsales->paid : '0.00') . ' (' . $this->sma->formatMoney($cashsales->total ? $cashsales->total : '0.00') . ')'; ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('ch_sale'); ?>:</h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($chsales->paid ? $chsales->paid : '0.00') . ' (' . $this->sma->formatMoney($chsales->total ? $chsales->total : '0.00') . ')'; ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('cc_sale'); ?>:</h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                            <span><?= $this->sma->formatMoney($ccsales->paid ? $ccsales->paid : '0.00') . ' (' . $this->sma->formatMoney($ccsales->total ? $ccsales->total : '0.00') . ')'; ?></span>
                        </h4></td>
                </tr>
            <?php if ($pos_settings->paypal_pro) { ?>
                                        <tr>
                                            <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('paypal_pro'); ?>:</h4></td>
                                            <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                                                    <span><?= $this->sma->formatMoney($pppsales->paid ? $pppsales->paid : '0.00') . ' (' . $this->sma->formatMoney($pppsales->total ? $pppsales->total : '0.00') . ')'; ?></span>
                                                </h4></td>
                                        </tr>
            <?php } ?>
            <?php if ($pos_settings->stripe) { ?>
                                        <tr>
                                            <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('stripe'); ?>:</h4></td>
                                            <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                                                    <span><?= $this->sma->formatMoney($stripesales->paid ? $stripesales->paid : '0.00') . ' (' . $this->sma->formatMoney($stripesales->total ? $stripesales->total : '0.00') . ')'; ?></span>
                                                </h4></td>
                                        </tr>
            <?php } ?>
                <tr>
                    <td width="300px;" style="font-weight:bold;"><h4><?= lang('total_sales'); ?>:</h4></td>
                    <td width="200px;" style="font-weight:bold;text-align:right;"><h4>
                            <span><?= $this->sma->formatMoney($totalsales->paid ? $totalsales->paid : '0.00') . ' (' . $this->sma->formatMoney($totalsales->total ? $totalsales->total : '0.00') . ')'; ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid #DDD;"><h4><?= lang('refunds'); ?>:</h4></td>
                    <td style="text-align:right;border-top: 1px solid #DDD;"><h4>
                            <span><?= $this->sma->formatMoney($refunds->returned ? $refunds->returned : '0.00') . ' (' . $this->sma->formatMoney($refunds->total ? $refunds->total : '0.00') . ')'; ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('expenses'); ?>:</h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                            <span><?php
            $expense = $expenses ? $expenses->total : 0;
            echo $this->sma->formatMoney($expense) . ' (' . $this->sma->formatMoney($expense) . ')';
            ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('total_cash'); ?></strong>:</h4>
                    </td>
                    <td style="text-align:right;"><h4>
                            <span><strong><?= $cashsales->paid ? $this->sma->formatMoney(($cashsales->paid + ($this->session->userdata('cash_in_hand')) - $expense) - ($refunds->returned ? $refunds->returned : 0)) : $this->sma->formatMoney($this->session->userdata('cash_in_hand') - $expense); ?></strong></span>
                        </h4></td>
                </tr>
            </table>
            
            ----->
            <?php
          // if ($suspended_bills) {
            //    echo '<hr><h3>' . lang('opened_bills') . '</h3><table class="table table-hovered table-bordered"><thead><tr><th>' . lang('customer') . '</th><th>' . lang('date') . '</th><th>' . lang('total_items') . '</th><th>' . lang('amount') . '</th><th><i class="fa fa-trash-o"></i></th></tr></thead><tbody>';
            //    foreach ($suspended_bills as $bill) {
             //       echo '<tr><td>' . $bill->customer . '</td><td>' . $this->sma->hrld($bill->date) . '</td><td class="text-center">' . $bill->count . '</td><td class="text-right">' . $bill->total . '</td><td class="text-center"><a href="#" class="tip po" title="<b>' . $this->lang->line("delete_bill") . '</b>" data-content="<p>' . lang('r_u_sure') . '</p><a class=\'btn btn-danger po-delete\' href=\'' . site_url('pos/delete/' . $bill->id) . '\'>' . lang('i_m_sure') . '</a> <button class=\'btn po-close\'>' . lang('no') . '</button>"  rel="popover"><i class="fa fa-trash-o"></i></a></td></tr>';
              //  }
              //  echo '</tbody></table>';
           // } 
            ?>
            <div class="row">
                <div class="form-group">                  
                    <div class="col-md-1">
                        <label for="thousand">1000</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <?php echo form_error('thousand'); ?>
                    <div class="col-md-2">                        
                        <input id="thousand" type="number"  min="1" step="1"  name="thousand" maxlength="10" data-fv-numeric="true" rel="1000" class="close_register_input kb-pad" value="<?php echo set_value('thousand'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_thousand" type="text" name="total_thousand" maxlength="10" data-fv-numeric="true" readonly="readonly" value="<?php echo set_value('total_thousand'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">                    
                    <div class="col-md-1">
                        <label for="five_hundred">500</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <?php echo form_error('five_hundred'); ?>
                        <input id="five_hundred" type="number"  min="1" step="1" class="close_register_input" data-fv-numeric="true" rel="500" name="five_hundred" maxlength="10" value="<?php echo set_value('five_hundred'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_five_hundred" type="text" name="total_five_hundred" data-fv-numeric="true" maxlength="10" value="<?php echo set_value('total_five_hundred'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="hundred">100</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <?php echo form_error('hundred'); ?>
                        <input id="hundred" type="number" name="hundred"  min="1" step="1" data-fv-numeric="true" rel="100" class="close_register_input" maxlength="10" value="<?php echo set_value('hundred'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_hundred" type="text" name="total_hundred" data-fv-numeric="true" maxlength="10" value="<?php echo set_value('total_hundred'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="fifty">50 </label>
                    </div>
                    <?php echo form_error('fifty'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="fifty" type="number" name="fifty" rel="50"  min="1" step="1" data-fv-numeric="true"  class="close_register_input" maxlength="10" value="<?php echo set_value('fifty'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_fifty" type="text" name="total_fifty" data-fv-numeric="true"  maxlength="10" value="<?php echo set_value('total_fifty'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="twenty">20</label>
                    </div>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <?php echo form_error('twenty'); ?>
                    <div class="col-md-2">
                        <input id="twenty" type="number" name="twenty" rel="20" min="0" step="1" data-fv-numeric="true"  class="close_register_input" maxlength="10" value="<?php echo set_value('twenty'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_twenty" type="text"  min="1" step="1" name="total_twenty" data-fv-numeric="true" maxlength="10" value="<?php echo set_value('total_twenty'); ?>"  />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="ten">10</label>
                    </div>
                    <?php echo form_error('ten'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="ten" type="number"  min="1" step="1" name="ten" data-fv-numeric="true" rel="10" class="close_register_input" maxlength="10" value="<?php echo set_value('ten'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_ten" type="text" data-fv-numeric="true" name="total_ten" maxlength="10" value="<?php echo set_value('total_ten'); ?>"  />
                    </div>
                    <!--qweqweqeqweqweqweqqqqqqqqqqqqqqqqwq-->
                </div>
            </div>
            
            <!--Code add by Ankit--> 
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="five">5</label>
                    </div>
                    <?php echo form_error('five'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="five" type="number"  min="1" step="1" name="five" data-fv-numeric="true" rel="5" class="close_register_input" maxlength="10" value="<?php echo set_value('five'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_five" type="text" data-fv-numeric="true" name="total_five" maxlength="10" value="<?php echo set_value('total_five'); ?>"  />
                    </div>
                   
                </div>
            </div>
            
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="two">2</label>
                    </div>
                    <?php echo form_error('two'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="two" type="number"  min="1" step="1" name="two" data-fv-numeric="true" rel="2" class="close_register_input" maxlength="10" value="<?php echo set_value('two'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_two" type="text" data-fv-numeric="true" name="total_two" maxlength="10" value="<?php echo set_value('total_two'); ?>"  />
                    </div>
                    
                </div>
            </div>
            
            <div class="row">
                <div class="form-group">
                    <div class="col-md-1">
                        <label for="one">1</label>
                    </div>
                    <?php echo form_error('one'); ?>
                    <div class="col-md-1">                        
                        <span>&#10005;</span>                       
                    </div>
                    <div class="col-md-2">
                        <input id="one" type="number"  min="1" step="1" name="one" data-fv-numeric="true" rel="1" class="close_register_input" maxlength="10" value="<?php echo set_value('one'); ?>"  />
                    </div>
                    <div class="col-md-1">                        
                        <p>&#61;</p>                       
                    </div>
                    <div class="col-md-2">         
                        <input id="total_one" type="text" data-fv-numeric="true" name="total_one" maxlength="10" value="<?php echo set_value('total_one'); ?>"  />
                    </div>
                    
                </div>
            </div>
            
         <!--End code add by Ankit-->   
            
            

            <div class="row no-print">
                <div class="col-sm-12">
                    <!--<div class="col-sm-6">-->
                    <div class="row">
                        <div class="col-sm-4">
                    <div class="form-group">
                        <?= lang("total_cash", "total_cash_submitted"); ?>
                        <?php $total_cash = ($cashsales->paid ? $cashsales->paid + ($cash_in_hand ? $cash_in_hand : $this->session->userdata('cash_in_hand')) - $expense - ($cashrefunds->returned ? $cashrefunds->returned : 0) : (($cash_in_hand ? $cash_in_hand : $this->session->userdata('cash_in_hand')) - $expense)); ?>
                        <?= form_hidden('total_cash', $total_cash, 'id="total_cash"'); ?>
                        <?= form_input('total_cash_submitted', '', 'class="form-control input-tip" readonly="readonly" id="total_cash_submitted" required="required"'); ?>
                    </div>
                        </div>
                        <div class="col-sm-4">
                            <?php if($ccAmount==''){$ccAmount=0;}?>
                            <?php if($cc1Amount==''){$cc1Amount=0;}?>
                            <?php if($cvAmount==''){$cvAmount=0;}?>
                            <?php 
								
								$tt= ($ccAmount + $cc1Amount); 
							?>
                            <div class="form-group">
                        <?= lang("total_amount", "total_amount_submitted"); ?>
                       
                        <?= form_input('total_amount_submitted', (isset($tt) ? $tt : '0'), 'class="form-control input-tip" id="total_amount_submitted" required="required" readonly="readonly"'); ?>
                    </div>
                            
                    
                        </div></div>
                    <!--</div>-->
                    <div class="row">
                        <div class="col-sm-4">
                            
                            <div class="form-group">
                        <?= lang("Total_Debit_Card_Amount", "total_debit_card_submitted"); ?>
                        
                        <?= form_input('total_debit_card_amount', (isset($ccAmount) ? $ccAmount : '0'), 'class="form-control input-tip" id="total_debit_card_amount_submitted" required="required" readonly="readonly"'); ?>
                    </div>
                    
                        </div>
                        <div class="col-sm-4">
                            
                            <div class="form-group">
                        <?= lang("Total_Debit_Card_Slips", "total_dc_slips_submitted"); ?>
                        <?= form_hidden('total_dc_slips', $ccsales->total_dc_slips); ?>
                        <?= form_input('total_cc_slips_submitted', (isset($_POST['total_cc_slips_submitted']) ? $_POST['total_cc_slips_submitted'] : $ccsales->total_cc_slips), 'class="form-control input-tip" id="total_cc_slips_submitted" required="required" readonly="readonly"'); ?>
                    </div>
                            
                        </div>
                    </div>    
			
                   <div class="row">
                       <div class="col-sm-4">
                           <div class="form-group">
                        <?= lang("total_credit_card_amount", "total_credit_card_submitted"); ?>
                        
                        <?= form_input('total_credit_card_amount', (isset($cc1Amount) ? $cc1Amount : '0'), 'class="form-control input-tip" id="total_credit_card_amount_submitted" required="required" readonly="readonly"'); ?>
                    </div>
                           
                       </div>  
                       <div class="col-sm-4">
                           <div class="form-group">
                        <?= lang("total_cc_slips", "total_cc_slips_submitted"); ?>
                        <?= form_hidden('total_cc_slips', $ccsales1->total_cc_slips); ?>
                        <?= form_input('total_cc_slips_submitted', (isset($_POST['total_cc_slips_submitted']) ? $_POST['total_cc_slips_submitted'] : $ccsales1->total_cc_slips), 'class="form-control input-tip" id="total_cc_slips_submitted" required="required" readonly="readonly"'); ?>
                    </div>
                           
                       </div>  
                   </div>   
					
                    <div class="row">
                       <div class="col-sm-4">
						<div class="form-group">
							<?= lang("total_credit_voucher_amount", "total_credit_voucher_submitted"); ?>
							
							<?= form_input('total_credit_voucher_submitted', (isset($cvAmount) ? $cvAmount : '0'), 'class="form-control input-tip" id="total_credit_voucher_amount_submitted" required="required" readonly="readonly"'); ?>
						</div>
                    
                       </div>
					   
						<div class="col-sm-4">
							<div class="form-group">
								<?= lang("Amount_Deposited_In_Bank", "amount_deposited_in_bank"); ?>
								<?php 
								
								 $expense = !empty($expenses->total) ? $expenses->total : 0;
								echo form_input('expenses', $expense, 'class="form-control input-tip" id="expenses" required="required" readonly="readonly"'); ?>
							</div>   
						</div>
					
                    </div>
					<!---
					<table class="table table-hovered">
						<thead>
							<tr>
								<th><?= lang("Reciever", "receiver"); ?></th>
								<th><?= lang("Amount", "amount"); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach($expenses as $k=>$v){ 
								
							?>
							<tr>
								<td class="text-center"><?=$v->first_name.' '.$v->last_name?></td>
								<td class="text-center"><?=$v->amount?></td>
							</tr>
							<?php }?>
							<tr>
								<td class="text-center"><?= lang("Total_Collection", "total_collection"); ?></td>
								<td class="text-center"><?=($total_expense->total)?></td>
							</tr>
						</tbody>
					</table>
					------->
                </div>

                <!--Transfer Tab Disable due to swatch update @ Ankit-->
<!--                <div class="col-sm-6">
                    <?php //if ($suspended_bills) { ?>

                        <div class="form-group">
                            <?//= lang("transfer_opened_bills", "transfer_opened_bills"); ?>
                            <?php
                            //$u = $user_id ? $user_id : $this->session->userdata('user_id');
                            //$usrs[-1] = lang('delete_all');
                            //$usrs[0] = lang('leave_opened');
                            //foreach ($users as $user) {
                               // if ($user->id != $u) {
                               //     $usrs[$user->id] = $user->first_name . ' ' . $user->last_name;
                               // }
                           // }
                            ?>
                            <?//= form_dropdown('transfer_opened_bills', $usrs, (isset($_POST['transfer_opened_bills']) ? $_POST['transfer_opened_bills'] : 0), 'class="form-control input-tip" id="transfer_opened_bills" required="required"'); ?>
                        </div>

                    <?php //} ?>

                   
                </div>-->
            </div>
            <div class="form-group no-print">
                <label for="note"><?= lang("note"); ?></label>

                <div
                    class="controls"> <?= form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?> </div>
            </div>

        </div>
        <div class="modal-footer no-print">
            <?= form_submit('close_register', lang('close_register'), 'class="btn btn-primary close_register_submit"'); ?>
            <!--
                <input type="submit" name="close_register" value="Submit" id="submitBtn" data-toggle="modal" data-target="#confirm-submit" class="btn btn-primary" />
            --->
        </div>
    </div>
    <?= form_close(); ?>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                 <h3 id="myModalLabel3">Confirmation Heading</h3>

            </div>
            <div class="modal-body">
                <p>Are You Sure You want To submit The Form</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close This Modal</button>
                <button class="btn-primary btn" id="SubForm">Confirm and Submit The Form</button>
            </div>
        </div>
    </div>
</div>


<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function () {
        var sum = parseInt($("#total_credit_voucher_amount_submitted").val())+parseInt($("#total_credit_card_amount_submitted").val())+parseInt($("#total_debit_card_amount_submitted").val());
        $("#total_amount_submitted").val(sum);
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
    });
    function addAlert(message, type) {
        $('#alerts').empty().append(
                '<div class="alert alert-' + type + '">' +
                '<button type="button" class="close" data-dismiss="alert">' +
                '&times;</button>' + message + '</div>');
    }

    $(document).ready(function () {
		$('#total_cash_submitted').val(0);
        $('.close_register_input').on('input change propertychange paste', function () {

            var currency = parseInt($(this).attr("rel"));
			var number = $(this).val();
			if((number != null) && (number != undefined)){
				var amount = currency * parseInt(number);
				
				if(!isNaN(amount)){
					$('#total_' + $(this).attr('name')).val(amount);
				}else{
					var amount = 0;
					$('#total_' + $(this).attr('name')).val(amount);
				}
				var t_thousand = isNaN(parseInt($('#total_thousand').val())) ? 0 : parseInt($('#total_thousand').val());
				var t_five_hundred = isNaN(parseInt($('#total_five_hundred').val())) ? 0 : parseInt($('#total_five_hundred').val());
				var t_hundred = isNaN(parseInt($('#total_hundred').val())) ? 0 : parseInt($('#total_hundred').val());
				var t_fifty = isNaN(parseInt($('#total_fifty').val())) ? 0 : parseInt($('#total_fifty').val());
				var t_twenty = isNaN(parseInt($('#total_twenty').val())) ? 0 : parseInt($('#total_twenty').val());
				var t_ten = isNaN(parseInt($('#total_ten').val())) ? 0 : parseInt($('#total_ten').val());
                                
                                var t_five = isNaN(parseInt($('#total_five').val())) ? 0 : parseInt($('#total_five').val());
                                var t_two = isNaN(parseInt($('#total_two').val())) ? 0 : parseInt($('#total_two').val());
                                var t_one = isNaN(parseInt($('#total_one').val())) ? 0 : parseInt($('#total_one').val());
								
				
				var total = t_thousand + t_five_hundred + t_hundred + t_fifty + t_twenty + t_ten + t_five + t_two + t_one;
				
				$('#total_cash_submitted').val(total);
                                var sum=total+parseInt($("#total_credit_voucher_amount_submitted").val())+parseInt($("#total_credit_card_amount_submitted").val())+parseInt($("#total_debit_card_amount_submitted").val());
                                $("#total_amount_submitted").val(sum);
			}
        });

	

        $('#formfield').submit(function (event) {
            var currentForm = this;
			//var expense = isNaN($("#expenses").val()) ? 0 : $("#expenses").val();
            var total_cash = <?= $total_cash ?>;
            //var total_cash_submitted = $('#total_cash_submitted').val() - parseInt(expense);
           var total_cash_submitted = $('#total_cash_submitted').val();
//alert("total_cash"+total_cash);alert("total_cash_submitted"+total_cash_submitted);
            var diff = (total_cash > total_cash_submitted) ? (total_cash - total_cash_submitted) : (total_cash_submitted - total_cash);
            event.preventDefault();
            if (total_cash_submitted) {
				if(diff != 0){
					bootbox.confirm("Are you sure you want to submit\<br/>\difference : " + diff, function (result) {
						if (result) {
							currentForm.submit();
							return false;
						}
						
						bootbox.hideAll()
					});
				}else{
					currentForm.submit();
				}
			}
        });
    });
	

   
</script>
<script> 
    $('input[type=number]').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') ); }
        );

</script>


