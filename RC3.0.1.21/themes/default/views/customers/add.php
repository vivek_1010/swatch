<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_customer'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-customer-form');
        echo form_open_multipart("customers/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label class="control-label"
                       for="customer_group"><?php echo $this->lang->line("default_customer_group"); ?></label>

                <div class="controls"> <?php
                    foreach ($customer_groups as $customer_group) {
                        $cgs[$customer_group->id] = $customer_group->name;
                    }
                    echo form_dropdown('customer_group', $cgs, $this->Settings->customer_group, 'class="form-control tip select" id="customer_group" style="width:100%;" required="required" ');
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                   <!-- <div class="form-group company">
                        <?= lang("company", "company"); ?>
                        <?php echo form_input('company', '', 'class="form-control tip" id="company" data-bv-notempty="true"'); ?>
                    </div> -->
                    <div class="form-group">
                       <?= lang("Gender", "Gender"); ?>
                        <select name="gender" id="gender" required="required">
                        <option value="">--Select--</option>
                      <option value="M">Male</option>
                      <option value="F">Female</option>
                     
                     </select>
                   </div>
                    <div class="form-group person">
                        <?= lang("First name", "First name"); ?><?php echo " *";?>
                        <?php echo form_input('name', '', 'class="form-control tip " id="name" data-bv-notempty="true"'); ?>
                    </div>
                     
                   
                    <!--<div class="form-group company">
                    <?= lang("contact_person", "contact_person"); ?>
                    <?php echo form_input('contact_person', '', 'class="form-control" id="contact_person" data-bv-notempty="true"'); ?>
                </div>-->
                    
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="text" name="phone" class="form-control" required="required" id="phone" maxlength="10"/>
                    </div>
                    <!--<div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
                    </div>
                      <div class="form-group">
                        <?= lang("Pan no", "Pan no"); ?>
                        <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no"'); ?>
                    </div>
                                       
                    <div class="form-group">
                        <?= lang("state", "state"); ?>
                        <?php echo form_input('state', '', 'class="form-control" id="state"'); ?>
                    </div> -->

                </div>
                <div class="col-md-6">
                       <div class="form-group">
                       <?= lang("Salutation", "Salutation"); ?>
                   <select name='salutation' id='salutation'>
                       <option value="">--Select--</option>
                      
                     
                     </select>
                   </div>
                       <div class="form-group person">
                        <?= lang("Last name", "Last name"); ?><?php echo " *";?>
                        <?php echo form_input('lname', '', 'class="form-control tip" id="lname" data-bv-notempty="true" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" class="form-control" required="required" id="email_address"/>
                    </div>
                    <!--<div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php echo form_input('city', '', 'class="form-control" id="city" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("country", "country"); ?>
                        <?php echo form_input('country', '', 'class="form-control" id="country"'); ?>
                    </div> -->
                  <!--  <div class="form-group">
                        <?= lang("ccf1", "cf1"); ?>
                        <?php echo form_input('cf1', '', 'class="form-control" id="cf1"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("ccf2", "cf2"); ?>
                        <?php echo form_input('cf2', '', 'class="form-control" id="cf2"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("ccf3", "cf3"); ?>
                        <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("ccf4", "cf4"); ?>
                        <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("ccf5", "cf5"); ?>
                        <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("ccf6", "cf6"); ?>
                        <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                    </div> -->
                </div>
            </div>


        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_customer', lang('add_customer'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script> 
    $('#phone').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^0-9]+/i, '') ); }
        );

</script>
<script>
    
    
    $('input[type="text"]').keyup(function(evt){
    var txt = $(this).val();
    $(this).val(txt.replace(/^(.)|\s(.)/g, function($1){ return $1.toUpperCase( ); }));
});

$('#name').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^A-Z0-9]+/i, '') ); }
        );
$('#lname').bind('keyup blur',function(){ 
            var node = $(this);
            node.val(node.val().replace(/[^A-Z0-9]+/i, '') ); }
        );		
</script>
<script type="text/javascript">

  
    $(document).ready(function (e) { 
 
//on change event of gender load salutation
   $(document).on('change','#gender' ,function(event){
  //alert($(this).val());
    $('#salutation').html(''); 
   if($(this).val() == 'M'){
      var list= "<option value='1'>Mr</option>";
      list+="<option value='2'>Dr</option>";
//     var list="<option value=''>--Select--</option>";
//         list+="<option value='1'>Mr</option>";
//         list+="<option value='2'>Dr</option>";
     }else if($(this).val() == 'F'){
         var list= "<option value='1'>Miss</option>";
      list+="<option value='2'>Mrs</option>";
      list+="<option value='3'>Dr</option>";
//        var list="<option value=''>--Select--</option>";
//         list+="<option value='3'>Miss</option>";
//         list+="<option value='4'>Mrs</option>";
//          list+="<option value='5'>Dr</option>";
          }else if($(this).val() == ''){
              var list= "<option value=''>--Select--</option>";
          }
        $('#salutation').append(list);
    }); 
	
        $('#add-customer-form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, 
			fields:{
				phone:{
					validators:{
						notEmpty:{message:'mobile number is required and cannot be left empty'},
						digits: {message: 'Only digits are allowed in mobile number.'},
						regexp: {
									regexp: /^[9 8 7]\d{9}$/,
									message: 'Mobile number should be of 10 digits only and starts with 7,8,9.'
						}
					}
				}
				
			}
        });
		
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });
</script>
