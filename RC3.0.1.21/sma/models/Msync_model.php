<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Msync_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('multipledb');
        $this->load->model("auth_model");
        $this->load->database();
    }

// S.No.-01 (Intermediate -> POS)

public function insert_into_pos_currency_table(){
    $n = '$';
	
    $a= $this->multipledb->import_Currency_info(); 
    $ar= $this->multipledb->currencyTableUpdate();
    //$del= $this->multipledb->get_currencyTableDelFLG();
    if(empty($a)){
                  $msg= "Currency Records already saved...";
                  if(!empty($ar[0]['code'])){
                  $this->db->update_batch('currencies', $ar, 'id');
                  $w = "UPDATE ebiz7.intrm".$n."curr a INNER JOIN swatch_final.sma_currencies b ON a.CURR_ID = b.curr_id SET a.UPD_FLG=' ' WHERE a.CURR_ID = b.curr_id";
                  $this->insert_id_into_global_table($w);
                  }
                  return $msg;
              }       
    else{
            $this->db->insert_batch('currencies', $a);
            if(!empty($ar[0]['code'])){ 
            $this->db->update_batch('currencies', $ar, 'id'); }
            $w = "UPDATE ebiz7.intrm".$n."curr a INNER JOIN swatch_final.sma_currencies b ON a.CURR_ID = b.curr_id SET a.POS_CURR_ID= b.id, a.CREATE_FLG='1',a.UPD_FLG='' WHERE a.CURR_ID = b.curr_id";
            $this->insert_id_into_global_table($w);
            $msg= "Currency Records save successfully...";
            return $msg;
        }  
 }

 // S.NO-02 (Intermediate -> POS)
public function insert_into_pos_tax_table(){
    $n='$';
    $a= $this->multipledb->import_tax_rate_info();
    $ar= $this->multipledb->import_tax_rate_UPD();
    $this->db->select('rate');
    $q = $this->db->get('tax_rates');
          if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $data[] = $row;
             }
         }
   if(!empty($data))
          {
            $data= $this->convert_obj_to_arry($data);
            $pp = array();
            foreach($data as $k=>$v){
               $pp[] = $v['rate'];
            }
            for($i=0;$i<count($a);$i++)
               { 
                    if(!in_array($a[$i]['rate'], $pp))
                       {
                         $p[$i]=$a[$i];
                        
                       }
             }
           }
 
if(empty($data))
          {    
           $p = $this->multipledb->import_tax_rate_info();    
          }  
if($this->db->insert_batch('tax_rates', $p)){
   //$w = "UPDATE ebiz7.intrm".$n."itm".$n."grp".$n."org a INNER JOIN swatch_final.sma_tax_rates b ON (a.ORG_ID = b.org_id AND a.GRP_ID = b.grp_id) SET a.POS_GRP_ID=b.id, CREATE_FLG='1' WHERE a.ORG_ID = b.org_id AND a.GRP_ID = b.grp_id";
  //$this->insert_id_into_global_table($w);
    $msg= "TAX Rates Records save successfully...";
    return $msg;
}
  else {
    $msg= "TAX Rates Records Already saved...";
    return $msg;
  }

 if(!empty($ar[0]['rate'])){
                  $this->db->update_batch('tax_rates', $ar, 'id');
                  //$w = "UPDATE ebiz7.intrm".$n."itm".$n."grp".$n."org a INNER JOIN swatch_final.sma_tax_rates b ON a.POS_GRP_ID = b.id SET a.UPD_FLG=' ' WHERE a.POS_GRP_ID = b.id";
                 // $this->insert_id_into_global_table($w);
                  $w1="UPDATE swatch_final.sma_products a INNER JOIN swatch_final.sma_tax_rates b ON (a.org_id = b.org_id AND a.grp_id = b.grp_id) SET a.tax_rate = b.id, a.tupd_flg='1' WHERE a.org_id = b.org_id AND a.grp_id = b.grp_id";
                  $this->db->query($w1);
                  }   
}

// S.NO.-03 (Intermediate -> POS)    
public function insert_into_pos_category_table(){
    $n='$';
    $a= $this->multipledb->import_warehouse_categories_info();
    $ar= $this->multipledb->categoryTableUpdate();
   if(!empty($a)){
    $this->db->insert_batch('categories', $a);  
    $w = "UPDATE ebiz7.intrm".$n."itm".$n."grp a INNER JOIN swatch_final.sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID= b.id WHERE a.GRP_ID = b.code";
    $w1 = "UPDATE ebiz7.intrm".$n."itm".$n."grp".$n."org a INNER JOIN swatch_final.sma_categories b ON a.GRP_ID = b.code SET a.POS_GRP_ID= b.id, CREATE_FLG='1' WHERE a.GRP_ID = b.code";
    $this->insert_id_into_global_table($w);
    $this->insert_id_into_global_table($w1); 
    $msg= "Category Records save successfully...";
    return $msg;
     }
   else{
    $msg= "Category Records already saved...";
    return $msg;
   } 
  if(!empty($ar[0]['name'])){
                  $this->db->update_batch('categories', $ar, 'id');
                  $w = "UPDATE ebiz7.intrm".$n."itm".$n."grp".$n."org a INNER JOIN swatch_final.sma_categories b ON a.GRP_ID = b.code SET UPD_FLG='' WHERE a.GRP_ID = b.code";
                  $this->insert_id_into_global_table($w);
                 // $this->insert_into_pos_tax_table();
                  $w1="UPDATE swatch_final.sma_products a INNER JOIN swatch_final.sma_categories b ON a.grp_id = b.code SET a.category_id = b.id WHERE a.grp_id = b.code";
                  $this->db->query($w1);
    }
}

// S.NO.-04 (Intermediate -> POS)

public function insert_into_pos_warehouse_table(){
    $n = '$';
    $a= $this->multipledb->import_store_info(); 
    $biller= $this->multipledb->import_biller_store_info();
    if(!empty($a)){
                 $this->db->insert_batch('warehouses', $a);
                 $this->db->insert_batch('companies', $biller);
                 $w = "UPDATE ebiz7.intrm".$n."org a INNER JOIN swatch_final.sma_warehouses b ON a.ORG_ID = b.org_id SET a.POS_ORG_ID= b.id WHERE a.ORG_ID = b.org_id";
                 $this->insert_id_into_global_table($w);
                 $msg= "Store Details saved successfully...";
                return $msg;
                }      
    else{
            $msg= "Store Details already saved...";
            return $msg;
        }
}

// S.NO.-05 (Intermediate -> POS)
public function insert_into_pos_usertable(){
    $n = '$';
    $a= $this->multipledb->import_user_info();
    $ar= $this->multipledb->userTableUpdate();
   if(empty($a)){
                  $msg= "User Records already saved...";
                  if(!empty($ar[0]['username'])){
                  $this->db->update_batch('users', $ar, 'id');
                  $w = "UPDATE ebiz7.intrm".$n."usr a INNER JOIN swatch_final.sma_users b ON a.USR_ID = b.usr_id SET a.UPD_FLG=' ' WHERE a.USR_ID = b.usr_id";
                  $this->insert_id_into_global_table($w);
                  }
                  return $msg;
                }  
else {
            $this->db->insert_batch('users', $a);
           if(!empty($ar[0]['username'])){ 
                $this->db->update_batch('users', $ar, 'id'); }
            $w = "UPDATE ebiz7.intrm".$n."usr a INNER JOIN swatch_final.sma_users b ON a.USR_ID = b.usr_id SET a.POS_USR_ID= b.id, a.CREATE_FLG='1',a.UPD_FLG='' WHERE a.USR_ID = b.usr_id";
            $this->insert_id_into_global_table($w);
            $msg= "User Records save successfully...";
            return $msg;
        }  
}

public function insert_into_pos_group_table(){
    $n = '$';
    $a= $this->multipledb->import_eo_catg_info(); 
    $ar= $this->multipledb->eoCatgTableUpdate();
    if(empty($a)){
                  $msg= "Group Records All ready saved in data base...";
                  if(!empty($ar[0]['name'])){
                  $this->db->update_batch('groups', $ar, 'id');
                  $w = "UPDATE otherdb.intrm".$n."eo".$n."catg a INNER JOIN otherpos.sma_groups b ON a.CATG_NM = b.name SET a.UPD_FLG=' ' WHERE a.CATG_NM = b.name";
                  $this->insert_id_into_global_table($w);
                  }
                  return $msg;
              }      
           
    if($this->db->insert_batch('groups', $a)){
            if(!empty($ar[0]['name'])){ 
            $this->db->update_batch('groups', $ar, 'id'); }
            $w = "UPDATE otherdb.intrm".$n."eo".$n."catg a INNER JOIN otherpos.sma_groups b ON a.CATG_NM = b.name SET a.POS_EO_CATG_ID= b.id, a.CREATE_FLG='1',a.UPD_FLG='' WHERE a.CATG_NM = b.name";
            $this->insert_id_into_global_table($w);
            $msg= "Group Records save successfully...";
            return $msg;
        }  
        else{
            $msg= "Group Records Not Saved...";
            return $msg;
        }
}

// I.T.-01 (Pos -> intermediate)
public function insert_into_intermediate_eotable(){
   $n = '$';
   //$w= "SELECT * FROM sma_companies WHERE id!= '1' AND group_name!='biller' AND eo_id IS NULL OR eo_id= ''";
   $w= "SELECT * FROM sma_companies WHERE id!= '1' AND group_name!='biller' AND sync_flg IS NULL OR sync_flg= ''";
   $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    $w1= "SELECT * FROM sma_companies WHERE upd_flg= '1' AND id!= '1' AND group_name!='biller'";
    $q1 = $this->db->query($w1);
    if ($q1->result() > 0) {
            $records1[] = $q1->result();
           }  
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                //'group_id' => '3', //$records[0][$i]->EO_CATG_ID,
                //'group_name'=>'customer',//$this->getGroupsName($records[0][$i]->EO_CATG_ID),
                //'customer_group_id'=>1,
                //'customer_group_name'=>'General',
                'POS_EO_ID' => $records[0][$i]->id,
                'EO_NM'=>$records[0][$i]->name,
                //'company'=>'Swatch Group',
                'EO_PHONE' => $records[0][$i]->phone,
                //'EO_MAIL'=> $records[0][$i]->email,
                'VAT_NO'=>$records[0][$i]->vat_no,
                'EO_TYPE' => 'C'
                );
             $ps[$i] = array(
                'POS_EO_ID' => $records[0][$i]->id,
                'EO_TYPE' => 'C'
                );
           }
        $ar= $this->multipledb->sync_into_intermediate_eotable($data, $ps);
        return $ar;
        
      } 

      if($records1){
        for($i=0;$i<count($records1[0]);$i++) {
             $data[$i] = array(
                'POS_EO_ID' => $records1[0][$i]->id,
                'EO_NM'=>$records1[0][$i]->name,
                'EO_PHONE' => $records1[0][$i]->phone,
                //'EO_MAIL'=> $records1[0][$i]->email,
                'VAT_NO'=>$records1[0][$i]->vat_no,
                'EO_TYPE' => 'C'
                );
        }
        $ar1= $this->multipledb->sync_upd_into_intermediate_eotable($data);
        return $ar1;
      }            
}


// I.T.-02 (Pos -> intermediate) sale
public function insert_into_intermediate_slsInvtable(){
   $n = '$';
   $w= "SELECT * FROM sma_sales WHERE sync_flg IS NULL OR sync_flg = '' ";
   $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                 
                'ORG_ID' => $this->getORG_Id($records[0][$i]->warehouse_id),
                'POS_DOC_ID'=> $records[0][$i]->id,
                'DOC_ID'=> $records[0][$i]->reference_no,
                'DOC_DT'=> date('Y-m-d', strtotime($records[0][$i]->date)),
                'EO_ID'=> $this->getEO_Id($records[0][$i]->customer_id),
                'POS_EO_ID'=> $records[0][$i]->customer_id,
                'SLS_REP_ID'=> $this->getUSR_Id($records[0][$i]->sales_executive_id),      // user id
                'POS_SLS_REP_ID' => $records[0][$i]->sales_executive_id,
                'TAX_BASIS'=> 'NULL',
                'TAX_ID'=> $records[0][$i]->order_tax_id,
                'TAX_VAL'=> $records[0][$i]->total_tax,
                'DISC_BASIS'=> 'NULL',
                'DISC_TYPE'=> 'NULL',
                'POS_INV_TOTAL'=> $records[0][$i]->grand_total, 
                'DISC_VAL'=> $records[0][$i]->total_discount,  // verify
                //'SYNC_FLG'=> '1',
                'DISC_TOTAL_VAL'=> $records[0][$i]->total_discount
             );
         }
        $ar= $this->multipledb->sync_into_intermediate_slsInv($data);
        return $ar;
      }
        return false; 
}

public function getORG_Id($wid){
    $q = $this->db->query("SELECT org_id FROM sma_warehouses WHERE id='$wid'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->org_id;
        }
        return FALSE;
    
}
public function getEO_Id($cid){
    $q = $this->db->query("SELECT eo_id FROM sma_companies WHERE id='$cid'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->eo_id;
        }
        return FALSE;
    
}
public function getUSR_Id($uid){
    $q = $this->db->query("SELECT usr_id FROM sma_users WHERE id='$uid'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->eo_id;
        }
        return FALSE;
    
}

// I.T.-03 (Pos -> intermediate) sale Item
public function insert_into_intermediate_slsInvItemtable(){
   $n = '$';
   $w= "SELECT * FROM sma_sale_items WHERE sync_flg IS NULL OR sync_flg = '' ";
   $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'ORG_ID' => $this->getORG_Id($records[0][$i]->warehouse_id),
                'POS_DOC_ID'=> $records[0][$i]->id,
                'DOC_ID'=> $records[0][$i]->sale_id,
                'ITM_ID'=> $records[0][$i]->product_code,
                'POS_ITM_ID'=> $records[0][$i]->product_id,
                'SR_NO'=> $this->getsrno($records[0][$i]->product_id), //write function
                'LOT_NO'=> $this->getlot($records[0][$i]->product_id),
                'ITM_QTY'=> $records[0][$i]->quantity,
                //'ITM_UOM'=>
                //'POS_ITM_UOM'=>
                //'ITM_UOM_BS'=>
                'POS_ITM_UOM_BS'=> $records[0][$i]->net_unit_price, // verify
                'SLS_PRICE'=> $records[0][$i]->real_unit_price,
                'TAX_VAL'=> $records[0][$i]->item_tax,
                 //'TAX_ID'=> $records[0][$i]->tax_rate_id,
                'DISC_TOTAL_VAL'=> $records[0][$i]->item_discount,
                'DISC_TYPE'=> 'NULL',
                'DISC_VAL'=> $records[0][$i]->discount,  // verify
                'ITM_TOTAL_VAL'=> $records[0][$i]->net_unit_price,
                
              );
            }
         $ar= $this->multipledb->sync_into_intermediate_slsInvItem($data);
        return $ar;
      }
        return false; 
}

public function getlot($pid){
    $q = $this->db->query("SELECT lot_no FROM sma_products WHERE id='$pid'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->lot_no;
        }
        return FALSE;
    
}
public function getsrno($pid){
    $q = $this->db->query("SELECT sr_no FROM sma_products WHERE id='$pid'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->sr_no;
        }
        return FALSE;
    
}

// I.T.-04 (Pos -> intermediate) sale return
public function insert_into_intermediate_slsRmatable(){
   $n = '$';
   $w= "SELECT * FROM sma_return_sales WHERE sync_flg IS NULL OR sync_flg = '' ";
   $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                 
                'ORG_ID' => $this->getORG_Id($records[0][$i]->warehouse_id),
                'POS_DOC_ID'=> $records[0][$i]->id,
                'DOC_ID'=> $records[0][$i]->reference_no,
                'DOC_DT'=> date('Y-m-d', strtotime($records[0][$i]->date)),
                'REF_INV_DOC_ID'=> $records[0][$i]->sale_id, // verify
                'EO_ID'=> $this->getEO_Id($records[0][$i]->customer_id),
                'POS_EO_ID'=> $records[0][$i]->customer_id,
                'REF_VOUCHER_ID'=> $this->getRefVoucher_Id($records[0][$i]->sales_reference_no), // verify
         );

       }
       $ar= $this->multipledb->sync_into_intermediate_slsRma($data);
        return $ar;
      }
        return false; 
}
public function getRefVoucher_Id($rno){
    $q = $this->db->query("SELECT id, invoice_no FROM sma_gift_cards WHERE invoice_no='$rno'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            if($data[0]->invoice_no=='')
               return $data[0]->invoice_no;
            else
               return $data[0]->id;  
        }
        return FALSE;
    
}

// I.T.-05 (Pos -> intermediate) Return sale Item
public function insert_into_intermediate_slsrmaItemtable(){
   $n = '$';
   $w= "SELECT * FROM sma_return_items WHERE sync_flg IS NULL OR sync_flg = '' ";
   $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'ORG_ID' => $this->getORG_Id($records[0][$i]->warehouse_id),
                'POS_DOC_ID'=> $records[0][$i]->id,
                'DOC_ID'=> $records[0][$i]->return_id,
                'ITM_ID'=> $records[0][$i]->product_code,
                'POS_ITM_ID'=> $records[0][$i]->product_id,
                'SR_NO'=> $this->getsrno($records[0][$i]->product_id), //write function
                'LOT_NO'=> $this->getlot($records[0][$i]->product_id),
                'ITM_QTY'=> $records[0][$i]->quantity,
                //'ITM_UOM'=>
                //'POS_ITM_UOM'=>
                //'ITM_UOM_BS'=>
                'POS_ITM_UOM_BS'=> $records[0][$i]->net_unit_price, // verify
                'SLS_PRICE'=> $records[0][$i]->real_unit_price,
                //' TAX_REVERSAL_FLG'=>
                'TAX_VAL'=> $records[0][$i]->item_tax,
                 //'TAX_ID'=> $records[0][$i]->tax_rate_id,
                'DISC_TOTAL_VAL'=> $records[0][$i]->item_discount,
                'DISC_TYPE'=> 'NULL',
                'DISC_VAL'=> $records[0][$i]->discount,  // verify
                'ITM_TOTAL_VAL'=> $records[0][$i]->net_unit_price,
                
              );

          }
       $ar= $this->multipledb->sync_into_intermediate_slsRmaItem($data);
        return $ar;
      }
        return false; 
}


// I.T-000 (Pos -> intermediate) User Info
public function insert_into_intermediate_usertable(){
    $n = '$';
    $w= "SELECT * FROM sma_users WHERE usr_id IS NULL OR usr_id= '' AND id!= '1'";
    $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    $w1= "SELECT * FROM sma_users WHERE upd_flg= '1' AND id!= '1'";
    $q1 = $this->db->query($w1);
    if ($q1->result() > 0) {
            $records1[] = $q1->result();
           }       
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'POS_USR_ID' => $records[0][$i]->id, 
                'USR_NAME' => $records[0][$i]->username,  
                'USR_PWD' => $records[0][$i]->password,
                'USR_ACTV' => $records[0][$i]->active,
                'USR_IMG' => $records[0][$i]->avatar,
                'USR_GNDR' => $records[0][$i]->gender,
                'USR_CONTACT_NO' => $records[0][$i]->phone,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1,
                'USR_DISC' => $records[0][$i]->show_discount,
                'USR_ID_MOD_DT' => date("Y-m-d",$records[0][$i]->created_on)
                 );

            $s = $records[0][$i]->active;
                 if($s ==''){
                    $p= 'Y';}
                 else{$p = 'N';}  
                $data[$i]['USR_ACTV'] = $p;

    }
    
        $ar= $this->multipledb->sync_into_intermediate_usertable($data);
        
      }
   if($records1){
        for($i=0;$i<count($records1[0]);$i++) {
             $upd[$i] = array(
                'POS_USR_ID' => $records1[0][$i]->id, 
                'USR_NAME' => $records1[0][$i]->username,  
                'USR_PWD' => $records1[0][$i]->password,
                'USR_ACTV' => $records1[0][$i]->active,
                'USR_IMG' => $records1[0][$i]->avatar,
                'USR_GNDR' => $records1[0][$i]->gender,
                'USR_CONTACT_NO' => $records1[0][$i]->phone,
                'USR_DISC' => $records1[0][$i]->show_discount,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d",$records1[0][$i]->created_on)
                 );

            $s = $records1[0][$i]->active;
                 if($s ==''){
                    $p= 'Y';}
                 else{$p = 'N';}  
                $upd[$i]['USR_ACTV'] = $p;

    }
    
        $ar1= $this->multipledb->sync_upd_into_intermediate_usertable($upd);
        
      }   
    
}
public function insert_into_intermediate_currtable(){
    $n = '$';
    $w= "SELECT * FROM sma_currencies WHERE curr_id IS NULL OR curr_id= ''";
    $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    $w1= "SELECT * FROM sma_currencies WHERE upd_flg= '1'";
    $q1 = $this->db->query($w1);
    if ($q1->result() > 0) {
            $records1[] = $q1->result();
           }       
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'POS_CURR_ID' => $records[0][$i]->id, 
                'CURR_NOTATION' => $records[0][$i]->code,
                'CURR_NM' => $records[0][$i]->name,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                );
         }
    
        $ar= $this->multipledb->sync_into_intermediate_currtable($data);
        
      }
   if($records1){
        for($i=0;$i<count($records1[0]);$i++) {
             $upd[$i] = array(
                'POS_CURR_ID' => $records1[0][$i]->id, 
                'CURR_NOTATION' => $records1[0][$i]->code,
                'CURR_NM' => $records1[0][$i]->name,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                 );
        }
    
        $ar1= $this->multipledb->sync_upd_into_intermediate_currtable($upd);
        
      }   
 }
public function insert_into_intermediate_grouptable(){
    $n = '$';
    $w= "SELECT * FROM sma_groups WHERE catg_id IS NULL OR catg_id= '' AND id!= '1'";
    $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    $w1= "SELECT * FROM sma_groups WHERE upd_flg= '1' AND id!= '1'";
    $q1 = $this->db->query($w1);
    if ($q1->result() > 0) {
            $records1[] = $q1->result();
           }       
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'POS_EO_CATG_ID' => $records[0][$i]->id, 
                'CATG_NM' => $records[0][$i]->name,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                );
         }
    
        $ar= $this->multipledb->sync_into_intermediate_eocatgtable($data);
        
      }
   if($records1){
        for($i=0;$i<count($records1[0]);$i++) {
             $upd[$i] = array(
                'POS_EO_CATG_ID' => $records1[0][$i]->id, 
                'CATG_NM' => $records1[0][$i]->name,
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                 );
        }
    
        $ar1= $this->multipledb->sync_upd_into_intermediate_eocatgtable($upd);
        
      }   
 } 
public function insert_into_intermediate_orgtable(){
    $n = '$';
    $w= "SELECT * FROM sma_warehouses WHERE org_id IS NULL OR org_id= ''";
    $q = $this->db->query($w);
    if ($q->result() > 0) {
            $records[] = $q->result();
           }
    $w1= "SELECT * FROM sma_warehouses WHERE upd_flg= '1'";
    $q1 = $this->db->query($w1);
    if ($q1->result() > 0) {
            $records1[] = $q1->result();
           }       
    if($records){
        for($i=0;$i<count($records[0]);$i++) {
             $data[$i] = array(
                'POS_ORG_ID' => $records[0][$i]->id, 
                'ORG_ID_PARENT' => $records[0][$i]->code,
                'ORG_DESC' => $records[0][$i]->name,
                'ORG_COUNTRY_ID'=>$this->multipledb->getCountryId($records[0][$i]->address),
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                );
         }
    
        $ar= $this->multipledb->sync_into_intermediate_orgtable($data);
        
      }
   if($records1){
        for($i=0;$i<count($records1[0]);$i++) {
             $upd[$i] = array(
                'POS_ORG_ID' => $records1[0][$i]->id,
                'ORG_ID_PARENT' => $records1[0][$i]->code,
                'ORG_DESC' => $records1[0][$i]->name,
                'ORG_COUNTRY_ID'=>$this->multipledb->getCountryId($records1[0][$i]->address),
                'USR_ID_CREATE' => 1, 
                'USR_ID_MOD' => 1, 
                'USR_ID_MOD_DT' => date("Y-m-d")
                 );
        }
    
        $ar1= $this->multipledb->sync_upd_into_intermediate_orgtable($upd);
        
      }   
 }  
public function insert_id_into_global_table($w){ 
    
    $CI = &get_instance();
    $this->db2 = $CI->load->database('db2', TRUE);
    if($this->db2->query($w))
        {
        return true;
       } 
    else{
          return false;
        }
}
public function convert_obj_to_arry($ar)
 {
      foreach($ar as $key => $value)
        {
             $ar[$key] = (array) $value;
        }
      return $ar;  
 }
public function getWarehouseId($wid){ 
    $q = $this->db->query("SELECT id FROM sma_warehouses WHERE code='$wid' ");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->id;
        }
        return FALSE;

}    
public function getCat1($id){
     $q = $this->db->query("SELECT id FROM sma_categories WHERE code='$id'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->id;
        }
        return FALSE;

}

// S.No.- 06 (Intermediate -> POS)
public function insert_into_Pos_product_table(){
$ar= $this->productTableUPD();
$n='$';
$w="INSERT INTO swatch_final.sma_products(code,name,unit,cost,price,category_id,subcategory_id,quantity,tax_rate,details,warehouse,barcode_symbology,product_details,serialized,lot_no,sr_no,org_id,grp_id) SELECT a.ITM_ID,b.ITM_DESC,10,c.UOM_BASIC,c.PRICE_SLS,f.id,b.UOM_BASIC,c.AVAIL_QTY,g.id,b.ITM_LONG_DESC,e.id,b.ITM_LEGACY_CODE,b.ITM_LONG_DESC,b.SERIALIZED_FLG,c.LOT_NO,c.SR_NO,a.ORG_ID,b.GRP_ID FROM ebiz7.intrm".$n."prod".$n."org a INNER JOIN ebiz7.intrm".$n."prod b ON b.ITM_ID = a.ITM_ID INNER JOIN swatch_final.sma_warehouses e ON e.org_id = a.ORG_ID INNER JOIN swatch_final.sma_categories f ON f.code = b.GRP_ID LEFT JOIN   ebiz7.intrm".$n."itm".$n."stock".$n."dtl c ON (c.ITM_ID = a.ITM_ID AND c.ORG_ID = a.ORG_ID) LEFT JOIN swatch_final.sma_tax_rates g ON (g.org_id = a.ORG_ID AND g.grp_id = b.GRP_ID) WHERE a.POS_ITM_ID IS NULL OR a.POS_ITM_ID = ''";
 
if($this->db->query($w))  
{
  $msg= "Product Records save successfully...";
  $w1= "UPDATE ebiz7.intrm".$n."prod".$n."org a INNER JOIN swatch_final.sma_products b ON (a.ITM_ID = b.code AND a.ORG_ID = b.org_id) SET a.POS_ITM_ID= b.id, a.CREATE_FLG='1' WHERE a.ITM_ID = b.code AND a.ORG_ID = b.org_id";
  $this->insert_id_into_global_table($w1);

  return $msg;
}
else{
  $msg= "Product Records Not saved...";
  return $msg;
}
 } 

public function productTableUPD() {
  $n='$';
  $w="UPDATE swatch_final.sma_products q INNER JOIN ebiz7.intrm".$n."prod".$n."org a ON q.id= a.POS_ITM_ID INNER JOIN ebiz7.intrm".$n."prod b ON b.ITM_ID = a.ITM_ID INNER JOIN swatch_final.sma_warehouses e ON e.org_id = a.ORG_ID INNER JOIN swatch_final.sma_categories f ON f.code = b.GRP_ID LEFT JOIN ebiz7.intrm".$n."itm".$n."stock".$n."dtl c ON (c.ITM_ID = a.ITM_ID AND c.ORG_ID = a.ORG_ID) LEFT JOIN swatch_final.sma_tax_rates g ON (g.org_id = a.ORG_ID AND g.grp_id = b.GRP_ID) SET q.code= a.ITM_ID,q.name= b.ITM_DESC,q.cost= c.UOM_BASIC,q.price= c.PRICE_SLS,q.category_id= f.id,q.quantity=c.AVAIL_QTY,q.tax_rate= g.id,q.details= b.ITM_LONG_DESC,q.warehouse= e.id,q.barcode_symbology= b.ITM_LEGACY_CODE,q.product_details= b.ITM_LONG_DESC,q.serialized= b.SERIALIZED_FLG,q.lot_no= c.LOT_NO,q.sr_no= c.SR_NO,q.org_id= a.ORG_ID,q.grp_id=b.GRP_ID, q.tupd_flg='1' WHERE a.UPD_FLG='1'";
    if($this->db->query($w))  
   {
     
      $w1= "UPDATE ebiz7.intrm".$n."prod".$n."org a INNER JOIN swatch_final.sma_products b ON a.POS_ITM_ID = b.id SET a.UPD_FLG='' WHERE a.POS_ITM_ID = b.id";
        $this->db->query($w1);
      return true;
     }  

else{
      return false;
    }
}  

// S.No.- 07 (Intermediate -> POS)

public function insert_into_pos_warehouse_product_table(){
  $ar= $this->import_warehouse_product_TableUPD();
  $w="INSERT INTO swatch_final.sma_warehouses_products(product_id,warehouse_id,lot_no,quantity,org_id,grp_id) SELECT a.id,a.warehouse,a.lot_no,a.quantity,a.org_id,a.grp_id FROM swatch_final.sma_products a WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
  if($this->db->query($w))  
 {
  $this->import_purchase_item_info222();
  $msg= "Product Records save in warehouse successfully...";
  $w1= "UPDATE swatch_final.sma_products a INNER JOIN swatch_final.sma_warehouses_products b ON a.id = b.product_id SET a.trsfr_flg= '1' WHERE a.id = b.product_id";
  $this->db->query($w1);
  return $msg;
}
else{
       $msg= "Product Records Not saved in warehouse...";
       return $msg;
     }
}  

public function import_warehouse_product_TableUPD(){

  $w= "UPDATE swatch_final.sma_warehouses_products z INNER JOIN swatch_final.sma_products a SET z.product_id = a.id,z.warehouse_id = a.warehouse,z.lot_no = a.lot_no,z.quantity = a.quantity,z.org_id = a.org_id,z.grp_id = a.grp_id WHERE a.tupd_flg= '1'";
  if($this->db->query($w))  
   {
      $this->import_purchase_item_TableUPD222();
     $w1= "UPDATE swatch_final.sma_products a INNER JOIN swatch_final.sma_warehouses_products b ON a.id = b.product_id SET a.tupd_flg='' WHERE a.id = b.product_id";
        $this->db->query($w1);
      return true;
 }
else{

  return false;
   }
}
public function import_purchase_item_info222(){
  $n= '$';
  $w= "INSERT INTO swatch_final.sma_purchase_items(product_id,product_code,product_name,net_unit_cost,quantity,warehouse_id,item_tax,
    tax_rate_id,tax,subtotal,quantity_balance,date,status,unit_cost,lot_no,sr_no,org_id,grp_id) SELECT a.id,a.code,a.name,(a.price -((a.price * b.rate) /(100 + b.rate))) AS unit_cost,a.quantity,a.warehouse,((a.price * b.rate) /(100 + b.rate)) AS item_tax,a.tax_rate,b.rate,a.price,a.quantity,date('Y-m-d'),'received',a.price,a.lot_no,a.sr_no,a.org_id,a.grp_id FROM swatch_final.sma_products a INNER JOIN swatch_final.sma_tax_rates b ON(b.id = a.tax_rate AND b.org_id = a.org_id AND b.grp_id = a.grp_id) WHERE a.trsfr_flg IS NULL OR a.trsfr_flg = ''";
    if($this->db->query($w))  
   {
    return true;
   }
   else{
    return false;
   }

}
public function import_purchase_item_TableUPD222(){
     $n= '$';
  $w= "UPDATE swatch_final.sma_purchase_items z INNER JOIN swatch_final.sma_products a ON a.id= z.product_id INNER JOIN swatch_final.sma_tax_rates b ON(b.id = a.tax_rate AND b.org_id = a.org_id AND b.grp_id = a.grp_id) SET z.product_code = a.code, z.name = a.name,
  z.net_unit_cost = (a.price -((a.price * b.rate) /(100 + b.rate))) AS unit_cost, z.quantity = a.quantity, z.item_tax =  ((a.price * b.rate) /(100 + b.rate)) AS item_tax, z.tax_rate_id = a.tax_rate, z.tax = b.rate, z.subtotal = a.price, z.quantity_balance = a.quantity, z.lot_no = a.lot_no, z.sr_no = a.sr_no, z.org_id = a.org_id, z.grp_id = a.grp_id WHERE a.tupd_flg= '1'"; 
  if($this->db->query($w))  
   {
    return true;
   }
   else{
    return false;
   }  
}

// S.No.- 08 (Intermediate -> POS)
public function insert_into_pos_companies_table(){
  $ar= $this->customer_info_tableUPD();
$n= '$';
$w= "INSERT INTO swatch_final.sma_companies(group_id,group_name,customer_group_id,customer_group_name,name,company,phone,email,   vat_no,address,country,eo_type,eo_id,sync_flg) SELECT '3','customer','1','General',a.EO_NM,'Swatch Group',a.EO_PHONE,'Not Available',a.VAT_NO,b.ADDRESS,c.CNTRY_DESC,a.EO_TYPE,a.EO_ID,'1' FROM ebiz7.intrm".$n."eo a INNER JOIN ebiz7.intrm".$n."eo".$n."add b ON (b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE) LEFT JOIN ebiz7.intrm".$n."cntry c ON c.CNTRY_ID = a.EO_CNTRY_ID WHERE a.EO_TYPE='C' AND a.POS_EO_ID IS NULL OR a.POS_EO_ID = ''";
if($this->db->query($w))  
{
  $msg= "customer Records save successfully...";
  $w1= "UPDATE ebiz7.intrm".$n."eo a INNER JOIN swatch_final.sma_companies z ON (z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE) LEFT JOIN ebiz7.intrm".$n."eo".$n."add b ON (b.EO_ID = z.eo_id AND z.eo_type = b.EO_TYPE) RIGHT JOIN ebiz7.intrm".$n."eo".$n."add".$n."org c ON (c.EO_ID = z.eo_id AND z.eo_type = c.EO_TYPE) RIGHT JOIN ebiz7.intrm".$n."eo".$n."org d ON (d.EO_ID = z.eo_id AND z.eo_type = d.EO_TYPE) SET a.POS_EO_ID = z.id, b.POS_EO_ID = z.id, c.POS_EO_ID = z.id, c.CREATE_FLG = '1', d.POS_EO_ID = z.id, d.CREATE_FLG = '1' WHERE z.eo_id = a.EO_ID AND z.eo_type = a.EO_TYPE";
  $this->db->query($w1);
  return $msg;
}
else{
      $msg= "customer Records Not saved...";
       return $msg;
     }
}

public function customer_info_tableUPD(){
  $n= '$';
  $w= "UPDATE swatch_final.sma_companies z INNER JOIN ebiz7.intrm".$n."eo a ON a.POS_EO_ID = z.id INNER JOIN ebiz7.intrm".$n."eo".$n."add b ON (b.EO_ID = a.EO_ID AND b.EO_TYPE = a.EO_TYPE) LEFT JOIN ebiz7.intrm".$n."cntry c ON c.CNTRY_ID = a.EO_CNTRY_ID LEFT JOIN ebiz7.intrm".$n."eo".$n."org d ON (d.EO_ID = a.EO_ID AND d.EO_TYPE = d.EO_TYPE) SET z.name = a.EO_NM, z.vat_no = a.VAT_NO, z.phone = a.EO_PHONE, z.email = 'Not Available', z.address = b.ADDRESS, z.country = c.CNTRY_DESC, z.sync_flg='1' WHERE a.EO_TYPE='C' AND d.UPD_FLG= '1'";
   if($this->db->query($w))  
   {
      $w1= "UPDATE ebiz7.intrm".$n."eo".$n."org a INNER JOIN swatch_final.sma_companies b ON a.POS_EO_ID = b.id SET a.UPD_FLG='' WHERE a.POS_EO_ID = b.id";
      $this->db->query($w1);
      return true;
     }

else{
       return false;
     }
}

//I.T-Product QTY LESS

public function update_product_Qty(){
  $n= '$';
  $w= "UPDATE ebiz7.intrm".$n."itm".$n."stock".$n."dtl p INNER JOIN swatch_final.sma_products a ON (p.ITM_ID = a.code AND p.ORG_ID = a.org_id AND p.LOT_NO = a.lot_no) SET p.AVAIL_QTY = a.quantity WHERE a.psale='1'";
  if($this->db->query($w))  
   {
      $w1= "UPDATE swatch_final.sma_products a SET a.psale='' WHERE a.psale='1'";
      $this->db->query($w1);
      return true;
     }

else{
       return false;
     }
}

public function getTaxIndex($gid, $orgt){
    $a = $this->multipledb->getTax($gid, $orgt);
    $q = $this->db->query("SELECT id FROM sma_tax_rates WHERE rate='$a'");
        if ($q->result() > 0) {
            foreach (($q->result()) as $row) {
                  $data[] = $row;
            }
            return $data[0]->id;
        }
        return FALSE;
    }
// Sync all intermediate table to POS data base
public function sync_to_pos_data_table(){
	
	
	$this->insert_into_pos_currency_table(); 
    $this->insert_into_pos_tax_table();
    $this->insert_into_pos_category_table();
	$this->insert_into_pos_warehouse_table();
	$this->insert_into_pos_usertable();
	$this->insert_into_Pos_product_table();
    $this->insert_into_pos_warehouse_product_table();
    $this->insert_into_pos_companies_table();
}

// Sync all pos data table to intermediate data base
public function sync_to_intermediate_data_table(){
	
	
	$this->insert_into_intermediate_eotable();
	$this->insert_into_intermediate_slsInvtable();
	$this->insert_into_intermediate_slsInvItemtable();
    $this->insert_into_intermediate_slsRmatable();
    $this->insert_into_intermediate_slsrmaItemtable();
    $this->update_product_Qty();

}		

}
