<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper("url");
        $this->load->model("Sales_model");
        $this->load->library("pagination");
		//$this->load->model("msync_model");
        //$this->load->library("multipledb");
        

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        $this->load->library('form_validation');
        $this->load->model('db_model');
        //$this->session->set_userdata('last_activity', now());
    }

    public function index()
    {   //$a= $this->msync_model->sync_to_pos_data_table();
        //print_r($a); 
        //die(); 
         
        if ($this->Settings->version == '2.3') {
            $this->session->set_flashdata('warning', 'Please complete your update by synchronizing your database.');
            redirect('sync');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['sales'] = $this->db_model->getLatestSales();
        $this->data['quotes'] = $this->db_model->getLastestQuotes();
        $this->data['purchases'] = $this->db_model->getLatestPurchases();
        $this->data['transfers'] = $this->db_model->getLatestTransfers();
        $this->data['customers'] = $this->db_model->getLatestCustomers();
        $this->data['suppliers'] = $this->db_model->getLatestSuppliers();
        $this->data['chatData'] = $this->db_model->getChartData();
        $this->data['stock'] = $this->db_model->getStockValue();
        $this->data['bs'] = $this->db_model->getBestSeller();
        $lmsdate = date('Y-m-d', strtotime('first day of last month')) . ' 00:00:00';
        $lmedate = date('Y-m-d', strtotime('last day of last month')) . ' 23:59:59';
        $this->data['lmbs'] = $this->db_model->getBestSeller($lmsdate, $lmedate);
        $bc = array(array('link' => '#', 'page' => lang('dashboard')));
        $meta = array('page_title' => lang('dashboard'), 'bc' => $bc);
//        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
//        $biller_id = $this->session->all_userdata()['biller_id'];
//        echo $warehouse_id."<br>".$biller_id; die();
        $this->page_construct('dashboard', $meta, $this->data);
     }


    function promotions()
    {
        $this->load->view($this->theme . 'promotions', $this->data);
    }

    function image_upload()
    {
        if (DEMO) {
            $error = array('error' => $this->lang->line('disabled_in_demo'));
            echo json_encode($error);
            exit;
        }
        $this->security->csrf_verify();
        if (isset($_FILES['file'])){
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '500';
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $error = array('error' => $error);
                echo json_encode($error);
                exit;
            }
            $photo = $this->upload->file_name;
            $array = array(
                'filelink' => base_url() . 'assets/uploads/images/' . $photo
            );
            echo stripslashes(json_encode($array));
            exit;

        } else {
            $error = array('error' => 'No file selected to upload!');
            echo json_encode($error);
            exit;
        }
    }

    function set_data($ud, $value)
    {
        $this->session->set_userdata($ud, $value);
        echo true;
    }

    function hideNotification($id = NULL)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    function language($lang = false)
    {
        if ($this->input->get('lang')) {
            $lang = $this->input->get('lang');
        }
        //$this->load->helper('cookie');
        $folder = 'sma/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            $cookie = array(
                'name' => 'language',
                'value' => $lang,
                'expire' => '31536000',
                'prefix' => 'sma_',
                'secure' => false
            );

            $this->input->set_cookie($cookie);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function download($file)
    {
        $this->load->helper('download');
        force_download('./files/'.$file, NULL);
        exit();
    }
/**
     * Author  Ankit
     * Detail : For find out today sales
     * Date 22-04-2016
     */
    
    function todaysales()
    {

		$biller_id = $this->session->all_userdata()['biller_id'];
       $warehouse_id = $this->session->all_userdata()['warehouse_id'];

        $user = $this->site->getUser();
        $warehouse_id = $user->warehouse_id;
        $d=mktime(11, 10, 54, 3, 14, 2016);
        $dt= date("Y-m-d", $d);
        $dt1= date("Y-m-d");
        $this->load->library('datatables');
        if($biller_id==NULL){
        $this->datatables
//                ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                ->from("sales")
//                ->where('Date(date)', $dt1); 
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('Date(date)', $dt1)
                ->group_by('sales.id', 'sale_items.sale_id');
        }
		
        else {
            $this->datatables
//                ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                ->from("sales")
//                ->where('biller_id', $biller_id)     
//                ->where('Date(date)', $dt1); 
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id)    
                ->where('Date(date)', $dt1)
                ->group_by('sales.id', 'sale_items.sale_id');
            
        }
         echo $this->datatables->generate();
    }
    /**
     * Author  Ankit
     * Detail : For find out current month sales
     * Date 22-04-2016
     */
    
    function currentmonth()

    {
        
        //$user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $biller_id = $this->session->all_userdata()['biller_id'];
        $m = date("m");
        $y= date("Y");
       $this->load->library('datatables');
       if($biller_id==NULL){
        $this->datatables
//                ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                ->from("sales")
//                ->where('MONTH(date)', $m)
//                ->where('YEAR(date)', $y);
               // ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/(sum(unit_price)+order_tax))*100 as dis , sum(total+product_discount) as 'basic', total_tax, grand_total")
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('MONTH(date)', $m)
                ->where('YEAR(date)', $y)
                ->group_by('sales.id', 'sale_items.sale_id');
       }
       else{
           $this->datatables
//                ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                ->from("sales")
//                ->where('biller_id', $biller_id)
//                ->where('MONTH(date)', $m)
//                ->where('YEAR(date)', $y);
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id) 
                ->where('MONTH(date)', $m)
                ->where('YEAR(date)', $y)
                ->group_by('sales.id', 'sale_items.sale_id');
       }

       echo $this->datatables->generate();
     }
    /**
     * Author  Ankit
     * Detail : For find out last month sales
     * Date 22-04-2016
     */ 
    function lastmonth()
    {

        $biller_id = $this->session->all_userdata()['biller_id'];
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        $m2 = date("Y-m-d", strtotime("first day of last month"));
        $d = new DateTime('first day of this month');
        $m1= $d->format('Y-m-d');  
        $this->load->library('datatables');

        if($biller_id==NULL){
        $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('date BETWEEN "'. date('Y-m-d', strtotime($m2)). '" and "'. date('Y-m-d', strtotime($m1)).'"');
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($m2)). '" and "'. date('Y-m-d', strtotime($m1)).'"')
                ->group_by('sales.id', 'sale_items.sale_id');
        }
       else {
           $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('biller_id', $biller_id)   
//                 ->where('date BETWEEN "'. date('Y-m-d', strtotime($m2)). '" and "'. date('Y-m-d', strtotime($m1)).'"');
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id)   
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($m2)). '" and "'. date('Y-m-d', strtotime($m1)).'"')
                ->group_by('sales.id', 'sale_items.sale_id');
           
       }

		
                       
        echo $this->datatables->generate();
    }
    /**
     * Author  Ankit
     * Detail : For find out the period sale between two date input by user
     * Date 22-04-2016
     */
    function getPeriodSales()
    {
		
        $ar=array();
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $biller_id = $this->session->all_userdata()['biller_id'];
        $user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        $str= $this->input->post('date');
        $ar= explode("|",$str);
        $s= $ar[0]; 
        $e= $ar[1]; 
        $date = str_replace('/', '-', $s);
        $s1= date('Y-m-d', strtotime($date));
        $date1 = str_replace('/', '-', $e);
        $e1= date('Y-m-d', strtotime($date1. ' +1 day'));
        $this->load->library('datatables');

        if($biller_id==NULL){
        $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"');
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"')
                ->group_by('sales.id', 'sale_items.sale_id');
        }
      else {
          $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('biller_id', $biller_id)  
//                 ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"');
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                  ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id)
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"')
                ->group_by('sales.id', 'sale_items.sale_id');
      }

        echo $this->datatables->generate();
    }
    /**
     * Author  Ankit
     * Detail : For find out YTD sale (calender year) take input from user
     * Date 25-04-2016
     */
   function getytdSales()
    {

        $biller_id = $this->session->all_userdata()['biller_id'];
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        
        $str= $this->input->post('year');
       
        $this->load->library('datatables');

        if($biller_id==NULL){
        $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('YEAR(date)', $str);
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('YEAR(date)', $str)
                ->group_by('sales.id', 'sale_items.sale_id');
        }

        else {
            $this->datatables
//                 ->select("date, reference_no, customer, sale_status, grand_total, payment_status, paid, id")
//                 ->from("sales")
//                 ->where('biller_id', $biller_id)   
//                 ->where('YEAR(date)', $str);
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id)    
                ->where('YEAR(date)', $str)
                ->group_by('sales.id', 'sale_items.sale_id');
            
        }

        echo $this->datatables->generate();
    } 
    /**
     * Author  Ankit
     * Detail : For find out return sales
     * Date 25-04-2016
     */
    function getSaleRreturn()
    {
		
        $ar=array();
        $biller_id = $this->session->all_userdata()['biller_id'];
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        $str= $this->input->post('date');
        $ar= explode("|",$str);
        $s= $ar[0]; 
        $e= $ar[1]; 
        $date = str_replace('/', '-', $s);
        $s1= date('Y-m-d', strtotime($date));
        $date1 = str_replace('/', '-', $e);
        $e1= date('Y-m-d', strtotime($date1. ' +1 day'));
        //echo $e1; die;
        $this->load->library('datatables');
        if($biller_id==NULL){
        $this->datatables
//                 ->select("date, reference_no, customer, biller, product_tax, total_tax, grand_total, id")
//                ->from("return_sales")
//                 ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"');
               // ->where('date>=',date('Y-m-d', strtotime($s1)))
                //->where('date<=',date('Y-m-d', strtotime($e1)));
                ->select($this->db->dbprefix('return_sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/real_unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("return_sales")
                ->join('return_items', 'return_items.return_id=return_sales.id', 'left')
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"')
                ->group_by('return_items.id');
                 

        }
       else {
           $this->datatables
//                 ->select("date, reference_no, customer, biller, product_tax, total_tax, grand_total, id")
//                 ->from("return_sales")
//                 ->where('sales.biller_id', $biller_id)
//                ->where('sales.warehouse_id', $warehouse_id)   
//                ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"');
                ->select($this->db->dbprefix('return_sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/real_unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("return_sales")
                ->join('return_items', 'return_items.return_id=return_sales.id', 'left')
                ->where('return_sales.biller_id', $biller_id)
                ->where('return_sales.warehouse_id', $warehouse_id)
                ->where('date BETWEEN "'. date('Y-m-d', strtotime($s1)). '" and "'. date('Y-m-d', strtotime($e1)).'"')
                ->group_by('return_items.id');
                
                   
           
       }
                 
        echo $this->datatables->generate();
    } 
    /**
     * Author  Ankit
     * Detail : For find out sales discount
     * Date 26-04-2016
     */

   function getSaleDiscount()
    {

        $user = $this->site->getUser();
        //$warehouse_id = $user->warehouse_id;
        $warehouse_id = $this->session->all_userdata()['warehouse_id'];
        $biller_id = $this->session->all_userdata()['biller_id'];
        $this->load->library('datatables');

        if($biller_id==NULL){
        $this->datatables
                // ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(unit_price) as 'unit_sum', order_tax, order_discount , (order_discount/(sum(unit_price)+order_tax))*100 as dis , grand_total")
                ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                ->from("sales")
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->where(array('total_discount >' => '0'))
                ->group_by('sales.id', 'sale_items.sale_id');
                
        }
        else {
            $this->datatables
                 ->select($this->db->dbprefix('sales') . ".id as id, date, reference_no, sum(real_unit_price) as 'mrp', total_discount , (total_discount/unit_price)*100 as dis , total as 'basic', total_tax, grand_total")
                 ->from("sales")
                 ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                 ->where('sales.biller_id', $biller_id)
                ->where('sales.warehouse_id', $warehouse_id)   
                 ->where('total_discount >', '0')
                 ->group_by('sales.id', 'sale_items.sale_id');
            
        }
       

        echo $this->datatables->generate();
    }  
    
    
    
    
    

}
