<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Till extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->model('till_model');
        $this->load->helper('text');
        $this->pos_settings = $this->pos_model->getSetting();       
        $this->data['pos_settings'] = $this->pos_settings;
        $this->session->set_userdata('last_activity', now());
        $this->lang->load('till', $this->Settings->language);
        $this->load->library('form_validation');
    }
    
     /*
     * Added by ajay on 25-05-2015
     * code for add till
     */
    public function addTill(){
        $this->sma->checkPermissions('index');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load->view('till/add');
    }
    
    /*
     * Added by ajay on 25-05-2015
     * code for manage till
     */
    public function manageTill(){
        $this->sma->checkPermissions('index');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load->view();
    }

}
