<?php

//error_reporting(0);
   
    function getRealIp() {
       if (!empty($_SERVER['HTTP_CLIENT_IP'])) {  //check ip from share internet
         $ip=$_SERVER['HTTP_CLIENT_IP'];
       } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  //to check ip is pass from proxy
         $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
       } else {
         $ip=$_SERVER['REMOTE_ADDR'];
       }
       return $ip;
    }

    
        // START FORM PROCESSING
	if (isset($_POST['submit'])) { // Form has been submitted.
	
	//  MAKE SURE THE "FROM" EMAIL ADDRESS DOESN'T HAVE ANY NASTY STUFF IN IT
			
			$pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i"; 
            if (preg_match($pattern, trim(strip_tags($_POST['req-email'])))) { 
                $cleanedFrom = trim(strip_tags($_POST['req-email'])); 
            } else { 
                $error_message = "The email address you entered was invalid. Please make sure you email address is typed correctly!"; 
            } 
			
		if((!isset($_POST['req-name'])) || (empty($_POST['req-name']))){
			$error_message = "Full Name is required.";
		} elseif ((!isset($_POST['req-email'])) || (empty($_POST['req-email']))) {
			$error_message = "Email Address is required.";
		} elseif((!isset($_POST['typeOfChange'])) && (empty($_POST['typeOfChange']))) {
			$error_message = "Type of Change is required.";
		} elseif((!isset($_POST['newText'])) || (empty($_POST['newText']))) {
			$error_message = "Detailed Message is required.";
		} 
			
        
           if ( !isset($error_message) ) {
            
            // PREPARE THE BODY OF THE MESSAGE

			$message = '<html><body>';
			$message .= '<h1>Stock Manager Advance</h1>';
			$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message .= "<tr style='background: #eee;'><td style='width: 150px;'><strong>Name:</strong> </td><td style='width: 550px;'>" . strip_tags($_POST['req-name']) . "</td></tr>";
			$message .= "<tr><td style='width: 150px;'><strong>Email:</strong> </td><td style='width: 550px;'>" . strip_tags($_POST['req-email']) . "</td></tr>";
			$message .= "<tr><td style='width: 150px; background: #eee;'><strong>Type of Change:</strong> </td><td style='background: #eee; width: 550px;'>" . strip_tags($_POST['typeOfChange']) . "</td></tr>";
			$message .= "<tr><td style='width: 150px; vertical-align:top;'><strong>Message Content:</strong> </td><td style='width: 550px;'>" . nl2br(htmlentities($_POST['newText'])) . "</td></tr>";
			$message .= "<tr><td style='width: 150px; background: #eee;'><strong>Sender IP:</strong> </td><td style='background: #eee; width: 550px;'>" . getRealIp() . "</td></tr>";
			$message .= "</table>";
			$message .= "</body></html>";
			
			
            
            //   CHANGE THE BELOW VARIABLES TO YOUR NEEDS
             
			$to = 'mian.saleem@yahoo.com';
			
			$subject = 'Stock Manager Reqest';


			$headers = 'From: ' . $cleanedFrom . "\r\n" . 
			'Reply-To: '. $cleanedFrom . "\r\n" .
		        'X-Mailer: PHP/' . phpversion() . "\r\n" . 
		        "MIME-Version: 1.0\r\n" . 
		        "Content-Type: text/html; charset=utf-8\r\n" . 
		        "Content-Transfer-Encoding: 8bit\r\n\r\n"; 
			

            if (mail($to, $subject, $message, $headers)) {
              $message_success = 'Your message has been sent. Thank you!';
            } else {
              $error_message = 'There was a problem sending the email.';
            }

		   }
            // DON'T BOTHER CONTINUING TO THE HTML...
           // die();
        
        }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>User Guide | Stock Manager Advance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/docs.css" rel="stylesheet">
    <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript">
$('.box').click(function() {

    $(this).animate({
        left: '-50%'
    }, 500, function() {
        $(this).css('left', '150%');
        $(this).appendTo('#container');
    });

    $(this).next().animate({
        left: '50%'
    }, 500);
});​
</script>

  </head>

  <body data-spy="scroll" data-target=".bs-docs-sidebar">

    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="http://tecdiary.net" target="_blank">Tecdiary IT Solutions</a>
          <div class="nav-collapse collapse">
             <ul class="nav">
              <li>
                <a href="./index.html">Home</a>
              </li>
              <li class="">
                <a href="./installation.html">Instalattion</a>
              </li>
              <li class="">
                <a href="./user-guide.html">User Guide</a>
              </li>
              <li>
                <a href="./support.html">Support &amp; Customization</a>
              </li> 
              <li>
                <a href="./credit.html">Credits</a>
              </li> 
              <li class="active">
                <a href="./send_request.php">Feature Request</a>
              </li> 
            </ul>
            </ul>
          </div>
        </div>
      </div>
    </div>

<!-- Subhead
================================================== -->
<header class="jumbotron subhead" id="overview">
  <div class="container">
    <h1>Bug / Feature Request</h1>
    <p class="lead">Please report bugs in Stock Manager. If you have any suggestions, please fill the form below to send us message.</p>
  </div>
</header>


  <div class="container">

 

        <!-- Forms
        ================================================== -->
        

        <section id="forms">
        
        <?php  if(isset($message_success)) { echo "<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>" . $message_success . "</div>"; } ?>
		<?php  if(isset($error_message)) { echo "<div class=\"alert alert-error\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>" . $error_message . "</div>"; } ?>
        
        
          <form action="<?php $PHP_SELF ?>" method="post" name="form1" id="change-form">
          
            <strong>Full Name:</strong><br>
              <input class="input-xxlarge" type="text" id="prependedInput" name="req-name" placeholder="Full Name" value="<?php  if(isset($_POST['req-name'])) { echo $_POST['req-name']; } ?>">
              <br>
            <strong>Email Address:</strong><br>
              <input class="input-xxlarge" type="text" id="prependedInput"  name="req-email" placeholder="Email Address" value="<?php  if(isset($_POST['req-email'])) { echo $_POST['req-email']; } ?>">
              <br>
            <strong>Type of Change:</strong><br>
            <label class="radio" style="margin-top: 5px;" name="typeOfChange">
              <input type="radio" name="typeOfChange" value="Change to Existing Content" id="optionsRadios1" <?php  if(isset($_POST['typeOfChange']) && $_POST['typeOfChange'] == "Change to Existing Content") { echo "checked"; } ?>>
              Change to existing content (Bug)
            </label>
            <label class="radio" name="typeOfChange">
              <input type="radio" name="typeOfChange" value="Add New Feature" id="optionsRadios2" <?php  if(isset($_POST['typeOfChange']) && $_POST['typeOfChange'] == "Add New Feature") { echo "checked"; } ?>>
              Add new feature (Feature Request)
            </label>	
         
             <strong>Detailed Message:</strong><br>
            <textarea class="input-xxlarge" style="height: 120px;" name="newText"><?php  if(isset($_POST['newText'])) { echo $_POST['newText']; } ?></textarea>
           
            <br>
        
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
              <button type="reset" class="btn btn-danger">Reset</button>
            
            </form>
            	

        </section>
 

  </div>



     <!-- Footer
    ================================================== -->
    <footer class="footer">
      <div class="container">
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2012 Tecdiary IT Solutions. <a href="http://tecdiary.com" target="_blank">tecdiary.com</a> - <a href="http://tecdiary.net" target="_blank">tecdiary.net</a> - <a href="http://tecdiary.org" target="_blank">tecdiary.org</a></p> 
        <p>Designed and built with all the love in the world by <a href="http://twitter.github.com/bootstrap/" target="_blank">Bootstrap</a></p>
        
      </div>
    </footer>



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/google-code-prettify/prettify.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>
    <script src="assets/js/bootstrap-affix.js"></script>
    <script src="assets/js/application.js"></script>



  </body>
</html>
